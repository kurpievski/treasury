
PATH = File.dirname(__FILE__) + '/';
PATCHES_DIR = 'patches/';
PATCHES_FILE = 'applied_patches.txt'

PATCHES_FILE_PATH = "#{PATH}#{PATCHES_FILE}";
PATCHES_GLOB = "#{PATH}#{PATCHES_DIR}*.sh"
PATCHES_DIR_PATH = "#{PATH}#{PATCHES_DIR}"
TEMPSCRIPT = "#{PATH}temp_patch_script.sh"

applied_patches = []

if File.exist?(PATCHES_FILE_PATH)
    applied_patches = IO.readlines(PATCHES_FILE_PATH).map {|i| i.sub("\n", "")}
end

patch_list = Dir.glob(PATCHES_GLOB).map {|i| i.sub(PATCHES_DIR_PATH, '') }
patch_list.sort!

patches_to_apply = patch_list.select {|i| !applied_patches.include?(i) }

puts "-- No patches to apply on this VM. All good." if patches_to_apply.empty?


for patch in patches_to_apply
    puts "Applying patch: #{patch}"
    fullPath = "#{PATCHES_DIR_PATH}#{patch}"

    temp = File.open(TEMPSCRIPT, "w")
    temp.write (IO.readlines(fullPath).map {|i| i.sub("\r", "").sub("\n", "")}).join("\n")
    temp.close

    system("bash #{TEMPSCRIPT}")
    applied_patches << patch
end

File.delete(TEMPSCRIPT) if File.exist?(TEMPSCRIPT)

fl = File.open(PATCHES_FILE_PATH, "w")
fl.write applied_patches.join("\n")
fl.close