#!/usr/bin/env bash

echo "Switching user to `whoami`"

echo "Applying patches.."
ruby /app/vagrant/patches.rb

echo "Linking motd"
ln -f -s /app/vagrant/motd.txt /etc/motd

echo "Linking apache vhosts"
rm -rf /etc/apache2/sites-enabled
rm -rf /etc/apache2/sites-available
ln -sf /app/vagrant/apache /etc/apache2/sites-enabled
ln -sf /app/vagrant/apache /etc/apache2/sites-available

echo "Linking commands"
ln -s /app/vagrant/scripts/* /usr/bin

echo "Linking domains"
rm -rf /var/www/treasury
ln -sf /app/backend/web /var/www/treasury
rm -rf /var/www/cahoot
ln -sf /app/cahoot.geekseller.com/public /var/www/cahoot
echo "Restarting web-stack..."
restart-server