#!/usr/bin/env bash

github_token=$(echo "$1")

echo "Switching user to `whoami`"

echo "Configuring composer..."
composer config --global github-oauth.github.com ${github_token}
composer self-update

init-local