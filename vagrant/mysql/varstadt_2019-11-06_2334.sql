-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: varstadt
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `medium_id` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `abstract` text,
  `is_premium` tinyint(1) NOT NULL DEFAULT '0',
  `source_date` date NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `language` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_article__medium` (`medium_id`),
  CONSTRAINT `fk_article__medium` FOREIGN KEY (`medium_id`) REFERENCES `medium` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (1,'W poszukiwaniu wiecznego życia',1,'https://www.facebook.com/1714882952150815/posts/2150410401931399',NULL,0,'2019-11-05',1572981167,1572981167,'PL'),(2,'Pełzająca kontrrewolucja',2,'https://www.facebook.com/107959060574412/posts/149634703073514',NULL,0,'2019-11-05',1572981167,1572981167,'PL'),(3,'Wolsung – Bon Ton: Kapłani',3,'https://erpegis.wordpress.com/2019/11/04/wolsung-bon-ton-kaplani/',NULL,0,'2019-11-04',1572981167,1572981167,'PL'),(4,'Podróż w czasie',4,'https://wyjsciezpiwnicy.blogspot.com/2019/11/podroz-w-czasie.html',NULL,0,'2019-11-04',1572981167,1572981167,'PL'),(5,'Upadek Lemurii, Sezon 3. Podsumowanie kampanii Conan 2d20.',5,'https://wolfgangschwarzenatter.blogspot.com/2019/11/upadek-lemurii-sezon-3-podsumowanie.html',NULL,0,'2019-11-04',1572981167,1572981167,'PL'),(6,'Ascetyczny scenariusz: Wścibskie Dzieciaki - Halloweenowy Bal',6,'https://www.facebook.com/404634013674125/posts/574359393368252','Przygoda w stylu Scooby-Doo do Meddling Kids.',0,'2019-11-02',1572981167,1572982617,'PL'),(8,'[Zew Cthulhu] The Things We Leave Behind - recenzja',8,'https://squirel-spg.blogspot.com/2019/11/zew-cthulhu-things-we-leave-behind.html','Zbiór zawiera sześć soczystych i niekonwencjonalnych scenariuszy do rozegrania w czasach współczesnych.',1,'2019-11-02',1572981167,1572987202,'PL'),(9,'W jakich etapach przebiega żałoba po śmierci Bohatera Gracza?',9,'https://www.facebook.com/2197446783670850/posts/2548378255244366','Tekst niewątpliwie humorystyczny.\r\n',0,'2019-11-02',1572981167,1572985609,'PL'),(10,'Polecajka z dreszczykiem',10,'https://www.grajkolektyw.pl/aktualnosci/polecajka-z-dreszczykiem/','Przedstawienie pięciu niekonwencjonalnych gier - każdy znajdzie coś dla siebie!\r\n',0,'2019-11-01',1572981167,1572986812,'PL'),(11,'Dyniowice',7,'https://www.facebook.com/1932707267024444/posts/2168038433491325','Haloweenowy pomysł na sesje dla dzieci.\r\n',0,'2019-11-01',1572981167,1572986976,'PL'),(12,'Faerun: Jesień w Zapomnianych Krainach',11,'https://skalawyzwania.pl/faerun-jesien-w-zapomnianych-krainach/?utm_source=rss&utm_medium=rss&utm_campaign=faerun-jesien-w-zapomnianych-krainach','Opis jesiennych świąt Fearunu.\r\n',0,'2019-10-31',1572981167,1572987253,'PL'),(13,'Dyniowe pole',10,'https://www.grajkolektyw.pl/graj/graj-obrazem/dyniowe-pole/','Pomysł na przygodę dla paczki nastolatków z amerykańskiego miasteczka.\r\n',0,'2019-10-31',1572981167,1572987387,'PL'),(15,'Halloween 2019 - Zrodzeni z mroźnego szału i zemsty, czyli rzecz o draugrach',12,'http://jaxoblog.blogspot.com/2019/10/halloween-2019-zrodzeni-z-mroznego-szau.html','Nasz erpegowy alchemik tym razem przygotował dla nas wspaniałą miksturę charakterystyk i pomysłów na przygody, używając do tego sproszkowanych skandynawskich nieumarłych.',1,'2019-10-31',1572981167,1572988137,'PL'),(16,'Hrabina Emmanuelle von Liebowitz',13,'https://www.facebook.com/1895172574028385/posts/2411418252403812','Drugoedycyjny opis znanej w świecie Warhammera persony.',0,'2019-10-30',1572981167,1573061059,'PL'),(17,'Z notatnika alchemika #4 - Recenzja Strange New Worlds, zbioru przygód do Star Trek Adventures',12,'http://jaxoblog.blogspot.com/2019/10/z-notatnika-alchemika-4-recenzja.html','Ciekawy i polecany przez Jaxę zbiór przygód do Star Treka spod znaku 2d20.',0,'2019-10-30',1572981167,1572988819,'PL'),(18,'Bogowie Starego Świata',1,'https://www.facebook.com/1714882952150815/posts/2150131588625947','Tym razem Enc snuje opowieść o dwóch, skonfliktowanych ze sobą, największych kultach Imperium.',1,'2019-10-29',1572981167,1572989026,'PL'),(19,'Jestem fanem #3 - Jestem fanem mechanik zasobów i ustalania zasad stołu',12,'http://jaxoblog.blogspot.com/2019/10/jestem-fanem-3-jestem-fanem-mechanik.html','Dla osób, które przegapiły publikacje na FB.\r\n',1,'2019-10-29',1572981167,1572989459,'PL'),(20,'Kryształy Czasu - Epizon V - Paladynat kontratakuje /129/Stało się. MAG zaprzestał wydawania KCtów, a...',14,'https://www.facebook.com/150279362366378/posts/504877953573182','Mająca spory potencjał humorystyczny ostatnia część historii pierwszej polskiej gry RPG\r\n',0,'2019-10-29',1572981167,1572992192,'PL'),(21,'Tu były smoki. Manuskrypt Bufkina w praktyce – postmortem.',5,'https://wolfgangschwarzenatter.blogspot.com/2019/10/tu-byy-smoki-manuskrypt-bufkina-w.html','Solidny test (cała kampania) Beyond This Point be Dragons. Tylko dla zainteresowanych erpegową archeologią!',0,'2019-10-29',1572981167,1572989948,'PL'),(22,'Parę słów o Arystokratach',3,'https://erpegis.wordpress.com/2019/10/28/pare-slow-o-arystokratach/','Kolejny, obszerny tekst o arystokracji w Wolsungu. Tytulatura i nie tylko.',0,'2019-10-28',1572981167,1572990131,'PL'),(23,'Którędy do skarbu?',2,'https://www.facebook.com/107959060574412/posts/146308410072810','Tekst dla entuzjastów penetrowania wszystkich lochów.',0,'2019-10-28',1572981167,1572990370,'PL'),(24,'Tube Screamer Dungeon — Part One: Proof of Concept',15,'https://rollingboxcars.com/2019/11/05/the-tube-screamer-dungeon-step-one-proof-of-concept/',NULL,0,'2019-11-05',1572981611,1572981611,'ENG'),(25,'Monthly Encounter #03: A Purple Worm Problem (lvls 11-16)',16,'https://www.tribality.com/2019/11/05/monthly-encounter-03-a-purple-worm-problem-lvls-11-16/',NULL,0,'2019-11-05',1572981611,1572981611,'ENG'),(26,'Afterlife–Wandering Souls Review',17,'https://gnomestew.com/afterlife-wandering-souls-review/',NULL,0,'2019-11-05',1572981611,1572981611,'ENG'),(27,'8 Fast Facts About D&D&rsquo;s Magic Missile Spell',18,'https://dmdavid.com/tag/8-fun-facts-about-dds-magic-missile-spell/',NULL,0,'2019-11-05',1572981611,1572981611,'ENG'),(28,'UA: Class Feature Variants Breakdown, Part One',16,'https://www.tribality.com/2019/11/04/ua-class-feature-variants-breakdown-part-one/',NULL,0,'2019-11-05',1572981611,1572981611,'ENG'),(29,'1d20 Magical Weapon Accessories: Scabbards, Quivers, Dueling Gloves, ect...',19,'https://sheepandsorcery.blogspot.com/2019/11/1d20-magical-weapon-accessories.html',NULL,0,'2019-11-05',1572981611,1572981611,'ENG'),(30,'7d8 Road-Weary Wanderers',20,'https://throneofsalt.blogspot.com/2019/11/7d8-road-weary-wanderers.html',NULL,0,'2019-11-05',1572981611,1572981611,'ENG'),(31,'Mud and Floods',21,'http://monstersandmanuals.blogspot.com/2019/11/mud-and-floods.html',NULL,0,'2019-11-04',1572981611,1572981611,'ENG'),(32,'The Art Of Traps: Making The Rogue Cry',17,'https://gnomestew.com/the-art-of-traps-making-the-rogue-cry/',NULL,0,'2019-11-04',1572981611,1572981611,'ENG'),(33,'Random GM Tip – The Numbers That We Say',22,'https://thealexandrian.net/wordpress/43860/roleplaying-games/random-gm-tip-the-numbers-that-we-say',NULL,0,'2019-11-04',1572981611,1572981611,'ENG'),(34,'Travel Methods in Your Game',16,'https://www.tribality.com/2019/11/04/travel-methods-in-your-game/',NULL,0,'2019-11-04',1572981611,1572981611,'ENG'),(35,'The Mud King of Stoney Creek',23,'https://tenfootpole.org/ironspike/?p=6297',NULL,0,'2019-11-04',1572981611,1572981611,'ENG'),(36,'Kitbashing Dungeons',24,'http://slyflourish.com/kitbashing_dungeons.html',NULL,0,'2019-11-04',1572981611,1572981611,'ENG'),(37,'Ptolus: Running the Campaign – In-Jokes',22,'https://thealexandrian.net/wordpress/43857/roleplaying-games/ptolus-running-the-campaign-in-jokes','Inspirująca sytuacja na sesji.\r\n',0,'2019-11-01',1572981611,1572993126,'ENG'),(39,'(5e) The Convent of the Weeping Moon',23,'https://tenfootpole.org/ironspike/?p=6293','Niezbyt pozytywna recenzja przygody do D&D 5E.',0,'2019-11-02',1572981611,1572993367,'ENG'),(40,'Five Epic Fantasy Conflicts Other Than War',25,'https://mythcreants.com/blog/five-epic-fantasy-conflicts-other-than-war/','Posty Mythcreants są najczęściej skierowane do pisarzy, ale część z nich możemy wykorzystać także podczas tworzenia scenariuszy.\r\n',0,'2019-11-02',1572981611,1572993456,'ENG'),(42,'Accessibility Tools ft.FATE',17,'https://gnomestew.com/accessibility-tools-ft-fate/','Słowo o niepełnosprawności postaci w FATE.',0,'2019-11-01',1572981611,1572993877,'ENG'),(43,'A Guide to Roleplaying Elves',16,'https://www.tribality.com/2019/11/01/a-guide-to-roleplaying-elves/','Wskazówki do odgrywania elfa pod 5E.',0,'2019-11-01',1572981611,1572993988,'ENG'),(44,'Esoteric Enterprises -  actual play!',27,'https://cavegirlgames.blogspot.com/2019/11/esoteric-enterprises-actual-play.html','Test gry OSR - Esoteric Enterprises.',0,'2019-11-01',1572981611,1572994091,'ENG'),(45,'Terrible & Awesome Sorcerers',28,'https://weirdwonderfulworlds.blogspot.com/2019/10/terrible-awesome-sorcerers.html','Generator (rzuty kostką) magów.\r\n',0,'2019-10-31',1572981611,1572994186,'ENG'),(46,'Nicobobinise Your Game: Interpreting Dreams and Omens',21,'http://monstersandmanuals.blogspot.com/2019/11/nicobobinise-your-game-interpreting.html','Wprowadzenie graczy do krainy snów.\r\n',0,'2019-10-31',1572981611,1572994235,'ENG'),(47,'Mysterious Monday – The Halloween Edition',15,'https://rollingboxcars.com/2019/10/31/mysterious-monday-the-halloween-edition/','Kilka króciutkich historyjek na Halloween, które można potraktować jako pomysły na fabułę.\r\n',0,'2019-10-31',1572981611,1572994323,'ENG'),(48,'Talk is Cheap; So is Thought',29,'https://theangrygm.com/talk-is-cheap-so-is-thought/','Dołączam z kronikarskiego obowiązku, ale szczerze powiedziawszy ja nie mogę się przebić przez te ocenzurowane przekleństwa ;-).',0,'2019-10-31',1572981611,1573060972,'ENG'),(49,'November 2019 RPG Blog Carnival – Welcome to the World of Survival Horror',30,'https://daemonsanddeathrays.wordpress.com/2019/10/31/november-2019-rpg-blog-carnival-welcome-to-the-world-of-survival-horror/','Jeśli jesteś twórcą bloga o rpg, być może zainteresuje Cię ten blogowy karnawał.\r\n',0,'2019-10-31',1572981611,1572994426,'ENG'),(50,'Ghosts',31,'https://goblinpunch.blogspot.com/2019/10/ghosts.html','Duchy jako przeciwnicy w RPG. Autor unika podawania statystyk.\r\n',0,'2019-10-31',1572981611,1572994485,'ENG'),(52,'All that Glitters is Chrome — Cyberpunk Red Jumpstart Kit',15,'https://rollingboxcars.com/2019/10/30/all-that-glitters-is-chrome-cyberpunk-red-jumpstart-kit/','Recenzja startera do nowej edycji Cyberpunka.\r\n',0,'2019-10-30',1572981611,1572994760,'ENG'),(53,'The Joy of Insects: 4 Real-Life Creatures You Can Use to Bug Your Players',17,'https://gnomestew.com/the-joy-of-insects-4-real-life-creatures-you-can-use-to-bug-your-players/','Pomysły na wykorzystanie istniejących w prawdziwym świecie stworzeń jako przeciwników graczy.\r\n',0,'2019-10-30',1572981611,1572994867,'ENG'),(54,'Plague, War, & Famine 1',23,'https://tenfootpole.org/ironspike/?p=6289','Recenzja przygody OSR. Jest ona negatywna, ale polecam przeczytać komentarze.\r\n',0,'2019-10-30',1572981611,1573060836,'ENG'),(55,'Ptolus: Running the Campaign – Dungeon as a Theater of Operations',22,'https://thealexandrian.net/wordpress/43843/roleplaying-games/ptolus-running-the-campaign-dungeon-as-a-theater-of-operations',NULL,0,'2019-10-25',1572981611,1572981611,'ENG'),(56,'Halloween Roundup 2019',16,'https://www.tribality.com/2019/10/29/halloween-roundup-2019/','Zestawienie Halloweenowych blogowych wpisów (z różnych stron) z poprzednich lat.\r\n',0,'2019-10-29',1572981611,1572995063,'ENG'),(57,'Monstrous Races: Half-Vampire/Dhampyr & Ghost',16,'https://www.tribality.com/2019/10/29/monstrous-races-half-vampire-dhampyr-ghost/','Krótki opis umiejętności dwóch typów nieumarłych.\r\n',0,'2019-10-29',1572981611,1572995158,'ENG'),(58,'Insight Played Wrong Can Become the Most Unrealistic Thing In D&D, a Game With Djinns In Bottles Who Grant Wishes',18,'https://dmdavid.com/tag/insight-played-wrong-can-become-the-most-unrealistic-thing-in-dd-a-game-with-djinns-in-bottles-who-grant-wishes/','Kolejny tekst na temat grupowych rzutów.\r\n',0,'2019-10-29',1572981611,1573060202,'ENG'),(59,'Scraps of Undeath',31,'https://goblinpunch.blogspot.com/2019/10/scraps-of-undeath.html','Opis kilku typów nieumarłych.\r\n',0,'2019-10-29',1572981611,1573060015,'ENG'),(60,'The Greatest OSR Blog Posts Known to Man',21,'http://monstersandmanuals.blogspot.com/2019/10/the-greatest-osr-blog-posts-known-to-man.html','Zestawienie tekstów o tematyce OSR.\r\n',0,'2019-10-28',1572981611,1572995268,'ENG'),(61,'Quick Shot on Big Book of Amazing Tales',32,'http://briebeau.com/thoughty/2019/10/quick-shot-on-big-book-of-amazing-tales/','Rzut okiem na system dla dzieci.\r\n',0,'2019-10-28',1572981611,1572995312,'ENG'),(63,'9 Steps for a Successful New Group Launch',17,'https://gnomestew.com/steps-for-successful-launch/','Poradnik dla MG zbierających grupę graczy.\r\n',0,'2019-10-28',1572981611,1572995423,'ENG'),(64,'Creating a Magic System, P2',16,'https://www.tribality.com/2019/10/28/creating-a-magic-system-p2/','Druga część poradnika na temat tworzenia systemu magii w RPG.\r\n',0,'2019-10-28',1572981611,1572995468,'ENG'),(65,'The Psionic Crucible of the Fat Cannibal',23,'https://tenfootpole.org/ironspike/?p=6285','Recenzja krótkiej przygody typu dungeon.',0,'2019-10-28',1572981611,1573059932,'ENG'),(66,'F$&% CR, There&rsquo;s a Better Way (Part 2)',29,'https://theangrygm.com/f-cr-theres-a-better-way-part-2/','Kontynuacja wpisu na temat projektowania \r\npotyczek.\r\n',0,'2019-10-28',1572981611,1572995521,'ENG'),(67,'The Beats of a Dungeon Crawl',24,'http://slyflourish.com/beats_of_a_dungeon_crawl.html','Dla osób chcących udoskonalić swój dungeon crawling.\r\n',0,'2019-10-28',1572981611,1572995583,'ENG'),(68,'The Tangled Cladistics of the &quot;So-Called&quot; Undead',31,'https://goblinpunch.blogspot.com/2019/10/the-tangled-cladistics-of-so-called.html','Długi tekst uzupełniający wiedzę o nieumarłych.\r\n',0,'2019-10-28',1572981611,1572995614,'ENG'),(69,'The Song of Thra',33,'https://barkingalien.blogspot.com/2019/10/the-song-of-thra.html','Rozważania na temat jak miałaby wyglądać gra RPG w uniwersum znanym z filmu oraz serialu Netflix - The Dark Crystal\r\n',0,'2019-10-28',1572981611,1572995679,'ENG');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article_tag`
--

DROP TABLE IF EXISTS `article_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_article_tag__article` (`article_id`),
  KEY `fk_article_tag__tag` (`tag_id`),
  CONSTRAINT `fk_article_tag__article` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`),
  CONSTRAINT `fk_article_tag__tag` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_tag`
--

LOCK TABLES `article_tag` WRITE;
/*!40000 ALTER TABLE `article_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `article_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medium`
--

DROP TABLE IF EXISTS `medium`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medium` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `publisher_id` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `fk_medium__publisher` (`publisher_id`),
  CONSTRAINT `fk_medium__publisher` FOREIGN KEY (`publisher_id`) REFERENCES `publisher` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medium`
--

LOCK TABLES `medium` WRITE;
/*!40000 ALTER TABLE `medium` DISABLE KEYS */;
INSERT INTO `medium` VALUES (1,'ABC gier fabularnych (Facebook)','https://www.facebook.com/1714882952150815',1,1572981167,1572981167),(2,'Kołek niewiary (Facebook)','https://www.facebook.com/107959060574412',2,1572981167,1572981167),(3,'No ja nie mogę…','https://erpegis.wordpress.com/',3,1572981167,1572981167),(4,'Wyjście z piwnicy','http://wyjsciezpiwnicy.blogspot.com/',4,1572981167,1572981167),(5,'Fear and loathing in the Old World','http://wolfgangschwarzenatter.blogspot.com/',5,1572981167,1572981167),(6,'Erpegowy Matecznik (Facebook)','https://www.facebook.com/404634013674125',6,1572981167,1572981167),(7,'Fajerbol Junior - gry fabularne dla dzieci (Facebook)','https://www.facebook.com/1932707267024444',7,1572981167,1572981167),(8,'Staroświecka Pracownia Gier Imć Pani Wiewiórskiej','http://squirel-spg.blogspot.com/',8,1572981167,1572981167),(9,'Dystrykt Psychofabularny (Facebook)','https://www.facebook.com/2197446783670850',9,1572981167,1572981167),(10,'GRAJ! Kolektyw','https://www.grajkolektyw.pl/',10,1572981167,1572981167),(11,'Skala Wyzwania','https://skalawyzwania.pl/',11,1572981167,1572981167),(12,'RPGowa Alchemia','http://jaxoblog.blogspot.com/',12,1572981167,1572981167),(13,'Warhammer Fantasy Roleplay 2 PL (Facebook)','https://www.facebook.com/1895172574028385',13,1572981167,1572981167),(14,'Holistyczny przewodnik po grach (Facebook)','https://www.facebook.com/150279362366378',14,1572981167,1572981167),(15,'Rolling Boxcars','https://rollingboxcars.com/',15,1572981611,1572981611),(16,'Tribality','https://www.tribality.com/',16,1572981611,1572981611),(17,'Gnome Stew','https://gnomestew.com/',17,1572981611,1572981611),(18,'DMDavid','https://dmdavid.com/',18,1572981611,1572981611),(19,'Sheep and Sorcery','https://sheepandsorcery.blogspot.com/',19,1572981611,1572981611),(20,'Throne of Salt','http://throneofsalt.blogspot.com/',20,1572981611,1572981611),(21,'Monsters and Manuals','http://monstersandmanuals.blogspot.com/',21,1572981611,1572981611),(22,'The Alexandrian','https://thealexandrian.net/',22,1572981611,1572981611),(23,'tenfootpole.org','https://tenfootpole.org/ironspike',23,1572981611,1572981611),(24,'Sly Flourish','http://slyflourish.com/',24,1572981611,1572981611),(25,'Mythcreants','https://mythcreants.com/',25,1572981611,1572981611),(26,'Jeffs Gameblog','http://jrients.blogspot.com/',26,1572981611,1572981611),(27,'Cavegirl\'s Game Stuff','https://cavegirlgames.blogspot.com/',27,1572981611,1572981611),(28,'Weird & Wonderful Worlds','https://weirdwonderfulworlds.blogspot.com/',28,1572981611,1572981611),(29,'The Angry GM','https://theangrygm.com/',29,1572981611,1572981611),(30,'Daemons & Deathrays','https://daemonsanddeathrays.wordpress.com/',30,1572981611,1572981611),(31,'Goblin Punch','http://goblinpunch.blogspot.com/',31,1572981611,1572981611),(32,'Thoughty','http://briebeau.com/thoughty',32,1572981611,1572981611),(33,'BARKING ALIEN','http://barkingalien.blogspot.com/',33,1572981611,1572981611);
/*!40000 ALTER TABLE `medium` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1572883429),('m130524_201442_init',1572883429),('m190124_110200_add_verification_token_column_to_user_table',1572883429),('m191001_181919_create_base_tables',1572883430),('m191021_064510_alter_article_table',1572883430),('m191025_085326_alter_article_table',1572883430),('m191031_082933_clean_and_fix_the_tables',1572883431);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publisher`
--

DROP TABLE IF EXISTS `publisher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publisher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publisher`
--

LOCK TABLES `publisher` WRITE;
/*!40000 ALTER TABLE `publisher` DISABLE KEYS */;
INSERT INTO `publisher` VALUES (1,'ABC gier fabularnych (Facebook)',1572981167,1572981167),(2,'Kołek niewiary (Facebook)',1572981167,1572981167),(3,'No ja nie mogę…',1572981167,1572981167),(4,'Wyjście z piwnicy',1572981167,1572981167),(5,'Fear and loathing in the Old World',1572981167,1572981167),(6,'Erpegowy Matecznik (Facebook)',1572981167,1572981167),(7,'Fajerbol Junior - gry fabularne dla dzieci (Facebook)',1572981167,1572981167),(8,'Staroświecka Pracownia Gier Imć Pani Wiewiórskiej',1572981167,1572981167),(9,'Dystrykt Psychofabularny (Facebook)',1572981167,1572981167),(10,'GRAJ! Kolektyw',1572981167,1572981167),(11,'Skala Wyzwania',1572981167,1572981167),(12,'RPGowa Alchemia',1572981167,1572981167),(13,'Warhammer Fantasy Roleplay 2 PL (Facebook)',1572981167,1572981167),(14,'Holistyczny przewodnik po grach (Facebook)',1572981167,1572981167),(15,'Rolling Boxcars',1572981611,1572981611),(16,'Tribality',1572981611,1572981611),(17,'Gnome Stew',1572981611,1572981611),(18,'DMDavid',1572981611,1572981611),(19,'Sheep and Sorcery',1572981611,1572981611),(20,'Throne of Salt',1572981611,1572981611),(21,'Monsters and Manuals',1572981611,1572981611),(22,'The Alexandrian',1572981611,1572981611),(23,'tenfootpole.org',1572981611,1572981611),(24,'Sly Flourish',1572981611,1572981611),(25,'Mythcreants',1572981611,1572981611),(26,'Jeffs Gameblog',1572981611,1572981611),(27,'Cavegirl\'s Game Stuff',1572981611,1572981611),(28,'Weird & Wonderful Worlds',1572981611,1572981611),(29,'The Angry GM',1572981611,1572981611),(30,'Daemons & Deathrays',1572981611,1572981611),(31,'Goblin Punch',1572981611,1572981611),(32,'Thoughty',1572981611,1572981611),(33,'BARKING ALIEN',1572981611,1572981611);
/*!40000 ALTER TABLE `publisher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-06 23:34:16
