<?php

use yii\db\Migration;

/**
 * Class m191031_082933_clean_and_fix_the_tables
 */
class m191031_082933_clean_and_fix_the_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%tag}}', 'frequency');

        $this->alterColumn('{{%article}}', 'is_premium',
            $this->boolean()->notNull()->defaultValue(0)
        );

        $this->alterColumn('{{%article}}', 'source_date',
            $this->date()->notNull()
        );

        $this->alterColumn('{{%article}}', 'language',
            $this->string()->notNull()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%tag}}', 'frequency', $this->integer());
    }
}
