<?php

use yii\db\Migration;

/**
 * Class m191021_064510_alter_article_table
 */
class m191021_064510_alter_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%article}}', 'language', $this->string());
        $this->addColumn('{{%article}}', 'hash', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%article}}', 'language');
        $this->dropColumn('{{%article}}', 'hash');
    }
}
