<?php

use yii\db\Migration;

/**
 * Class m191001_181919_create_base_tables
 */
class m191001_181919_create_base_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%article}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'medium_id' => $this->integer(),
            'url' => $this->string(),
            'abstract' => $this->text(),
            'is_premium' => $this->boolean(),
            'source_date' => $this->dateTime(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createTable('{{%medium}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->unique(),
            'url' => $this->string(),
            'publisher_id' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createTable('{{%publisher}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createTable('{{%tag}}', [
            'id' => $this->primaryKey(),
            'frequency' => $this->integer(),
            'name' => $this->string(),
        ]);

        $this->createTable('{{%article_tag}}', [
            'id' => $this->primaryKey(),
            'article_id' => $this->integer(),
            'tag_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk_article__medium',
            '{{%article}}', 'medium_id',
            '{{%medium}}',  'id'
        );

        $this->addForeignKey(
            'fk_medium__publisher',
            '{{%medium}}', 'publisher_id',
            '{{%publisher}}',  'id'
        );

        $this->addForeignKey(
            'fk_article_tag__article',
            '{{%article_tag}}', 'article_id',
            '{{%article}}',  'id'
        );

        $this->addForeignKey(
            'fk_article_tag__tag',
            '{{%article_tag}}', 'tag_id',
            '{{%tag}}',  'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%article}}');
        $this->dropTable('{{%medium}}');
        $this->dropTable('{{%publisher}}');
        $this->dropTable('{{%tag}}');
        $this->dropTable('{{%article_tag}}');
    }
}
