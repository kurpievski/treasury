<?php

use yii\db\Migration;

/**
 * Class m191025_085326_alter_article_table
 */
class m191025_085326_alter_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%article}}', 'source_date', $this->date());
        $this->dropColumn('{{%article}}', 'hash');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%article}}', 'source_date', $this->dateTime());
        $this->addColumn('{{%article}}', 'hash', $this->string());
    }
}
