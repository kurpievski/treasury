<?php

namespace backend\controllers;

use backend\external\Inoreader\Inoreader;
use common\models\Article;
use Yii;
use common\models\Medium;
use common\models\search\MediumSearch;
use yii\db\ActiveQuery;
use yii\db\IntegrityException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MediumController implements the CRUD actions for Medium model.
 */
class MediumController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Medium models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MediumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Medium model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Medium model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Medium();
        if ($model->load($request->post()) && $model->save()) {


            return $request->isAjax
                ? Json::encode(['id' => $model->id])
                : $this->redirect(['view', 'id' => $model->id]);
        }

        return $request->isAjax
            ? Json::encode(['id' => $model->id])
            : $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing Medium model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {


            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Medium model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionSubscriptionsPl()
    {
        return $this->subscriptions(Article::FEED_PL);
    }

    public function actionSubscriptionsEng()
    {
        return $this->subscriptions(Article::FEED_ENG);
    }

    private function subscriptions($language)
    {
        /** @var Inoreader $inoreader */
        $inoreader = Yii::$app->inoreader->getInstance();

        if (is_string($inoreader)) {
            return $this->redirect($inoreader);
        }

        $response = $inoreader->reader()->subscriptionsList();
        $filteredSubscriptions = \array_filter($response['subscriptions'], function ($v) use ($language) {
            foreach ($v['categories'] as $category) {
                if ($category['label'] == 'RPG-'.$language) {
                    return true;
                }
            }
            return false;
        });

        $subscriptions = ArrayHelper::map($filteredSubscriptions, 'title', 'htmlUrl');
        ksort($subscriptions, SORT_FLAG_CASE | SORT_STRING);

        return $this->render('list', [
            'subscriptions' => $subscriptions,
        ]);
    }

    /**
     * Finds the Medium model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Medium the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Medium::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
