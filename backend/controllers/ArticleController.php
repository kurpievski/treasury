<?php

namespace backend\controllers;

use backend\external\Inoreader\Inoreader;
use common\helpers\BooleanHelper;
use common\models\Medium;
use kartik\grid\EditableColumnAction;
use Yii;
use common\models\Article;
use common\models\search\ArticleSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'edit' => [
                'class' => EditableColumnAction::class,
                'modelClass' => Article::class,
                'outputValue' => function ($model, $attribute, $key, $index) {
                    if (strpos($attribute, 'is_')) {
                        return BooleanHelper::getListOptions()[$model->$attribute];
                    }

                    if ($attribute == 'tags') {
                        return implode(', ', $model->gridTags());
                    }

                    return $model->$attribute;
                },
                'showModelErrors' => true,
            ],
        ]);
    }


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['browse'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionBrowse()
    {
        $this->layout = 'simple';

        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('browse', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionList($start, $end)
    {
        $this->layout = 'list';

        $baseQuery = Article::find()->with(['tags'])
            ->joinWith(['publisher'])
            ->select(['article.id', 'article.name', 'article.url', 'abstract', 'source_date', 'medium_id'])
            ->andWhere(['between', 'source_date', $start, $end])
            ->orderBy(['source_date' => \SORT_ASC]);

        $feeds = [];
        foreach ([Article::FEED_PL => 'Polska blogosfera', Article::FEED_ENG => 'Zagraniczna blogosfera'] as $language => $title) {
            $query = clone $baseQuery;
            $feeds[$title] = ArrayHelper::index($query
                ->andWhere(['=', 'language', $language])
                ->all(), null, ['publisher.name']);

            ksort($feeds[$title]);
        }

        return $this->render('list', [
            'feeds' => $feeds,
        ]);
    }

    public function actionFeed()
    {
        /** @var Inoreader|bool $inoreader */
        $inoreader = Yii::$app->inoreader->getInstance();

        if (is_string($inoreader)) {
            return $this->redirect('/site/inoreader');
        }

        $request = Yii::$app->request;

        if ($request->isAjax) {
            try {
                $response = Json::decode($request->getRawBody());
                foreach ($response['data'] as $item) {
                    $article = new Article([
                        'name' => $item['name'],
                        'url' => $item['url'],
                        'source_date' => $item['date'],
                        'language' => $item['language'],
                        'is_premium' => 0,
                        'scenario' => Article::SCENARIO_IMPORT,
                    ]);

                    $article->populateRelation('medium', new Medium([
                        'name' => $item['medium'],
                        'url' => $item['medium_url'],
                    ]));
                    if (!$article->save()) {
                        return json_encode($article->getErrors());
                    }
                }
            } catch (\Exception $e) {
                return 0;
            }

            return 1;
        }

        $streamContents = $inoreader->getUnreadStreamContents('user/-/state/com.google/starred', 1000);
        $dataProvider = Article::createDataProviderForStreamContents($streamContents);

        return $this->render('feed', [
            'dataProvider' => $dataProvider,
            'timestamp' => $inoreader->getLastStreamingTimestamp(),
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['create']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, ['tags']);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }

        $request = Yii::$app->getRequest();

        if ($request->isAjax) {
            return $this->renderPartial('update', [
                'model' => $model,
            ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionTogglePremium($id)
    {
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->is_premium = !$model->is_premium;
        $model->save(false);

        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $with = [])
    {
        $query = Article::find()->where(['id' => $id]);

        if ($with) {
            $query->with($with);
        }

        if (($model = $query->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
