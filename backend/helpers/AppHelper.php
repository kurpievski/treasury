<?php

namespace backend\helpers;

use backend\external\Inoreader\Inoreader;
use yii\caching\DbDependency;
use yii\db\ActiveQuery;
use yii\helpers\Html;

/**
 * Class AppHelper
 * @package backend\helpers
 */
class AppHelper
{
    const WEEK_LAST = 'last';
    const WEEK_NEXT = 'next';

    /**
     * @param string $week
     * @param array $params
     * @return string
     */
    static public function generateWeeksLink(string $week, $url, array $params)
    {
        $format = 'Y-m-d';

        if ($week == static::WEEK_LAST) {
            $start = date($format,strtotime('last wednesday'));
            $end = date($format,strtotime('this tuesday'));
            $string = 'Current week';
        }

        if ($week == static::WEEK_NEXT) {
            $start = date($format,strtotime('this wednesday'));
            $end = date($format,strtotime('next tuesday'));
            $string = 'Next week';
        }

        $options = [$url];
        if (count($params) == 2) {
            foreach ($params as $param => $valuePattern) {
                $options[$param] = sprintf($valuePattern, $$param);
            }
        } else {
            foreach ($params as $param => $valuePattern) {
                $options[$param] = sprintf($valuePattern, $start, $end);
            }
        }

        $text = sprintf('%s: %s - %s', $string, $start, $end);

        return Html::a($text, $options, ['class' => "btn btn-default"]);
    }

    /**
     * @param ActiveQuery $query
     * @return array|\yii\db\ActiveRecord[]
     */
    static public function getCachedModels(ActiveQuery $query)
    {
        return $query
            ->cache(0, new DbDependency([
                'sql' => $query->createCommand()->getRawSql()
            ]))
            ->all();
    }
}
