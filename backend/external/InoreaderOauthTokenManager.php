<?php

namespace backend\external;

use BaseApi\Contracts\TokenManagerInterface;
use BaseApi\OauthToken;

/**
 * Class InoreaderOauthTokenManager
 * @package backend\external
 */
class InoreaderOauthTokenManager implements TokenManagerInterface
{
    /**
     * @return mixed
     */
    public function getData()
    {
        return \Yii::$app->cache->get('inoreaderTokenData');
    }

    /**
     * @param OauthToken $token
     */
    public function setData(OauthToken $token)
    {
        \Yii::$app->cache->set('inoreaderTokenData', [
            'access_token' => $token->getAccessToken(),
            'expires' => $token->getExpires(),
            'expires_in' => $token->getExpiresIn(),
            'token_type' => $token->getTokenType(),
            'scope' => $token->getScope(),
            'refresh_token' => $token->getRefreshToken(),
        ]);
    }
}
