<?php

namespace backend\external\Inoreader;

use backend\external\Inoreader\Endpoint\OauthEndpoint;
use backend\external\Inoreader\Endpoint\ReaderEndpoint;
use backend\external\Inoreader\Traits\InputValidatorAwareTrait;
use BaseApi\Client;
use BaseApi\Exception\InvalidArgumentException;
use BaseApi\Traits\OauthAwareTrait;
use Symfony\Component\OptionsResolver\OptionsResolver;
use yii\db\Exception;

/**
 * Class Inoreader
 * @package backend\external\Inoreader
 */
class Inoreader extends Client
{
    use OauthAwareTrait;

    public const BASE_URI = 'https://www.inoreader.com/';
    public const OAUTH_URL = 'https://www.inoreader.com/oauth2/auth';

    public const SCOPE_READ = 'read';
    public const SCOPE_READ_WRITE = 'read write';

    protected $lastStreamingTimestamp;

    /**
     * @param $options
     */
    protected function configureOptions(&$options)
    {
        $options['oauthEndpointClass'] = OauthEndpoint::class;

        $resolver = $this->getOptionsResolver();
        $this->configureOauthOptions($resolver);
        $resolver->setRequired(['clientSecret']);
    }

    /**
     * @return ReaderEndpoint
     */
    public function reader(): ReaderEndpoint
    {
        return $this->getEndpoint(ReaderEndpoint::class);
    }

    /**
     * Adds a subscription
     *
     * @param string $streamId
     * @param $title
     * @param string $folder
     * @return bool
     */
    public function subscribe(string $streamId, $title, $folder = ''): bool
    {
        $params = [
            't' => $title,
            'a' => $folder,
        ];

        $response = $this->reader()->editSubscription(ReaderEndpoint::ACTION_SUBSCRIBE, $streamId, $params);

        return $this->processOkResponseType($response);
    }

    /**
     * Adds a subscription without params
     *
     * @param string $streamId
     * @param array $result var used for storing the response
     * @return bool
     */
    public function addSubscription(string $streamId, &$result = []): bool
    {
        $result = $this->reader()->addSubscription($streamId);

        return isset($result['numResults']) && $result['numResults'];
    }

    /**
     * Subscription edit
     *
     * @param string $streamId
     * @param $options
     *      $params = [
     *          'title' => string,
     *          'addToFolder' => string,
     *          'removeFromFolder' => string,
     *      ]
     * @return bool
     */
    public function editSubscription(string $streamId, $options): bool
    {
        if (empty($options)) {
            throw new InvalidArgumentException('$options cannot be empty');
        }

        $resolver = new OptionsResolver();
        $resolver->setDefined(['title', 'addToFolder', 'removeFromFolder']);
        $options = $resolver->resolve($options);

        $params = [
            't' => $options['title'] ?? '',
            'a' => $options['addToFolder'] ?? '',
            'r' => $options['removeFromFolder'] ?? '',
        ];

        $response = $this->reader()->editSubscription(ReaderEndpoint::ACTION_EDIT, $streamId, $params);

        return $this->processOkResponseType($response);
    }

    /**
     * Removes a subscription
     *
     * @param string $streamId
     * @return bool
     */
    public function unsubscribe(string $streamId): bool
    {
        $response = $this->reader()->editSubscription(ReaderEndpoint::ACTION_UNSUBSCRIBE, $streamId);

        return $this->processOkResponseType($response);
    }

    /**
     * @param $streamId
     * @param int $limit
     * @return array
     */
    public function getUnreadStreamContents($streamId, $limit = 0): array
    {
        $params = [
            'xt' => ReaderEndpoint::EXCLUDE_TARGET_READ,
            'n' => $limit ?: ''
        ];

        $unreadStreamContents = $this->reader()->streamContents($streamId, $params);

        $this->lastStreamingTimestamp = $unreadStreamContents['updated'];

        return $unreadStreamContents;
    }

    public function markAllAsRead(string $streamId): bool
    {
        $lastStreamingTimestamp = $this->lastStreamingTimestamp;

        if (!$lastStreamingTimestamp) {
            throw new \Exception('No last streaming timestamp set');
        }

        $response = $this->reader()->markAllAsRead($streamId, $lastStreamingTimestamp);

        return $this->processOkResponseType($response);
    }

    /**
     * Returns the timestamp of the last fetched stream contents. Should be used with marking as read.
     *
     * @return int
     */
    public function getLastStreamingTimestamp(): int
    {
        return $this->lastStreamingTimestamp;
    }

    /**
     * @param mixed $lastStreamingTimestamp
     */
    public function setLastStreamingTimestamp($lastStreamingTimestamp)
    {
        $this->lastStreamingTimestamp = (int)$lastStreamingTimestamp;
    }

    /**
     * @param $response
     * @return bool
     */
    protected function processOkResponseType($response): bool
    {
        return \in_array('OK', $response);
    }
}
