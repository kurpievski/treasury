<?php

namespace backend\external\Inoreader\Endpoint;

use BaseAPI\Contracts\ClientInterface;
use BaseApi\Contracts\EndpointInterface;
use BaseApi\Endpoint\AbstractEndpoint;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ReaderEndpoint
 * @package backend\external\Inoreader\Endpoint
 */
class ReaderEndpoint extends AbstractEndpoint implements EndpointInterface
{
    public const EXCLUDE_TARGET_READ = 'user/-/state/com.google/read';

    public const ACTION_EDIT = 'edit';
    public const ACTION_SUBSCRIBE = 'subscribe';
    public const ACTION_UNSUBSCRIBE = 'unsubscribe';

    protected $baseUrn = 'reader/api/0';

    /**
     * @param $options
     */
    protected function addRequestOptions(&$options)
    {
        $options['headers']['Content-Type'] = 'application/x-www-form-urlencoded';
        $options['headers']['Authorization'] = 'Bearer '.$this->client->getToken()->getAccessToken();
        $options['headers']['AppId'] = $this->options->clientId;
        $options['headers']['AppKey'] = $this->options->clientSecret;
    }

    /**
     * This method returns basic information about the logged in user.
     * https://www.inoreader.com/developers/user-info
     *
     * @return array
     */
    public function userInformation(): array
    {
        return $this->sendRequest(
            ClientInterface::HTTP_METHOD_GET,
            $this->getUrn(['user-info'])
        );
    }

    /**
     * This method is used to subscribe to feeds.
     * https://www.inoreader.com/developers/add-subscription
     *
     * @param string $streamId
     * @return array
     */
    public function addSubscription(string $streamId): array
    {
        $options['query']['quickadd'] = $streamId;

        return $this->sendRequest(
            ClientInterface::HTTP_METHOD_POST,
            $this->getUrn(['subscription', 'quickadd']),
            $options
        );
    }

    /**
     * This method is used to rename the subscription, add it to a folder, remove it from folder or unsubscribe from it.
     * You can also subscribe and directly rename it and add it to folder via this method.
     * https://www.inoreader.com/developers/edit-subscription
     *
     * @param string $action
     * @param string $streamId
     * @param array $params
     * @return array
     */
    public function editSubscription(string $action, string $streamId, array $params = []): array
    {
        $resolver = new OptionsResolver();
        $resolver->setDefined(['t', 'a', 'r']);

        $options['query'] = \array_merge([
            'ac' => $action,
            's' => $streamId
        ], $resolver->resolve($params));

        return $this->sendRequest(
            ClientInterface::HTTP_METHOD_POST,
            $this->getUrn(['subscription', 'edit']),
            $options
        );
    }

    /**
     * This method is used to fetch the unread counters for folders, tags and feeds.
     * https://www.inoreader.com/developers/unread-counts
     *
     * @return array
     */
    public function unreadCounters(): array
    {
        return $this->sendRequest(
            ClientInterface::HTTP_METHOD_GET,
            $this->getUrn(['unread-count'])
        );
    }

    /**
     * This method fetches the current subscriptions for the logged user
     * https://www.inoreader.com/developers/subscription-list
     *
     * @return array
     */
    public function subscriptionsList(): array
    {
        return $this->sendRequest(
            ClientInterface::HTTP_METHOD_GET,
            $this->getUrn(['subscription', 'list'])
        );
    }

    /**
     * This method fetches the current folders and tags for the user
     * https://www.inoreader.com/developers/tag-list
     *
     * @param bool $types
     * @param bool $counts
     * @return array
     */
    public function list($types = false, $counts = false): array
    {
        $options['query']['types'] = $types ? 1 : 0;
        $options['query']['counts'] = $counts ? 1 : 0;

        return $this->sendRequest(
            ClientInterface::HTTP_METHOD_GET,
            $this->getUrn(['tag', 'list']),
            $options
        );
    }

    /**
     * This method returns the articles for a given collection
     * https://www.inoreader.com/developers/stream-contents
     *
     * @param $streamId
     * @param array $params
     * @return array
     */
    public function streamContents($streamId, $params = []): array
    {
        $resolver = new OptionsResolver();
        $resolver->setDefined(['n', 'r', 'ot', 'xt', 'it', 'c', 'sq',
            'globalSearch', 'output', 'includeAllDirectStreamsIds']
        );
        $resolver->setAllowedTypes('n', 'int');
        $resolver->setAllowedValues('r', 'o');
        $resolver->setAllowedTypes('ot', 'int');
        $resolver->setAllowedValues('globalSearch', 1);
        $resolver->setAllowedValues('output', 'json');
        $resolver->setAllowedValues('includeAllDirectStreamsIds', false);

        $options['query'] = $resolver->resolve($params);

        return $this->sendRequest(
            ClientInterface::HTTP_METHOD_GET,
            $this->getUrn(['stream', 'contents', $streamId]),
            $options
        );
    }

    /**
     * What you get via this call is a list of folders and the system user/-/state/com.google/root folder, which is the
     * root level of the subscriptions tree. For more information, see Sort IDs.
     * https://www.inoreader.com/developers/preference-list
     *
     * @return array
     */
    public function streamPreferenceList(): array
    {
        return $this->sendRequest(
            ClientInterface::HTTP_METHOD_GET,
            $this->getUrn(['preference', 'stream', 'list'])
        );
    }

    /**
     * This method marks all items in a given stream as read. Please provide the ts parameter - unix timestamp,
     * generated the last time the list stream was fetched and displayed to the user, so it won't mark as read items
     * that the user never got.
     * https://www.inoreader.com/developers/mark-all-as-read
     *
     * @param string $streamId
     * @param int $timestamp
     * @return array
     */
    public function markAllAsRead(string $streamId, int $timestamp): array
    {
        $options['query']['ts'] = $timestamp;
        $options['query']['s'] = $streamId;

        return $this->sendRequest(
            ClientInterface::HTTP_METHOD_POST,
            $this->getUrn(['mark-all-as-read']),
            $options
        );
    }
}
