<?php
/**
 * Created by PhpStorm.
 * User: kurpievski
 * Date: 2019-10-24
 * Time: 23:56
 */

namespace backend\external\Inoreader\Endpoint\Model;


use BaseApi\Endpoint\Model;

class Stream extends Model
{

    public function __construct($data)
    {
        foreach ($data['items'] as $key => $item) {
            $items[$item['id']] = new StreamItem;
        }
    }
}