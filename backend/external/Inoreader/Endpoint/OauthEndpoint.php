<?php

namespace backend\external\Inoreader\Endpoint;

use BaseAPI\Contracts\ClientInterface;
use BaseApi\Contracts\EndpointInterface;
use BaseApi\Endpoint\AbstractEndpoint;
use backend\external\Inoreader\Traits\InputValidatorAwareTrait;

/**
 * Class PrivateEndpoint
 * @package backend\external\Inoreader\Endpoint
 */
class OauthEndpoint extends AbstractEndpoint implements EndpointInterface
{
    use InputValidatorAwareTrait;

    public $baseUrn = 'oauth2/';

    /**
     * @return array
     */
    protected function addRequestOptions(&$options)
    {
        $options['headers']['Content-Type'] = 'application/x-www-form-urlencoded';
        $options['form_params']['client_id'] = $this->options->clientId;
        $options['form_params']['client_secret'] = $this->options->clientSecret;
    }

    /**
     * @param string $code
     * @return array
     */
    public function token(string $code)
    {
        return $this->sendRequest(
            ClientInterface::HTTP_METHOD_POST, $this->getUrn(['token']), [
                'form_params' => [
                    'code' => $code,
                    'redirect_uri' => $this->options->redirectUri,
                    'grant_type' => 'authorization_code',
                ]
            ]
        );
    }

    /**
     * @param string $refreshToken
     * @return array
     */
    public function refreshToken(string $refreshToken)
    {
        return $this->sendRequest(
            ClientInterface::HTTP_METHOD_POST, $this->getUrn(['token']), [
                'form_params' => [
                    'refresh_token' => $refreshToken,
                    'grant_type' => 'refresh_token',
                ]
            ]
        );
    }
}
