<?php

namespace backend\external\Inoreader\Traits;

use BaseAPI\Exception\InvalidArgumentException;

/**
 * Trait InputValidatorAwareTrait
 * @package backend\external\Inoreader\Traits
 */
trait InputValidatorAwareTrait
{
    /**
     * @param string $input
     * @param string $provided
     * @param string $validFormat
     *
     * @throws InvalidArgumentException
     */
    private function validateDateInput(string $input, string $provided, string $validFormat)
    {
        $providedDateProperlyFormatted = \DateTime::createFromFormat($validFormat, $provided);

        if (!$providedDateProperlyFormatted || $providedDateProperlyFormatted->format($validFormat) != $provided) {
            throw new InvalidArgumentException(
                sprintf(
                    'The provided date value "%s" for argument "%s" is invalid. Valid date format: "%s"',
                    $provided,
                    $input,
                    $validFormat
                )
            );
        }
    }

    private function atLeastOneKeyExists($keys, array $array): bool
    {
        return (bool)\array_intersect($keys, \array_keys($array));
    }

    /**
     * @param string $input
     * @param array $provided
     *
     * @throws InvalidArgumentException
     */
    private function XvalidateArrayInput(string $input, array $provided, array $validFields = [])
    {
        $filteredProvided = array_filter($provided);
        if ((empty($filteredProvided))) {
            throw new InvalidArgumentException(
                sprintf('The provided array for argument "%s" is empty.', $input)
            );
        }

        if ($validFields) {
            $this->validateArrayElements($input, $provided, $validFields);
        }
    }

    /**
     * @param array $provided
     * @param array $validFields
     *
     * @throws InvalidArgumentException
     */
    private function XvalidateArrayElements(string $input, array $provided, array $validFields)
    {
        foreach ($validFields as $validField => $attributes) {
            if ($validField[0] == '*') {
                $validField = substr($validField, 1);
                if (!in_array($validField, array_keys($provided))) {
                    throw new InvalidArgumentException(
                        sprintf(
                            'The provided array for argument "%s" lacks required key "%s".',
                            $input,
                            $validField
                        )
                    );
                }
            }

            if (isset($provided[$validField])) {
                $type = $this->getTypeFromAttributes($attributes);
                $this->validateArrayElement($provided[$validField], $type, $input, $attributes, $validField);
            }
        }
    }

    /**
     * @param $attributes
     * @return string
     */
    private function XgetTypeFromAttributes($attributes): string
    {
        if (is_array($attributes)) {
            if (isset($attributes['type'])) {
                return (string)$attributes['type'];
            }
        } else {
            return (string)$attributes;
        }
    }

    /**
     * @param $provided
     * @param $type
     * @param $input
     * @param $attributes
     * @param $validField
     *
     * @throws InvalidArgumentException
     */
    private function XvalidateArrayElement($provided, $type, $input, $attributes, $validField)
    {
        if ($type == 'date') {
            if (isset($attributes['format'])) {
                $format = $attributes['format'];
                if ($this->validateDateInput($input, $provided, $format)) {
                    throw new InvalidArgumentException(
                        sprintf(
                            'The provided array for argument "%s" contains an element "%s" of wrong type. Valid type: date format %',
                            $input,
                            $validField,
                            $format
                        )
                    );
                }
            }
        } else {
            $validatorMethod = 'is_'.$type;
            if (!$validatorMethod($provided)) {
                throw new InvalidArgumentException(
                    sprintf(
                        'The provided array for argument "%s" contains an element "%s" of wrong type. Valid type: %s',
                        $input,
                        $validField,
                        $type
                    )
                );
            }
        }
    }
}
