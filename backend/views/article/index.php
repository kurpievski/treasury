<?php

use backend\helpers\AppHelper;
use common\helpers\BooleanHelper;
use common\models\Article;
use common\models\Medium;
use common\models\Tag;
use kartik\editable\Editable;
use kartik\grid\SerialColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Articles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Article'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel, 'page' => 'index']); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
			['class' => SerialColumn::class],
            ['attribute' => 'name',],
            [
				'attribute' => 'medium',
				'value' => 'medium.name',
				'filter' => ArrayHelper::map(AppHelper::getCachedModels(Medium::find()), 'id', 'name'),

			],
			[
				'class'=>'kartik\grid\EditableColumn',
				'attribute'=>'abstract',
                'editableOptions'=> [
					'formOptions' => ['action' => ['/article/edit']],
					'inputType' => Editable::INPUT_TEXTAREA
				]
			],
            [
				'attribute' => 'is_premium',
				'format' => 'boolean',
				'filter' => BooleanHelper::getListOptions(),
			],
            [
                'attribute' => 'source_date',
                'format' => 'date',
				'filterInputOptions' => [
					'autocomplete' => 'off',
					'class' => 'form-control'
				],
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => ([
                    'attribute' => 'only_date',
                    'presetDropdown' => true,
                    'convertFormat' => false,
                    'pluginOptions' => [
                        'separator' => ' - ',
                        'format' => 'YYYY-MM-DD',
                        'locale' => [
                            'format' => 'YYYY-MM-DD'
                        ],
                    ],
                    'pluginEvents' => [
                        "apply.daterangepicker" => "function() { apply_filter('only_date') }",
                    ],
                ]),
            ],
            [
                'attribute' => 'language',
                'filter' => Article::getLanguages(),
            ],
            [
                'class' => 'backend\external\kartik\grid\EditableColumn',
                'attribute' => 'tags',
                'format' => 'html',
                'editableOptions' => [
                    'displayValue' => function ($model) {
    					$tags = implode(', ', $model->gridTags());
    					return $tags ?: '(not set)';
					},
                    'submitOnEnter' => false,
                    'header'=>'Tags',
                    'formOptions' => ['action' => ['/article/edit']],
                    'inputType'=>Editable::INPUT_SELECT2,
                    'options' => [
                        'attribute' => 'tags',
                        'data' => ArrayHelper::map(AppHelper::getCachedModels(Tag::find()), 'id', 'name'),
						'options' => [
                            'multiple' => true,
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'tags' => true,
                            'tokenSeparators' => [','],
                            'maximumInputLength' => 100,
                        ],
                        'pluginEvents' => [
                            "select2:select" => "function() {
									var select = $(this);
									var data = {
									  tags: $(this).val()
									};
									$.post('".Url::to(['tag/create'])."', data, function (data) {
										$.each(data, function(key, element) {
											var option = select.find('option:last');
											option.val(element.id);
										});
									}, 'json');
								}"
                        ]
                    ],
                ],
            ],
            [
				'class' => 'yii\grid\ActionColumn',
				'template' => '{update} {delete} {link} {toggle-premium}',
				'buttons' => [

					'link' => function ($url, $model, $key) {
                        return Html::a(
							Html::tag('span', '', ['class' => 'glyphicon glyphicon-sunglasses']),
							$model->url, [
								'target' => '_blank',
								'title' => 'Open the article in a new tab',
								'aria-label' => 'Open the article in a new tab',
							]
						);
                    },
                    'toggle-premium' => function ($url, $model, $key) {
                        return Html::a(Html::tag('span', '',
							['class' => 'glyphicon glyphicon-asterisk']),
							$url, [
								'aria-label' => 'Toggle premium',
								'data-pjax' => 1,
								'data-method' => 'post',
							]
						);
                    },
					'delete' => function ($url, $model, $key) {
                        return Html::a(Html::tag('span', '',
							['class' => 'glyphicon glyphicon-trash']),
							$url, [
								'title' => 'Delete',
								'aria-label' => 'Delete',
								'data-confirm' => 'Are you sure you want to delete this item?',
								'data-pjax' => 1,
								'data-method' => 'post',
							]
						);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a(Html::tag('span', '',
							['class' => 'glyphicon glyphicon-pencil']),
							$url, [
								'title' => 'Update',
								'aria-label' => 'Update',
								'data-pjax' => 0,
								'class' => 'update-article'
							]
						);
                    }
				]
			],
        ],
    ]); ?>

<?php Pjax::end();?>

</div>
