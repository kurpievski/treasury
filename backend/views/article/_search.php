<?php

use backend\helpers\AppHelper;
use common\models\Tag;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\ArticleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-search">
	<div class="row">
    <?php $form = ActiveForm::begin([
        'action' => [$page],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

	<div class="col-xs-10">
    <?= $form->field($model, 'tags')->widget(Select2::class, [
        'data' => ArrayHelper::map(AppHelper::getCachedModels(Tag::find()), 'id', 'name'),
        'options' => [
            'placeholder' => Yii::t('app', 'Select/add tags ...'),
            'multiple' => true
        ],
        'pluginOptions' => [
            'tags' => true,
            'allowClear' => true,
            'tokenSeparators' => [','],
            'maximumInputLength' => 100,
        ],
        'pluginEvents' => [
            "select2:select" => "function() {
				var data = {
				  tags: $(this).val()
				};
            }"]
    ]) ?>
	</div>

    <div class="form-group browse-submit col-xs-2">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
    </div>
	</div>

    <?php ActiveForm::end(); ?>

</div>
