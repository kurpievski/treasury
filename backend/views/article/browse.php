<?php

use backend\helpers\AppHelper;
use common\helpers\BooleanHelper;
use common\models\Article;
use common\models\Medium;
use common\models\Tag;
use kartik\editable\Editable;
use kartik\grid\SerialColumn;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\caching\DbDependency;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Treasury'

?>
<div class="browse-index blog-field">
    <?php Pjax::begin(); ?>
    <?php echo $this->render('_search', ['model' => $searchModel, 'page' => 'browse']); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
			['class' => SerialColumn::class],
            ['attribute' => 'name',],
            [
				'attribute' => 'medium',
				'value' => 'medium.name',
				'filter' => ArrayHelper::map(AppHelper::getCachedModels(Medium::find()->orderBy(['name' => SORT_ASC])), 'id', 'name'),

			],
			['attribute'=>'abstract'],
//            [
//				'attribute' => 'is_premium',
//				'format' => 'boolean',
//				'filter' => BooleanHelper::getListOptions(),
//			],
            [
                'attribute' => 'source_date',
                'format' => 'date',
				'filterInputOptions' => [
					'autocomplete' => 'off',
					'class' => 'form-control'
				],
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => ([
                    'attribute' => 'only_date',
                    'presetDropdown' => true,
                    'convertFormat' => false,
                    'pluginOptions' => [
                        'separator' => ' - ',
                        'format' => 'YYYY-MM-DD',
                        'locale' => [
                            'format' => 'YYYY-MM-DD'
                        ],
                    ],
                    'pluginEvents' => [
                        "apply.daterangepicker" => "function() { apply_filter('only_date') }",
                    ],
                ]),
            ],
            [
                'attribute' => 'language',
                'filter' => Article::getLanguages(),
            ],
            [
                'attribute' => 'tags',
				'filter' => false,
				'format' => 'raw',
				'value' => function ($model) {
                    /** @var Article $model */
    				return implode(', ', $model->gridTags());
				}
            ],
        ],
    ]); ?>

<?php Pjax::end(); ?>
</div>
