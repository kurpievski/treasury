<?php

use common\models\Article;
use common\models\Medium;
use common\models\Tag;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'language')->dropDownList(Article::getLanguages()) ?>

    <?= $form->field($model, 'medium_id')->widget(Select2::class, [
        'data' => ArrayHelper::map(Medium::find()->select(['id', 'name'])->all(), 'id', 'name'),
        'options' => ['placeholder' => Yii::t('app', 'Select/add a medium ...')],
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [','],
            'maximumInputLength' => 50,
        ],
        'pluginEvents' => [
            "select2:select" => "function() {
				var mediumName = $(this).val();
				var data = {
				  Medium: {name: mediumName}
				};
				$.post('".Url::to(['medium/create'])."', data, function (data) {
					var option = $('#article-medium_id option[value=\"'+mediumName+'\"]');
					if (data.id) {
						option.val(data.id);
					}
				}, 'json');
            }"]
    ]) ?>

    <?= $form->field($model, 'tags')->widget(Select2::class, [
        'data' => ArrayHelper::map(Tag::find()->select(['id', 'name'])->all(), 'id', 'name'),
        'options' => [
			'placeholder' => Yii::t('app', 'Select/add tags ...'),
            'multiple' => true
		],
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [','],
            'maximumInputLength' => 100,
        ],
        'pluginEvents' => [
            "select2:select" => "function() {
				var data = {
				  tags: $(this).val()
				};
				$.post('".Url::to(['tag/create'])."', data, function (data) {
					$.each(data, function(key, element) {
						var option = $('#article-tags option[value=\"'+element.name+'\"]');
						option.val(element.id);	
					});
				}, 'json');
            }"]
    ]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'abstract')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'is_premium')->checkbox() ?>

    <?= $form->field($model, 'source_date')->widget(DatePicker::class, [
        'type' => DatePicker::TYPE_INLINE,
        'pluginOptions' => [
            'format' => ' yyyy-mm-dd'
        ]
    ])
	?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
