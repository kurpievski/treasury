<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $groupedArticles array */

$this->title = Yii::t('app', 'Articles');
$this->params['breadcrumbs'][] = $this->title;

?><!-- wp:paragraph {"backgroundColor":"light-gray"} -->
<p class="has-background has-light-gray-background-color">Wstęp. Grafika pochodzi z  <a href="https://www.facebook.com/Welcome-to-sunny-B%C3%B6genhafen-The-Empire-in-pictures-673560376369502/">Welcome to sunny Bögenhafen: The Empire in pictures</a>.</p>
<!-- /wp:paragraph -->

<?php foreach ($feeds as $title => $feed): ?>
<!-- wp:paragraph {"align":"center","textColor":"white","backgroundColor":"black","fontSize":"medium"} -->
<p style="text-align:center" class="has-text-color has-background has-medium-font-size has-white-color has-black-background-color"><strong><?=$title?></strong></p>
<!-- /wp:paragraph -->

<?php foreach ($feed as $medium => $articles): ?><!-- wp:paragraph {"backgroundColor":"light-gray"} --><p class="has-background has-light-gray-background-color"><strong><?=$medium?></strong><?php $count = count($articles); foreach ($articles as $index => $article): ?><strong><a rel="noreferrer noopener" aria-label="<?=$article->name?> (otwiera się na nowej zakładce)" href="<?=$article->url?>" target="_blank"><br><?=$article->name?></a> [<?= implode(', ', $article->gridTags())?>]</strong><?php if ($article->abstract): ?><br><?=$article->abstract.($index+1 == $count ? '' : '<br>')?><?php endif; endforeach;?></p>
<!-- /wp:paragraph -->

<?php endforeach; ?>
<?php endforeach; ?>