<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Articles feed');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
				'attribute' => 'name',
				'format' => 'raw',
				'contentOptions' => [
					'contenteditable' => 'true',
					'class' => 'name'
				],
			],
			[
				'attribute' => 'language',
				'format' => 'raw',
				'contentOptions' => [
					'class' => 'language'
				],
			],
			[
				'attribute' => 'url',
                'format' => 'html',
				'contentOptions' => [
					'class' => 'url'
				],
                'value' => function ($model) {
                    return Html::a(Yii::t('app', 'link'), $model->url);
                },
			],
            [
				'label' => 'Medium',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::a($model->medium->name, $model->medium->url);
                },
                'contentOptions' => [
                    'class' => 'medium'
                ],

			],
            [
				'attribute' => 'source_date',
                'contentOptions' => [
                    'class' => 'date'
                ],
			],
            [
				'class' => 'yii\grid\ActionColumn',
				'template' => '{include}',
				'buttons' => [
					'include' => function ($url, $model, $key) {
    				return Html::checkbox('include', true, ['class' => 'include']);
				}
				],
			],
        ],
    ]);

    echo Html::button(Yii::t('app', 'Save'), ['id' => 'save']);

    ?>
</div>

<?php
$this->registerJs('
var timestamp = '.$timestamp.';
$("#save").click(function (e) {
	var data = [];
	$(".include:checked").each(function (index, element) {
		var tr = $(element.closest("tr"));
		var mediumElement = tr.find(".medium a");
		
		data.push({
			name: tr.find(".name").text(),
			language: tr.find(".language").text(),
			url: tr.find(".url a").attr("href"),
			medium: mediumElement.text(),
			medium_url: mediumElement.attr("href"),
			date: tr.find(".date").text(),
		});
	});
	
	var formParams = {
		data: data,
		timestamp: timestamp
	};
	
	$.post("", JSON.stringify(formParams), function(response) {
		alert("OK!");
	}, "json");
});
');