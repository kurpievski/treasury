<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="container" style="font-family: monospace; white-space: pre;"><?= \str_replace(["\n"], [""], htmlspecialchars($content)); ?></div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
