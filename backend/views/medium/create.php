<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Medium */

$this->title = Yii::t('app', 'Create Medium');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Media'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="medium-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
