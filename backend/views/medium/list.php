<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $groupedArticles array */

$this->title = Yii::t('app', 'Articles');
$this->params['breadcrumbs'][] = $this->title;

?>
<ul>
<?php foreach ($subscriptions as $subscription => $url): ?>
	<li><a href="<?=$url?>"><?=$subscription?></a></li>
<?php endforeach; ?>
</ul>
