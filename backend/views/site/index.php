<?php

/* @var $this yii\web\View */

use app\helpers\Html;
use backend\helpers\AppHelper;

$this->title = 'Treasury';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Article feed</h1>

        <p><a class="btn btn-lg btn-success" href="/article/feed">Get new articles from Inoreader</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Browse articles</h2>

				<?= AppHelper::generateWeeksLink(AppHelper::WEEK_LAST, 'article/index', ['ArticleSearch[source_date]' => '%s - %s']) ?>
				<?= AppHelper::generateWeeksLink(AppHelper::WEEK_NEXT, 'article/index', ['ArticleSearch[source_date]' => '%s - %s']) ?>

            </div>
            <div class="col-lg-4">
				<h2>Generate list code</h2>

                <?= AppHelper::generateWeeksLink(AppHelper::WEEK_LAST, 'article/list', ['start' => '%s', 'end' => '%s']) ?>
                <?= AppHelper::generateWeeksLink(AppHelper::WEEK_NEXT, 'article/list', ['start' => '%s', 'end' => '%s']) ?>

            </div>
            <div class="col-lg-4">
            </div>
        </div>

    </div>
</div>
