<?php
/**
 * Created by PhpStorm.
 * User: kurpievski
 * Date: 2019-10-25
 * Time: 10:46
 */

namespace backend\components;

use backend\external\Inoreader\Inoreader as InoreaderApi;
use backend\external\InoreaderOauthTokenManager;
use yii\base\Component;
use yii\helpers\Url;

class Inoreader extends Component
{
    /**
     * @var \backend\external\Inoreader\Inoreader
     */
    public $api;
    public $clientId;
    public $clientSecret;

    /**
     * @return InoreaderApi|string
     */
    public function getInstance(string $code = '', string $state = '')
    {
        if (!$this->api) {
            $this->api = new InoreaderApi([
                'clientId' => 999999752,
                'clientSecret' => '426eyBtoModGpN_jJ_t96sGj1uGBOfGo',
                'redirectUri' => Url::canonical(),
            ]);
        }

        $isTokenValid = $this->api->isTokenValid(new InoreaderOauthTokenManager(), $code, $state);

        return $isTokenValid ? $this->api : $this->api->getOAuthUrl();
    }
}