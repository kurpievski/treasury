<?php
namespace app\assets;

use yii\web\AssetBundle;

class UnifyAsset extends AssetBundle
{
    public $sourcePath = '@unify';
    public $css = [
        'vendor/bootstrap/bootstrap.min.css',
        'vendor/icon-awesome/css/font-awesome.min.css',
        'vendor/icon-line/css/simple-line-icons.css',
        'vendor/icon-etlinefont/style.css',
        'vendor/icon-line-pro/style.css',
        'vendor/icon-hs/style.css',
        'vendor/animate.css',
        'vendor/dzsparallaxer/dzsparallaxer.css',
        'vendor/dzsparallaxer/dzsscroller/scroller.css',
        'vendor/dzsparallaxer/advancedscroller/plugin.css',
        'vendor/fancybox/jquery.fancybox.min.css',
        'vendor/slick-carousel/slick/slick.css',
        'vendor/hs-megamenu/src/hs.megamenu.css',
        'vendor/hamburgers/hamburgers.min.css',
        'css/unify-core.css',
        'css/unify-components.css',
        'css/unify-globals.css',
    ];
    public $js = [
        'vendor/jquery-migrate/jquery-migrate.min.js',
        'vendor/popper.min.js',
        'vendor/bootstrap/bootstrap.min.js',
        'vendor/appear.js',
        'vendor/slick-carousel/slick/slick.js',
        'vendor/hs-megamenu/src/hs.megamenu.js',
        'vendor/dzsparallaxer/dzsparallaxer.js',
        'vendor/dzsparallaxer/dzsscroller/scroller.js',
        'vendor/dzsparallaxer/advancedscroller/plugin.js',
        'vendor/fancybox/jquery.fancybox.min.js',
        'js/hs.core.js',
        'js/components/hs.carousel.js',
        'vendor/gmaps/gmaps.js',
        'js/components/gmap/hs.map.js',
        'js/components/hs.header.js',
        'js/helpers/hs.hamburgers.js',
        'js/components/hs.tabs.js',
        'js/components/hs.popup.js',
        'js/components/hs.counter.js',
        'js/components/hs.go-to.js',
    ];
}
