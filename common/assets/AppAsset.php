<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/assets/assets/css/custom.css',
        'https://fonts.googleapis.com/css?family=Roboto',
        'https://fonts.googleapis.com/css?family=Vollkorn',
        'https://fonts.googleapis.com/css?family=Fira+Sans',
    ];
    public $js = [
        '/assets/assets/js/custom.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'app\assets\UnifyAsset',
    ];
}
