<?php

return [

    'class' => 'app\components\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'enableStrictParsing' => false,
    'urlElements' => [
        'site' => Yii::t('app', 'strona'),
        'index' => Yii::t('app', 'index'),
        'contact' => Yii::t('app', 'kontakt'),
        'page' => Yii::t('app', 'stronka'),
    ],
    'rules' => [
        'page/<id:>' => 'site/page',
    ],
    'links' => [
        'main' => [
            'title' => Yii::t('app', 'Strona główna'),
            'href' => [''],
        ],
        'about' => [
            'title' => Yii::t('app','O mnie'),
            'href' => ['page/o-mnie'],
        ],
        'services' => [
            'title' => Yii::t('app','Usługi'),
            'href' => ['page/uslugi'],
        ],
        'salary' => [
            'title' => Yii::t('app','Wynagrodzenie'),
            'href' => ['page/wynagrodzenie'],
        ],
        'contact' => [
            'title' => Yii::t('app','Kontakt'),
            'href' => ['site/kontakt'],
        ],
        'test' => [
            'title' => Yii::t('app','Kontakt'),
            'href' => 'http://www.wp.pl',
        ],
    ],

];
