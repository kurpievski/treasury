<?php

namespace common\models\search;

use common\models\ArticleTag;
use kartik\daterange\DateRangeBehavior;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Article;
use yii\helpers\ArrayHelper;

/**
 * ArticleSearch represents the model behind the search form of `common\models\Article`.
 */
class ArticleSearch extends Article
{
    public $createTimeRange;
    public $createTimeStart;
    public $createTimeEnd;
    public $medium;
    public $tags;

    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::class,
                'attribute' => 'source_date',
                'dateStartAttribute' => 'createTimeStart',
                'dateEndAttribute' => 'createTimeEnd',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'medium_id', 'is_premium', 'created_at', 'updated_at'], 'integer'],
            [['name', 'url', 'abstract', 'source_date', 'language', 'medium', 'tags'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Article::find()->indexBy('id')->joinWith(['tags', 'medium']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['source_date'=>SORT_DESC]],
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        $dataProvider->sort->attributes['medium'] = [
            'asc' => ['medium.name' => SORT_ASC],
            'desc' => ['medium.name' => SORT_DESC],
        ];

        $this->load($params);

        if ($this->tags) {
            $countTags = count($this->tags);
            $articlesIds = ArticleTag::find()
                ->select(['id', 'article_id', 'tag_id'])
                ->where(['tag_id' => $this->tags])->asArray()->all();

            $articlesIds = array_keys(array_filter(
                ArrayHelper::map($articlesIds, 'tag_id', 'tag_id', 'article_id'),
                function ($value) use ($countTags) {
                    return count($value) == $countTags;
                }
            ));

            if ($articlesIds) {
                $query->andWhere(['article.id' => $articlesIds]);
            }
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'medium_id' => $this->medium,
            'is_premium' => $this->is_premium,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $start = $this->createTimeStart ? date('Y-m-d', $this->createTimeStart) : null;
        $stop = $this->createTimeEnd ? date('Y-m-d', $this->createTimeEnd) : null;

        $query->andFilterWhere(['>=', 'source_date', $start])
            ->andFilterWhere(['<=', 'source_date', $stop]);

        $query->andFilterWhere(['like', 'article.name', $this->name])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'abstract', $this->abstract])
            ->andFilterWhere(['like', 'language', $this->language]);

        return $dataProvider;
    }
}
