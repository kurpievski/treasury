<?php

namespace common\models;

use common\extensions\ActiveRecord;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "medium".
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property int $publisher_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Article[] $articles
 * @property Publisher $publisher
 */
class Medium extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'medium';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['publisher_id', 'created_at', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'url'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['publisher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Publisher::class, 'targetAttribute' => ['publisher_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'url' => Yii::t('app', 'Url'),
            'publisher_id' => Yii::t('app', 'Publisher ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::class, ['medium_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisher()
    {
        return $this->hasOne(Publisher::class, ['id' => 'publisher_id']);
    }

    /**
     * @param array $streamContentsItem
     * @return array
     */
    public static function createAttributesFromStreamContentsItem(array $streamContentsItem): array
    {
        return [
            'name' => $streamContentsItem['origin']['title'] ?: '',
            'url' => $streamContentsItem['origin']['htmlUrl'] ?: '',
        ];
    }
}
