<?php

namespace common\models;

use common\extensions\ActiveRecord;
use Helper\Scenario;
use Yii;
use yii\base\InvalidArgumentException;
use yii\behaviors\TimestampBehavior;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property string $name
 * @property int $medium_id
 * @property string $url
 * @property string $abstract
 * @property int $is_premium
 * @property string $source_date
 * @property string $language
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Medium $medium
 */
class Article extends ActiveRecord
{
    const SCENARIO_IMPORT = 'import';

    const FEED_PL = 'PL';
    const FEED_ENG = 'ENG';

    const FEEDS = [
        self::FEED_PL => 'user/1005024023/label/RPG-PL',
        self::FEED_ENG => 'user/1005024023/label/RPG-ENG'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function scenarios()
    {
        return ArrayHelper::merge([
            self::SCENARIO_IMPORT => []
        ], parent::scenarios());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_premium', 'created_at', 'updated_at'], 'integer'],
            [['abstract'], 'string'],
            [['source_date'], 'safe'],
            [['language'], 'safe'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'url'], 'string', 'max' => 255],
            [['medium_id'], 'exist', 'skipOnError' => true, 'targetClass' => Medium::class, 'targetAttribute' => ['medium_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'medium_id' => Yii::t('app', 'Medium ID'),
            'url' => Yii::t('app', 'Url'),
            'abstract' => Yii::t('app', 'Abstract'),
            'is_premium' => Yii::t('app', 'Is Premium'),
            'source_date' => Yii::t('app', 'Source Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'language' => Yii::t('app', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedium()
    {
        return $this->hasOne(Medium::class, ['id' => 'medium_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisher()
    {
        return $this->hasOne(Publisher::class, ['id' => 'publisher_id'])->via('medium');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleTags()
    {
        return $this->hasMany(ArticleTag::class, ['article_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::class, ['id' => 'tag_id'])->via('articleTags');
    }

    public function load($data, $formName = null)
    {
        if (isset($data['Article']['tags'])) {
            $this->populateRelation('tags',
                Tag::find()->where(['id' => $data['Article']['tags']])->all()
            );
        }

        return parent::load($data, $formName);
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->scenario == self::SCENARIO_IMPORT) {
            $db = Yii::$app->getDb();

            $transaction = $db->beginTransaction();
            try {
                $mediumName = $this->medium->name;
                $medium = Medium::findByName($mediumName)->one();
                if (!$medium) {
                    $publisher = Publisher::findByName($mediumName)->one();
                    $medium = $this->medium;

                    if (!$publisher) {
                        $publisher = new Publisher(['name' => $mediumName]);
                        $publisher->save();
                        $medium->publisher_id = $publisher->id;
                    }

                    $medium->save();
                }
                $this->medium_id = $medium->id;

                $result = parent::save($runValidation, $attributeNames);
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                $result = false;
            }
            return $result;
        }

        return parent::save($runValidation, $attributeNames);
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes) {
        $existingTags = $this->getArticleTags()->all();
        $formTags = $this->tags;

        if ($existingTags) {
            $tagsToDelete = \array_udiff($existingTags, $formTags, function ($objA, $objB) {
                $idA = \get_class($objA) == Tag::class ? $objA->id : $objA->tag_id;
                $idB = \get_class($objB) == Tag::class ? $objB->id : $objB->tag_id;

                return $idA - $idB;
            });

            foreach ($tagsToDelete as $tagToDelete) {
                /** @var ArticleTag $tagToDelete */
                $tagToDelete->delete();
            }
        }

        foreach ($this->tags as $tag) {
            if (!in_array($tag->id, ArrayHelper::getColumn($existingTags, 'tag_id'))) {
                $this->link('tags', $tag);
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @param array $streamContents
     * @return ArrayDataProvider
     */
    public static function createDataProviderForStreamContents(array $streamContents)
    {
        $allModels = [];
        foreach ($streamContents['items'] as $streamContentsItem) {
            $model = new Article(static::createAttributesFromStreamContentsItem($streamContentsItem));
            $medium = new Medium(Medium::createAttributesFromStreamContentsItem($streamContentsItem));
            $model->populateRelation('medium', $medium);

            $allModels[] = $model;
        }

        return new ArrayDataProvider([
            'allModels' => $allModels,
            'pagination' => [
                'pageSize' => 80,
            ],
        ]);
    }

    /**
     * @param array $streamContentsItem
     * @return array
     */
    public static function createAttributesFromStreamContentsItem(array $streamContentsItem): array
    {
        $language = '';
        foreach ($streamContentsItem['categories'] as $category) {
            if (strpos($category, 'RPG-ENG')) {
                $language = 'ENG';
            }
            if (strpos($category, 'RPG-PL')) {
                $language = 'PL';
            }
        }

        $url = $streamContentsItem['canonical'][0]['href'] ?: '';

        return [
            'name' => StringHelper::truncateWords($streamContentsItem['title'], 15) ?: '',
            'language' => $language,
            'url' => $url,
            'source_date' => date('Y-m-d', $streamContentsItem['published'] ?: 0),
        ];
    }

    public function gridTags()
    {
        if (!$this->tags) {
            return [];
        }
        return ArrayHelper::map($this->tags, 'id', 'name');
    }

    static public function getLanguages()
    {
        return [
            Article::FEED_PL => Article::FEED_PL,
            Article::FEED_ENG => Article::FEED_ENG
        ];
    }
}
