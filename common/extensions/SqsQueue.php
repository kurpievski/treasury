<?php

namespace common\extensions;

use console\jobs\BaseJob;
use UrbanIndo\Yii2\Queue\Job;
use UrbanIndo\Yii2\Queue\Queues\SqsQueue as BaseSqsQueue;
use Yii;
use yii\di\Instance;

class SqsQueue extends BaseSqsQueue
{
    /**
     * Purge the whole queue.
     * @return boolean
     */
    public function purge()
    {
        $response = $this->getClient()->getQueueAttributes([
            'QueueUrl' => $this->url,
        ]);

        if ($response) {
            while ($job = $this->fetch()) {
                echo "Deleting job {$job->id}. Route: {$job->route}...";
                echo $this->delete($job) ? '[OK]' : '[FAILED]';
                echo PHP_EOL;
            }
            return true;
        }

        return false;
    }

    public function send($jobClass, $data = [])
    {
        if (is_string($jobClass)) {
            $jobClass = ['class' => $jobClass];
        }

        $jobClass['data'] = $data;

        /** @var Job $job */
        $job = Instance::ensure($jobClass, Job::class);
        $this->postJob($job);
    }
}