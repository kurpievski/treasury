<?php

/**
 * EExportMenu.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 28-Jul-15
 * Time: 20:12
 */

namespace common\extensions;

use Box\Spout\Writer\Style\StyleBuilder;
use common\extensions\excelwriter\Writer;
use common\helpers\HJson;
use common\models\ReportProgress;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use Yii;
use ReflectionClass;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\grid\CheckboxColumn;
use yii\grid\Column;
use yii\grid\DataColumn;
use yii\grid\ActionColumn;
use kartik\export\ExportMenu;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;

class EExportMenu extends ExportMenu
{
    const BATCH_SIZE = 2000;

    /** @var int maximum execution time */
    public $timeLimit = 300;

    /** @var array Excel export column config [Excel Letter => PHPExcel_Cell_DataType::TYPE_STRING] */
    public $excelColumnTypeConfig = [];

    /**
     * @var boolean should format numeric cells
     */
    public $formatNumericCells = false;

    /**
     * @var boolean should format currency cells
     */
    public $formatCurrencyCells = true;

    /**
     * @var boolean should force string cells
     */
    public $forceStringCells = false;

    /**
     * @var bool remove the auto filter
     */
    public $autoFilter = false;

    /**
     * @var bool export the footer
     */
    public $exportFooter = true;

    /**
     * @var bool freeze header
     */
    public $freezeHeader = false;

    /**
     * @var bool
     */
    public $useQueue = false;

    /**
     * @var callback to update the percentage
     */
    public $updatePercentage = null;

    /**
     * @var string
     */
    public $gridName = '';

    /**
     * @var array the default style configuration
     */
    protected $_defaultStyleOptions = [
        self::FORMAT_EXCEL => [
            'font' => [
                'bold' => true,
                'color' => [
                    'argb' => '00000000',
                ],
            ],
            'fill' => [
                'type' => 'solid',
                'color' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ],
        self::FORMAT_EXCEL_X => [
            'font' => [
                'bold' => false,
                'color' => [
                    'argb' => '',
                ],
            ],
            'fill' => [
                'type' => 'none',
                'color' => [
                    'argb' => '',
                ],
            ],
        ],
    ];

    protected $reportData = [];

    public $storeFile = false;

    public $defaultConfig;

    /** @var \Box\Spout\Writer\XLSX\Writer */
    public $spoutObject;

    public $spoutConfig = [];

    /**
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        if (!empty($config)) {
            $this->defaultConfig = $config;
        }
        parent::__construct($config);
    }

    public function init()
    {
        if (empty($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
        if (empty($this->exportRequestParam)) {
            $this->exportRequestParam = 'exportFull_' . $this->options['id'];
        }
        $this->_columnSelectorEnabled = $this->showColumnSelector && $this->asDropdown;

        if (Yii::$app->request->isConsoleRequest) {
            $this->_triggerDownload = false;
        } else {
            $this->_triggerDownload = Yii::$app->request->post($this->exportRequestParam, false);
        }

        if (!$this->stream) {
            $this->target = self::TARGET_SELF;
        }
        if ($this->_triggerDownload) {
            if ($this->stream) {
                Yii::$app->controller->layout = false;
            }
            $this->_exportType = $_POST[self::PARAM_EXPORT_TYPE];
            $this->_columnSelectorEnabled = $_POST[self::PARAM_COLSEL_FLAG];
            $this->initSelectedColumns();
        }
        if ($this->dynagrid) {
            $this->_columnSelectorEnabled = false;
            $options = $this->dynagridOptions;
            $options['columns'] = $this->columns;
            $options['storage'] = 'db';
            $options['gridOptions']['dataProvider'] = $this->dataProvider;
            $dynagrid = new DynaGrid($options);
            $this->columns = $dynagrid->getColumns();
        }
        GridView::init();
    }

    /**
     * Initializes PHP Excel Object Instance
     *
     * @return void
     * @throws \Box\Spout\Common\Exception\IOException
     */
    public function initSpout()
    {
        $this->spoutObject = Writer::getWriterInstance($this->spoutConfig);

        $this->folder = trim(Yii::getAlias($this->folder));
        if (!file_exists($this->folder)) {
            $this->folder = Yii::getAlias('@webroot');
        }
        $config = isset($this->exportConfig[$this->_exportType]) ? $this->exportConfig[$this->_exportType] : [];
        $file = self::slash($this->folder) . $this->filename . '.' . $config['extension'];
        $this->spoutObject->openToFile($file);
    }

    /**
     * @inheritdoc
     * @throws \Box\Spout\Common\Exception\IOException
     */
    public function run()
    {
        $this->unsetNoExportColumns();

        if ($this->_triggerDownload) {
            set_time_limit($this->timeLimit);
        }

        if ($this->_triggerDownload && $this->useQueue && $this->getIsExcelRequest()) {
            $this->dispatchQueue();
            $url = Url::to(Yii::$app->request->referrer);
            return Yii::$app->getResponse()->redirect($url, 302);
        } else if ($this->storeFile) {
            $this->storeFile();
        } else {
            parent::run();
        }
    }

    /**
     * This has basically the same as parent::run(), but point to save the file
     *
     * @throws InvalidConfigException
     * @throws \Box\Spout\Common\Exception\IOException
     */
    public function storeFile()
    {
        $this->folder = '@console/runtime';

        $this->initI18N(__DIR__);
        $this->initColumnSelector();
        $this->setVisibleColumns();
        $this->initExport();
        $this->initSpout();
        $this->generateHeader();
        $this->generateBody();
        $this->generateFooter();
        $this->spoutObject->close();

        $config = isset($this->exportConfig[$this->_exportType]) ? $this->exportConfig[$this->_exportType] : [];
        return $this->filename . '.' . $config['extension'];
    }

    /**
     * Dispatch the queue event
     *
     * @return bool
     * @throws InvalidConfigException
     */
    public function dispatchQueue()
    {
        if (empty($this->gridName)) {
            throw new InvalidConfigException("The 'gridName' setting for queue must be setup in 'exportConfig'.");
        }

        $total = $this->getTotalRecords();
        $model = new ReportProgress([
            'serialized_criteria' => HJson::encode($this->defaultConfig),
            'status' => ReportProgress::STATUS_GENERATING,
            'total_trips' => $total,
            'total_generated' => 0,
            'type' => ReportProgress::TYPE_GRID,
            'grid_name' => $this->gridName
        ]);

        if ($model->save()) {
            Yii::$app->amqp->publish('store_report', Json::encode($model->attributes));

            Yii::$app->getSession()->setFlash(
                'info',
                "Your report generation has been put in queue with the name 'Report {$model->id}'."
            );

            return true;
        } else {
            foreach ($model->getErrors() as $attribute => $error) {
                Yii::$app->getSession()->setFlash('error', $error);
            }
        }

        return false;
    }

    private function getTotalRecords()
    {
        $dataProvider = $this->defaultConfig['dataProvider'];
        if ($dataProvider instanceof ActiveDataProvider) {
            /** @var \yii\db\Query $query */
            $query = $dataProvider->query;
            return $query->count();
        } else {
            return $this->dataProvider->getTotalCount();
        }
    }

    protected function unsetNoExportColumns()
    {
        if (!empty($this->noExportColumns)) {
            $colIndexes = is_array($this->noExportColumns) ? $this->noExportColumns : [$this->noExportColumns];
            foreach ($colIndexes as $i => $index) {
                array_splice($this->columns, $index - $i, 1);
            }
        }
    }

    /**
     * Generates an output data row with the given data model and key.
     *
     * @param mixed $model the data model to be rendered
     * @param mixed $key the key associated with the data model
     * @param integer $index the zero-based index of the data model among the model array returned by [[dataProvider]].
     *
     * @return void
     */
    public function generateRow($model, $key, $index)
    {
        if (empty($this->spoutObject)) {
            return parent::generateRow($model, $key, $index);
        }

        $row = [];
        foreach ($this->getVisibleColumns() as $column) {
            $value = $this->getColumnValue($model, $key, $index, $column);
            $row[] = $this->treatValue($value);
        }
        return $row;
    }

    public function treatValue($value)
    {
        $data = html_entity_decode($value);
        $data = str_replace('$', '', $data, $count);
        $data = str_replace(',', '', $data);

        $maxNumericLength = 15;
        if (strlen($data) < $maxNumericLength) {
            if (is_numeric($data)) {
                $data = floatval($data);
            }
        }

        return $data;
    }

    public function getIsExcelRequest()
    {
        if ($this->_exportType === self::FORMAT_EXCEL || $this->_exportType === self::FORMAT_EXCEL_X) {
            return true;
        }

        return false;
    }

    public function getViewPath()
    {
        $class = new ReflectionClass(ExportMenu::className());

        return dirname($class->getFileName()) . DIRECTORY_SEPARATOR . 'views';
    }

    protected function getColumnValue($model, $key, $index, $column)
    {
        if ($column instanceof ActionColumn || $column instanceof CheckboxColumn) {
            return '';
        } else if ($column instanceof DataColumn) {
            if (!$this->enableFormatter || empty($column->format)) {
                return $column->getDataCellValue($model, $key, $index);
            }

            $trimmedValue = trim($column->getDataCellValue($model, $key, $index));
            if (empty($trimmedValue)) {
                return $trimmedValue;
            }
            return $this->formatter->format(
                $column->getDataCellValue($model, $key, $index),
                isset($column->format) ? $column->format : 'raw'
            );
        } else if ($column instanceof Column) {
            return $column->renderDataCell($model, $key, $index);
        } else if ($column instanceof BaseObject) {
            if (!$column->hasProperty('attribute')) {
                return '';
            }

            if (($model instanceof BaseObject) && $model->hasProperty($column->attribute)) {
                return $model->{$column->attribute};
            }

            if (isset($model[$column->attribute])) {
                return $model[$column->attribute];
            }
        }

        return '';
    }

    public function fetchModelsByBatch($query)
    {
        $columns = $this->getVisibleColumns();
        $style = $this->parseStyle([]);

        foreach ($query->batch(static::BATCH_SIZE) as $models) {
            if (count($columns) == 0) {
                $cell = $this->_objPHPExcelSheet->setCellValue('A1', $this->emptyText, true);
                $model = reset($models);
                $this->raiseEvent('onRenderDataCell', [$cell, $this->emptyText, $model, null, 0, $this]);
                return 0;
            }

            /**
             * @var int $index
             * @var \yii\db\ActiveRecord $model
             */
            foreach ($models as $index => $model) {
                $key = $model->getPrimaryKey();
                $rowArray = $this->generateRow($model, $key, $this->_endRow);
                $this->spoutObject->addRowWithStyle($rowArray, $style);
                $this->_endRow++;

                if (!empty($this->updatePercentage) && is_callable($this->updatePercentage)) {
                    call_user_func($this->updatePercentage, $this->_endRow);
                }
            }
        }
        return 0;
    }

    public function fetchModelsByDataProvider()
    {
        $columns = $this->getVisibleColumns();
        $style = $this->parseStyle([]);
        $models = array_values($this->dataProvider->getModels());
        while (count($models) > 0) {
            if (count($columns) == 0) {
                $cell = $this->_objPHPExcelSheet->setCellValue('A1', $this->emptyText, true);
                $model = reset($models);
                $this->raiseEvent('onRenderDataCell', [$cell, $this->emptyText, $model, null, 0, $this]);
                return 0;
            }

            /**
             * @var int $index
             * @var \yii\db\ActiveRecord $model
             */
            foreach ($models as $index => $model) {
                $key = $model->getPrimaryKey();
                $rowArray = $this->generateRow($model, $key, $this->_endRow);
                $this->spoutObject->addRowWithStyle($rowArray, $style);
                $this->_endRow++;

                if (!empty($this->updatePercentage) && is_callable($this->updatePercentage)) {
                    call_user_func($this->updatePercentage, $this->_endRow);
                }
            }

            if ($this->dataProvider->pagination) {
                $this->dataProvider->pagination->page++;
                $this->dataProvider->refresh();
                $models = $this->dataProvider->getModels();
            } else {
                $models = [];
            }
        }
    }

    /**
     * Generates the output data body content.
     *
     * @return int the number of output rows.
     */
    public function generateBody()
    {
        /** ActiveDataProvider $this->dataProvider */
        if ($this->dataProvider instanceof ActiveDataProvider && !empty($this->spoutObject)) {

            $query = $this->dataProvider->query;
            $query->limit = null;
            if ($query instanceof Query) {
                return $this->fetchModelsByBatch($query);
            } else {
                return $this->fetchModelsByDataProvider();
            }
        } else {
            return parent::generateBody();
        }
    }

    /**
     * The default array from ExportMenu is the following. This function changes its to the box/spout format.
     * ~~~php
     * [
     *  'font' => [
     *      'bold' => true,
     *      'color' => [
     *          'argb' => 'FFFFFFFF',
     *      ],
     *  ],
     * ~~~
     *
     * @param $styleArray
     * @return Style
     */
    protected function parseStyle($styleArray = [])
    {
        $style = new StyleBuilder();
        if (!empty($styleArray['font']['bold']) && $styleArray['font']['bold'] === true) {
            $style->setFontBold();
        }
        if (!empty($styleArray['font']['color']['argb'])) {
            $style->setFontColor($styleArray['font']['color']['argb']);
        }
        return $style->build();
    }

    /**
     * Generates the output data header content.
     *
     * @return void
     */
    public function generateHeader()
    {
        if (!empty($this->spoutObject)) {
            $this->generateSpoutHeader();
        } else {
            $this->generatePHPExcelHeader();
        }
    }

    /**
     * generate spout row header
     */
    public function generateSpoutHeader()
    {
        $columns = $this->getVisibleColumns();
        if (count($columns) == 0) {
            return;
        }
        $rowArray = [];
        $style = $this->parseStyle($this->styleOptions);
        if (!empty($this->caption)) {
            $rowsArray[] = [$this->caption];
        }
        foreach ($this->getVisibleColumns() as $column) {
            /** @var Column $column */
            $head = ($column instanceof DataColumn) ? $this->getColumnHeader($column) : $column->header;
            $rowArray[] = $head;
            $this->raiseEvent('onRenderHeaderCell', [null, $head, $this]);
        }
        $this->spoutObject->addRowWithStyle($rowArray, $style);
    }

    /**
     * Cloned from the parent generateHeader method, removing the freeze header logic
     * @throws \PHPExcel_Exception
     */
    public function generatePHPExcelHeader()
    {
        $columns = $this->getVisibleColumns();
        if (count($columns) == 0) {
            return;
        }
        $sheet = $this->_objPHPExcelSheet;
        $style = ArrayHelper::getValue($this->styleOptions, $this->_exportType, []);
        $colFirst = self::columnName(1);
        if (!empty($this->caption)) {
            $sheet->setCellValue($colFirst . $this->_beginRow, $this->caption, true);
            $this->_beginRow += 2;
        }
        $this->_endCol = 0;
        foreach ($this->getVisibleColumns() as $column) {
            $this->_endCol++;
            /**
             * @var Column $column
             */
            $head = ($column instanceof DataColumn) ? $this->getColumnHeader($column) : $column->header;
            $id = self::columnName($this->_endCol) . $this->_beginRow;
            $cell = $sheet->setCellValue($id, $head, true);
            // Apply formatting to header cell
            $sheet->getStyle($id)->applyFromArray($style);
            $this->raiseEvent('onRenderHeaderCell', [$cell, $head, $this]);
        }
        for ($i = $this->_headerBeginRow; $i < ($this->_beginRow - 1); $i++) {
            $sheet->mergeCells($colFirst . $i . ":" . self::columnName($this->_endCol) . $i);
            $sheet->getStyle($colFirst . $i)->applyFromArray($style);
        }
    }

    public function generateFooter()
    {
        if (empty($this->spoutObject)) {
            parent::generateFooter();
            return;
        }

        $style = $this->parseStyle();
        //check if it will export the footer
        if (!$this->exportFooter) {
            return;
        }

        $columns = $this->getVisibleColumns();
        if (count($columns) == 0) {
            return;
        }

        $rowsArray = [];
        foreach ($this->getVisibleColumns() as $n => $column) {
            $rowsArray[] = trim($column->footer) !== '' ? $this->treatValue($column->footer) : '';
        }
        $this->spoutObject->addRowWithStyle($rowsArray, $style);
    }
}