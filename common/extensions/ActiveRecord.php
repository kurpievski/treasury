<?php
/**
 * Created by PhpStorm.
 * User: kurpievski
 * Date: 2019-10-24
 * Time: 23:30
 */

namespace common\extensions;

use yii\db\ActiveQuery;

/**
 * Class ActiveRecord
 * @package common\extensions\ActiveRecord
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    /**
     * @param string $name
     * @return ActiveQuery
     */
    public static function findByName(string $name): ActiveQuery
    {
        return self::find()->where(['name' => $name]);
    }
}