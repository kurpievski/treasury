<?php
/**
 * Created by PhpStorm.
 * User: igor.golub@2amigos.us
 * Date: 08/06/2017
 * Time: 16:47
 */

namespace common\helpers;


class FileHelper extends \yii\helpers\FileHelper
{
    const SIZE_MB = 1048576; //in bytes

    const MIME_TYPE_PDF = 'application/pdf';
    const MIME_TYPE_DOCX = 'application/vnd.openxmlformats-officedocument';
    const MIME_TYPE_OCTET_STREAM = 'application/octet-stream';
    const MIME_TYPE_EXCEL_2003 = 'application/vnd.ms-excel';
    const MIME_TYPE_EXCEL_2007 = 'application/vnd.openxmlformats-officedocument';
    const MIME_TYPE_EXCEL_2010 = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

    public static function sanitizeName($name)
    {
        return preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), $name);
    }

    public static function joinSeparator()
    {
        $path_items = func_get_args();
        $separator = array_shift($path_items);
        $processed_items = [];
        $last_index = count($path_items) - 1;
        foreach ($path_items as $index => $path_item) {
            if (empty($path_item)) {
                continue;
            }

            if ($path_item[0] == $separator && $index > 0 && strlen($path_item) > 1) {
                $path_item = substr($path_item, 1);
            }

            if (empty($path_item)) {
                continue;
            }

            if ($path_item[strlen($path_item) - 1] == $separator && $index != $last_index) {
                $path_item = substr($path_item, 0, strlen($path_item) - 1);
            }

            $processed_items[] = $path_item;
        }

        return implode($separator, $processed_items);
    }

    public static function join()
    {
        $path_items = func_get_args();
        array_unshift($path_items, DIRECTORY_SEPARATOR);
        return forward_static_call_array(static::class . '::joinSeparator', $path_items);
    }
}