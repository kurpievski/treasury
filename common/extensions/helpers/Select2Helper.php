<?php
/**
 * Created by PhpStorm.
 * User: Nikola Simanic <nikola.simanic@2amigos.us>
 * Date: 9/2/2016
 * Time: 4:28 PM
 */

namespace common\helpers;

use common\models\Company;
use common\models\Facility;
use common\models\FrequentNote;
use common\models\Group;
use common\models\Payor;
use frontend\modules\cac_los\models\Emt;
use frontend\modules\cac_los\models\State;
use kartik\select2\Select2;
use Yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\db\Expression;

class Select2Helper
{
    /**
     * Configuration - Select2 configuration + additional configuration:
     * controllerId - ID of the controller which will be used for AJAX requests
     *
     * @param array $overrideConfig
     * @return array Select2 Config
     */
    public static function getDefaultConfig($overrideConfig = [])
    {
        $controllerId = static::popOption($overrideConfig, 'controllerId');
        $actionId = static::popOption($overrideConfig, 'actionId', 'autocomplete');
        $placeholder = static::popOption($overrideConfig, 'placeholder', '');

        $excludeIdsReadTarget = Json::encode(static::popOption($overrideConfig, 'excludeIdsReadTarget', ''));
        $additionalParams = Json::encode(static::popOption($overrideConfig, 'additionalParams', ''));
        $requestProcessors = Json::encode(static::popOption($overrideConfig, 'requestProcessors', []));

        $config = [
            'pluginLoading' => false,
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => [
                'placeholder' => $placeholder
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 1,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Loading...'; }")
                ],
                'ajax' => [
                    'url' => Url::to("/{$controllerId}/{$actionId}"),
                    'dataType' => 'json',
                    'delay' => 500,
                    'data' => new JsExpression("function(params) { 
                        
                        var readTarget = $({$excludeIdsReadTarget});
                        
                        var excludeIds = [];
                        
                        readTarget.each(function(index, element) {
                            var value = $(element).val();
                            
                            if (value.length > 0) {
                                excludeIds.push(value);
                            }
                        });
                        
                        var processors = {$requestProcessors};
                        var extendParams = {};
                        
                        processors.forEach(function(processor) {
                            processor(extendParams, params.term, params);
                        });
                        
                        var additionalParams = {$additionalParams};
                        
                        return $.extend({ 
                            query: params.term,
                            except: excludeIds.join(',')
                        }, extendParams, additionalParams);
                    }"),
                ]
            ]
        ];

        return ArrayHelper::merge($config, $overrideConfig);
    }

    protected static function popOption(&$config, $option, $default = null)
    {
        if (!isset($config[$option])) {
            return $default;
        }

        $poppedOption = $config[$option];
        unset($config[$option]);
        return $poppedOption;
    }

    public static function getFacilityConfig($model, $options = [])
    {
        $attribute = empty($options['attribute']) ? 'facility_id' : $options['attribute'];
        $initialValue = !empty($model->{$attribute}) ? ArrayHelper::getValue(Facility::findById($model->{$attribute}), 'name', '') : '';

        JsHelper::registerTemplate('facility_select2_expanded', Yii::$app->view->render('//jstemplates/facility_select2_expanded'));

        return static::getDefaultConfig(ArrayHelper::merge([
            'model' => $model,
            'attribute' => $attribute,
            'controllerId' => 'facility',
            'initValueText' => Html::encode($initialValue),
            'pluginOptions' => [
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function (result) {
                              if (result.loading) {
                                    return "<i class=\"icon ion-load-c ion-spin\"></i> Loading...";
                              }
                              return ems.template.renderCached("facility_select2_expanded", result);
                        }'),
            ]
        ], $options));
    }

    public static function getPayorConfig($model, $options = [])
    {
        $attribute = empty($options['attribute']) ? 'payor_id' : $options['attribute'];

        $initialValue = !empty($model->{$attribute}) ? Payor::getNameById($model->{$attribute}) : '';

        JsHelper::registerTemplate('payor_select2_expanded', Yii::$app->view->render('//jstemplates/payor_select2_expanded'));

        return static::getDefaultConfig(ArrayHelper::merge([
            'model' => $model,
            'attribute' => $attribute,
            'controllerId' => 'payor',
            'initValueText' => $initialValue,
            'pluginOptions' => [
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function (result) {
                              if (result.loading) {
                                    return "<i class=\"icon ion-load-c ion-spin\"></i> Loading...";
                              }
                              return ems.template.renderCached("payor_select2_expanded", result);
                        }'),
            ]
        ], $options));
    }

    public static function getPayorCodeConfig($model, $options = [])
    {
        $attribute = empty($options['attribute']) ? 'payor_id' : $options['attribute'];

        return static::getDefaultConfig(ArrayHelper::merge([
            'model' => $model,
            'attribute' => $attribute,
            'controllerId' => 'emsurance',
        ], $options));
    }

    public static function getCustomerFieldConfig($model, $options = [])
    {
        $attribute = empty($options['attribute']) ? 'name' : $options['attribute'];
        $actionId = empty($options['actionId']) ? 'autocomplete' : $options['actionId'];

        return static::getDefaultConfig(ArrayHelper::merge([
            'model' => $model,
            'attribute' => $attribute,
            'controllerId' => 'customer',
            'actionId' => $actionId,
        ], $options));
    }

    public static function getStateConfig($model, $options = [])
    {
        $attribute = empty($options['attribute']) ? 'state_id' : $options['attribute'];

        $initialValue = ArrayHelper::getValue(State::findOne(['id' => $model->{$attribute}]), 'name', '');

        return static::getDefaultConfig(ArrayHelper::merge([
            'model' => $model,
            'attribute' => $attribute,
            'controllerId' => 'cac_los/state',
            'initValueText' => $initialValue,
        ], $options));
    }

    public static function getEmtConfig($model, $options = [])
    {
        $attribute = empty($options['attribute']) ? 'emt_id' : $options['attribute'];

        $initialValue = ArrayHelper::getValue(Emt::findOne(['id' => $model->{$attribute}]), 'code', '');

        return static::getDefaultConfig(ArrayHelper::merge([
            'model' => $model,
            'attribute' => $attribute,
            'controllerId' => 'cac_los/emt',
            'initValueText' => $initialValue,
        ], $options));
    }

    public static function getUserConfig($model, $options = [])
    {
        $attribute = empty($options['attribute']) ? 'name' : $options['attribute'];
        $actionId = empty($options['actionId']) ? 'autocomplete' : $options['actionId'];

        return static::getDefaultConfig(ArrayHelper::merge([
            'model' => $model,
            'attribute' => $attribute,
            'controllerId' => 'user',
            'actionId' => $actionId,
        ], $options));
    }

    public static function getCompanyConfig($model, $options = [])
    {
        $attribute = empty($options['attribute']) ? 'company_id' : $options['attribute'];

        $initialValue = !empty($model->{$attribute}) ? Company::getNameById($model->{$attribute}) : '';

        return static::getDefaultConfig(ArrayHelper::merge([
            'model' => $model,
            'attribute' => $attribute,
            'controllerId' => 'company',
            'initValueText' => $initialValue !== false ? $initialValue : '',
        ], $options));
    }

    public static function getMultipleGroupConfig($model, $options = [])
    {
        $attribute = empty($options['attribute']) ? 'group_ids' : $options['attribute'];

        $data = !empty($model->{$attribute}) ? Group::find()
            ->select(['id', 'text' => 'description'])
            ->where([Group::tableName() . '.id' => $model->{$attribute}])
            ->asArray()
            ->cachedList('id', 'text') : [];

        return static::getDefaultConfig(ArrayHelper::merge([
            'model' => $model,
            'attribute' => $attribute,
            'controllerId' => 'group',
            'data' => $data
        ], $options));
    }

    public static function getFrequentNoteConfig($model, $options = [])
    {
        $attribute = empty($options['attribute']) ? 'frequent_note_id' : $options['attribute'];

        $initialValue = ArrayHelper::getValue(FrequentNote::findOne(['id' => $model->{$attribute}]), 'name', '');

        return static::getDefaultConfig(ArrayHelper::merge([
            'model' => $model,
            'attribute' => $attribute,
            'controllerId' => 'frequent-note',
            'initValueText' => $initialValue,
        ], $options));
    }
}