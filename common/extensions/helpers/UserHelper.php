<?php
/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 **/

namespace common\helpers;


use common\models\User;

class UserHelper
{
    public static function getUserNames($ids)
    {
        return User::find()
            ->select(['first_name', 'last_name', 'id'])
            ->where(['id' => array_unique($ids)])
            ->asArray()
            ->indexBy('id')
            ->cachedAll();
    }

    public static function namesMatch($name1, $name2)
    {
        $numberOfMatches = 0;
        $parts1 = static::getNameParts($name1);
        $parts2 = static::getNameParts($name2);

        foreach ($parts1 as $name1Part) {
            if (in_array($name1Part, $parts2)) {
                $numberOfMatches++;
            }
        }

        $oneNameIsOnlyLastName = (count($parts1) == 1) || (count($parts2) == 1);

        // At least two name parts matched ... so we assume it is safe to say they are close enough
        return $oneNameIsOnlyLastName ? $numberOfMatches >= 1 : $numberOfMatches >= 2;
    }

    public static function getNameParts($name)
    {
        $result = [];
        $nameParts = array_map('trim', explode(',', strtolower($name)));
        foreach ($nameParts as $namePart) {
            $parts = array_map('trim', explode(' ', strtolower($namePart)));
            foreach ($parts as $part) {
                if (!empty($part)) {
                    $result[] = $part;
                }
            }
        }

        return $result;
    }
}