<?php
/**
 * Created by PhpStorm.
 * User: Nikola Radovic <nikola@2amigos.us>
 * Date: 8/25/2016
 * Time: 9:37 PM
 */

namespace common\helpers;


class ArrayHelper extends \yii\helpers\ArrayHelper
{
    public static function iterate($array, $matchCallback)
    {
        foreach ($array as $i => $item) {
            if ($matchCallback($item)) {
                return true;
            }

            if (is_array($item) && static::iterate($item, $matchCallback)) {
                return true;
            }
        }

        return false;
    }

    public static function removeByValue(&$array, $val)
    {
        foreach ($array as $i => $item) {
            if ($item == $val) {
                array_splice($array, $i, 1);
            }
        }
    }

    public static function uniqueCaseInsensitive($values)
    {
        return array_intersect_key(
            $values,
            array_unique(array_map("strtolower", $values))
        );
    }

    public static function flatten($multiArray, $skipSameValues = false)
    {
        $flattenedArray = [];
        $recursiveIterator = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($multiArray));

        foreach ($recursiveIterator as $value) {
            if ($skipSameValues && in_array($value, $flattenedArray)) {
                continue;
            }

            $flattenedArray[] = $value;
        }

        return $flattenedArray;
    }

    public static function arraysMatch($arrayA, $arrayB, $matchKeys = true, $exact = false)
    {
        if ($matchKeys && $exact) {
            return $arrayA === $arrayB;
        }

        $expectedIterator = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($arrayA));
        $realIterator = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($arrayB));

        foreach ($expectedIterator as $expectedKey => $expectedItem) {
            $realIterator->next();
            if ($matchKeys) {
                if ($exact && $expectedKey !== $realIterator->key()) {
                    return false;
                } else if ($expectedKey != $realIterator->key()) {
                    return false;
                }
            }

            if ($exact && $expectedItem !== $realIterator->current()) {
                return false;
            } else if ($expectedItem != $realIterator->current()) {
                return false;
            }
        }

        return true;
    }

    public static function fullDiff($array1, $array2)
    {
        return array_merge(array_diff($array1, $array2), array_diff($array2, $array1));
    }

    public static function filterArrayByArray(&$inputArray, &$byArray, $filterKey = null)
    {
        foreach ($inputArray as $key => $inputItem) {
            $searchValue = $filterKey === null ? $inputItem : static::getValue($inputItem, $filterKey);
            $position = array_search($searchValue, $byArray);

            if ($position !== false) {
                unset($inputArray[$key]);
            }
        }
    }

    public static function addRowSumsAtEnd(&$rows, $sumKey = 'total')
    {
        foreach ($rows as $key => $row) {
            $rows[$key][$sumKey] = array_sum($row);
        }
    }

    public static function groupByColumn($array, $column)
    {
        $result = [];

        foreach ($array as $item) {
            $key = $item[$column];

            unset($item[$column]);

            if (!isset($result[$key])) {
                $result[$key] = [];
            }

            $result[$key][] = $item;
        }
        return $result;
    }

    public static function addValueToKey(&$array, $value, $key)
    {
        if (isset($array[$key])) {
            $array[$key][] = $value;
        } else {
            $array[$key] = [$value];
        }
    }

    public static function pop(array &$array, $key = null)
    {
        if (empty($key)) {
            return array_pop($array);
        }

        $value = self::getValue($array, $key);
        self::remove($array, $key);

        return $value;
    }

    public static function getValue($array, $key, $default = null)
    {
        if ($key instanceof \Closure) {
            return $key($array, $default);
        }

        if (is_array($key)) {
            $lastKey = array_pop($key);
            foreach ($key as $keyPart) {
                $array = static::getValue($array, $keyPart);
            }
            $key = $lastKey;
        }

        if (is_array($array) && (isset($array[$key]) || array_key_exists($key, $array))) {
            return $array[$key];
        }

        if (($pos = strrpos($key, '.')) !== false) {
            $array = static::getValue($array, substr($key, 0, $pos), $default);
            $key = substr($key, $pos + 1);
        }

        if (is_object($array)) {
            if ($array->$key === 0 || $array->$key === '0') {
                return $array->$key;
            }
            return $array->$key ? $array->$key : $default;
        } elseif (is_array($array)) {
            return (isset($array[$key]) || array_key_exists($key, $array)) ? $array[$key] : $default;
        }

        return $default;
    }
}
