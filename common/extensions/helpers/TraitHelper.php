<?php

namespace common\helpers;

class TraitHelper
{
    public static function isTraitPresent($classInstance, $traitName)
    {
        $traits = class_uses($classInstance);

        $parentClass = get_parent_class($classInstance);

        if ($parentClass !== false) {
            $traits = array_merge($traits, class_uses($parentClass));
        }

        foreach($traits as $trait) {
            if ((new \ReflectionClass($trait))->getShortName() == $traitName) {
                return true;
            }
        }

        return false;
    }
}