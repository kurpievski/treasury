<?php
/**
 * Created by PhpStorm.
 * Nikola Radovic <nikola@2amigos.us>
 * Date: 22.03.2017.
 * Time: 20:35
 */

namespace common\helpers;

use yii\helpers\Html;

class IconHelper
{
    const ICON_FOLDER = 'folder-o';
    const ICON_FILE = 'file-o';
    const ICON_IMAGE = 'file-image-o';
    const ICON_PDF = 'file-pdf-o';
    const ICON_VIDEO = 'file-video-o';
    const ICON_EXCEL = 'file-excel-o';
    const ICON_AUDIO = 'file-audio-o';
    const ICON_WORD = 'file-word-o';
    const ICON_POWER_POINT = 'file-powerpoint-o';
    const ICON_COMPRESSED = 'file-archive-o';
    const ICON_CAMPAIGN = 'bullhorn';

    const ICON_DEFAULT = 'file-o';

    protected static $iconMap = [
        'file' => self::ICON_FILE,
        'token' => self::ICON_FILE,
        'folder' => self::ICON_FOLDER,
        'campaign' => self::ICON_CAMPAIGN,
        'image/jpeg' => self::ICON_IMAGE,
        'image/png' => self::ICON_IMAGE,
        'image/tiff' => self::ICON_IMAGE,
        'image/bmp' => self::ICON_IMAGE,
        'image/gif' => self::ICON_IMAGE,
        'application/msword' => self::ICON_WORD,
        'application/vnd.oasis.opendocument.text' => self::ICON_WORD,
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => self::ICON_WORD,
        'application/vnd.openxmlformats-officedocument.presentationml.slideshow' => self::ICON_POWER_POINT,
        'audio/mpeg3' => self::ICON_AUDIO,
        "audio/ogg" => self::ICON_AUDIO,
        "audio/x-wav" => self::ICON_AUDIO,
        "application/ogg" => self::ICON_AUDIO,
        "application/mp4" => self::ICON_AUDIO,
        'video/avi' => self::ICON_VIDEO,
        'video/ogg' => self::ICON_VIDEO,
        'video/mp4' => self::ICON_VIDEO,
        'video/x-msvideo' => self::ICON_VIDEO,
        'video/x-flv' => self::ICON_VIDEO,
        'application/x-tar' => self::ICON_COMPRESSED,
        'application/x-rar-compressed' => self::ICON_COMPRESSED,
        'application/zip' => self::ICON_COMPRESSED,
        'application/x-7z-compressed' => self::ICON_COMPRESSED,
        FileHelper::MIME_TYPE_EXCEL_2003 => self::ICON_EXCEL,
        FileHelper::MIME_TYPE_EXCEL_2007 => self::ICON_EXCEL,
        FileHelper::MIME_TYPE_EXCEL_2010 => self::ICON_EXCEL,
        'text/plain' => self::ICON_FILE,
        'application/pdf' => self::ICON_PDF,
    ];

    public static function getFaByMimeType($mimeType)
    {
        $iconName = ArrayHelper::getValue(self::$iconMap, $mimeType, self::ICON_DEFAULT);

        return "fa fa-{$iconName}";
    }

    public static function getByMimeType($mimeType, array $options = [])
    {
        Html::addCssClass($options, static::getFaByMimeType($mimeType));

        return Html::tag('i', ' ', $options);
    }

    public static function getIconTemplateMap()
    {
        $templateMap = [];

        foreach (static::$iconMap as $iconName => &$iconIdentifier) {
            $templateMap[$iconName] = static::getIconTemplateFromName($iconIdentifier);
        }

        return $templateMap;
    }

    protected static function getIconTemplateFromName($name)
    {
        return 'fa fa-' . $name;
    }

    public static function getColorClassTemplateMap()
    {
        return [
            'file' => 'noti-primary',
            'folder' => 'noti-warning',
            'token' => 'noti-custom',
            'campaign' => 'noti-success',
        ];
    }
}