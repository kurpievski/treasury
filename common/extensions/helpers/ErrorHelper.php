<?php
/**
 * ErrorHelper.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 20-Oct-15
 * Time: 18:19
 */

namespace common\helpers;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\helpers\Html;

class ErrorHelper
{
    public static function setErrorFlash($message)
    {
        Yii::$app->getSession()->setFlash('error', $message);
    }

    public static function setSuccessFlash($message)
    {
        Yii::$app->getSession()->setFlash('success', $message);
    }

    public static function setWarningFlash($message)
    {
        Yii::$app->getSession()->setFlash('warning', $message);
    }

    public static function getErrorSummary(Model $model)
    {
        if (!$model->hasErrors()) {
            return "";
        }

        $errorList = $model->getErrors();

        $errorHtml = "";

        foreach($errorList as $attribute => $errorArray) {

            $childModel = $model->hasProperty($attribute) ?  $model->{$attribute} : $attribute;

            $errorItems = Html::tag('div', Html::tag('strong', $model->getAttributeLabel($attribute))) .
                Html::beginTag('ul');

            if (!($childModel instanceof Model)) {
                foreach($errorArray as $error) {
                    $errorItems .= Html::tag('li', $error);
                }

                $errorHtml .= $errorItems . Html::endTag('ul');
                continue;
            }

            foreach($errorArray as $innerErrors) {

                if (is_string($innerErrors)) {
                    $errorItems .= Html::tag('li', $innerErrors);
                    continue;
                }

                foreach($innerErrors as $childAttribute => $errors) {
                    $errorItems .= Html::beginTag('li');

                    $shouldGetAttributeLabel =
                        $childModel->hasProperty($childAttribute) ||
                        ($childModel instanceof ActiveRecord && $childModel->hasAttribute($childAttribute));

                    if ($shouldGetAttributeLabel) {
                        $errorItems .= $childModel->getAttributeLabel($childAttribute) . ":";
                    } else {
                        $errorItems .= !empty($childAttribute) ? $childAttribute . ':' : '';
                    }

                    $errorItems .= Html::beginTag('ul');

                    foreach($errors as $error) {
                        $errorItems .= Html::tag('li', $error);
                    }

                    $errorItems .= Html::endTag('ul') . Html::endTag('li');
                }
            }

            $errorHtml .= $errorItems . Html::endTag('ul');
        }

        return $errorHtml;
    }
}