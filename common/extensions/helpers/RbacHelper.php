<?php
/**
 * Author: Srdjan Drakul
 * Date: 1/22/2016 4:27 PM
 * Email: srdjan_drakul@yahoo.com
 */

namespace common\helpers;

use common\models\Attribute;
use common\models\Document;

class RbacHelper
{
    const ROLE_SUPERADMIN = 'superadmin';

    const MASTER_EKONA_ROLE = "ekona__master_role";
    const MASTER_EKONA_PERMISSIONS = 'ekona__master_permissions';
    const MASTER_FRONTEND_EKONA_PERMISSIONS = 'ekona__master_frontend_permission';
    const MASTER_BACKEND_EKONA_PERMISSIONS = 'ekona__master_backend_permission';

    const ROLE_CLIENT_PARENT = 'ekona__client_parent';

    const PERMISSION_ADD_EDIT_DELETE_CLIENTS = 'ekona__add_edit_delete_clients';
    const PERMISSION_ADD_PAGES = 'ekona__add_pages';
    const PERMISSION_ANNOTATE_DOCUMENTS = 'ekona__annotate_documents';
    const PERMISSION_UPDATE_DOCUMENT_PAGES = 'ekona__update_document_pages';
    const PERMISSION_INDEX_DATA_ELEMENTS_CRUD = 'ekona__a_e_d_index_data_elements';
    const PERMISSION_INDEX_VALUES_CRUD = 'ekona__a_e_d_index_values';
    const PERMISSION_USERS_CRUD = 'ekona__a_e_d_users';
    const PERMISSION_WORKFLOWS_CRUD = 'ekona__a_e_d_workflows';
    const PERMISSION_COPY_PASTE_DOCUMENTS = 'ekona__copy_paste_documents_into_another_document_type';
    const PERMISSION_DELETE_DOCUMENTS = 'ekona__delete_documents';
    const PERMISSION_EDIT_OR_BACKDATE_WORK_DATE = 'ekona__edit_or_backdate_work_date';
    const PERMISSION_IMPORT_DOCUMENTS = 'ekona__import_documents';
    const PERMISSION_INDEX_DOCUMENTS = 'ekona__index_documents';
    const PERMISSION_MOVE_DOCUMENTS = 'ekona__move_documents';
    const PERMISSION_PRINT_EXPORT_DOCUMENTS = 'ekona__print_export_documents';
    const PERMISSION_REINDEX_DOCUMENTS = 'ekona__reindex_documents';
    const PERMISSION_REMOVE_ITEMS_FROM_RECYCLE_BIN = 'ekona__remove_items_form_recycle_bin';
    const PERMISSION_REORDER_DOCUMENTS = 'ekona__reorder_documents';
    const PERMISSION_REPORTS = 'ekona__reports';
    const PERMISSION_SCAN_DOCUMENTS = 'ekona__scan_documents';
    const PERMISSION_SEARCH_DOCUMENTS = 'ekona__search_documents';
    const PERMISSION_VIEW_AUDIT_LOGS = 'ekona__view_audit_logs';
    const PERMISSION_VIEW_DOCUMENTS = 'ekona__view_document';
    const PERMISSION_VIEW_ITEMS_IN_RECYCLE_BIN = 'ekona__view_items_in_the_recycle_bin';
    const PERMISSION_DELETE_MULTIPLE_DOCUMENTS = 'ekona__delete_multiple_documents';
    const PERMISSION_ACCESS_REMOTE_INDEXER = 'ekona__access_remote_indexer';
    const PERMISSION_WORKBOOK_REPORT = 'ekona__access_workbook_report';
    const PERMISSION_SAVED_SEARCHES = 'ekona__saved_searches';
    const PERMISSION_TRANSMITTAL_REPORT = 'ekona__transmittal_report';
    const PERMISSION_PACKET_REPORT = 'ekona__packet_report';
    const PERMISSION_SEARCH_EXCEPTIONS = 'ekona__search_exceptions';
    const PERMISSION_VIEW_BATCH_NAME_COLUMN = 'ekona__view_batch_name_column';
    const PERMISSION_VIEW_CREATED_ON_COLUMN = 'ekona__view_created_on_column';
    const PERMISSION_VIEW_DOCUMENT_TYPE_TREE = 'ekona__view_document_type_tree';

    const PERMISSION_VIEW_AND_EDIT_NOTES = 'ekona__view_and_edit_notes';
    const PERMISSION_VIEW_INDEXER = 'ekona__view_indexer';

    const PERMISSION_CAN_ACCESS_BACKEND = 'ekona__can_access_backend_permission';

    const PERMISSION_EDIT_ENTRY_DATE = 'ekona__update_entry-date_attribute';
    const PERMISSION_EDIT_DOCUMENT_STATUS = 'ekona__update_status_attribute';
    const PERMISSION_VIEW_DOCUMENT_STATUS = 'ekona__view_status_attribute';

    const RBAC_PERMISSIONS_SECTION = 'permissions';
    const RBAC_FEATURE_SECTION = 'feature';
    const RBAC_DOC_TYPE_PERMISSION_SECTION = 'document_type';
    const RBAC_DOC_TYPE_PERMISSION_PREFIX = 'ekona__document_type_';

    public static function getAttributePermission($attributeKey)
    {
        return "ekona__update_" . str_ireplace('-', '_', $attributeKey) . "_attribute";
    }

    public static function getPermissionFromDocumentTypeKey($key)
    {
        return !empty($key) ? static::RBAC_DOC_TYPE_PERMISSION_PREFIX . $key : null;
    }

    public static function getDocumentTypeKeyFromPermission($permission)
    {
        return StringHelper::replaceFirst(static::RBAC_DOC_TYPE_PERMISSION_PREFIX, '', $permission);
    }

    public static function getAttributeViewPermission($attributeKey)
    {
        return "ekona__view_" . str_ireplace('-', '_', $attributeKey) . "_attribute";
    }


    public static function isRBACAttribute($attributeKey)
    {
        static $rbacAttributes = [
            Attribute::KEY_STATUS
        ];

        return in_array($attributeKey, $rbacAttributes);
    }
}