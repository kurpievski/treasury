<?php

namespace common\helpers;

class RowHelper
{
    const DELETED_ROW = 1;
    const NOT_DELETED_ROW = 0;
}