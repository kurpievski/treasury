<?php
/**
 * Created by PhpStorm.
 * User: Nikola Simanic
 * Date: 9/21/2016
 * Time: 10:09 PM
 */
namespace common\helpers;

class FlashHelper
{
    public static function setSuccess($message)
    {
        self::setFlashMessage('success', $message);
    }

    public static function setError($message)
    {
        self::setFlashMessage('error', $message);
    }

    public static function removeFlash()
    {
        \Yii::$app->getSession()->removeAllFlashes();
    }

    /**
     * show errors directly from model
     * @param $model
     */
    public static function setErrorsFlashes($model)
    {
        if (!empty($model->errors)) {
            foreach ($model->errors as $key => $modelStatements) {
                if (!is_array($modelStatements)) {
                    continue;
                }
                foreach ($modelStatements as $message) {
                    \Yii::$app->getSession()->addFlash('error', $message);
                }
            }
        }
    }

    protected static function setFlashMessage($type, $message)
    {
        \Yii::$app->getSession()->addFlash($type, $message);
    }
}