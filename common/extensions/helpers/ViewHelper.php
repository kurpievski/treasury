<?php
/**
 * Stanislav Uloznik <hola@2amigos.us>
 * Company: 2amigOS! <https://2amigos.us>.
 */

namespace common\helpers;

use emspire\models\EmspirePaymentSearch;
use emspire\models\Registration;
use Yii;
use yii\helpers\Html;

class ViewHelper
{
    const CSS_CLASS_SUCCESS = 'success';
    const CSS_CLASS_DANGER = 'danger';
    const CSS_CLASS_WARNING = 'warning';
    const CSS_CLASS_DEFAULT = 'default';
    const CSS_CLASS_PRIMARY = 'primary';

    public static function getPaymentStatusLabel($status)
    {
        $paymentStatusLabelClasses = [
            EmspirePaymentSearch::STATUS_ACCEPTED => self::CSS_CLASS_SUCCESS,
            EmspirePaymentSearch::STATUS_REJECTED => self::CSS_CLASS_DANGER,
            EmspirePaymentSearch::STATUS_CANCELLED => self::CSS_CLASS_DANGER,
            EmspirePaymentSearch::STATUS_FLAGGED_PENDING => self::CSS_CLASS_WARNING,
            EmspirePaymentSearch::STATUS_PENDING => self::CSS_CLASS_DEFAULT
        ];

        return Html::tag('span', $status, ['class' => "label label-{$paymentStatusLabelClasses[$status]}"]);
    }

    public static function getRegistrationStatusLabel($status)
    {
        $registrationStatusLabelClasses = [
            Registration::STATUS_NEW => self::CSS_CLASS_DANGER,
            Registration::STATUS_REJECTED_PAYMENT => self::CSS_CLASS_DANGER,
            Registration::STATUS_PENDING_PAYMENT => self::CSS_CLASS_DEFAULT,
            Registration::STATUS_PAID => self::CSS_CLASS_SUCCESS
        ];

        return Html::tag('span', $status, ['class' => "label label-{$registrationStatusLabelClasses[$status]}"]);
    }

}
