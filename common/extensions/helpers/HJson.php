<?php
/**
 * Created by PhpStorm.
 * Author: Eric Maicon <eric@2amigos.us>
 * Date: 29/01/16
 * Time: 15:16
 */

namespace common\helpers;

use SuperClosure\Serializer;
use \yii\helpers\Json;

class HJson extends Json
{
    public static function encode($value, $options = 320)
    {
        $value = static::processJson($value);
        return json_encode($value, $options);
    }

    /**
     * @inheritdoc
     */
    protected static function processJson($data)
    {
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                if (is_array($value) || is_object($value)) {
                    $data[$key] = static::processJson($value);
                }
            }
        }

        if (is_callable($data)) {
            $serializer = new Serializer();
            $data = $serializer->serialize($data);
        }

        if (is_object($data)) {
            foreach (get_object_vars($data) as $key => $value) {
                $data->$key = static::processJson($value);
            }

            $data = serialize($data);
        }

        return $data;
    }

    /**
     * @inheritdoc
     */
    public static function decode($json, $asArray = true)
    {
        $decode = json_decode((string) $json, $asArray);

        return static::unprocessJson($decode);
    }

    /**
     * @param $data
     * @return array|callable|mixed
     */
    protected static function unprocessJson($data)
    {
        if (is_string($data) && is_numeric(strpos($data, ':27:'))) {
            $data = unserialize($data);
            foreach (get_object_vars($data) as $key => $value) {
                $data->$key = static::unprocessJson($value);
            }
        }

        if (is_string($data) && is_numeric(strpos($data, 'C:32:"SuperClosure\SerializableClosure"'))) {
            $serializer = new Serializer();
            $data = $serializer->unserialize($data);
        }

        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $data[$key] = static::unprocessJson($value);
            }
        }

        return $data;
    }
}