<?php
/**
 * Created by PhpStorm.
 * User: ericmaicon
 * Date: 09/02/16
 * Time: 09:25
 */

namespace common\helpers;

class EnvHelper
{
    /**
     * @return string
     */
    public static function getEnvPrefix()
    {
        return defined('YII_ENV') && YII_ENV !== 'prod' ? YII_ENV . '__' : '';
    }

    public static function getIsProd()
    {
        return defined('YII_ENV') && YII_ENV === 'prod';
    }

    public static function getIsLocal()
    {
        return defined('YII_ENV') && YII_ENV === 'local';
    }

    public static function getTokenKey()
    {
        return YII_ENV . '_token';
    }

    public static function getEnvName()
    {
        return !self::getIsProd() ? ('[' . ucfirst(YII_ENV) . ']') : '';
    }

    public static function getIsMenuExpandedTokenKey()
    {
        return YII_ENV . '_isMenuExpanded';
    }

    public static function getActiveTripTabTokenKey()
    {
        return YII_ENV . '_tripTab';
    }

    public static function getLabel()
    {
        if (!defined('YII_ENV')) {
            return '--';
        }

        return ucfirst(YII_ENV);
    }
}