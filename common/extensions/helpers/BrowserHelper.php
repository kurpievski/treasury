<?php
/**
 * BrowserHelper.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 14-Jul-15
 * Time: 20:54
 */

namespace common\helpers;

use Yii;

class BrowserHelper
{
    public static function isInternetExplorer()
    {
        return self::isIE10OrLower() || self::isIE11() || self::isIE12();
    }

    public static function isIE10OrLower()
    {
        return self::multiCheckUserAgent([
            'msie',
            'microsoft internet explorer',
            'msnb',
            'trident',
            'mspie',
            'pocket',
        ]);
    }

    public static function isIE11()
    {
        $userAgent = Yii::$app->getRequest()->getUserAgent();

        return stripos($userAgent, 'Trident/') !== false;
    }

    public static function isIE12()
    {
        $userAgent = Yii::$app->getRequest()->getUserAgent();

        return stripos($userAgent, 'Edge/') !== false;
    }

    protected static function multiCheckUserAgent($checkArray)
    {
        $userAgent = Yii::$app->getRequest()->getUserAgent();

        foreach($checkArray as $item) {
            if (stripos($userAgent, $item) !== false) {
                return true;
            }
        }

        return false;
    }
}