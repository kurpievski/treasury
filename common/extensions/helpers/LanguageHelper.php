<?php

/**
 * Created by PhpStorm.
 * User: Goran Sarenac <goransarenac91@hotmail.com>
 * Date: 8/16/2016
 * Time: 4:38 PM
 */

namespace common\helpers;

use lajax\translatemanager\models\Language;
use Yii;
use yii\helpers\Url;

class LanguageHelper
{
    protected static $cachedLanguages = [];

    public static function getLanguages()
    {
        return [
            Yii::$app->sourceLanguage => ArrayHelper::getValue(static::getLanguagesNamebyId(Yii::$app->sourceLanguage), 'name', ''),
            'es-ES' => 'Spanish'
        ];
    }

    public static function getLanguagesNamebyId($id)
    {
        return Language::find()->select(['name'])->where(['language_id' => $id])->asArray()->one();
    }

    public static function getDropdownItems()
    {
        $languages = static::getAllActiveLanguages();

        $items = [];
        foreach ($languages as $language) {
            if ($language->language_id != Yii::$app->language) {
                $items[] = [
                    'class' => 'dropdown-item',
                    'label' => $language->name,
                    'url' => Url::to(['/site/change-language', 'language' => $language->language_id])
                ];
            }
        }

        return $items;
    }

    public static function getAllActiveLanguages()
    {
        if (empty(static::$cachedLanguages)) {
            return static::$cachedLanguages = Language::findAll(['status' => 1]);
        }
        return static::$cachedLanguages;
    }

    public static function getCategories()
    {
        $languageSources = \lajax\translatemanager\models\LanguageSource::find()
            ->select('category')
            ->where(['like', 'category', 'survey'])
            ->distinct()
            ->all();

        $categories = [];
        foreach ($languageSources as $languageSource) {
            $categories[$languageSource->category] = $languageSource->category;
        }

        return $categories;
    }

    public static function getLanguageNames($includeSource = false)
    {
        $languages = static::getAllActiveLanguages();

        $languageNames = [];
        foreach ($languages as $language) {
            if ($includeSource || $language->language_id != Yii::$app->sourceLanguage) {
                $languageNames[$language->language_id] = $language->name;
            }
        }

        return $languageNames;
    }

    public static function getAllActiveLanguageName()
    {
        $languages = static::getAllActiveLanguages();

        $languageNames = [];
        foreach ($languages as $language) {
            $languageNames[$language->language_id] = $language->name;
        }

        return $languageNames;
    }

    public static function setLanguage($language)
    {
        $cookies = Yii::$app->response->cookies;

        $cookies->add(new \yii\web\Cookie([
            'name' => 'language',
            'value' => $language,
        ]));
    }
}

