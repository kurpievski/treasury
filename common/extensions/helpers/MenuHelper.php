<?php
/**
 * Email: srdjan.drakul@2amigos.us
 * Date: 9/15/2017
 * Time: 5:44 PM
 */

namespace common\helpers;

use common\models\Settings;
use frontend\models\EmsuranceForm;
use Yii;

class MenuHelper
{
    const TRIP_TAB_CUSTOMER = 'customer';
    const TRIP_TAB_SERVICE = 'service';
    const TRIP_TAB_MEDICAL = 'medical';
    const TRIP_TAB_BILLING = 'billing';
    const TRIP_TAB_HISTORY = 'history';

    public static function getItems()
    {
        $fullUrl = \Yii::$app->request->resolve();
        $url = $fullUrl[0];
        $controller = Yii::$app->controller->id;
        $module = Yii::$app->controller->module->id;
        $action = Yii::$app->controller->action->id;

        return [
            [
                'label' => '<i class="fa fa-home"></i> Dashboard ',
                'url' => ['/site/index'],
                'active' => empty($url) || in_array($url, ['site/index']),
                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_DASHBOARD)
            ],
            [
                'label' => '<i class="fa fa-cogs"></i>Administration',
                'url' => "javascript:;",
                'options' => ['class' => 'has-child has-sub'],
                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_CAC_SETTINGS) || Yii::$app->user->can(RbacHelper::PERMISSION_CAC_TECHNOSOFT_API)
                    || Yii::$app->user->can(RbacHelper::PERMISSION_CAC_EMSCIENCE) || Yii::$app->user->can(RbacHelper::PERMISSION_NARRATIVE_KEYWORDS)
                    || Yii::$app->user->can(RbacHelper::PERMISSION_NARRATIVE_CATEGORY) || Yii::$app->user->can(RbacHelper::PERMISSION_DOCUMENT_UPLOAD)
                    || Yii::$app->user->can(RbacHelper::PERMISSION_DOCUMENT_CATEGORY) || Yii::$app->user->can(RbacHelper::PERMISSION_DOCUMENT_STOP_WORDS)
                    || Yii::$app->user->can(RbacHelper::PERMISSION_FACILITY_VALUE_MAPPING) || Yii::$app->user->can(RbacHelper::PERMISSION_FACILITY_COMPANY_MAPPING)
                    || Yii::$app->user->can(RbacHelper::PERMISSION_CUSTOMER_SEARCH) || Yii::$app->user->can(RbacHelper::PERMISSION_CUSTOMER_MERGE)
                    || Yii::$app->user->can(RbacHelper::PERMISSION_CUSTOMER_LIST) || Yii::$app->user->can(RbacHelper::PERMISSION_PROCESSING_SETTINGS)
                    || Yii::$app->user->can(RbacHelper::PERMISSION_BILLING_HINTS) || Yii::$app->user->can(RbacHelper::PERMISSION_CAC_ERROR_REPORT)
                    || Yii::$app->user->can(RbacHelper::PERMISSION_CAC_ADMINISTRATION),
                'items' => [
                    [
                        'label' => 'CAC',
                        'url' => "javascript:;",
                        'options' => ['class' => 'has-child'],
                        'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_CAC_SETTINGS) || Yii::$app->user->can(RbacHelper::PERMISSION_CAC_TECHNOSOFT_API)
                            || Yii::$app->user->can(RbacHelper::PERMISSION_CAC_EMSCIENCE) || Yii::$app->user->can(RbacHelper::PERMISSION_CAC_ADMINISTRATION),
                        'submenuTemplate' => "\n<ul class='drop-down lvl-3'>\n{items}\n</ul>\n",
                        'items' => [
                            [
                                'label' => 'Settings',
                                'active' => in_array($url, ['cac-settings/index']),
                                'url' => ['/cac-settings/index'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_CAC_SETTINGS)
                            ],
                            [
                                'label' => 'Technosoft API',
                                'active' => in_array($url, ['technosoft']),
                                'url' => ['/technosoft'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_CAC_TECHNOSOFT_API)
                            ],
                            [
                                'label' => 'Internal API',
                                'active' => in_array($url, ['cac']),
                                'url' => ['/cac'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_CAC_ADMINISTRATION)
                            ],
                            [
                                'label' => 'CAC Error Report',
                                'active' => in_array($url, ['cac-error-report']),
                                'url' => ['/cac-error-report'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_CAC_ERROR_REPORT)
                            ],
                            [
                                'label' => 'CAC Test Batches',
                                'active' => in_array($url, ['cac/test-batch']),
                                'url' => ['/cac/test-batch'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_CAC_ADMINISTRATION)
                            ],
                        ]
                    ],
                    [
                        'label' => 'Emsurance',
                        'url' => "javascript:;",
                        'options' => ['class' => 'has-child'],
                        'visible' => Yii::$app->getUser()->getIsAdmin(),
                        'submenuTemplate' => "\n<ul class='drop-down lvl-3'>\n{items}\n</ul>\n",
                        'items' => [
                            [
                                'label' => 'Settings',
                                'active' => in_array($url, ['emsurance/settings']),
                                'url' => ['/emsurance/settings']
                            ],
                            [
                                'label' => 'Audit Log',
                                'active' => in_array($url, ['emsurance/audit-log']),
                                'url' => ['/emsurance/audit-log']
                            ]
                        ]
                    ],
                    [
                        'label' => 'Documents',
                        'url' => "javascript:;",
                        'options' => ['class' => 'has-child'],
                        'submenuTemplate' => "\n<ul class='drop-down lvl-3'>\n{items}\n</ul>\n",
                        'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_DOCUMENT_UPLOAD)
                            || Yii::$app->user->can(RbacHelper::PERMISSION_DOCUMENT_CATEGORY)
                            || Yii::$app->user->can(RbacHelper::PERMISSION_DOCUMENT_STOP_WORDS),
                        'items' => [
                            [
                                'label' => 'Upload',
                                'active' => in_array($url, ['document/index']),
                                'url' => ['/document/index'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_DOCUMENT_UPLOAD)
                            ],
                            [
                                'label' => 'Categories',
                                'active' => $controller == 'category' && $action == 'index',
                                'url' => ['/category/index'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_DOCUMENT_CATEGORY)
                            ],
                            [
                                'label' => 'Keywords',
                                'active' => $controller == 'stop-word',
                                'url' => ['/stop-word/index'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_DOCUMENT_STOP_WORDS)
                            ]
                        ]
                    ],
                    [
                        'label' => 'Customer',
                        'url' => "javascript:;",
                        'options' => ['class' => 'has-child'],
                        'submenuTemplate' => "\n<ul class='drop-down lvl-3'>\n{items}\n</ul>\n",
                        'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_CUSTOMER_SEARCH)
                            || Yii::$app->user->can(RbacHelper::PERMISSION_CUSTOMER_MERGE)
                            || Yii::$app->user->can(RbacHelper::PERMISSION_CUSTOMER_LIST),
                        'items' => [
                            [
                                'label' => 'Search',
                                'active' => $controller == 'customer' && $action == 'customer-search',
                                'url' => ['/customer/customer-search'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_CUSTOMER_SEARCH)
                            ],
                            [
                                'label' => 'Merge',
                                'active' => $controller == 'customer' && $action == 'merge',
                                'url' => ['/customer/merge'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_CUSTOMER_MERGE)
                            ],
                            [
                                'label' => 'List',
                                'active' => $controller == 'customer' && $action == 'index',
                                'url' => ['/customer/index'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_CUSTOMER_LIST)
                            ]
                        ]
                    ],
                    [
                        'label' => 'Mapping',
                        'url' => "javascript:;",
                        'options' => ['class' => 'has-child'],
                        'submenuTemplate' => "\n<ul class='drop-down lvl-3'>\n{items}\n</ul>\n",
                        'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_PAYOR_MAPPING) || Yii::$app->user->can(RbacHelper::PERMISSION_FACILITY_MAPPING)
                            || Yii::$app->user->can(RbacHelper::PERMISSION_FACILITY_VALUE_MAPPING) || Yii::$app->user->can(RbacHelper::PERMISSION_FACILITY_COMPANY_MAPPING)
                            || Yii::$app->user->can(RbacHelper::PERMISSION_NARRATIVE_KEYWORDS) || Yii::$app->user->can(RbacHelper::PERMISSION_NARRATIVE_CATEGORY)
                            || Yii::$app->user->can(RbacHelper::PERMISSION_INCIDENT_MAPPING_ACCESS) || Yii::$app->user->can(RbacHelper::PERMISSION_INVALID_SSN_MAP_ACCESS),
                        'items' => [
                            [
                                'label' => 'Facility',
                                'active' => $controller == 'facility-map' && ($action == 'index' || $action == 'history'),
                                'url' => ['/facility-map/index'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_FACILITY_MAPPING)
                            ],
                            [
                                'label' => 'Facility Exceptions',
                                'active' => $controller == 'facility' && $action == 'exception',
                                'url' => ['/facility/exception'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_FACILITY_MAPPING)
                            ],
                            [
                                'label' => 'Payor',
                                'active' => $controller == 'payor-map' && ($action == 'index' || $action == 'history'),
                                'url' => ['/payor-map/index'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_PAYOR_MAPPING)
                            ],
                            [
                                'label' => 'Payor Exceptions',
                                'active' => $controller == 'payor' && $action == 'exception',
                                'url' => ['/payor/exception'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_PAYOR_MAPPING)
                            ],
                            [
                                'label' => 'Incident',
                                'active' => $controller == 'company' && $action == 'incident-map',
                                'url' => ['/company/incident-map'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_INCIDENT_MAPPING_ACCESS)
                            ],
                            [
                                'label' => 'Invalid SSN',
                                'active' => $controller == 'invalid-ssn-map',
                                'url' => ['/invalid-ssn-map/index'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_INVALID_SSN_MAP_ACCESS)
                            ],

                            [
                                'label' => 'Ordering Facility',
                                'url' => "javascript:;",
                                'options' => ['class' => 'has-child'],
                                'submenuTemplate' => "\n<ul class='drop-down lvl-4'>\n{items}\n</ul>\n",
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_FACILITY_VALUE_MAPPING) || Yii::$app->user->can(RbacHelper::PERMISSION_FACILITY_COMPANY_MAPPING),
                                'items' => [
                                    [
                                        'label' => 'Value',
                                        'active' => $controller == 'ordering-facility-map' && $action == 'index',
                                        'url' => ['/ordering-facility-map/index'],
                                        'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_FACILITY_VALUE_MAPPING)
                                    ],
                                    [
                                        'label' => 'Company',
                                        'active' => $controller == 'company' && $action == 'index',
                                        'url' => ['/company/index'],
                                        'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_FACILITY_COMPANY_MAPPING)
                                    ]
                                ]
                            ],
                            [
                                'label' => 'Agency',
                                'url' => "javascript:;",
                                'options' => ['class' => 'has-child'],
                                'submenuTemplate' => "\n<ul class='drop-down lvl-4'>\n{items}\n</ul>\n",
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_FACILITY_VALUE_MAPPING) || Yii::$app->user->can(RbacHelper::PERMISSION_FACILITY_COMPANY_MAPPING),
                                'items' => [
                                    [
                                        'label' => 'Value',
                                        'active' => $controller == 'agency-map' && $action == 'index',
                                        'url' => ['/agency-map/index'],
                                        'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_FACILITY_VALUE_MAPPING)
                                    ],
                                    [
                                        'label' => 'Agency ID',
                                        'active' => $controller == 'agency-map' && $action == 'agency-id',
                                        'url' => ['/agency-map/agency-id'],
                                        'visible' => false
                                    ],
                                    [
                                        'label' => 'Company',
                                        'active' => $controller == 'company' && $action == 'index',
                                        'url' => ['/company/index'],
                                        'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_FACILITY_COMPANY_MAPPING)
                                    ]
                                ]
                            ],
                            [
                                'label' => 'Narrative',
                                'url' => "javascript:;",
                                'options' => ['class' => 'has-child'],
                                'submenuTemplate' => "\n<ul class='drop-down lvl-4'>\n{items}\n</ul>\n",
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_NARRATIVE_KEYWORDS) || Yii::$app->user->can(RbacHelper::PERMISSION_NARRATIVE_CATEGORY),
                                'items' => [
                                    [
                                        'label' => 'Keywords',
                                        'active' => in_array($url, ['narrative-keywords']),
                                        'url' => ['/narrative-keywords'],
                                        'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_NARRATIVE_KEYWORDS)
                                    ],
                                    [
                                        'label' => 'Categories',
                                        'active' => in_array($url, ['narrative-category']),
                                        'url' => ['/narrative-category'],
                                        'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_NARRATIVE_CATEGORY)
                                    ]
                                ]
                            ],
                            [
                                'label' => 'Auto Charges',
                                'active' => $controller == 'autocharge-map',
                                'url' => ['/autocharge-map/index'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_VIEW_AUTOCHARGE_MAP) || Yii::$app->user->can(RbacHelper::PERMISSION_UPDATE_AUTOCHARGE_MAP)
                            ],
                        ]
                    ],
                    [
                        'label' => 'Processing settings',
                        'url' => "javascript:;",
                        'options' => ['class' => 'has-child'],
                        'submenuTemplate' => "\n<ul class='drop-down lvl-3'>\n{items}\n</ul>\n",
                        'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_PROCESSING_SETTINGS),
                        'items' => [
                            [
                                'label' => 'Company',
                                'url' => ['/export-rules/index']
                            ],
                            [
                                'label' => 'Company/Ordering Facility',
                                'active' => $controller === 'company-ordering-facility-settings',
                                'url' => ['/company-ordering-facility-settings/index']
                            ]
                        ]
                    ],
                    [
                        'label' => 'Billing Hints',
                        'url' => ['/billing-hints/index'],
                        'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_BILLING_HINTS)
                    ],
                    [
                        'label' => 'Trip Note Types',
                        'url' => ['/trip-note-type/index'],
                        'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_TRIP_NOTE_TYPE)
                    ],
                    [
                        'label' => 'Imports',
                        'url' => "javascript:;",
                        'options' => ['class' => 'has-child'],
                        'submenuTemplate' => "\n<ul class='drop-down lvl-3'>\n{items}\n</ul>\n",
                        'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_RECONCILIATION_DASHBOARD) || Yii::$app->user->can(RbacHelper::PERMISSION_RECONCILIATION_EXCEPTIONS)
                            || Yii::$app->user->can(RbacHelper::PERMISSION_RECONCILIATION_DAILY_REPORT),
                        'items' => [
                            [
                                'label' => 'Reconciliation Dashboard',
                                'active' => $controller == 'reconciliation' && $action == 'index',
                                'url' => ['/reconciliation/index'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_RECONCILIATION_DASHBOARD)
                            ],
                            [
                                'label' => 'Exception Log',
                                'active' => $controller == 'reconciliation' && $action == 'exceptions',
                                'url' => ['/reconciliation/exceptions'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_RECONCILIATION_EXCEPTIONS)
                            ],
                            [
                                'label' => 'Daily Report',
                                'active' => $controller == 'reconciliation' && $action == 'daily-report',
                                'url' => ['/reconciliation/daily-report'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_RECONCILIATION_DAILY_REPORT)
                            ],
                            [
                                'label' => 'Monthly Report',
                                'active' => $controller == 'reconciliation' && $action == 'monthly-report',
                                'url' => ['/reconciliation/monthly-report'],
                                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_RECONCILIATION_DAILY_REPORT)
                            ],
                            [
                                'label' => 'Invalidate Notes',
                                'active' => $controller == 'file-action' && $action == 'index',
                                'url' => ['/file-action/index']
                            ]
                        ]
                    ]
                ]
            ],
            [
                'label' => '<i class="fa fa-ambulance"></i>Trips',
                'url' => ['/trip/index'],
                'active' => $controller == 'trip' && in_array($action, ['index', 'update']),
                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_TRIP_VIEW_ONLY)
            ],
            [
                'label' => '<i class="fa fa-umbrella"></i>EMSurance',
                'url' => ['/emsurance'],
                'active' => in_array($url, ['emsurance', 'emsurance/index']) && ArrayHelper::getValue($fullUrl, '1.tab', EmsuranceForm::TAB_FORM) === EmsuranceForm::TAB_FORM,
                'visible' => Settings::getIsEmsuranceEnabled() && Yii::$app->user->can(RbacHelper::PERMISSION_EMSURANCE)
            ],
            [
                'label' => '<i class="fa fa-file"></i> Reports',
                'url' => "javascript:;",
                'options' => ['class' => 'has-child has-sub'],
                'submenuTemplate' => "\n<ul class='drop-down lvl-2'>\n{items}\n</ul>\n",
                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_PAYOR_MAPPING) || Yii::$app->user->can(RbacHelper::PERMISSION_CAC_REPORT)
                    || Yii::$app->user->can(RbacHelper::PERMISSION_EMSURANCE_REPORT_VIEW) || Yii::$app->user->can(RbacHelper::PERMISSION_EMSURANCE_REPORT_EXPORT)
                    ||  Yii::$app->user->can(RbacHelper::PERMISSION_BILLER_PRODUCTION_REPORT),
                'items' => [
                    [
                        'label' => 'Payor Change Report',
                        'url' => ['/payor-change-report/index'],
                        'active' => in_array($url, ['payor-change-report/index']),
                        'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_PAYOR_MAPPING)
                    ],
                    [
                        'label' => 'CAC Report',
                        'active' => in_array($url, ['cac-report']),
                        'url' => ['/cac-report'],
                        'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_CAC_REPORT)
                    ],
                    [
                        'label' => 'Emsurance',
                        'active' => in_array($url, ['emsurance/index']) && ArrayHelper::getValue($fullUrl, '1.tab') === EmsuranceForm::TAB_REPORTS,
                        'url' => ['/emsurance/index', 'tab' => EmsuranceForm::TAB_REPORTS],
                        'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_EMSURANCE_REPORT_VIEW) || Yii::$app->user->can(RbacHelper::PERMISSION_EMSURANCE_REPORT_EXPORT)
                    ],
                    [
                        'label' => 'Biller Production',
                        'active' => in_array($url, ['trip/biller-production']),
                        'url' => ['/trip/biller-production'],
                        'visible' => RbacHelper::PERMISSION_BILLER_PRODUCTION_REPORT
                    ],
                    [
                        'label' => 'Cac Biller Change',
                        'active' => in_array($url, ['trip/cac-biller-change-report']),
                        'url' => ['/trip/cac-biller-change-report'],
                        'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_CAC_REPORT)
                    ]
                ]
            ],
            [
                'label' => '<i class="fa fa-code"></i>CAC Module',
                'url' => "javascript:;",
                'options' => ['class' => 'has-child has-sub'],
                'visible' => Yii::$app->user->can(RbacHelper::PERMISSION_CAC_ADMINISTRATION),
                'items' => [
                    [
                        'label' => 'LOS CAC',
                        'url' => 'javascript:;',
                        'options' => ['class' => 'has-child'],
                        'submenuTemplate' => "\n<ul class='drop-down lvl-3'>\n{items}\n</ul>\n",
                        'visible' => true,
                        'items' => [
                            [
                                'label' => 'Mappings',
                                'url' => "javascript:;",
                                'options' => ['class' => 'has-child'],
                                'submenuTemplate' => "\n<ul class='drop-down lvl-4'>\n{items}\n</ul>\n",
                                'visible' => true,
                                'items' => [
                                    [
                                        'label' => 'DOA Mappings',
                                        'url' => ['/cac_los/priority-flag-sentence/index'],
                                        'active' => $controller == 'priority-flag-sentence',
                                        'visible' => true
                                    ],
                                    [
                                        'label' => 'SOP - Skills',
                                        'url' => ['/cac_los/scope-of-practice/index'],
                                        'active' => $controller == 'scope-of-practice',
                                        'visible' => true
                                    ],
                                    [
                                        'label' => 'SOP - Medications',
                                        'url' => ['/cac_los/medication/index'],
                                        'active' => $controller == 'medication',
                                        'visible' => true
                                    ],
                                    [
                                        'label' => 'Client Override Rules',
                                        'url' => ['/cac_los/dispatch-protocol/index'],
                                        'active'  => $controller == 'dispatch-protocol' && $module == 'cac_los',
                                        'visible' => true
                                    ],
                                    [
                                        'label' => 'Settings',
                                        'url' => ['/cac_los/settings/index'],
                                        'active'  => $controller == 'settings' && $module == 'cac_los',
                                        'visible' => true
                                    ]
                                ]
                            ],
                            [
                                'label' => 'Rules',
                                'url' => ['/cac_los/rule/index'],
                                'active' => $controller == 'rule' && $module == 'cac_los',
                                'visible' => true
                            ],
                            [
                                'label' => 'EMT',
                                'url' => ['/cac_los/emt/index'],
                                'active' => $controller == 'emt',
                                'visible' => true
                            ]
                        ]
                    ],
                    [
                        'label' => 'Priority CAC',
                        'url' => 'javascript:;',
                        'options' => ['class' => 'has-child'],
                        'submenuTemplate' => "\n<ul class='drop-down lvl-3'>\n{items}\n</ul>\n",
                        'visible' => true,
                        'items' => [
                            [
                                'label' => 'Mappings',
                                'url' => "javascript:;",
                                'options' => ['class' => 'has-child'],
                                'submenuTemplate' => "\n<ul class='drop-down lvl-4'>\n{items}\n</ul>\n",
                                'visible' => true,
                                'items' => [
                                    [
                                        'label'   => 'Pickup Mappings',
                                        'url'     => ['/cac_priority/pickup/index'],
                                        'active'  => $controller == 'pickup',
                                        'visible' => true
                                    ],
                                    [
                                        'label'   => 'E Destinations',
                                        'url'     => ['/cac_priority/emergency-destination/index'],
                                        'active'  => $controller == 'emergency-destination',
                                        'visible' => true
                                    ],
                                    [
                                        'label'   => 'NE Destinations',
                                        'url'     => ['/cac_priority/non-emergency-destination/index'],
                                        'active'  => $controller == 'non-emergency-destination',
                                        'visible' => true
                                    ],
                                    [
                                        'label' => 'E/NE Keywords',
                                        'url'     => ['/cac_priority/emergency-flag-sentence/index'],
                                        'active'  => $controller == 'emergency-flag-sentence',
                                        'visible' => true
                                    ],
                                    [
                                        'label'   => 'Client Override Rules',
                                        'url'     => ['/cac_priority/dispatch-protocol/index'],
                                        'active'  => $controller == 'dispatch-protocol' && $module == 'cac_priority',
                                        'visible' => true
                                    ]
                                ]
                            ],
                            [
                                'label'   => 'Rules',
                                'url'     => ['/cac_priority/priority-rule/index'],
                                'active'  => $controller == 'priority-rule',
                                'visible' => true
                            ]
                        ]
                    ],
                    [
                        'label' => 'MN CAC',
                        'url' => 'javascript:;',
                        'options' => ['class' => 'has-child'],
                        'submenuTemplate' => "\n<ul class='drop-down lvl-3'>\n{items}\n</ul>\n",
                        'visible' => true,
                        'items' => [
                            [
                                'label' => 'Mappings',
                                'url' => "javascript:;",
                                'options' => ['class' => 'has-child'],
                                'submenuTemplate' => "\n<ul class='drop-down lvl-4'>\n{items}\n</ul>\n",
                                'visible' => true,
                                'items' => [
                                    [
                                        'label'   => 'Conditions',
                                        'url'     => ['/cac_mn/condition/index'],
                                        'active'  => $controller == 'condition',
                                        'visible' => true
                                    ]
                                ]
                            ],
                            [
                                'label'   => 'Rules',
                                'url'     => ['/cac_mn/rule/index'],
                                'active'  => $controller == 'rule' && $module == 'cac_mn',
                                'visible' => true
                            ],
                            [
                                'label'   => 'Detractors',
                                'url'     => ['/cac_mn/detractor/index'],
                                'active'  => $controller == 'detractor' && $module == 'cac_mn',
                                'visible' => true
                            ]
                        ]
                    ],
                    [
                        'label' => 'Audit Log',
                        'url' => ['/cac/audit-log/index'],
                        'active' => $controller == 'audit-log',
                        'visible' => true
                    ]
                ]
            ],
            [
                'label' => '<i class="fa fa-power-off"></i>Log out',
                'url' => ['/site/logout']
            ]
        ];
    }
}
