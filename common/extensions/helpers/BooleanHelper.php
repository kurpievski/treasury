<?php
/**
 * Created by PhpStorm.
 * User: vprorok
 * Date: 3/14/2017
 * Time: 2:31 PM
 */

namespace common\helpers;


class BooleanHelper
{
    public static function getListOptions()
    {
        return [
            0 => 'No',
            1 => 'Yes'
        ];
    }

    public static function convertToYesOrNo($value)
    {
        return $value ? 'Y' : 'N';
    }

    public static function convertToInteger($value)
    {
        return $value ? 1 : 0;
    }

    public static function convertToYesOrNoStrict($getValue)
    {
        if ($getValue === 1) {
            return 'Yes';
        }

        if ($getValue === 0) {
            return 'No';
        }

        return '';
    }

    public static function getListOptionsIndexedByLetters()
    {
        return [
            'Y' => 'Yes',
            'N' => 'No'
        ];
    }

    public static function convertYesOrNoToInteger($value)
    {
        return strtolower($value) == 'yes' ? 1 : 0;
    }
}