<?php

namespace common\helpers;

class PasswordHelper
{
    public static function getRandomizedPasswordString($alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789", $length = 8)
    {
        $password = [];
        $alphaLength = strlen($alphabet) - 1;

        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $password[$i] = $alphabet[$n];
        }

        return implode($password);
    }
}