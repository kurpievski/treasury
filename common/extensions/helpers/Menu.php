<?php
namespace app\helpers;

use Yii;
use yii\web\Link;

class Menu
{
    public static function prepareElements(array $links, array $rules = []): array
    {
        foreach ($links as $id => &$link) {
            /* @var $link Link */

            $link = [
                'text' => $link->title,
                'href' => $link->href,
                'active' => self::isActive($rules[$id] ?? $link->href, !isset($rules[$id])),
            ];
        }

        return $links;
    }

    /**
     * @param $urls
     * @return bool
     */
    public static function isActive($urls, $strict = true): bool
    {
        if (!is_array($urls)) {
            $urls = [$urls];
        }

        foreach ($urls as $url) {
            $currentUrl = Yii::$app->getRequest()->getUrl();
            if ($strict) {
                if ($currentUrl == $url) {
                    return true;
                }
            } else {
                if (strpos($currentUrl, $url) !== false) {
                    return true;
                }
            }
        }

        return false;
    }
}
