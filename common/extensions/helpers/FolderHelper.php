<?php
/**
 * Created by PhpStorm.
 * User: vprorok
 * Date: 10/20/2016
 * Time: 10:23 PM
 */

namespace common\helpers;


class FolderHelper
{
    public static function createSubFolders($parentFolder, $folderName)
    {
        if (is_array($folderName)) {
            foreach ($folderName as $folder) {
                $checkFolder = $parentFolder . DIRECTORY_SEPARATOR . $folder;

                if (!is_dir($checkFolder)) {
                    $old = umask(0);
                    @mkdir($checkFolder, 0777);
                    umask($old);
                }
            }
        } else {
            $newFolder = $parentFolder . DIRECTORY_SEPARATOR . $folderName;

            if (!is_dir($newFolder)) {
                $old = umask(0);
                @mkdir($newFolder, 0777);
                umask($old);
            }
        }
    }

    public static function clearFolder($path){
        if (is_dir($path)) {
            $files = glob($path . DIRECTORY_SEPARATOR . '*'); // get all file names
            if (!empty($files)) {
                foreach ($files as $file) { // iterate files
                    if (is_file($file))
                        unlink($file); // delete file
                }
            }
        }
    }

    /**
     * Move file to target File
     * @param type $fileName
     * @param type $targerFile
     */
    public static function moveFile($fileName, $targerFile)
    {
        $partern = " `!#$%^&*()_+|;',.<>?";
        $mvcmd = "mv " . addcslashes($fileName, $partern) . ' ' . addcslashes($targerFile, $partern);
        shell_exec($mvcmd);
    }
}