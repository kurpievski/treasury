<?php
/**
 * ModelHelper.php
 *
 * Author: Aleksandar Panic <aleksandar.panic@2amigos.us>
 * Date: 30-Mar-16
 * Time: 22:04
 */

namespace common\helpers;


class ModelHelper
{
    public static function getMultiSelectIds($values)
    {
        if (!empty($values)) {
            $removeIndices = [];

            foreach ($values as $index => $id) {
                if (empty($id)) {
                    $removeIndices[] = $index;
                } else if ($id == 'multiselect-all') {
                    $removeIndices[] = $index;
                }
            }

            foreach ($removeIndices as $index) {
                unset($values[$index]);
            }
        }

        if (is_string($values) || empty($values)) {
            return [];
        }

        return $values;
    }

}