<?php

namespace common\helpers;

use Carbon\Carbon;
use DateTime;
use DateTimeZone;

/**
 *
 * User: nikola radovic <dzona065@gmail.com>
 * Date: 06/09/14
 * Time: 16:49
 *
 * Example of use:
 *
 * $unixtime = HTime::dateTimeToTimestamp('2009-04-01 15:36:13');
 * echo HTime::UTCToPST("M d, Y - H:i:s", $unixtime);
 */
class TimeHelper
{
    const SHORTDATE = "j/n/y";
    const DATE_FORMAT = 'm/d/Y';
    const TIME_FORMAT = 'H:i:s';
    const DASH_DATE_FORMAT = 'm-d-Y';
    const SQL_DATE_FORMAT = 'Y-m-d';
    const SQL_TIME_FORMAT = 'H:i:s';
    const SQL_DATETIME_FORMAT = 'Y-m-d H:i:s';
    const UNIX_DATE_FORMAT = 'U';
    const DATETIME_FORMAT = 'm/d/Y H:i';
    const MONTH_TEXT_FORMAT = 'F';
    const WEEK_DAYS = 7;
    const MINUTE = 60;
    const HOUR = 3600;
    const DAY = 86400;
    const WEEK = 604800;
    const MONTH = 2592000; //30 days
    const YEAR = 31556926;
    const MONDAY = 'monday';
    const TUESDAY = 'tuesday';
    const WEDNESDAY = 'wednesday';
    const THURSDAY = 'thursday';
    const FRIDAY = 'friday';
    const SATURDAY = 'saturday';
    const SUNDAY = 'sunday';
    const JANUARY = 'january';
    const FEBRUARY = 'february';
    const MARCH = 'march';
    const APRIL = 'april';
    const MAY = 'may';
    const JUN = 'jun';
    const JULY = 'july';
    const AUGUST = 'august';
    const SEPTEMBER = 'september';
    const OCTOBER = 'october';
    const NOVEMBER = 'november';
    const DECEMBER = 'december';
    const WEEK_START = self::SUNDAY;
    const WEEK_END = self::SATURDAY;
    const DATE_PICKER_FORMAT = 'M/D/YYYY';
    const DATE_RANGE_PICKER_FORMAT = 'MM/DD/YYYY';
    const EMSURANCE_TIME_FORMAT = 'Y_m_d_H_i_s';
    const MOMENT_TIME_FORMAT = 'MM-DD-YYYY';

    const DEFAULT_SERVER_TIMEZONE = 'America/New_York';
    const UTC_TIMEZONE = 'UTC';
    const CHART_DATE_FORMAT = 'Y,m-1,d';
    const CHART_DATETIME_FORMAT = 'Y,m-1,d,H,i';

    const TIME_SPAN_DAY = 'timespan_day';
    const TIME_SPAN_WEEK = 'timespan_week';
    const TIME_SPAN_MONTH = 'timespan_month';
    const TIME_SPAN_YEAR = 'timespan_year';

    const TIME_DAY_END = ' 23:59:59';
    const TIME_DAY_START = ' 00:00:00';

    const DATERANGE_PERIOD_TODAY = 'Today';
    const DATERANGE_PERIOD_THIS_WEEK = 'This Week';
    const DATERANGE_PERIOD_THIS_MONTH = 'This Month';
    const DATERANGE_PERIOD_THIS_YEAR = 'This Year';

    /**
     * get circular days list
     * day => next_day
     *
     * @return array
     */
    private static function getDayCircularList()
    {
        return array(
            self::MONDAY => self::TUESDAY,
            self::TUESDAY => self::WEDNESDAY,
            self::WEDNESDAY => self::THURSDAY,
            self::THURSDAY => self::FRIDAY,
            self::FRIDAY => self::SATURDAY,
            self::SATURDAY => self::SUNDAY,
            self::SUNDAY => self::MONDAY,
        );
    }

    /**
     * Return ordered week days array
     * set by self::WEEK_START and self::WEEK_END
     *
     * @return array
     */
    public static function getWeekDays()
    {
        $availableDays = self::getDayCircularList();
        $nextDay = self::WEEK_START;
        $weekDays = array($nextDay);

        while ($nextDay !== self::WEEK_END) {
            $nextDay = $availableDays[$nextDay];
            $weekDays[] = $nextDay;
        }

        return $weekDays;
    }

    public static function getDefaultOffPeakWorkingDays()
    {
        return array(
            self::MONDAY,
            self::TUESDAY,
            self::WEDNESDAY,
            self::THURSDAY,
            self::FRIDAY,
            self::SATURDAY,
            self::SUNDAY
        );
    }

    public static function getDefaultOnPeakWorkingDays()
    {
        return self::getDefaultOffPeakWorkingDays();
    }

    public static function getMonthList()
    {
        return array(
            self::JANUARY => 'January',
            self::FEBRUARY => 'February',
            self::MARCH => 'March',
            self::APRIL => 'April',
            self::MAY => 'May',
            self::JUN => 'Jun',
            self::JULY => 'July',
            self::AUGUST => 'August',
            self::SEPTEMBER => 'September',
            self::OCTOBER => 'October',
            self::NOVEMBER => 'November',
            self::DECEMBER => 'December',
        );
    }

    public static function getNumberedMonthList()
    {
        return [
            1 => "(01) January",
            2 => "(02) February",
            3 => "(03) March",
            4 => "(04) April",
            5 => "(05) May",
            6 => "(06) June",
            7 => "(07) July",
            8 => "(08) August",
            9 => "(09) September",
            10 => "(10) October",
            11 => "(11) November",
            12 => "(12) December"
        ];
    }

    public static function getTimeSpanLabels()
    {
        return [
            self::TIME_SPAN_DAY => 'Day',
            self::TIME_SPAN_WEEK => 'Week',
            self::TIME_SPAN_MONTH => 'Month',
            self::TIME_SPAN_YEAR => 'Year'
        ];
    }

    public static function getYearList($fromYear = null, $numYears = 11)
    {
        $fromYear = empty($fromYear) ? date('Y') : $fromYear;

        $yearList = [];

        for ($i = $fromYear; $i < $fromYear + $numYears; $i++) {
            $yearList[$i] = $i;
        }

        return $yearList;
    }

    /**
     * @param $timestamp int in milliseconds
     * @param $format string see http://www.php.net/manual/en/function.date.php
     *
     * @return string - formatted date/time
     */
    public static function format($timestamp, $format = self::DATE_FORMAT)
    {
        if (empty($timestamp)) {
            return null;
        }

        return date($format, $timestamp);
    }

    public static function formatSqlDateToLocalDate($date)
    {
        return TimeHelper::changeDateTimeFormat($date, TimeHelper::SQL_DATE_FORMAT, TimeHelper::DATE_FORMAT);
    }

    public static function formatSqlDateToLocalDashDate($date)
    {
        return TimeHelper::changeDateTimeFormat($date, TimeHelper::SQL_DATE_FORMAT, TimeHelper::DASH_DATE_FORMAT);
    }

    public static function formatLocalDateToSqlDate($date)
    {
        return TimeHelper::changeDateTimeFormat($date, TimeHelper::DATE_FORMAT, TimeHelper::SQL_DATE_FORMAT);
    }

    public static function formatSqlDateTimeToLocalDateTime($date)
    {
        return TimeHelper::changeDateTimeFormat($date, TimeHelper::SQL_DATETIME_FORMAT, TimeHelper::DATETIME_FORMAT);
    }

    public static function formatLocalToUnix($local)
    {
        return TimeHelper::changeDateTimeFormat($local, TimeHelper::DATE_FORMAT, TimeHelper::UNIX_DATE_FORMAT);
    }

    public static function formatUnixToLocal($unix)
    {
        return TimeHelper::changeDateTimeFormat($unix, TimeHelper::UNIX_DATE_FORMAT, TimeHelper::DATE_FORMAT);
    }

    public static function formatUnixToLocalDateTime($unix)
    {
        return TimeHelper::changeDateTimeFormat($unix, TimeHelper::UNIX_DATE_FORMAT, TimeHelper::DATETIME_FORMAT);
    }

    public static function formatUnixToSqlDateTime($unix)
    {
        return TimeHelper::changeDateTimeFormat($unix, TimeHelper::UNIX_DATE_FORMAT, TimeHelper::SQL_DATETIME_FORMAT);
    }

    public static function formatUnixToSqlDate($unix)
    {
        return TimeHelper::changeDateTimeFormat($unix, TimeHelper::UNIX_DATE_FORMAT, TimeHelper::SQL_DATE_FORMAT);
    }

    public static function formatUnixToSqlTime($unix)
    {
        return TimeHelper::changeDateTimeFormat($unix, TimeHelper::UNIX_DATE_FORMAT, TimeHelper::SQL_TIME_FORMAT);
    }

    public static function formatTime($time, $format = 'H:i')
    {
        if (empty($time)) {
            return '00:00:00';
        }

        try {
            $dateTime = new \DateTime($time);

            return $dateTime->format($format);
        } catch (\Exception $e) {
            return $time;
        }
    }

    /**
     * Returns a date string
     *
     * @param $timestamp
     *
     * @return string
     */
    public static function onlyDate($timestamp)
    {
        return self::format($timestamp, self::SHORTDATE);
    }

    /**
     * Calculates the difference between two dates
     *
     * @param string $interval the difference to be returned. Possible values are:
     *
     * - s: seconds
     * - i: minutes
     * - h: hours
     * - d: days
     * - w: weeks
     * - m: months
     * - y: years
     *
     * @param int|string $dateFrom
     * @param int|string $dateTo
     * @param bool $usingTimeStamps whether we are passing a timestamp (int) or a date string
     *
     * @return int the difference interval
     */
    public static function dateDiff($interval, $dateFrom, $dateTo, $usingTimeStamps = false)
    {
        if (!$usingTimeStamps) {
            $dateFrom = strtotime($dateFrom);
            $dateTo = strtotime($dateTo);
        }

        $timeDiff = $dateTo - $dateFrom;
        $timeDiff = $timeDiff > 0 ? $timeDiff : 0;
        $factor = 0;

        switch ($interval) {
            case 's':
                $factor = 1;
                break;
            case 'i':
                $factor = self::MINUTE;
                break;
            case 'h':
                $factor = self::HOUR;
                break;
            case 'd':
                $factor = self::DAY;
                break;
            case 'w':
                $factor = self::WEEK;
                break;
            case 'm':
                $factor = self::MONTH;
                break;
            case 'y':
                $factor = self::YEAR;
        }

        return $timeDiff > $factor ? round($timeDiff / $factor) : 0;
    }

    public static function toDateTime($timestamp, $format = 'Y-m-d H:i:s', $gmtOffset = false)
    {
        return $gmtOffset ? gmdate($format, $timestamp) : date($format, $timestamp);
    }

    public static function toTimestamp($dateTime)
    {
        // dateTimeToTimestamp expects MySQL format
        // If it gets a fully numeric value, we'll assume it's a timestamp
        // You can comment out this if block if you don't want this behavior
        if (is_numeric($dateTime)) {
            // You should probably log an error here
            return $dateTime;
        }
        $date = new DateTime($dateTime);
        $ret = $date->format('U');
        return ($ret < 0 ? 0 : $ret);
    }

    public static function UTCToPST($format, $time)
    {
        $dst = intval(date("I", $time));
        $tzOffset = intval(date('Z', time()));
        return date($format, $time + $tzOffset - 28800 + $dst * 3600);
    }

    /**
     * Helper function to get timestamp of first available week day, from given date.
     * If $weekday is same as $date's and function will returt $date,
     * otherwise function will provide date of next week's weekday.
     * Default weekday index is 6, which is equivalent for 'saturday'
     *
     * Example:
     * HTime::nextWeekday(new DateTime('2014-01-20'), 2);  //echo strtotime('2014-01-21');
     * HTime::nextWeekday(new DateTime('2014-01-22'), 2);  //echo strtotime('2014-01-28');
     *
     * @param DateTime $dateTime
     * @param int $weekday Week day index (Start of week is 0 for Sunday)
     *
     * @return DateTime
     */
    public static function nextWeekday(DateTime $dateTime, $weekday = 6)
    {
        $weekdays = self::getWeekDays();

        if ($dateTime->format('w') == $weekday) {
            return $dateTime;
        }

        return $dateTime->modify("next {$weekdays[$weekday]}");
    }

    /**
     * MySQL YEARWEEK mode 6 equivalent
     * based on ISO-8601 with Sunday as first day of week
     * US week calendar
     *
     * @param DateTime $dateTime
     *
     * @return integer
     */
    public static function getYearWeek(DateTime $dateTime)
    {
        $minDaysInWeek = 4;
        $firstDayOfYear = clone $dateTime;
        $lastDayOfYear = clone $dateTime;
        $yearStartingDay = $firstDayOfYear->modify('first day of January this year')->format('N');
        $numberOfDaysInYear = $lastDayOfYear->modify('last day of December this year')->format('z') + 1;
        $ordinalDate = $dateTime->format('z') + 1;
        $weekOffset = $yearStartingDay < $minDaysInWeek ? 1 : 0;

        $weekNum = ceil(($ordinalDate + $yearStartingDay - self::WEEK_DAYS) / self::WEEK_DAYS) + $weekOffset;
        $maxWeekNum = ceil(($numberOfDaysInYear + $yearStartingDay - self::WEEK_DAYS) / self::WEEK_DAYS) + $weekOffset;

        if ($weekNum < 1) {
            return self::getYearWeek(clone $dateTime->modify('last day of December previous year'));
        }

        if ($weekNum > $maxWeekNum) {
            return self::getYearWeek(clone $dateTime->modify('first day of January next year'));
        }

        return intval($dateTime->format('Y') . ($weekNum < 9 ? '0' . $weekNum : $weekNum));
    }

    /**
     * Helper function to get timestamp of first available month day, from given date.
     * If $dayOfMonth is prior to $date, function will returt date within $date month,
     * otherwise function will provide date from next month. If next month does not have
     * enough days as we want to, it will return last date in that month.
     *
     * Example:
     * HTime::nextDayOfMonth(strtotime('2014-01-20'), 21);  //echo strtotime('2014-01-21');
     * HTime::nextDayOfMonth(strtotime('2014-01-22'), 21);  //echo strtotime('2014-02-21');
     * HTime::nextDayOfMonth(strtotime('2014-01-19'), 30);  //echo strtotime('2014-02-28');
     *
     * @param timestamp $date
     * @param int $dayOfMonth
     *
     * @return timestamp
     */
    public static function nextDayOfMonth($date, $dayOfMonth)
    {
        $date = self::toTimestamp($date);
        $refDayOfMonth = date('j', $date);
        $dateInThisMonth = date('t', $date) > $dayOfMonth ?
            strtotime(date("Y-m-{$dayOfMonth}", $date)) :
            strtotime(date('Y-m-t', $date));

        if ($refDayOfMonth <= $dayOfMonth) {
            return $dateInThisMonth;
        }

        return self::getXMonthsToTheFuture($dateInThisMonth);
    }

    /**
     * Helper function to deal with different month length issue.
     * This function will provide timestamp for a same day in next month of given date.
     * If next month does not have so many days for given date, function will return timestamp
     * of last date from that month
     *
     * Example:
     * HTime::getXMonthsToFuture(strtotime('2014-01-30'));      //echo strtotime('2014-02-28');
     * HTime::getXMonthsToFuture(strtotime('2014-01-19'), 2);   //echo strtotime('2014-03-19');
     *
     * @param timestamp $baseTime
     * @param integer $months
     *
     * @return timestamp
     */
    public static function getXMonthsToTheFuture($baseTime, $months = 1)
    {
        $x_monthsToTheFuture = strtotime("+{$months} months", $baseTime);

        $monthBefore = (int)date("m", $baseTime) + 12 * (int)date("Y", $baseTime);
        $monthAfter = (int)date("m", $x_monthsToTheFuture) + 12 * (int)date("Y", $x_monthsToTheFuture);

        if ($monthAfter > $months + $monthBefore) {
            $x_monthsToTheFuture = strtotime(date("Ym01His", $x_monthsToTheFuture) . " -1 day");
        }

        return $x_monthsToTheFuture;
    }

    /**
     * return Ordinal numbers such as 21st, 33rd, etc.,
     * formed by combining a cardinal ten with an ordinal unit.
     *
     * @param integer $number
     *
     * @return string
     */
    public static function addOrdinalNumberSuffix($number)
    {
        $ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');

        if (($number % 100) >= 11 && ($number % 100) <= 13) {
            $abbreviation = $number . 'th';
        } else {
            $abbreviation = $number . $ends[$number % 10];
        }

        return $abbreviation;
    }

    /**
     * Formats input date format to another output date format using DateTime class.
     *
     * @see DateTime
     *
     * @param $inputDate string Input date which will be formatted.
     * @param $inputFormat string Input date format.
     * @param $outputFormat string Output date format.
     * @return string New date which corresponds to $outputFormat.
     */
    public static function changeFormat($inputDate, $inputFormat, $outputFormat)
    {
        $dateTime = DateTime::createFromFormat($inputFormat, $inputDate);

        if ($dateTime) {
            return $dateTime->format($outputFormat);
        }

        return $inputDate;
    }

    /**
     * Convert timestamp to date time param
     *
     * @param integer $datetime
     * @return string
     */
    public static function getAsDateTime($datetime)
    {
        if (empty($datetime)) {
            return '';
        }

        if (is_numeric($datetime)) {
            return date('Y-m-d H:i:s', $datetime);
        }

        return $datetime;
    }

    /**
     * Formats input date format to another output date format using DateTime class.
     *
     * @see DateTime
     *
     * @param $inputDate string Input date which will be formatted.
     * @param $inputFormat string Input date format.
     * @param $outputFormat string Output date format.
     * @return string New date which corresponds to $outputFormat.
     */
    public static function changeDateTimeFormat($inputDate, $inputFormat, $outputFormat)
    {
        $dateTime = DateTime::createFromFormat($inputFormat, $inputDate);

        if ($dateTime) {
            return $dateTime->format($outputFormat);
        }

        return $inputDate;
    }

    public static function getIsFirstDayOfMonth()
    {
        return date('j', time()) === '1';
    }

    public static function getIsMonday()
    {
        return date('D', time()) === 'Mon';
    }

    public static function createDateObjectFromString($date)
    {
        return new DateTime($date, new DateTimeZone(static::DEFAULT_SERVER_TIMEZONE));
    }

    public static function createUtcDateObject($datetime)
    {
        return new DateTime($datetime, new DateTimeZone(self::UTC_TIMEZONE));
    }

    public static function createDateObjectFromTimestamp($timestamp)
    {
        $date = new DateTime();
        $date->setTimezone(new DateTimeZone(static::DEFAULT_SERVER_TIMEZONE));
        $date->setTimestamp($timestamp);

        return $date;
    }

    public static function startOfDay($date)
    {
        $dateWithoutTime = self::createDateObjectFromString($date)->format(self::DATE_FORMAT);

        return self::createDateObjectFromString($dateWithoutTime)->format('U');
    }

    public static function endOfDay($date)
    {
        return self::startOfDay($date) + self::DAY - 1;
    }

    public static function getAge($dob)
    {
        $dobDate = self::createDateObjectFromString($dob);

        $today = self::createDateObjectFromString('now');

        return $today->diff($dobDate);
    }

    public static function getTimeSpanLimits($timeSpan = self::TIME_SPAN_DAY)
    {
        $carbon = new Carbon();

        $methodNames = self::getTimeSpanMethodNames($timeSpan);

        return ['start_time' => call_user_func([$carbon->copy(), $methodNames['start_time']])->toDateTimeString(), 'end_time' => call_user_func([$carbon->copy(), $methodNames['end_time']])->toDateTimeString()];
    }

    public static function getTimeSpanMethodNames($timeSpan = self::TIME_SPAN_DAY)
    {
        $methodNames = [
            self::TIME_SPAN_DAY => [
                'start_time' => 'startOfDay',
                'end_time' => 'endOfDay'
            ],
            self::TIME_SPAN_WEEK => [
                'start_time' => 'startOfWeek',
                'end_time' => 'endOfWeek'
            ],
            self::TIME_SPAN_MONTH => [
                'start_time' => 'startOfMonth',
                'end_time' => 'endOfMonth'
            ],
            self::TIME_SPAN_YEAR => [
                'start_time' => 'startOfYear',
                'end_time' => 'endOfYear'
            ],
        ];
        if (array_key_exists($timeSpan, $methodNames)) {
            return $methodNames[$timeSpan];
        }
        return $methodNames[self::TIME_SPAN_DAY];
    }

    public static function getStartAndEndOfWeek($date, $appendTime = true)
    {
        $currentDate = new DateTime($date);
        $dayOfWeek = $currentDate->format("w");

        $startDate = clone $currentDate->modify("-$dayOfWeek day");
        $endDate = $currentDate->modify("+6 day");

        return [
            $startDate->format(self::SQL_DATE_FORMAT) . ($appendTime ? ' 00:00:00' : ''),
            $endDate->format(self::SQL_DATE_FORMAT) . ($appendTime ? ' 23:59:59' : '')
        ];
    }

    public static function getAdultAgeDate($dob)
    {
        $dobDate = self::createDateObjectFromString($dob);
        $dobDate->add(date_interval_create_from_date_string('18 years'));

        return $dobDate->format(self::SQL_DATE_FORMAT);
    }

    public static function getSecondsAsReadableTime($seconds)
    {
        $time = round($seconds);
        if ($time >= 3600) {
            return sprintf('%02d:%02d:%02d', ($time / 3600), ($time / 60 % 60), $time % 60);
        }
        return sprintf('%02d:%02d:%02d', 0, ($time / 60 % 60), $time % 60);
    }

    public static function getSecondsFromReadableTime($time)
    {
        $timeArray = explode(':', $time);

        $s = array_pop($timeArray);
        $m = array_pop($timeArray);
        $h = array_pop($timeArray);

        return intval($h) * 3600 + intval($m) * 60 + intval($s);
    }

    public static function isWeekend($date)
    {
        return in_array(strtolower($date->format('l')), [self::SATURDAY, self::SUNDAY]);
    }

    public static function getHours($format = self::SQL_DATETIME_FORMAT)
    {
        $timestamp = time();

        $beginOfDay = strtotime("midnight", $timestamp);
        $endOfDay = strtotime("tomorrow", $beginOfDay) - 1;

        $startTime = new \DateTime(TimeHelper::format($beginOfDay, self::SQL_DATETIME_FORMAT));
        $endTime = new \DateTime(TimeHelper::format($endOfDay, self::SQL_DATETIME_FORMAT));
        $interval = new \DateInterval("PT1H");
        $period = new \DatePeriod($startTime, $interval, $endTime);

        $hours = [];
        foreach ($period as $time) {
            $hours[] = $time->format($format);
        }

        return $hours;
    }

    public static function getCurrentWeekDates($format = self::SQL_DATE_FORMAT, $startDate = null, $endDate = null)
    {
        $firstWeekDate = is_null($startDate) ? TimeHelper::format(strtotime('monday this week')) : $startDate;
        $lastWeekDate = is_null($endDate) ? TimeHelper::format(strtotime('sunday this week')) : $endDate;

        return self::getDatesInRange($firstWeekDate, $lastWeekDate, $includeWeekends = true, $format);
    }

    public static function getCurrentMonthDates($format = self::SQL_DATE_FORMAT, $startDate = null, $endDate = null)
    {
        $firstMonthDate = is_null($startDate) ? TimeHelper::format(strtotime('first day of this month')) : $startDate;
        $lastMonthDate = is_null($endDate) ? TimeHelper::format(strtotime('last day of this month')) : $endDate;

        return self::getDatesInRange($firstMonthDate, $lastMonthDate, $includeWeekends = true, $format);
    }

    public static function getCurrentYearMonths($format = self::SQL_DATE_FORMAT)
    {
        $start = (new DateTime(date("Y") . "-01-01"));
        $end = (new DateTime(date("Y") . "-12-31"));
        $interval = new \DateInterval("P1M");
        $period = new \DatePeriod($start, $interval, $end);

        $result = [];
        foreach ($period as $dt) {
            array_push($result, $dt->format($format));
        }

        return $result;
    }

    public static function getMonthsInRange($format, $fromDate, $toDate)
    {
        $fromUnix = strtotime($fromDate);
        $toUnix = strtotime($toDate);

        $fromDate = self::format($fromUnix, 'Y-m-01');
        $toDate = self::format($toUnix, 'Y-m-t');

        $start = (new DateTime($fromDate));
        $end = (new DateTime($toDate));
        $interval = new \DateInterval("P1M");
        $period = new \DatePeriod($start, $interval, $end);

        $result = [];
        foreach ($period as $dt) {
            array_push($result, $dt->format($format));
        }

        return $result;
    }

    public static function getDatesInRange($fromDate, $toDate, $includeWeekends = true, $format = self::SQL_DATE_FORMAT)
    {
        $startDate = new \DateTime($fromDate);
        $endDate = new \DateTime($toDate);
        $endDate->modify('+1 day');
        $interval = new \DateInterval("P1D");
        $period = new \DatePeriod($startDate, $interval, $endDate);

        $dates = [];
        foreach ($period as $date) {
            if ($includeWeekends || !self::isWeekend($date)) {
                $dates[] = $date->format($format);
            }
        }

        return $dates;
    }

    public static function getArrayFromSeconds($seconds)
    {
        $hours = floor($seconds / 3600);
        $minutes = floor(($seconds / 60) % 60);

        return [
            'hours' => $hours,
            'minutes' => $minutes,
            'totalInSeconds' => $seconds
        ];
    }

    public static function getFirstDayOfMonthForYearMonth($ym)
    {
        $year = substr($ym, 0, 4);
        $month = substr($ym, 4, 6);

        return date(self::SQL_DATE_FORMAT, strtotime("{$year}-{$month}-01"));
    }

    public static function getLastDayOfMonthForYearMonth($ym)
    {
        $year = substr($ym, 0, 4);
        $month = substr($ym, 4, 6);

        return date(self::SQL_DATE_FORMAT, strtotime("{$year}-{$month}-01 last day of this month"));
    }

    public static function getFirstDayOfYear($year)
    {
        return date(self::SQL_DATE_FORMAT, strtotime("{$year}-01-01"));
    }

    public static function getLastDayOfYear($year)
    {
        return date(self::SQL_DATE_FORMAT, strtotime("{$year}-12-31"));
    }

    public static function getNextMonth($date)
    {
        return $date->modify('+ 1 month');
    }

    public static function getPreviousMonth($date)
    {
        return $date->modify('- 1 month');
    }

    public static function getNextYear($date)
    {
        return $date->modify('+ 1 year');
    }

    public static function getPreviousYear($date)
    {
        return $date->modify('- 1 year');
    }

    public static function calculateTimePassedInYearsMonthsAndDays($dateFromWhichTimeIsCalculated)
    {
        $dateNow = TimeHelper::createDateObjectFromString('now');
        $dateNow = $dateNow->format('Y-m-d');
        $dateNow = TimeHelper::changeDateTimeFormat($dateNow, TimeHelper::DASH_DATE_FORMAT, TimeHelper::SQL_DATE_FORMAT);
        $dateDob = TimeHelper::changeDateTimeFormat($dateFromWhichTimeIsCalculated, TimeHelper::DASH_DATE_FORMAT, TimeHelper::SQL_DATE_FORMAT);
        $dateFrom = new DateTime($dateDob);
        $dateToday = new DateTime($dateNow);

        $age = $dateFrom->diff($dateToday);

        return [
            'years' => $age->y,
            'months' => $age->m,
            'days' => $age->d
        ];
    }

    public static function getDateRangeByPeriod($period)
    {
        if ($period == TimeHelper::DATERANGE_PERIOD_THIS_YEAR) {
            return [date('Y-01-01'), date('Y-12-31')];
        }

        if ($period == TimeHelper::DATERANGE_PERIOD_THIS_MONTH) {
            return [date('Y-m-01'), date('Y-m-t')];
        }

        if ($period == TimeHelper::DATERANGE_PERIOD_THIS_WEEK) {
            return TimeHelper::getStartAndEndOfWeek(date('Y-m-d h:i:s'), false);
        }

        if ($period == TimeHelper::DATERANGE_PERIOD_TODAY) {
            return [date('Y-m-d'), date('Y-m-d')];
        }

        $dates = explode(' - ', $period);
        $startDate = TimeHelper::changeDateTimeFormat($dates[0], TimeHelper::DATE_FORMAT, TimeHelper::SQL_DATE_FORMAT);
        $endDate = TimeHelper::changeDateTimeFormat($dates[1], TimeHelper::DATE_FORMAT, TimeHelper::SQL_DATE_FORMAT);

        return [date("{$startDate}"), date("{$endDate}")];
    }

    public function getDateRangePickerValue()
    {
        $dateRange = $this->getDateRangeByPeriod();

        $startDate = TimeHelper::changeDateTimeFormat($dateRange[0], TimeHelper::SQL_DATETIME_FORMAT, TimeHelper::DATE_FORMAT);
        $endDate = TimeHelper::changeDateTimeFormat($dateRange[1], TimeHelper::SQL_DATETIME_FORMAT, TimeHelper::DATE_FORMAT);

        return [$startDate, $endDate];
    }

    /**
     * Formats SQL time format (00:00:00.0000000 through 23:59:59.9999999) to another output date format.
     *
     * @param $inputDate string Input date which will be formatted.
     * @param $outputFormat string Output date format.
     * @return string New date which corresponds to $outputFormat.
     */
    public static function changeSqlTimeFormat($inputDate, $outputFormat)
    {
        $formattedDate = strstr($inputDate, '.', true);
        return static::changeFormat($formattedDate, self::SQL_TIME_FORMAT, $outputFormat);
    }

    public static function validateDate($date, $format)
    {
        return (DateTime::createFromFormat($format, $date) !== false);
    }

    public static function getStartOfDay($date)
    {
        return "{$date} 00:00:00";
    }

    public static function getEndOfDay($date)
    {
        return "{$date} 23:59:59";
    }
}
