<?php
/**
 * TimeProfilerHelper.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 08-Dec-15
 * Time: 20:42
 */

namespace common\helpers;


use Yii;

class TimeProfilerHelper
{
    const MAX_REQUEST_TIME = 30;

    protected static $_timerEvents = [];
    protected static $_timerStarted = false;

    protected static function init()
    {
        if (static::$_timerStarted) {
            return;
        }

        static::$_timerStarted = true;

        register_shutdown_function(function() {
            $sendList = false;
            $flushList = [];

            $firstEvent = current(static::$_timerEvents);
            $lastEvent = end(static::$_timerEvents);

            $previousActionTime = $firstEvent['time'];

            $lastEventDuration = abs(time() - $lastEvent['time']);

            $totalDuration = abs($lastEvent['time'] - $firstEvent['time']) + $lastEventDuration;

            if ($totalDuration >= self::MAX_REQUEST_TIME) {
                $sendList = true;
            }

            foreach (static::$_timerEvents as $event) {

                $duration = abs($event['time'] - $previousActionTime);

                if ($duration >= $event['tolerance']) {
                    $sendList = true;
                }

                $flushList[] =
                    sprintf("[T: %s | Diff: %s (s)] %s # %s | info: %s",
                        $event['time'],
                        $duration,
                        $event['name'],
                        $event['action'],
                        $event['message']
                    );

                $previousActionTime = $event['time'];
            }

            if ($sendList) {

                $flushList[] = "Time from last event until send: {$lastEventDuration} second(s)";
                $flushList[] = "Total duration: {$totalDuration} second(s)";

                MailHelper::appSendMailNotification(
                    'appAdminEmail',
                    [
                        'text' => 'Failed events while performing request',
                        'type' => 'Failed TimeProfiler events',
                        'additionalData' => $flushList
                    ],
                    'Failed TimeProfiler events'
                );
            }
        });
    }

    public static function addEvent($event, $action, $message, $timeout = 10)
    {
        static::init();

        static::$_timerEvents[] = [
            'name' => $event,
            'action' => $action,
            'time' => time(),
            'tolerance' => $timeout,
            'message' => $message
        ];
    }
}