<?php
/**
 * Created by PhpStorm.
 * User: Nikola Radovic <nikola@2amigos.us>
 * Date: 5/13/2016
 * Time: 10:41 PM
 */

namespace common\helpers;


class ConfigHelper
{
    private static $_config = [];

    public static function getConfig($section, $key)
    {
        $config = static::getConfigArray();

        $sectionConfig = ArrayHelper::getValue($config, $section);
        if (!isset($sectionConfig[$key])) {
            throw new \Exception("'{$section}' configuration is missing!");
        }

        return $sectionConfig[$key];
    }

    protected static function getConfigArray()
    {
        if (!empty(static::$_config)) {
            return static::$_config;
        }

        $path = static::getConfigFilePath();

        if (!file_exists($path)) {
            return [];
        }

        return static::$_config = parse_ini_file($path, true);
    }

    protected static function getConfigFilePath()
    {
        $ds = DIRECTORY_SEPARATOR;

        return realpath(\Yii::getAlias('@common') . $ds . '..' . $ds . '..') . $ds . 'config.txt';
    }

}