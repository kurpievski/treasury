<?php
namespace app\helpers;

use yii;

class Html extends yii\helpers\Html
{
    public const ICON_FONT_HS = 'hs-icon hs-icon-';
    public const ICON_FONT_FA = 'fa fa-';

    public static function faIcon($icon)
    {
        return static::tag('i', '', ['class' => 'fa fa-'.$icon]);
    }

    public static function hsIcon($icon)
    {
        return static::tag('i', '', ['class' => 'hs-icon hs-icon-'.$icon]);
    }

    public static function icon($icon, $prefix = '')
    {
        return static::tag('i', '', ['class' => $prefix.$icon]);
    }

    public static function imgCaption($img, $caption, $captionClass = 'g-top-15 g-left-15')
    {
        return join("\n", $imgCaption = [
            static::beginTag('figure', ['class' => 'g-pos-rel']),
            $img,
            static::beginTag('figurecaption', ['class' => 'g-pos-abs '.$captionClass]),
            $caption,
            static::endTag('figurecaption'),
            static::endTag('figure'),
        ]);
    }

    public static function btn($text, $url) {
        return static::tag('a', $text, ['href' => $url, 'class' => 'btn rounded-0 x-btn x-btn-black']);
    }

    public static function paragraph($paragraphs) {
        if (!is_array($paragraphs)) {
            $paragraphs = [$paragraphs];
        }

        $html = [];

        foreach ($paragraphs as $paragraph) {
            $html[] = static::beginTag('p').$paragraph.static::endTag('p');
        }

        return join("\n", $html);
    }

    public static function a($text, $url = null, $options = [])
    {
        if ($text instanceof yii\web\Link) {
            /* @var $text Link */
            $options = $url;
            $url = $text->href;
            $text = $text->title;
        }

        return parent::a($text, $url, $options);
    }
}
