<?php
namespace common\helpers;

use Yii;
use yii\widgets\ActiveForm;

/**
 * Class InputMaskHelper
 * @package common\components
 */
class MaskedInputHelper
{
    /**
     * Formatting rules for the masks. 'onUnMask' contains conversion to SQL format and 'onBeforeMask' contains
     * code to convert it back.
     */
    const MASKS = [
        TimeHelper::MOMENT_TIME_FORMAT => [
            'mask' => '99-99-9999',
            'onUnMask' =>
                'function(maskedValue, unmaskedValue) {
                    if (maskedValue.length > 0) {
                        $(".maskedinput").addClass("maskedinput-disabled");
                        date = maskedValue.split("-");
                        elements = [date[2], date[0], date[1]];
                        return elements.join("-");
                    }
                }',
            'onBeforeMask' =>
                'function(value, opts) {
                    if (value.length > 0) {
                        $(".maskedinput").removeClass("maskedinput-disabled");
                        date = value.split("-");
                        elements = [date[1], date[2], date[0]];
                        return elements.join("-");
                    }
                }',
        ],
    ];

    /**
     * Blocks and unblocks the maskendinputs when the value is unmasked. If there are no errors, the value is being
     * sent as unmasked (SQL). If the form has some custom processing, the logic from here should be copied there.
     */
    public static function registerJs($form)
    {
        $formId = '#'.$form->getId();

        Yii::$app->getView()->registerJs(sprintf('
            $("%s").submit(function () {
                $(".maskedinput").addClass("maskedinput-disabled");
                return true;
            });
        
            $("%s").on("afterValidate", function (event, attribute, messages, deferreds) {
                if (messages.length == 0) {
                    $(".maskedinput").inputmask().unmask();
                } else {
                    $(".maskedinput").removeClass("maskedinput-disabled");
                }
            });
        ', $formId, $formId));
    }

    /**
     * @param ActiveForm $form
     * @param string $mask
     * @param array $options
     * @return array
     */
    public static function getConfig(ActiveForm $form, string $mask, array $options)
    {
        static::registerJs($form);

        if (isset($options['class'])) {
            $options['class'] .= ' maskedinput maskedinput-disabled';
        } else {
            $options['class'] = 'maskedinput maskedinput-disabled';
        }

        if (!isset($options['placeholder'])) {
            $options['placeholder'] = $mask;
        }

        return [
            'mask' => static::getMask($mask),
            'options' => $options,
            'clientOptions' => [
                'onUnMask' => static::getOnUnMask($mask),
                'onBeforeMask' => static::getOnBeforeMask($mask),
            ],
        ];
    }

    /**
     * @param string $mask
     * @return string
     */
    public static function getMask(string $mask)
    {
        return static::getMaskAttribute($mask, 'mask');
    }

    /**
     * @param string $mask
     * @return string
     */
    public static function getOnUnMask(string $mask)
    {
        return static::getMaskAttribute($mask, 'onUnMask');
    }

    /**
     * @param string $mask
     * @return string
     */
    public static function getOnBeforeMask(string $mask)
    {
        return static::getMaskAttribute($mask, 'onBeforeMask');
    }

    /**
     * @param string $mask
     * @param string $attribute
     * @return mixed
     */
    public static function getMaskAttribute(string $mask, string $attribute)
    {
        return static::MASKS[$mask][$attribute];
    }
}