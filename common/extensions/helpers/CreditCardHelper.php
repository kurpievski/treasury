<?php

namespace common\helpers;
/**
 * May 7, 2015
 * 9:12:15 PM
 * @author Nikola Radovic <nikola@2amigos.us>
 */
class CreditCardHelper
{
    const TYPE_UNKNOWN = 'unknown';

    public static function getList()
    {
        return [
            'amex' => 'American Express',
            'diners_club_carte_blanche' => 'Diners Club Carte Blanche',
            'diners_club_international' => 'Diners Club International',
            'discover' => 'Discover Card',
            'jcb' => 'JCB',
            'laser' => 'Laser',
            'maestro' => 'Maestro',
            'mastercard' => 'MasterCard',
            'visa' => 'Visa',
            'visa_electron' => 'Visa Electron',
        ];
    }

    public static function getValidators()
    {
        return [
            'amex' => [
                'pattern' => '/^3[47]/',
                'length' => [15]
            ],
            'diners_club_carte_blanche' => [
                'pattern' => '/^30[0-5]/',
                'length' => [14]
            ],
            'diners_club_international' => [
                'pattern' => '/^36/',
                'length' => [14]
            ],
            'jcb' => [
                'pattern' => '/^35(2[89]|[3-8][0-9])/',
                'length' => [16]
            ],
            'laser' => [
                'pattern' => '/^(6304|670[69]|6771)/',
                'length' => [16, 17, 18, 19]
            ],
            'visa_electron' => [
                'pattern' => '/^(4026|417500|4508|4844|491(3|7))/',
                'length' => [16]
            ],
            'visa' => [
                'pattern' => '/^4/',
                'length' => [16]
            ],
            'mastercard' => [
                'pattern' => '/^5[1-5]/',
                'length' => [16]
            ],
            'maestro' => [
                'pattern' => '/^(5018|5020|5038|6304|6759|676[1-3])/',
                'length' => [12, 13, 14, 15, 16, 17, 18, 19]
            ],
            'discover' => [
                'pattern' => '/^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/',
                'length' => [16]
            ]
        ];
    }

    public static function getCardType($number)
    {
        foreach (self::getValidators() as $name => $validator) {
            if (preg_match($validator['pattern'], $number)) {
                return $name;
            }
        }
        return self::TYPE_UNKNOWN;
    }

    public static function isCardExpiredForTimestamp($cardExpirationMonth, $cardExpirationYear, $checkTimestamp)
    {
        $cardExpirationTimestamp = strtotime(sprintf('last day of %04d-%02d', $cardExpirationYear, $cardExpirationMonth));

        return $checkTimestamp >= $cardExpirationTimestamp;
    }

    public static function formatLastDigitsMask($lastDigits)
    {
        return "xxxx-xxxx-xxxx-{$lastDigits}";
    }
}
