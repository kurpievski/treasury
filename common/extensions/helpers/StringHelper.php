<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 24.1.2017.
 * Time: 16:20
 */

namespace common\helpers;


class StringHelper extends \yii\helpers\StringHelper
{
    const NONE_VALUE = 'none';
    const UNKNOWN_VALUE = 'unknown';

    const SELECT_ALL_VALUE = 'multiselect-all';

    public static function rTrimAfterFirstOccurrence($string, $char)
    {
        if (strpos($string, $char) === false) {
            return trim($string);
        }

        return trim(substr($string, 0, strpos($string, $char)));
    }

    public static function lTrimAfterFirstOccurrence($string, $char)
    {
        if (strpos($string, $char) === false) {
            return trim($string);
        }

        return trim(substr($string, strpos($string, $char) + 1));
    }

    public static function insertChar($string, $char, $position)
    {
        return substr($string, 0, $position) . $char . substr($string, $position);
    }

    public static function stringToHex($string)
    {
        $hexstr = unpack('H*', $string);
        return array_shift($hexstr);
    }

    public static function replaceFirst($haystack, $needle, $replace)
    {
        $pos = strpos($haystack, $needle);
        if ($pos !== false) {
            return substr_replace($haystack, $replace, $pos, strlen($needle));
        }

        return $haystack;
    }

    public static function getPhoneFormatted($phoneNumber)
    {
        if (empty($phoneNumber)) {
            return '';
        }
        $phoneNumber = preg_replace("/[^0-9]/", "", $phoneNumber);
        return '(' . substr($phoneNumber, 0, 3) . ') ' . substr($phoneNumber, 3, 3) . '-' . substr($phoneNumber, 6);
    }

    public static function getSSNFormatted($ssn)
    {
        if (empty($ssn)) {
            return '';
        }
        $ssn = preg_replace("/[^0-9]/", "", $ssn);
        return substr($ssn, 0, 3) . '-' . substr($ssn, 3, 2) . '-' . substr($ssn, 5);
    }

    public static function hasSubstring($string, $substring)
    {
        return strpos(strtolower($string), strtolower($substring)) !== false;
    }

    public static function getStopValues()
    {
        return [
            self::NONE_VALUE,
            self::UNKNOWN_VALUE,
        ];
    }

    public static function getIsValidRegex($string)
    {
        return @preg_match($string, '') !== FALSE;
    }

    public static function clearOccurrences($subject, $stringArray)
    {
        foreach ($stringArray as $string) {
            $subject = str_replace($string, '', $subject);
        }

        return $subject;
    }

    public static function clearSpecialChars($string)
    {
        $string = str_replace(' ', '-', $string);

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    }

    public static function splitSentences($text)
    {
        if (strpos($text, '.') !== false) {
            return preg_split('/(?<=[.?!])\s+(?=[a-z])/i', $text);
        }

        //split by comma is there are not dots
        return preg_split('/(?<=[,?!])\s+(?=[a-z])/i', $text);
    }

    public static function validateJson($string)
    {
        $jsonArray = @json_decode($string, true);

        return json_last_error() == JSON_ERROR_NONE && is_array($jsonArray) ? $jsonArray : null;
    }

    public static function normalizeIds(&$ids, $emptyOnSelectAll = false)
    {
        if (empty($ids)) {
            $ids = [];
        }

        if (!is_array($ids)) {
            if (is_string($ids) && strpos($ids, ',') !== false) {
                $ids = explode(',', $ids);
            } else {
                $ids = [$ids];
            }
        }

        if (($key = array_search(self::SELECT_ALL_VALUE, $ids)) !== false) {
            unset($ids[$key]);

            if ($emptyOnSelectAll) {
                $ids = [];
            }
        }
    }

    public static function formatAddress(array $address) : string
    {
        if (empty($address['address']) && empty($address['city']) && empty($address['state']) && empty($address['zip'])) {
            return '';
        }

        $fullAddress = $address['address'] . ' ' . $address['city'] . ' ' . $address['state'] . ' ' . $address['zip'] . ' USA';

        return preg_replace("/ \s+ | , /", '', $fullAddress);
    }
}