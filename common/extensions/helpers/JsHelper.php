<?php
/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 **/

namespace common\helpers;


use Yii;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;

class JsHelper
{
    protected static $debugEnvironments = ['local'];

    protected static $configs = [];
    protected static $pageConfigs = [];
    protected static $templates = [];
    protected static $debugData = [];

    const DEBUG_CONFIG = 'debug';
    const PAGE_CONFIG = 'pages';
    const APP_CONFIG = 'app';
    const TEMPLATE_CONFIG = 'templates';

    public static function getGlobalVars()
    {
        return [
            'csrfToken' => Yii::$app->request->getCsrfToken(),
            'urlSaveTripCustomer' => Url::to('/trip/save-trip'),
            'urlDeleteTrip' => Url::to('/trip/delete'),
            'urlListTrip' => Url::to('/trip/index'),
            'urlTripDetail' => Url::to('/trip/detail'),
            'urlNewCMSRow' => Url::to('/trip/new-signature'),
            'urlAjaxResetDatabase' => Url::to('/folder/reset'),
            'urlAjaxLoadListFolder' => Url::to('/folder/index'),
            'urlAjaxRunFolder' => Url::to('/folder/run'),
            'urlAjaxLoadZollData' => Url::to('/trip/load-zoll'),
            'urlAjaxApprovalDashboard' => Url::to('/trip/approval-dashboard'),
            'urlAjaxLoadTripTab' => Url::to('/trip/load-tab'),
            'controller' => [
                'route' => Yii::$app->requestedRoute,
                'id' => Yii::$app->controller->id,
                'action' => [
                    'id' => Yii::$app->controller->action->id
                ]
            ]
        ];
    }

    public static function configure($name, $config)
    {
        static::$configs[$name] = $config;
    }

    public static function configurePage($name, $config)
    {
        static::$pageConfigs[$name] = $config;
    }

    public static function getInitJs()
    {
        $configs = static::applyDefaultConfig(static::$configs);

        $jsConfig = Json::encode($configs);

        $runFunction = Yii::$app->request->isAjax || Yii::$app->request->isPjax ? 'runAjax' : 'run';

        return ";\n$(function() {  window.EmsmartMain.app.{$runFunction}({$jsConfig}) });";
    }

    public static function getGlobalVarJS()
    {
        $js = "";

        foreach (static::getGlobalVars() as $globalVar => $value) {
            $js .= "var \${$globalVar} = " . Json::encode($value) . ";\n";
        }

        return $js;
    }

    public static function registerTemplate($templateId, $template)
    {
        static::$templates[$templateId] = $template;
    }

    public static function logDebug($name, $data)
    {
        if (!YII_DEBUG && YII_ENV !== 'stage') {
            return;
        }

        static::$debugData['logs'][$name] = $data;
    }

    protected static function applyDefaultConfig($configs)
    {
        if (empty($configs[self::PAGE_CONFIG])) {

            $currentModuleId = Yii::$app->controller->module->id;
            $appId = Yii::$app->id;

            $configs[self::PAGE_CONFIG] = [
                'moduleId' => $appId === $currentModuleId ? null : $currentModuleId,
                'controllerId' => Yii::$app->controller->id,
                'actionId' => Yii::$app->controller->action->id,
                'route' => Yii::$app->controller->id . '/' . Yii::$app->controller->action->id,
                'isAjax' => Yii::$app->request->isAjax,
                'isPjax' => Yii::$app->request->isPjax,
                'configs' => (object)static::$pageConfigs
            ];
        }

        if (empty($configs[self::APP_CONFIG])) {
            $configs[self::APP_CONFIG] = [
                'environment' => YII_ENV,
                'debugMode' => in_array(YII_ENV, static::$debugEnvironments)
            ];
        }

        if (empty($configs[self::TEMPLATE_CONFIG])) {
            $configs[self::TEMPLATE_CONFIG] = (object)static::$templates;
        }

        if (empty($configs[self::DEBUG_CONFIG])) {
            $configs[self::DEBUG_CONFIG] = (object)static::$debugData;
        }

        return $configs;
    }
}