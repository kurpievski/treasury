<?php
/**
 * Created by PhpStorm.
 * User: Nikola Radovic <nikola@2amigos.us>
 * Date: 5/25/2016
 * Time: 1:03 AM
 */

namespace common\helpers;

class NumberHelper
{
    public static function formatBytes($bytes, $precision = 2)
    {
        $intBytes = intval($bytes);

        $base = $intBytes > 0 ? log((int)$bytes, 1024) : $intBytes;

        $suffixes = array('', 'K', 'M', 'G', 'T');

        return round(pow(1024, $base - floor($base)), $precision) . ' ' . ArrayHelper::getValue($suffixes, (int)abs(floor($base)), '');
    }

    public static function formatSeconds($seconds, $format = "i:s")
    {
        return gmdate($format, $seconds);
    }

    public static function isNumeric($string){
        return is_numeric($string) && strpos($string, 'E') === FALSE;
    }
}