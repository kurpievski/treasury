<?php
/**
 * Email: srdjan.drakul@2amigos.us
 * Date: 12/4/2017
 * Time: 5:18 PM
 */

namespace common\helpers;

use Yii;
use yii\db\Transaction;

class TransactionHelper
{
    public static function commitAll(Transaction $transaction)
    {
        while ($transaction->getIsActive()) {
            $transaction->commit();
        }
    }

    public static function rollbackAll(Transaction $transaction)
    {
        while ($transaction->getIsActive()) {
            $transaction->rollBack();
        }
    }

    public static function run($callback, $db = null)
    {
        $db = $db ?: Yii::$app->db;
        $t = $db->beginTransaction();

        try {
            $result = $callback();
            static::commitAll($t);
            return $result;
        } catch (\Exception $e) {
            static::rollbackAll($t);
            throw $e;
        }
    }
}