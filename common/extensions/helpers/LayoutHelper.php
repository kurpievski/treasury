<?php
/**
 * Boris Matic <boris.matic@2amigos.us>
 * Company: 2amigOS! <https://2amigos.us>
 */

namespace common\helpers;


use Yii;

class LayoutHelper
{
    public static function getPageClass()
    {
        return 'page-' . Yii::$app->controller->id . '-' . Yii::$app->controller->action->id;
    }
}