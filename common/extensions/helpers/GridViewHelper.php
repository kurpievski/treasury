<?php
namespace common\helpers;

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use Yii;
use yii\web\View;

/**
 * Created by PhpStorm.
 * User: Aleksandar Panic
 * Date: 02-Jun-15
 * Time: 19:36
 */
class GridViewHelper
{
    public static function getStandardGridConfig($mergeWith = [])
    {
        return ArrayHelper::merge([
            'responsive' => true,
            'striped' => true,
            'bootstrap' => true,
            'hover' => true,
            'floatHeader' => ! BrowserHelper::isInternetExplorer(),
            'floatHeaderOptions' => [
                'scrollingTop' => 85
            ],
            'pjax' => true,
        ], $mergeWith);
    }

    public static function getExportGridConfig(View $view, $exportGridName, $mergeWith = [])
    {
        if (empty($mergeWith['toolbar'])) {
            $mergeWith['toolbar'] = [
                '{export}'
            ];
        }

        $exportFileName = PDFExportHelper::getDefaultPDFFileName($exportGridName, false);

        return static::getStandardGridConfig(ArrayHelper::merge([
            'panel' => [
                'heading' => $exportGridName,
                'type' => GridView::TYPE_DEFAULT,
            ],
            'export' => GridViewHelper::getGridExportConfig($view) + ['filename' => $exportFileName],
            'exportConfig' => GridViewHelper::getGridExportItemsConfig($exportGridName, $exportFileName),
        ], $mergeWith));
    }

    public static function getGridExportConfig(View $view, $exportButtonId = 'export-button')
    {
        $view->registerJs("
        $(document).on('pjax:complete', function() {
          $('#{$exportButtonId}').dropdown();
        });");

        return [
            'showConfirmAlert' => false,
            'options' => [
                'id' => $exportButtonId
            ],
            'target' => ExportMenu::TARGET_SELF,
            'fontAwesome' => true
        ];
    }

    public static function getGridExportItemsConfig($exportTitle, $exportFileName, $isFa = true)
    {
        return $defaultExportConfig = [];
    }
}