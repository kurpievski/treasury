<?php
namespace common\helpers;

use Yii;
use common\models\Image;

class ImageHelper 
{
    public static function saveInTransaction($uploadedFiles)
    {
        if(empty($uploadedFiles)) {
            return false;
        }

        $imageIds = [];
        $transaction = Yii::$app->db->beginTransaction();
        try {
            /** @var Image[] $images */
            foreach ($uploadedFiles as $uploadedFile) {
                $image = new Image();
                $image->file = $uploadedFile;

                if(!$image->save()) {
                    throw new \Exception('Unable to save images. ' . implode('<br>', $image->getFirstErrors()));
                }
                
                $imageIds[] = $image->id;
            }

            $transaction->commit();
        } catch (\Exception $ex) {
            $transaction->rollback();
            throw new \Exception('Unable to save images. ' . $ex->getMessage());
        }

        return $imageIds;
    }

    public static function getImagesHash ($images)
    {
        $hash = "";
        /** @var \common\models\Image $image */
        foreach ($images as $image) {
            $hash .= $image->update_time . $image->id;
        }

        return md5($hash);
    }
}