<?php
/**
 * Created by PhpStorm.
 * User: vprorok
 * Date: 1/15/2016
 * Time: 4:23 AM
 */

namespace common\helpers;


use yii\db\Exception;
use console\helpers\TimingHelper;
use common\models\Attribute;
use common\models\AttributeValue;
use common\models\DocumentType;
use common\models\DocumentTypeAttribute;

class AttributeHelper
{
    protected static $_linkedAttributesMap = [];

    protected $attributesIdsByType = [];

    protected $cachedAttributes = [];
    protected $cachedAttributesByName = [];

    protected $integerAttributes = null;
    protected $numericStringAttributes = null;
    protected $floatAttributes = null;

    protected static $helperInstance;

    protected static $_instance;

    public function hasPredefinedValues($attributeId)
    {
        $attribute = $this->getAttribute($attributeId);
        if (empty($attribute)) {
            throw new InvalidAttributeIdException('invalid attribute');
        }
        return $attribute->hasPredefinedValues();
    }

    public function getAttributeValueText($attributeId, $valueId)
    {
        $attribute = $this->getAttribute($attributeId);

        $text = $attribute->getAttributeValueLabel($valueId);

        return $text;
    }

    public function isValidAttributeValue($attributeId, $value)
    {
        if ($this->isNumericAttribute($attributeId)) {
            if (is_array($value)) {
                foreach ($value as $v) {
                    if (!is_numeric($v)) {
                        return false;
                    }
                }
            } else if (is_string($value)) {
                $values = explode(',', $value);
                foreach ($values as $v) {
                    if (!is_numeric($v)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    protected $_nonDropDownAttributeIds = null;

    public function isNumericNonDropDownAttribute($attributeId)
    {
        if ($this->_nonDropDownAttributeIds === null) {
            $this->_nonDropDownAttributeIds = Attribute::find()
                ->where(['input_type' => Attribute::INPUT_TYPE_DROPDOWN])
                ->select('id')
                ->column();
        }

        if (in_array($attributeId, $this->_nonDropDownAttributeIds)) {
            return false;
        }

        return $this->isIntegerAttribute($attributeId) || $this->isFloatAttribute($attributeId);
    }

    public function isNumericAttribute($attributeId)
    {
        return $this->isIntegerAttribute($attributeId) || $this->isFloatAttribute($attributeId);
    }

    public function isIntegerAttribute($attributeId)
    {
        return in_array($attributeId, $this->getIntegerAttributes());
    }

    public function isFloatAttribute($attributeId)
    {
        return in_array($attributeId, $this->getFloatAttributes());
    }

    public function isNumericStringAttribute($attributeId)
    {
        return in_array($attributeId, $this->getNumericStringAttributes());
    }

    private function getIntegerAttributes()
    {
        if ($this->integerAttributes === null) {
            $this->integerAttributes = $this->getAttributeIdsByValueType(Attribute::VALUE_TYPE_INTEGER);
        }
        return $this->integerAttributes;
    }

    private function getFloatAttributes()
    {
        if ($this->floatAttributes === null) {
            $this->floatAttributes = $this->getAttributeIdsByValueType(Attribute::VALUE_TYPE_FLOAT);
        }

        return $this->floatAttributes;
    }

    private function getNumericStringAttributes()
    {
        if ($this->numericStringAttributes === null) {
            $this->numericStringAttributes = $this->getAttributeIdsByValueType(Attribute::VALUE_TYPE_NUMERIC_STRING);
        }
        return $this->numericStringAttributes;
    }

    public function isValueOfType($attributeId, $type)
    {
        $attribute = $this->getAttribute($attributeId);
        return $attribute->isValueOfType($type);
    }


    public function isDateAttribute($attributeId)
    {
        $attribute = $this->getAttribute($attributeId);
        return $attribute->isDateAttribute();
    }

    /**
     * @return AttributeHelper
     */
    public static function getInstance()
    {
        if (empty(static::$helperInstance)) {
            static::$helperInstance = new AttributeHelper();
        }
        return static::$helperInstance;
    }

    private function getAttributeIdsByValueType($valueType)
    {
        if (empty($this->attributesIdsByType[$valueType])) {
            $this->attributesIdsByType[$valueType] = Attribute::find()->where(['value_type' => $valueType])->select('id')->column();
        }
        return $this->attributesIdsByType[$valueType];
    }

    public function getAttribute($attributeId)
    {
        if (empty($this->cachedAttributes[$attributeId])) {
            $allAttributes = Attribute::findWithNoScope()->all();
            foreach ($allAttributes as $attribute) {
                $this->cachedAttributes[$attribute->id] = $attribute;
                $this->cachedAttributesByName[$attribute->name] = $attribute;
            }
        }
        return $this->cachedAttributes[$attributeId];
    }

    public static function getAttributeIdsByLabel($labels)
    {
        return Attribute::find()->where([
            'name' => $labels
        ])->select('id')->column();
    }

    public static function getAttributeIdByKey($key)
    {
        return Attribute::findWithNoScope()->where([
            'key' => $key
        ])->select('id')->scalar();
    }

    public static function getStatusAttributeId()
    {
        static $attributeId = null;

        if ($attributeId === null) {
            $attributeId = static::getAttributeIdByKey(Attribute::KEY_STATUS);
        }

        return $attributeId;
    }

    public function getByName($name)
    {
        if (empty($this->cachedAttributesByName[$name])) {
            $attribute = Attribute::find()->where(['name' => $name])->one();
            if ($attribute) {
                $this->cachedAttributes[$attribute->id] = $attribute;
                $this->cachedAttributesByName[$attribute->name] = $attribute;
            }
        }
        return empty($this->cachedAttributesByName[$name]) ? null : $this->cachedAttributesByName[$name];
    }

    public function extractValue(array $attribute = [])
    {
        if ($attribute['input_type'] == Attribute::INPUT_TYPE_DROPDOWN) {
            $indexedArray = ArrayHelper::index($attribute['attributeValues'], 'id');

            return array_key_exists('value', $attribute) ? ArrayHelper::getValue($indexedArray, [$attribute['value'], 'value_label'], '') : '';
        }

        return ArrayHelper::getValue($attribute, 'value', '');
    }

    public static function getMetaData(Attribute $attribute, DocumentTypeAttribute $documentTypeAttribute = null)
    {
        return $attribute->getAttributes() + [
                'meta' => [
                    'is_mandatory' => !empty($documentTypeAttribute) ? $documentTypeAttribute->isMandatory() : false,
                    'is_readonly' => $attribute->isReadOnly()
                ],
                'attributeValues' => !empty($documentTypeAttribute) ?
                    $documentTypeAttribute->getTypeAttributeValues($attribute) : $attribute->getKeyValuePairs()
            ];
    }

    public static function getUniquePaymentAttributes()
    {
        return [
            'amount',
            'check-number',
            'client',
            'check-date'
        ];
    }

    private static $_attrMap = [];

    public static function getPaymentAttributesMap()
    {
        if (!empty(static::$_attrMap)) {
            return static::$_attrMap;
        }

        return static::$_attrMap = Attribute::find()->where([
            'id' => DocumentTypeAttribute::find()->select('attribute_id')->where([
                'document_type_id' => DocumentType::find()->select('id')->where(['key' => DocumentType::KEY_PAYMENTS])
            ])
        ])->select('id, key')->indexBy('key')->column();
    }

    private static $_attrValMap = [];

    public static function getPaymentAttributeValuesMap()
    {
        if (!empty(static::$_attrValMap)) {
            return static::$_attrValMap;
        }

        return static::$_attrValMap = AttributeValue::find()->where([
            'attribute_id' => DocumentTypeAttribute::find()->select('attribute_id')->where([
                'document_type_id' => DocumentType::find()->select('id')->where(['key' => DocumentType::KEY_PAYMENTS])
            ])
        ])->select('id, value')->indexBy('value')->column();
    }

    public static function getPaymentAttributeValueKeyById($id)
    {
        $key = array_search($id, static::getPaymentAttributeValuesMap());

        return $key !== false ? $key : $id;
    }

    public static function getLinkedRangeAttributeMap()
    {
        if (!empty(static::$_linkedAttributesMap)) {
            return static::$_linkedAttributesMap;
        }

        static::$_linkedAttributesMap = ArrayHelper::map(Attribute::find()
            ->where(['is not', 'linked_with_id', null])
            ->select(['linked_with_id', 'id'])
            ->asArray()
            ->all(), 'id', 'linked_with_id');

        return static::$_linkedAttributesMap;
    }

    /** @return Attribute[] */
    public static function getKeyAttributeMap($keys)
    {
        return ArrayHelper::index(Attribute::findByKey($keys)->all(), 'key');
    }

    /** @return Attribute[] */
    public static function getKeyAttributeMapForDocumentTypeId($keys, $documentTypeId)
    {
        return ArrayHelper::index(Attribute::findByKeyAndDocumentTypeId($keys, $documentTypeId)->all(), 'key');
    }

}

class InvalidAttributeIdException extends Exception
{

}