<?php
namespace app\helpers;

use Yii;
use yii\web\Link;

class Links
{
    private $links = [];

    public function register($id, $options)
    {
        $this->links[$id] = new Link($options);
    }

    public function registerArray($links)
    {
        foreach ($links as $id => $options) {
            $this->register($id, $options);
        }
    }

    public function get($ids) {
        if (is_array($ids)) {
            $links = [];
            foreach ($ids as $id) {
                $links[] = $this->links[$id];
            }

            return $links;
        } else {
            return $this->links[$ids];
        }
    }
}
