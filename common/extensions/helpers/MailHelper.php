<?php

namespace common\helpers;

use Yii;

class MailHelper
{
    public static function sendMessage($view, $to, $subject, $params = [], $attachments = [])
    {
        $message = Yii::$app->mailer->compose($view, $params)
            ->setFrom(\Yii::$app->params['adminEmail'])
            ->setTo($to)
            ->setSubject(EnvHelper::getEnvName() . ' ' . $subject);

        foreach ($attachments as $attachmentName) {
            $message->attach($attachmentName);
        }

        return $message->send();
    }
}