<?php

namespace common\helpers;

class MoneyHelper
{
    public static function format($number, $withCurrency = true, $decimals = 2)
    {
        if (empty($number) && !is_numeric($number)) {
            return '';
        }

        return ($withCurrency ? '$' : '') . \Yii::$app->formatter->asDecimal(floatval($number), $decimals);
    }
}
