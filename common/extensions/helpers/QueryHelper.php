<?php

namespace common\helpers;

use yii\db\ActiveQuery;

class QueryHelper
{
    public static function parseNumberToQuery($number, $attribute, ActiveQuery $query)
    {
        if (is_numeric($number)) {
            $query->andWhere([$attribute => $number]);
            return;
        }

        if (empty($number)) {
            return;
        }

        $matches = [];

        if (preg_match('/^(\=|\>|\<|\>\=|\<\=)( +|)(\d+(\.\d+|))$/', $number, $matches)) {
            $operator = $matches[1];
            $value = $matches[3];

            $query->andWhere([$operator, $attribute, $value]);
        }
    }
}