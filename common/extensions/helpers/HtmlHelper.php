<?php
/**
 * Created by PhpStorm.
 * Nikola Radovic <nikola@2amigos.us>
 * Date: 16.12.2016.
 * Time: 00:13
 */

namespace common\helpers;


use yii\helpers\Html;

class HtmlHelper
{
    public static function getBoolIcon($boolValue)
    {
        $iconConfig = ['class' => 'ion ion-close-circled', 'style' => 'color:#f2dede'];

        if ($boolValue) {
            $iconConfig = ['class' => 'ion ion-checkmark-circled', 'style' => 'color:#dff0d8'];
        }

        return Html::tag('i', '', $iconConfig);
    }
}