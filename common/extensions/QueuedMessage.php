<?php
/**
 * QueuedMessage.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 27-Nov-15
 * Time: 23:40
 */

namespace common\extensions;


use yii\swiftmailer\Message;

class QueuedMessage extends Message
{
    public $data = [];

    public function setHtmlBody($html)
    {
        $this->data['htmlBody'] = $html;

        return parent::setHtmlBody($html);
    }

    public function setTextBody($text)
    {
        $this->data['textBody'] = $text;
        return parent::setTextBody($text);
    }

    public function setTo($to)
    {
        $this->data['to'] = $to;
        return parent::setTo($to);
    }

    public function setFrom($from)
    {
        $this->data['from'] = $from;

        return parent::setFrom($from);
    }

    public function setSubject($subject)
    {
        $this->data['subject'] = $subject;

        return parent::setSubject($subject);
    }

    public function setCc($cc)
    {
        $this->data['cc'] = $cc;

        return parent::setCc($cc);
    }

    public function setBcc($bcc)
    {
        $this->data['bcc'] = $bcc;

        return parent::setBcc($bcc);
    }

    public function getSerializedData()
    {
        return serialize($this->data);
    }
}