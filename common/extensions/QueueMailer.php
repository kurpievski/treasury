<?php
/**
 * Mailer.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 27-Nov-15
 * Time: 20:48
 */

namespace common\extensions;

use common\helpers\TimeProfilerHelper;
use UrbanIndo\Yii2\Queue\Job;
use Yii;
use yii\base\InvalidConfigException;

class QueueMailer extends \yii\swiftmailer\Mailer
{
    public $messageClass = 'common\extensions\QueuedMessage';

    public $queueName = 'queue';

    public $route = null;

    /** @var SqsQueue */
    private $_queue =  null;

    public function init()
    {
        $this->route = 'billing-mail/' . YII_ENV;

        if (!empty($this->queueName)) {

            $this->_queue = Yii::$app->get($this->queueName);

            if (!($this->_queue instanceof SqsQueue)) {
                throw new InvalidConfigException("queueName component must be an instance of SqsQueue class");
            }
        }

        parent::init();
    }

    public function sendImmediately($message)
    {
        $this->getTransport()->stop();
        TimeProfilerHelper::addEvent('queue-mailer', 'send-mail-immediate', 'Sending mail.');
        $result = parent::send($message);
        TimeProfilerHelper::addEvent('queue-mailer', 'send-mail-immediate', 'Sending mail finished.', 15);

        return $result;
    }

    public function send($message)
    {
        TimeProfilerHelper::addEvent('queue-mailer', 'send-mail', 'Checking if queue is available.');
        /** @var QueuedMessage $message */
        if (empty($this->_queue) || $this->_queue->isDisabled()) {
            TimeProfilerHelper::addEvent('queue-mailer', 'send-mail', 'Queue not available. Sending immediately.');
            return $this->sendImmediately($message);
        }

        TimeProfilerHelper::addEvent('queue-mailer', 'send-mail', 'Saving message to database.');
        $model = \common\models\Mailer::saveMessage($message->getSerializedData());

        TimeProfilerHelper::addEvent('queue-mailer', 'send-mail', 'Adding mail job to queue.');
        $job = new Job(['route' => $this->route, 'data' => ['messageId' => $model->id]]);
        if (!$this->_queue->post($job)) {
            TimeProfilerHelper::addEvent('queue-mailer', 'send-mail', 'Mail job failed to add. Sending immediately.');
            return $model->sendMailImmediately();
        } else {
            TimeProfilerHelper::addEvent('queue-mailer', 'send-mail', 'Mail job added.');
        }

        return true;
    }
}