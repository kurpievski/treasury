<?php

namespace common\extensions\traits;

trait AttributeHintTrait {

    public function getAttributeHint($attribute)
    {
        if (! empty($this->_attributeHints) && ! empty($this->_attributeHints[$attribute])) {
            return $this->_attributeHints[$attribute];
        }

        return '';
    }

}