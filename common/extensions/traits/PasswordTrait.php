<?php

namespace common\extensions\traits;

use Yii;

trait PasswordTrait
{
    protected $_passwordAttributeName = 'password_hash';
    protected $_passwordResetTokenAttributeName = 'password_reset_token';

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->{$this->_passwordAttributeName} = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->{$this->_passwordAttributeName});
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->{$this->_passwordResetTokenAttributeName} = $this->getGeneratedToken();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->{$this->_passwordResetTokenAttributeName} = null;
    }

    protected function getGeneratedToken()
    {
        return Yii::$app->security->generateRandomString() . '_' . time();
    }
}