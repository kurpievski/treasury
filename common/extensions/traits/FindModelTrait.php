<?php

namespace common\extensions\traits;
use yii\base\InvalidConfigException;
use yii\web\NotFoundHttpException;


trait FindModelTrait
{
    protected $_modelClass;
    protected $_loadedModel = null;

    protected $_scopingCallbacks = [];

    /**
     * Finds the model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param $id
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    /**
     * @param $id
     * @return null
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    protected function findModel($id)
    {
        if (empty($this->_modelClass)) {
            throw new InvalidConfigException("modelClass must be specified");
        }

        if ($this->_loadedModel) {
            return $this->_loadedModel;
        }

        if ($id !== null) {
            /* @var $class \yii\db\ActiveRecord */
            $class = $this->_modelClass;

            if (! empty($this->_scopingCallbacks)) {
                /* @var $model \yii\db\ActiveQuery */
                $model = $class::find();
                $primaryKeys = $class::primaryKey();

                $model->filterWhere([$primaryKeys[0] => $id]);

                foreach($this->_scopingCallbacks as $callback) {
                    if (! is_callable($callback)) {
                        continue;
                    }

                    $callback($model, $id);
                }

                $this->_loadedModel = $model->one();
            } else {
                $this->_loadedModel = $class::findOne($id);
            }

            if (empty($this->_loadedModel)) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }

            return $this->_loadedModel;
        }
    }

} 