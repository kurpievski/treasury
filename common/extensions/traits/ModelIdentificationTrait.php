<?php

namespace common\extensions\traits;

trait ModelIdentificationTrait
{
    /**
     * Returns the form name that this model class should use.
     *
     * The form name is mainly used by [[\yii\web\ActiveForm]] to determine how to name
     * the input fields for the attributes in a model. If the form name is "A" and an attribute
     * name is "b", then the corresponding input name would be "A[b]". If the form name is
     * an empty string, then the input name would be "b".
     *
     * By default, this method returns the model class name (without the namespace part)
     * as the form name. You may override it when the model is used in different forms.
     *
     * @return string the form name of this model class.
     */
    public function getFormName()
    {
        $reflector = new \ReflectionClass($this);
        return $reflector->getShortName();
    }

    /**
     * default form ID for the current model. Defaults to get_class()+'-form'
     */
    private $_formId;

    public function setFormId($value)
    {
        $this->_formId = $value;
    }

    public function getFormId()
    {
        if (null !== $this->_formId)
            return $this->_formId;
        else {
            $this->_formId = strtolower($this->getFormName()) . '-form';
            return $this->_formId;
        }
    }

    /**
     * default grid ID for the current model. Defaults to get_class()+'-grid'
     */
    private $_gridId;

    public function setGridId($value)
    {
        $this->_gridId = $value;
    }

    public function getGridId()
    {
        if (null !== $this->_gridId)
            return $this->_gridId;
        else {
            $this->_gridId = strtolower($this->getFormName()) . '-grid';
            return $this->_gridId;
        }
    }
}