<?php

namespace common\extensions;

use common\helpers\TraitHelper;
use yii\base\Model;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveField;

class EActiveForm extends ActiveForm
{
    /**
     * @return ActiveField
     */
    public function switchCheckBoxField(Model $model, $attribute, $options = [])
    {
        $labelName = ArrayHelper::getValue($options, 'labelName', false);
        $fieldOptions = ArrayHelper::getValue($options, 'fieldOptions', ['template' => '{input}']);
        $onText = ArrayHelper::getValue($options, 'onText', 'ON');
        $offText = ArrayHelper::getValue($options, 'offText', 'OFF');
        $noLabel = ArrayHelper::getValue($options, 'noLabel', false);
        $cssClass = ArrayHelper::getValue($options, 'class', 'col-md-12');
        $checkBoxOptions = ArrayHelper::getValue($options, 'checkBoxOptions', []);
        $checkboxListOptions = ArrayHelper::getValue($options, 'checkBoxListOptions', [
            'class' => $cssClass,
            'id' => false,
            'item' => function ($index, $label, $name, $checked, $value) use ($model, $attribute, $onText, $offText, $checkBoxOptions) {
                return
                    Html::hiddenInput(Html::getInputName($model, $attribute), '0') .
                    '<label class="switch-input">' .
                    Html::checkbox(Html::getInputName($model, $attribute), $checked, [
                        'value' => $value,
                        'id' => Html::getInputId($model, $attribute)
                    ], $checkBoxOptions) .
                    '<i data-swoff-text="' . $offText . '" data-swon-text="' . $onText . '"></i>' . $label . '</label>';
            },

        ]);

        return $this->field($model, $attribute, $fieldOptions)
            ->checkboxList(['1' => empty($noLabel) ? $model->getAttributeLabel($attribute) : ''], $checkboxListOptions)
            ->label(empty($noLabel) ? $labelName : false);
    }
}