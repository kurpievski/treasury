<?php
namespace common\extensions;

use common\extensions\excelwriter\ExportMenu;
use common\helpers\GridViewHelper;
use common\models\ReportProgressSearch;
use kartik\grid\GridFloatHeadAsset;
use kartik\grid\GridResizeColumnsAsset;
use kartik\grid\GridResizeStoreAsset;
use kartik\grid\GridView;
use Yii;
use yii\base\Exception;
use yii\grid\GridViewAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * EGridView.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 10-Jul-15
 * Time: 16:36
 */
class EGridView extends GridView
{
    const TYPE_STANDARD = 'standard-grid';
    const TYPE_EXPORT = 'export-grid';

    public $exportGridName;

    /**
     * @var array [
     * 'view' => '',
     * 'params' => '',
     */
    public $progressBar = [];

    /**
     * @var bool
     */
    public $useQueue = false;

    public static function widget($config = [])
    {
        //to avoid "Allowed memory size of 268435456 bytes exhausted"
        ini_set('memory_limit', '5G');
        set_time_limit(0);

        if (! empty($config['gridType'])) {
            switch ($config['gridType']) {
                case self::TYPE_STANDARD:
                    $widgetConfig = GridViewHelper::getStandardGridConfig($config);
                    break;
                case self::TYPE_EXPORT:
                    if (empty($config['viewContainer']) || empty($config['exportGridName'])) {
                        throw new Exception('Both viewContainer and exportGridName must be set.');
                    }
                    $widgetConfig = GridViewHelper::getExportGridConfig(
                        $config['viewContainer'],
                        $config['exportGridName'],
                        $config
                    );

                    unset($widgetConfig['viewContainer']);
                    break;
                default:
                    throw new Exception("Unknown grid");
            }

            unset($widgetConfig['gridType']);
        } else {
            $widgetConfig = $config;
        }

        $widgetConfig['export'] = ArrayHelper::merge([
            'exportRequestParam' => 'exportFull'
        ], ! empty($widgetConfig['export']) ? $widgetConfig['export'] : []);

        if (Yii::$app->getRequest()->post($widgetConfig['export']['exportRequestParam'], false) == '1') {
            Yii::$app->getResponse()->clearOutputBuffers();
        }

        return parent::widget($widgetConfig);
    }

    public function init()
    {
        parent::init();

        if (empty($this->pjaxSettings['options']['id'])) {
            $this->pjaxSettings['options']['id'] = $this->options['id'] . '-pjax-container';
        }

        if (empty($this->progressBar) && $this->useQueue) {
            $this->progressBar = [
                'view' => Yii::getAlias('@frontend') . '/views/shared/partials/_report.php',
                'params' => $this->loadProgressBarParams()
            ];
        }

        $this->pager = ArrayHelper::merge($this->pager, [
            'nextPageLabel' => 'Next',
            'prevPageLabel' => 'Prev',
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last',
        ]);
    }

    protected function getGridName()
    {
        $controller = Yii::$app->controller;
        $action = $controller->action->id;
        $widgetId = $this->id;

        return "{$controller->id}_{$action}_{$widgetId}";
    }

    /**
     * @return array
     */
    protected function loadProgressBarParams ()
    {
        $reportSearchModel = new ReportProgressSearch();
        $reportDataProvider = $reportSearchModel->search([
            'is_deleted' => 0,
            'grid_name' => $this->getGridName(),
        ]);

        return [
            'reportDataProvider' => $reportDataProvider,
        ];
    }

    protected function initExport()
    {
        set_time_limit(300);
        ini_set('memory_limit', '5G');
        if ($this->export === false) {
            return;
        }

        $this->exportConversions = ArrayHelper::merge(
            [
                ['from' => self::ICON_ACTIVE, 'to' => Yii::t('kvgrid', 'Active')],
                ['from' => self::ICON_INACTIVE, 'to' => Yii::t('kvgrid', 'Inactive')]
            ],
            $this->exportConversions
        );
    }

    /**
     * @return string|void
     */
    public function renderProgressBarReport ()
    {
        if ($this->export === false || !is_array($this->export) ||
            empty($this->exportConfig) || !is_array($this->exportConfig) ||
            !is_array($this->progressBar)
        ) {
            return '';
        }

        if (array_key_exists('view', $this->progressBar)) {
            return Yii::$app->view->renderFile($this->progressBar['view'], $this->progressBar['params']);
        }

        return '';
    }

    /**
     * Renders the export menu
     *
     * @return string
     */
    public function renderExport()
    {
        $exportConfig = array_merge(
            $this->export,
            [
                'dataProvider' => $this->dataProvider,
                'columns' => $this->columns,
                'dropDownOptions' => [
                    'class' => 'btn btn-default',
                    'label' => 'Export',
                    'menuOptions' => [
                        'class' => 'dropdown-menu dropdown-menu-right'
                    ]
                ],
            ]
        );

        unset(
            $exportConfig['fontAwesome']
        );

        return ExportMenu::widget($exportConfig);
    }

    /**
     * Registers client assets
     */
    protected function registerAssets()
    {
        $view = $this->getView();
        $script = '';
        if ($this->bootstrap) {
            GridViewAsset::register($view);
        }
        $gridId = $this->options['id'];

        if ($this->resizableColumns) {
            $store = '{store:null}';
            if ($this->persistResize) {
                GridResizeStoreAsset::register($view);
                $store = '';
            }
            $contId = $this->containerOptions['id'];
            GridResizeColumnsAsset::register($view);
            $script .= "$('#{$contId}').resizableColumns({$store});";
        }

        if ($this->floatHeader) {
            GridFloatHeadAsset::register($view);
            $this->floatHeaderOptions = ArrayHelper::merge(
                [
                    'floatTableClass' => 'kv-table-float',
                    'floatContainerClass' => 'kv-thead-float'
                ],
                $this->floatHeaderOptions
            );
            $opts = Json::encode($this->floatHeaderOptions);
            $script .= "$('#{$gridId} .kv-grid-table:first').floatThead({$opts});";
            $script .= "
            $('a[data-toggle=\"tab\"]').on('shown.bs.tab', function (e) {
               $('#{$gridId} .kv-grid-table:first').floatThead('reflow');
            });

            (function() {
                 var getTransitionEvent = function () {
                    var t;
                    var el = document.createElement('fakeelement');
                    var transitions = {
                      'transition':'transitionend',
                      'OTransition':'oTransitionEnd',
                      'MozTransition':'transitionend',
                      'WebkitTransition':'webkitTransitionEnd'
                    }

                    for(t in transitions){
                        if( el.style[t] !== undefined ){
                            return transitions[t];
                        }
                    }
                }

                var transitionEvent = getTransitionEvent();
                var leftColumn = $('#col-left');
                if (leftColumn.length > 0) {
                    transitionEvent && leftColumn[0].addEventListener(transitionEvent, function() {
                        $('#{$gridId} .kv-grid-table:first').floatThead('reflow');
                    });
                }

            })();
            ";
        }

        $this->_gridClientFunc = 'kvGridInit_' . hash('crc32', $script);
        $this->options['data-krajee-grid'] = $this->_gridClientFunc;
        $view->registerJs("var {$this->_gridClientFunc}=function(){\n{$script}\n}\n{$this->_gridClientFunc}();");
    }

    /**
     * a way to append progress bar in all grids
     */
    protected function initLayout()
    {
        $this->layout = str_replace('{items}', '{progressBar}{items}', $this->layout);
        $this->layout = str_replace('{progressBar}', $this->renderProgressBarReport(), $this->layout);

        parent::initLayout();
    }
}