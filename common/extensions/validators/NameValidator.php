<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 23.12.2016.
 * Time: 16:22
 */

namespace common\extensions\validators;


use yii\validators\RegularExpressionValidator;

class NameValidator extends RegularExpressionValidator
{
    public $pattern = '/^[a-zA-Z]+[0-9a-zA-Z .-]*$/';
}