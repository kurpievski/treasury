<?php

namespace common\components\controllers;

use common\components\actions\crud\IndexAction;
use common\components\actions\crud\SearchAction;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\components\actions\crud\CreateAction;
use common\components\actions\crud\ViewAction;
use common\components\actions\crud\UpdateAction;
use common\components\actions\crud\DeleteAction;

/**
 * Class CrudController
 *
 * @package strongsquirrel\crud
 */
class CrudController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => SearchAction::class,
                'modelClass' => $this->searchModelClass,
                'view' => 'index'
            ],
            'create' => [
                'class' => CreateAction::class,
                'modelClass' => $this->modelClass,
            ],
            'view' => [
                'class' => ViewAction::class,
                'modelClass' => $this->modelClass,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => $this->modelClass,
                'findModel' => function ($id) {
                    return $this->findModel($id);
                }
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => $this->modelClass,
            ],
        ];
    }

}
