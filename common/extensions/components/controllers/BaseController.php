<?php

/**
 * Created by PhpStorm.
 * Nikola Radovic <nikola@2amigos.us>
 * Date: 14.03.2017.
 * Time: 17:38
 */

namespace common\components\controllers;

use common\extensions\ActiveRecord;
use Yii;
use yii\filters\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;

/**
 * {@inheritdoc}
 */
class BaseController extends Controller
{
    /** @var string */
    public $modelClass;
    public $searchModelClass;
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'ruleConfig' => [
                    'class' => AccessRule::class,
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'view' => '@app/views/site/error'
            ],
        ];
    }

    /**
     * Render view depending weather it is ajax request or not
     *
     * @param $view
     * @param array $params
     * @return string
     */
    public function renderAjaxConditional($view, array $params = [])
    {
        return Yii::$app->request->getIsAjax() ? $this->renderAjax($view, $params) : $this->render($view, $params);
    }


    /**
     * Finds the CommissionRecord model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ActiveRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /** @var ActiveRecord $modelClass */
        $modelClass = $this->modelClass;

        /** @var $model ActiveRecord */
        if (($model = $modelClass::findOne($id)) === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }

}
