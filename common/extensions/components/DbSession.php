<?php
/**
 * Email: srdjan.drakul@2amigos.us
 * Date: 11/26/2018
 * Time: 9:58 AM
 */

namespace common\components;

use Yii;

class DbSession extends \yii\web\DbSession
{
    /**
     * Session write handler.
     * @internal Do not call this method directly.
     * @param string $id session ID
     * @param string $data session data
     * @return bool whether session write is successful
     */
    public function writeSession($id, $data)
    {
        if(empty($data)) {
            return true;
        }

        // exception must be caught in session write handler
        // http://us.php.net/manual/en/function.session-set-save-handler.php#refsect1-function.session-set-save-handler-notes
        try {
            $fields = $this->composeFields($id, $data);
            $fields = $this->typecastFields($fields);
            $this->db->createCommand()->upsert($this->sessionTable, $fields)->execute();
        } catch (\Exception $e) {
            Yii::$app->errorHandler->handleException($e);
            return false;
        }

        return true;
    }
}