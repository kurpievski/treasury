<?php
/**
 * Date: 10/26/2016
 * Time: 8:02 PM
 * Author: Igor Golub <igor.golub@2amigos.us>
 */

namespace common\components\api;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use Yii;
use yii\base\BaseObject;
use yii\helpers\Json;
use yii\web\HttpException;

class ApiRequest extends BaseObject
{
    const TYPE_POST = 'POST';
    const TYPE_GET = 'GET';

    const DATA_TYPE_QUERY = 'query';
    const DATA_TYPE_BODY = 'body';
    const DATA_TYPE_FORM_PARAMS = 'form_params';

    private $_accessToken;
    private $_errorMessage;
    private $_errorCode;
    private $_headers;
    private $_response;

    public $url;
    public $path;
    public $data = [];
    public $dataType = self::DATA_TYPE_QUERY;
    public $method = self::TYPE_GET;
    public $includeAuthorizationHeader = false;
    public $jsonEncode = true;
    public $grantPathIndex = 'internalUser.grantPath';
    public $grantClient;

    public function init()
    {
        parent::init();

        $this->_headers = [];
    }

    public function send()
    {
        $this->_response = false;
        $this->_errorCode = null;
        $this->_errorMessage = null;
        $url = $this->getFullUrl();

        try {
            $clientRequest = $this->getRequestConfig();

            $response = (new Client())->request($this->getMethod(), $url, $clientRequest);
            $contents = $response->getBody()->getContents();

            return $this->_response = json_decode($contents, true);

        } catch (ClientException $e) {
            $this->handleRequestException($e);
        } catch (ServerException $e) {
            $this->handleRequestException($e);
        } catch (ConnectException $e) {
            $this->_errorMessage = "Can't connect to {$url}";
            return false;
        }

        return false;
    }

    protected function getRequestConfig()
    {
        return [
            'verify' => false,
            $this->dataType => $this->getRequestData(),
            'headers' => $this->getHeaders()
        ];
    }

    public function getRequestData()
    {
        return ($this->dataType == self::DATA_TYPE_BODY && $this->jsonEncode) ? Json::encode($this->data) : $this->data;
    }

    public function getMethod()
    {
        return strtoupper($this->method);
    }

    public function getPath()
    {
        return !empty($this->path) ? '/' . trim($this->path, '/') : '';
    }

    public function setAccessToken($token)
    {
        $this->_accessToken = $token;
    }

    public function getAccessToken()
    {
        if (!empty($this->_accessToken)) {
            return $this->_accessToken;
        }

        return $this->_accessToken = $this->requestNewToken();
    }

    protected function getIsPost()
    {
        return $this->getMethod() === self::TYPE_POST;
    }

    public function setHeaders($headers)
    {
        $this->_headers = $headers;
    }

    public function getHeaders()
    {
        if ($this->includeAuthorizationHeader) {
            $this->_headers['Authorization'] = "Bearer {$this->getAccessToken()}";
        }

        if ($this->dataType == self::DATA_TYPE_BODY) {
            $this->_headers['Content-Type'] = 'application/json';
        }

        return $this->_headers;
    }

    public function getError()
    {
        $code = $this->getErrorCode();
        $message = $this->getErrorMessage();

        if (empty($code)) {
            return !empty($message) ? $message : null;
        }

        return $this->getErrorCode() . ' - ' . $this->getErrorMessage();
    }

    public function hasError()
    {
        return !empty($this->_errorCode) || !empty($this->_errorMessage);
    }

    public function getErrorMessage()
    {
        return $this->_errorMessage;
    }

    public function getErrorCode()
    {
        return $this->_errorCode;
    }

    protected function requestNewToken()
    {
        $auth = Yii::$app->params['auth'];
        $url = $auth['baseUrl'] . $auth[$this->grantPathIndex];

        try {
            $response = (new Client())->request('POST', $url, [
                'form_params' => [
                    'client_id' => $auth['clientId'],
                    'grant_type' => 'client_credentials',
                    'client_secret' => $auth['clientSecret'],
                    'grant_client' => $this->grantClient
                ]
            ]);
            $data = json_decode($response->getBody(), true);

            return $data['access_token'];
        } catch (ClientException $e) {
            $response = $e->getResponse();
            throw new HttpException($response->getStatusCode(), $response->getBody());
        } catch (ServerException $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                throw new HttpException($response->getStatusCode(), $response->getBody());
            }
        }

        return '';
    }

    public function getResponse()
    {
        return $this->_response;
    }

    public function isErrorResponse()
    {
        return $this->_response === false;
    }

    protected function handleRequestException(RequestException $e)
    {
        if (!$e->hasResponse()) {
            return;
        }

        $response = $e->getResponse();

        $this->_errorMessage = $response->getBody()->getContents();
        $this->_errorCode = $response->getStatusCode();
    }

    protected function getFullUrl()
    {
        return $this->url . $this->getPath();
    }
}
