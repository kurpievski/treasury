<?php
namespace common\components\tracking\contracts;

/**
 * Interface TrackerInterface
 * @package common\components\tracking\contracts
 */
interface TrackerInterface
{
    /**
     * @return string
     */
    public function getIp(): string;

    /**
     * @return string
     */
    public function getEvent(): string;

    /**
     * @return int|null
     */
    public function getUserId();

    /**
     * @return string
     */
    public function getUserAgent(): string;

    /**
     * @return mixed
     */
    public function track();
}