<?php
namespace common\components\tracking\contracts;

/**
 * Interface TrackerExtensionInterface
 * @package common\components\tracking\contracts
 */
interface TrackerExtensionInterface
{

    /**
     * @param TrackerInterface $tracker
     * @return void
     */
    public function run(TrackerInterface $tracker);
}