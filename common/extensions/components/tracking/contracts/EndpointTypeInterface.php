<?php
namespace common\components\tracking\contracts;

/**
 * Interface EndpointTypeInterface
 * @package common\components\tracking\contracts
 */
interface EndpointTypeInterface
{
    /**
     * @return string
     */
    public function getDateLabel(string $date);

    /**
     * @return string
     */
    public function getPeriod();

    /**
     * @return string
     */
    public function getDateFormat();
}