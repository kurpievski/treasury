<?php

namespace common\components\tracking;

use common\components\actions\Action;
use common\components\tracking\contracts\TrackerInterface;
use Yii;
use yii\base\Behavior;
use yii\base\Event;
use yii\base\Exception;
use yii\web\Controller;

/**
 * Class TrackingBadgeBehavior
 * @package common\components\behaviors
 */
class TrackingBehavior extends Behavior
{
    /**
     * @var array
     */
    public $trackerConfig;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            Controller::EVENT_AFTER_ACTION => 'afterAction',
        ];
    }

    /**
     * @param Event $event
     * @throws Exception
     */
    public function afterAction(Event $event)
    {
        /** @var $action Action */
        $action = $event->sender->action;
        $app = Yii::$app;
        $user = $app->user;
        $request = $app->request;

        /** @var TrackerInterface $tracker */
        $tracker = Yii::createObject($this->trackerConfig);

        if (!($tracker instanceof TrackerInterface)) {
            throw new Exception('Tracker has to implement '.TrackerInterface::class);
        }

        $tracker->ip = $request->getRemoteIP();
        $tracker->event = sprintf('%s/%s', $action->controller->id, $action->id);
        $tracker->userId = $user ? $user->id : null;
        $tracker->userAgent = $request->getUserAgent();
        $tracker->track();
    }
}
