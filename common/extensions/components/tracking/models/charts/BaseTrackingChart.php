<?php
/*
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace common\components\tracking\models\charts;

use common\components\highchartsajax\BaseChart;
use common\components\highchartsajax\ChartInterface;
use common\components\tracking\models\Tracking;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Class BaseTrackingChart
 * @package common\components\tracking\models\charts
 */
class BaseTrackingChart extends BaseChart implements ChartInterface
{
    public const KEY_COUNTER = 'counter';

    public const MODE_HITS = 'Hits';
    public const MODE_PATIENTS = 'Patients';

    public const MODE_DEFINITIONS = [
        self::MODE_HITS => 'count(*)',
        self::MODE_PATIENTS => 'count(distinct(user_id))',
    ];

    /**
     * @var string url to ajax action providing data
     */
    public $dataUrl;

    /**
     * @var string
     */
    public $dateFormat = 'F j, Y';

    /**
     * @var string
     */
    public $defaultRange = '-1 week';

    /**
     * @var string
     */
    public $mode = self::MODE_HITS;

    /**
     * @var string
     */
    public $date_range;

    /**
     * @var \DateTime
     */
    protected $from;
    /**
     * @var \DateTime
     */
    protected $to;

    /**
     * @inheritdoc
     */
    protected $chartType = ChartInterface::TYPE_CHART;
    /**
     * @var array
     */
    protected $data;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return \array_merge(parent::rules(), [
            [['date_range', 'mode'], 'safe'],
        ]);
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        $this->initDateRangeFields();

        $from = $this->from->format('U');
        $to = $this->to->format('U');

        return Tracking::find()->asArray()
            ->select([static::KEY_COUNTER => $this->getCounterExpression()])
            ->where(['between', 'created_at', $from, $to]);
    }

    protected function initDateRangeFields()
    {
        parent::init();

        if (empty($this->date_range)) {
            $this->setDefaultRange();
        }

        list($from, $to) = explode(' - ', $this->date_range);

        $this->from = new \DateTime($from.' 00:00:00');
        $this->to = new \DateTime($to.' 23:59:59');
    }

    protected function setDefaultRange()
    {
        $today = new \DateTime();
        $fromDate = clone $today;
        $this->date_range =  sprintf('%s - %s',
            $fromDate->modify($this->defaultRange)->format($this->dateFormat),
            $today->format($this->dateFormat)
        );
    }

    /**
     * @return Expression
     */
    protected function getCounterExpression()
    {
        return new Expression(static::MODE_DEFINITIONS[$this->mode]);
    }

    /**
     * @return array
     */
    public static function getModes()
    {
        $modes = array_keys(static::MODE_DEFINITIONS);

        return \array_combine($modes, $modes);
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        $types = array_keys(static::TYPE_DEFINITIONS);

        return \array_combine($types, $types);
    }

    /**
     * @inheritdoc
     */
    public function getConfig()
    {
        return [
            'chart' => ['renderTo' => $this->getId()],
            'exporting' => ['enabled' => true],
            'credits' => false,
            'title' => ['text' => ''],
        ];
    }
}
