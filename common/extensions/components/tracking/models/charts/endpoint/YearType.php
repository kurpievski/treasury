<?php
/*
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace common\components\tracking\models\charts\endpoint;

use common\components\tracking\contracts\EndpointTypeInterface;

/**
 * Class YearType
 * @package common\components\tracking\models\charts\endpoint
 */
class YearType implements EndpointTypeInterface
{
    /**
     * @param string $date
     * @return string
     */
    public function getDateLabel(string $date)
    {
        return $date;
    }

    /**
     * @return string
     */
    public function getPeriod()
    {
        return 'P1Y';
    }

    /**
     * @return string
     */
    public function getDateFormat()
    {
       return 'Y';
    }
}
