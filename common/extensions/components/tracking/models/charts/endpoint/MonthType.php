<?php
/*
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace common\components\tracking\models\charts\endpoint;

use common\helpers\TimeHelper;
use common\components\tracking\contracts\EndpointTypeInterface;

/**
 * Class MonthType
 * @package common\components\tracking\models\charts\endpoint
 */
class MonthType implements EndpointTypeInterface
{
    /**
     * @param string $date
     * @return string
     */
    public function getDateLabel(string $date)
    {
        $dateObject = TimeHelper::createDateObjectFromString($date);

        return $dateObject->format('M Y');
    }

    /**
     * @return string
     */
    public function getPeriod()
    {
        return 'P1M';
    }

    /**
     * @return string
     */
    public function getDateFormat()
    {
       return 'Y-m';
    }
}
