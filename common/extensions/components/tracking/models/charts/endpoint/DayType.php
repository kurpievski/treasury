<?php
/*
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace common\components\tracking\models\charts\endpoint;

use common\helpers\TimeHelper;
use common\components\tracking\contracts\EndpointTypeInterface;

/**
 * Class DayType
 * @package common\components\tracking\models\charts\endpoint
 */
class DayType implements EndpointTypeInterface
{
    /**
     * @param string $date
     * @return string
     */
    public function getDateLabel(string $date)
    {
        $dateObject = TimeHelper::createDateObjectFromString($date);

        return $dateObject->format(TimeHelper::DATE_FORMAT);
    }

    /**
     * @return string
     */
    public function getPeriod()
    {
        return 'P1D';
    }

    /**
     * @return string
     */
    public function getDateFormat()
    {
       return 'Y-m-d';
    }
}
