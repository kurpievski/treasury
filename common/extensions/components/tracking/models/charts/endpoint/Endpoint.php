<?php
/*
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace common\components\tracking\models\charts\endpoint;

use common\components\highchartsajax\ChartInterface;
use common\components\tracking\contracts\EndpointTypeInterface;
use common\components\tracking\models\charts\BaseTrackingChart;
use common\components\tracking\models\Tracking;
use yii\db\ActiveQuery;
use yii\db\Expression;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Endpoint
 * @package common\components\tracking\models\charts\endpoint
 */
class Endpoint extends BaseTrackingChart implements ChartInterface
{
    public const KEY_DATE = 'date';
    public const KEY_EVENT = 'event';

    public const TYPE_YEAR = 'Year';
    public const TYPE_MONTH = 'Month';
    public const TYPE_WEEK = 'Week';
    public const TYPE_DAY = 'Day';

    public const TYPE_DEFINITIONS = [
        self::TYPE_YEAR => '%Y',
        self::TYPE_MONTH => '%Y-%m',
        self::TYPE_WEEK => '%Y-%u',
        self::TYPE_DAY => '%Y-%m-%d',
    ];

    /**
     * @var string
     */
    public $type = self::TYPE_DAY;

    /**
     * @var boolean
     */
    public $total;

    /**
     * @var EndpointTypeInterface
     */
    protected $typeObject;
    /**
     * @var array
     */
    protected $dates;

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['type', 'total'], 'safe'],
        ]);
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        $this->data = $this->getQuery()->all();
        $this->typeObject = $this->createTypeObject();
        $this->setDates();

        return ArrayHelper::merge(parent::getConfig(), [
            'xAxis' => ['categories' => $this->getDateLabels()],
            'yAxis' => ['title' => ['text' => ''], 'minRange' => 100],
            'series' => $this->generateSeries(),
            'chart' => ['type' => 'spline'],
            'plotOptions' => ['line' => ['softThreshold' => false]]
        ]);
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        $query = parent::getQuery();

        $query->select(ArrayHelper::merge($query->select, [
            static::KEY_DATE => $this->getDateExpression(),
            static::KEY_EVENT => $this->getEventExpression()
        ]))->groupBy($this->total ? [static::KEY_DATE] : [static::KEY_EVENT, static::KEY_DATE]);

        return $query;
    }

    /**
     * @return Expression
     */
    protected function getDateExpression()
    {
        $typeDefinition = static::TYPE_DEFINITIONS[$this->type];

        return new Expression("date_format(from_unixtime(created_at), '{$typeDefinition}')");
    }

    /**
     * @return string|Expression
     */
    protected function getEventExpression()
    {
        return $this->total ? new Expression("'total'") : 'event';
    }

    /**
     * @return object
     */
    protected function createTypeObject()
    {
        return Yii::createObject(['class' => __NAMESPACE__.'\\'.$this->type.'Type']);
    }

    /**
     * @return array
     */
    protected function getDateLabels()
    {
        $typeObject = $this->typeObject;

        $labels = [];
        foreach ($this->dates as $date => $value) {
            $labels[] = $typeObject->getDateLabel($date);
        }

        return $labels;
    }

    protected function setDates()
    {
        $typeObject = $this->typeObject;
        $period = new \DatePeriod(
            $this->from,
            new \DateInterval($typeObject->getPeriod()),
            $this->to
        );

        $dates = [];
        foreach ($period as $date) {
            /* @var $date \DateTime */
            $formattedDate = $date->format($typeObject->getDateFormat());
            $dates[$formattedDate] = 0;
        }

        $this->dates = $dates;
    }

    /**
     * @return array
     */
    protected function generateSeries()
    {
        $seriesData = [];
        $totalCount = [];
        $dates = $this->dates;

        foreach ($this->data as $record) {
            $event = $record[static::KEY_EVENT];
            $date = $record[static::KEY_DATE];
            $count = (int)$record['counter'];

            if (!isset($seriesData[$event])) {
                $seriesData[$event] = $dates;
            }
            $seriesData[$event][$date] = $count;

            if (!isset($totalCount[$event])) {
                $totalCount[$event] = 0;
            }

            $totalCount[$event] += $count;
        }

        arsort($totalCount);

        $series = [];

        foreach ($totalCount as $event => $count) {
            $series[$event] = [
                'name' => Tracking::getEventName($event)." ($count)",
                'data' => array_values($seriesData[$event]),
            ];
        }

        if (!$this->total) {
            foreach (array_keys(Tracking::EVENT_NAMES) as $event) {
                if (!isset($series[$event])) {
                    $series[$event] = [
                        'name' => Tracking::getEventName($event)." (0)",
                        'data' => \array_fill(0, count($dates), 0),
                    ];
                }
            }
        }

        return array_values($series);
    }
}
