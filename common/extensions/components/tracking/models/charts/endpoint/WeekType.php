<?php
/*
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace common\components\tracking\models\charts\endpoint;

use common\components\tracking\contracts\EndpointTypeInterface;

/**
 * Class WeekType
 * @package common\components\tracking\models\charts\endpoint
 */
class WeekType implements EndpointTypeInterface
{
    /**
     * @param string $date
     * @return string
     */
    public function getDateLabel(string $date)
    {
        list($year, $week) = explode('-', $date);
        $dateObject = new \DateTime();
        $dateObject->setISODate($year, $week);
        $start = $dateObject->modify('-1 day')->format('m/d');
        $end = $dateObject->modify('+6 days')->format('m/d');

        return $start.'-'.$end;
    }

    /**
     * @return string
     */
    public function getPeriod()
    {
        return 'P1W';
    }

    /**
     * @return string
     */
    public function getDateFormat()
    {
       return 'Y-W';
    }
}
