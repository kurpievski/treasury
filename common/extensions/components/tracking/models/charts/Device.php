<?php
/*
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace common\components\tracking\models\charts;

use common\components\highchartsajax\ChartInterface;
use common\helpers\ArrayHelper;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Class Device
 * @package common\components\tracking\models\charts
 */
class Device extends BaseTrackingChart implements ChartInterface
{
    public const KEY_DEVICE = 'device';

    public const TYPE_GENERAL = 'General';
    public const TYPE_MODEL = 'Model';

    public const TYPE_DEFINITIONS = [
        self::TYPE_GENERAL => 'device_type',
        self::TYPE_MODEL => 'device_name',
    ];

    /**
     * @var string
     */
    public $type = self::TYPE_GENERAL;

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['type'], 'safe'],
        ]);
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        $this->data = $this->getQuery()->all();

        return ArrayHelper::merge(parent::getConfig(), [
            'tooltip' => ['pointFormat' => '{series.name}: <b>{point.percentage:.1f}%</b>',],
            'plotOptions' => [
                'pie' => [
                    'allowPointSelect' => true,
                    'cursor' => 'pointer',
                    'dataLabels' => [
                        'enabled' => true,
                        'format' => '<b>{point.name}</b>: {point.percentage:.1f} %',
                        'style' =>[
                            'color' => 'black',
                        ],
                    ],
                ],
            ],
            'series' => [[
                'name' => 'Visits',
                'colorByPoint' => true,
                'data' => $this->generateSeries(),
            ]],
            'chart' => [
                'plotBackgroundColor' => NULL,
                'plotBorderWidth' => NULL,
                'plotShadow' => false,
                'type' => 'pie',
            ],
        ]);
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        $query = parent::getQuery();

        $query
            ->select(\array_merge($query->select, [
                static::KEY_DEVICE => $this->getDeviceExpression()
            ]))
            ->groupBy([static::KEY_DEVICE])
            ->orderBy([static::KEY_COUNTER => \SORT_DESC]);

        return $query;
    }

    /**
     * @return string|Expression
     */
    protected function getDeviceExpression()
    {
        return static::TYPE_DEFINITIONS[$this->type];
    }

    /**
     * @return array
     */
    protected function generateSeries()
    {
        $seriesData = [];
        $totalCount = 0;
        foreach ($this->data as $record) {
            $count = (int)$record[static::KEY_COUNTER];

            $seriesData[] = [
                'name' => $record[static::KEY_DEVICE],
                'y' => $count,
            ];

            $totalCount += $count;
        }

        foreach ($seriesData as &$data) {
            $value = ($data['y'] / $totalCount) * 100;
            $data['y'] = round($value, 2);
        }

        return $seriesData;
    }
}
