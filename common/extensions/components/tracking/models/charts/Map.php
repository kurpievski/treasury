<?php
/*
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace common\components\tracking\models\charts;

use backend\assets\HighChartsMapAsset;
use common\components\highchartsajax\ChartInterface;
use yii\db\ActiveQuery;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Map
 * @package common\components\tracking\models\charts
 */
class Map extends BaseTrackingChart implements ChartInterface
{
    public const KEY_STATE = 'state';

    /**
     * @var string
     */
    protected $chartType = 'Map';

    public function init()
    {
        parent::init();
        HighChartsMapAsset::register(Yii::$app->view);
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        $this->data = $this->getQuery()->all();

        return ArrayHelper::merge(parent::getConfig(), [
            'chart' => [
                'map' => 'countries/us/us-all',
            ],
            'colorAxis' => ['min' => 0],
            'series' => [
                [
                    'data' => $this->generateSeries(),
                    'name' => 'Visits',
                    'states' => [
                        'hover' => [
                            'color' => '#BADA55'
                        ]
                    ],
                    'dataLabels' => [
                        'enabled' => true,
                        'format' => '{point.name}'
                    ]
                ]

            ],
        ]);
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        $query = parent::getQuery();
        $fields = [static::KEY_STATE];

        $query->select(\array_merge($query->select, $fields))
            ->andWhere(['country' => 'United States'])
            ->groupBy($fields)
            ->orderBy([static::KEY_COUNTER => \SORT_DESC]);

        return $query;
    }

    /**
     * @param $data
     * @param $dates
     * @return array
     */
    protected function generateSeries()
    {
        /* @var $seriesData array actual series data (hits per endpoint per specified date type*/
        $seriesData = [];
        $stateAbbreviations = $this->getStateAbbreviations();

        foreach ($this->data as $record) {
            $state = $record['state'];
            if (!isset($stateAbbreviations[$state])) {
                continue;
            }

            $stateAbbreviation = strtolower($stateAbbreviations[$state]);
            $count = (int)$record[static::KEY_COUNTER];

            $seriesData[] = ['us-'.$stateAbbreviation, $count];
        }

        return $seriesData;
    }

    /**
     * @return array
     */
    protected function getStateAbbreviations()
    {
        return [
            'Alabama'=>'AL',
            'Alaska'=>'AK',
            'Arizona'=>'AZ',
            'Arkansas'=>'AR',
            'California'=>'CA',
            'Colorado'=>'CO',
            'Connecticut'=>'CT',
            'Delaware'=>'DE',
            'Florida'=>'FL',
            'Georgia'=>'GA',
            'Hawaii'=>'HI',
            'Idaho'=>'ID',
            'Illinois'=>'IL',
            'Indiana'=>'IN',
            'Iowa'=>'IA',
            'Kansas'=>'KS',
            'Kentucky'=>'KY',
            'Louisiana'=>'LA',
            'Maine'=>'ME',
            'Maryland'=>'MD',
            'Massachusetts'=>'MA',
            'Michigan'=>'MI',
            'Minnesota'=>'MN',
            'Mississippi'=>'MS',
            'Missouri'=>'MO',
            'Montana'=>'MT',
            'Nebraska'=>'NE',
            'Nevada'=>'NV',
            'New Hampshire'=>'NH',
            'New Jersey'=>'NJ',
            'New Mexico'=>'NM',
            'New York'=>'NY',
            'North Carolina'=>'NC',
            'North Dakota'=>'ND',
            'Ohio'=>'OH',
            'Oklahoma'=>'OK',
            'Oregon'=>'OR',
            'Pennsylvania'=>'PA',
            'Rhode Island'=>'RI',
            'South Carolina'=>'SC',
            'South Dakota'=>'SD',
            'Tennessee'=>'TN',
            'Texas'=>'TX',
            'Utah'=>'UT',
            'Vermont'=>'VT',
            'Virginia'=>'VA',
            'Washington'=>'WA',
            'West Virginia'=>'WV',
            'Wisconsin'=>'WI',
            'Wyoming'=>'WY'
        ];
    }
}
