<?php
/*
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace common\components\tracking\models;

use common\components\orm\ActiveRecord;
use common\models\User;
use Yii;

/**
 * This is the model class for table "{{%tracking}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $event
 * @property string $state
 * @property string $country
 * @property string $city
 * @property string $ip_address
 * @property string $user_agent
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 * @property string $device_type
 * @property string $device_name
 *
 * @property User $user
 */
class Tracking extends ActiveRecord
{
    /**
     * Friendly endpoint names for the Endpoint Chart. If an endpoint does not have a nice name, the original name is
     * used.
     */
    public const EVENT_NAMES = [
        'achievement/badge' => 'Badge',
        'achievement-log/mark-as-read' => 'Mark Achievement',
        'dashboard/index' => 'Dashboard',
        'help/swagger' => 'Api Doc',
        'image/view' => 'Image',
        'oauth/token' => 'Login',
        'patient/register' => 'Register',
        'patient/view' => 'Patient Info',
        'patient/save-photo' => 'Save photo',
        'profile/register-device' => 'Register device',
        'profile/add-status' => 'Status',
        'profile/add-status-reaction' => 'Reaction',
        'profile/connection' => 'Connection',
        'profile/update-connection' => 'Update Connection',
        'profile/update-profile' => 'Update Profile',
        'product/index' => 'Product',
        'oauth/revoke' => 'Revoke token',
        'time/index' => 'Time',
        'water/create' => 'Create Water Goal',
        'water/update' => 'Update Water Goal',
        'water/view-reminder' => 'Water Reminder',
        'medication/index' => 'Medications',
        'medication/view-reminder' => 'Medication Reminder',
        'medication/daily-medication' => 'Daily Medication',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tracking}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['event'], 'required'],
            [['event', 'state', 'country', 'city', 'ip_address'], 'string', 'max' => 45],
            [['user_agent'], 'string', 'max' => 255],
            [['device_type'], 'string', 'max' => 32],
            [['device_name'], 'string', 'max' => 128],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'event' => 'Event',
            'state' => 'State',
            'country' => 'Country',
            'city' => 'City',
            'ip_address' => 'Ip Address',
            'user_agent' => 'User Agent',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'device_type' => 'Device Type',
            'device_name' => 'Device Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @param $event
     * @return string
     */
    public static function getEventName($event)
    {
        if (isset(self::EVENT_NAMES[$event])) {
            return self::EVENT_NAMES[$event];
        }

        return ucwords(str_replace(['/', '-'], ' ', $event));
    }
}
