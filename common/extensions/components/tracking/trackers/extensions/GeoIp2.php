<?php
namespace common\components\tracking\trackers\extensions;

use common\components\tracking\contracts\TrackerExtensionInterface;
use common\components\tracking\contracts\TrackerInterface;
use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;
use yii\base\BaseObject;
use Yii;

/**
 * Class GeoIp2
 * @package common\components\tracking\trackers\extensions
 */
class GeoIp2 extends BaseObject implements TrackerExtensionInterface
{
    /**
     * @var string
     */
    public $dbPath;

    /**
     * @var array[string]
     */
    protected $properties;

    public function init()
    {
        parent::init();

        $this->dbPath = Yii::getAlias($this->dbPath);
    }

    /**
     * @inheritDoc
     */
    public function run(TrackerInterface $tracker)
    {
        $dbReader = new Reader($this->dbPath);

        try {
            $record = $dbReader->city($tracker->getIp());
            $this->properties = [
                'country' => $record->country->name,
                'city' => $record->city->name,
                'state' => $record->mostSpecificSubdivision->name,
            ];
        } catch (AddressNotFoundException $e) {
            $this->properties = [
                'country' => 'unknown',
                'city' => 'unknown',
                'state' => 'unknown',
            ];
        }
    }

    /**
     * @param string $property
     * @return string
     */
    public function getProperty(string $property)
    {
        return $this->transliterate($this->properties[$property]);
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->getProperty('country');
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->getProperty('state');
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->getProperty('city');
    }

    /**
     * @param $string
     * @return string
     */
    protected function transliterate($string)
    {
        return iconv('UTF-8', 'US-ASCII//TRANSLIT', $string);
    }
}