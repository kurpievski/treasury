<?php
namespace common\components\tracking\trackers\extensions;

use common\components\tracking\contracts\TrackerExtensionInterface;
use common\components\tracking\contracts\TrackerInterface;
use DeviceDetector\DeviceDetector;
use yii\base\BaseObject;

/**
 * Class PiwikDeviceDetector
 * @package common\components\tracking\trackers\decorators
 */
class PiwikDeviceDetector extends BaseObject implements TrackerExtensionInterface
{
    /**
     * @var DeviceDetector
     */
    protected $deviceDetector;

    /**
     * @inheritDoc
     */
    public function run(TrackerInterface $tracker)
    {
        $deviceDetector = new DeviceDetector();
        $deviceDetector->setUserAgent($tracker->getUserAgent());
        $deviceDetector->parse();

        $this->deviceDetector = $deviceDetector;
    }

    /**
     * @return string
     */
    public function getName()
    {
        $deviceDetector = $this->deviceDetector;

        return trim($deviceDetector->getBrandName().' '.$deviceDetector->getModel()) ?: 'unknown';
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->deviceDetector->getDeviceName() ?: 'unknown';
    }
}