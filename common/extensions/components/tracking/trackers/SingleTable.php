<?php
namespace common\components\tracking\trackers;

use common\components\tracking\contracts\TrackerInterface;
use Yii;
use yii\base\Exception;
use yii\db\BaseActiveRecord;

/**
 * Class SingleTable
 * @package common\components\tracking\trackers
 */
class SingleTable extends Tracker implements TrackerInterface
{
    /**
     * @var string
     */
    public $activeRecordConfig;

    /**
     * @inheritDoc
     */
    public function track()
    {
        $activeRecord = Yii::createObject($this->activeRecordConfig);

        if (!($activeRecord instanceof BaseActiveRecord)) {
            throw new Exception('ActiveRecord has to extend '.BaseActiveRecord::class);
        }

        $this->runExtensions();
        $this->initMappings();

        $activeRecord->setAttributes($this->mappings);
        $activeRecord->save();
    }
}