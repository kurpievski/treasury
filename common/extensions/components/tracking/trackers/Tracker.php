<?php
namespace common\components\tracking\trackers;

use common\components\tracking\contracts\TrackerExtensionInterface;
use common\components\tracking\contracts\TrackerInterface;
use yii\base\BaseObject;
use yii\base\Exception;
use Yii;

/**
 * Class Tracker
 * @package common\components\tracking\trackers
 */
abstract class Tracker extends BaseObject implements TrackerInterface
{
    /**
     * @var string
     */
    public $ip;

    /**
     * @var string
     */
    public $event;

    /**
     * @var int|null
     */
    public $userId;

    /**
     * @var string
     */
    public $userAgent;

    /**
     * @var array
     */
    public $mappings;

    /**
     * @var array
     */
    public $extensions;

    /**
     * @inheritDoc
     */
    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @inheritDoc
     */
    public function getEvent(): string
    {
        return $this->event;
    }

    /**
     * @inheritDoc
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getUserAgent(): string
    {
        return $this->userAgent;
    }

    public function runExtensions()
    {
        foreach ($this->extensions as $alias => &$extension) {
            /** @var TrackerExtensionInterface $extension */
            $extension = Yii::createObject($extension);

            if (!($extension instanceof TrackerExtensionInterface)) {
                throw new Exception('Extension has to implement '.TrackerExtensionInterface::class);
            }

            $extension->run($this);
        }
    }

    protected function initMappings()
    {
        foreach ($this->mappings as &$property) {
            $propertyPartitioned = explode('.', $property);
            $count = count($propertyPartitioned);

            if ($count === 1) {
                $getter = 'get'.\ucfirst($property);
                $property = $this->$getter();
            } elseif ($count === 2) {
                list($extension, $extensionProperty) = $propertyPartitioned;
                $getter = 'get'.\ucfirst($extensionProperty);
                $property = $this->extensions[$extension]->$getter();
            } else {
                throw new Exception('Not valid Tracker mapping');
            }
        }
    }
}