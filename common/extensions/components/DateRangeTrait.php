<?php

namespace common\components;
use common\helpers\TimeHelper;

/**
 * Boris Matic <boris.matic@2amigos.us>
 * Company: 2amigOS! <https://2amigos.us>
 */
trait DateRangeTrait
{

    public function getPeriods()
    {
        return [
            TimeHelper::DATERANGE_PERIOD_TODAY => 'Today',
            TimeHelper::DATERANGE_PERIOD_THIS_WEEK => 'This Week',
            TimeHelper::DATERANGE_PERIOD_THIS_MONTH => 'This Month',
            TimeHelper::DATERANGE_PERIOD_THIS_YEAR => 'This Year'
        ];
    }

    public function getDateRangeByPeriod($period)
    {
        if ($period == TimeHelper::DATERANGE_PERIOD_THIS_YEAR) {
            return [date('Y-01-01 00:00:00'), date('Y-12-31 23:59:59')];
        }

        if ($period == TimeHelper::DATERANGE_PERIOD_THIS_MONTH) {
            return [date('Y-m-01 00:00:00'), date('Y-m-t 23:59:59')];
        }

        if ($period == TimeHelper::DATERANGE_PERIOD_THIS_WEEK) {
            return TimeHelper::getStartAndEndOfWeek(date('Y-m-d h:i:s'));
        }

        if ($period == TimeHelper::DATERANGE_PERIOD_TODAY) {
            return [date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59')];
        }

        $dates = explode(' - ', $period);
        $startDate = TimeHelper::changeDateTimeFormat($dates[0], TimeHelper::DATE_FORMAT, TimeHelper::SQL_DATE_FORMAT);
        $endDate = TimeHelper::changeDateTimeFormat($dates[1], TimeHelper::DATE_FORMAT, TimeHelper::SQL_DATE_FORMAT);

        return [date("{$startDate} 00:00:00"), date("{$endDate} 23:59:59")];
    }

    private function getDateRange()
    {
        return $this->getDateRangeByPeriod($this->{$this->periodAttribute});
    }

    public function getDateRangePickerValue()
    {
        $dateRange = $this->getDateRange();

        $startDate = TimeHelper::changeDateTimeFormat($dateRange[0], TimeHelper::SQL_DATETIME_FORMAT, TimeHelper::DATE_FORMAT);
        $endDate = TimeHelper::changeDateTimeFormat($dateRange[1], TimeHelper::SQL_DATETIME_FORMAT, TimeHelper::DATE_FORMAT);

        return [$startDate, $endDate];
    }
}