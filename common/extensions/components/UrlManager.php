<?php
namespace app\components;

use Yii;
use yii\web\Link;

class UrlManager extends \yii\web\UrlManager
{
    public $language;

    public $links = [];

    public $urlElements = [];

    public function init()
    {
        parent::init();

        if (empty($this->language)) {
            $this->language = Yii::$app->language;
        }

        if (!empty($this->links)) {
            foreach ($this->links as &$link) {
                if (is_array($link['href'])) {
                    $link['href'] = $this->createUrl($link['href']);
                }
                $link = new Link($link);
            }
        }
    }

    public function createUrl($params)
    {
        if (!empty($params[0])) {
            $urlElements = $this->urlElements;
            $route = explode('/', $params[0]);

            foreach ($route as &$element) {
                if (isset($urlElements[$element])) {
                    $element = $urlElements[$element];
                }
            }

            $params[0] = implode('/', $route);
        }

        return parent::createUrl($params);
    }

    public function parseRequest($request)
    {
        $pathInfo = explode('/', $request->getPathInfo());
        $urlElements = array_flip($this->urlElements);

        foreach ($pathInfo as &$element) {
            if (isset($urlElements[$element])) {
                $element = $urlElements[$element];
            }
        }

        $request->setPathInfo(implode('/',$pathInfo));

        return parent::parseRequest($request);
    }


    public function registerLink($id, $options)
    {
        $this->links[$id] = new Link($options);
    }

    public function registerLinks($links)
    {
        foreach ($links as $id => $options) {
            $this->registerLink($id, $options);
        }
    }

    public function getLinks($ids) {
        if (is_array($ids)) {
            $links = [];
            foreach ($ids as $id) {
                $links[$id] = $this->links[$id];
            }

            return $links;
        } else {
            return $this->links[$ids];
        }
    }
}
