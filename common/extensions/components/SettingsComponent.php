<?php

namespace common\components;

use Yii;
use yii\base\Component;

class SettingsComponent extends Component
{
    /**
     * @var array preload cached values
     */
    public $cache = array();

    /**
     * @var string model name for setting value
     */
    public $model = '\common\models\Settings';

    /**
     * Get application setting
     *
     * @param string $name
     * @param mixed $defaultValue
     * @return mixed
     */
    public function get($name, $defaultValue = null)
    {
        $modelClass = $this->model;

        $cachedValue = $this->getFromCache($name);
        if (!empty($cachedValue)) {
            return $cachedValue;
        }

        $value = $modelClass::get($name, $defaultValue);
        if ($value !== null) {
            $this->saveToCache($name, $value);
            return $value;
        }
        return $defaultValue;
    }

    public function getUncached($name, $defaultValue = null)
    {
        $modelClass = $this->model;

        $value = $modelClass::get($name, $defaultValue);

        if ($value !== null) {
            return $value;
        }

        return $defaultValue;
    }

    /**
     * Set application setting
     *
     * @param mixed $name
     * @param mixed $value
     */
    public function set($name, $value)
    {
        $modelClass = $this->model;

        $this->saveToCache($name, $value);
        $modelClass::set($name, $value);
    }

    /**
     * Get application setting from cache
     *
     * @param string $name
     * @return mixed
     */
    private function getFromCache($name)
    {
        if (!empty($this->cache[$name])) {
            return $this->cache[$name];
        }

        return Yii::$app->cache->get($name);
    }

    /**
     * Save application setting to cache
     *
     * @param string $name
     * @param mixed $value
     */
    private function saveToCache($name, $value)
    {
        $this->cache[$name] = $value;
        Yii::$app->cache->set($name, $value);
    }
}