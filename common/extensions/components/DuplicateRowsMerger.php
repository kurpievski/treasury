<?php
/**
 * Email: srdjan.drakul@2amigos.us
 * Date: 2/6/2019
 * Time: 4:00 PM
 */

namespace common\components;

use Yii;
use yii\base\BaseObject;
use yii\base\Exception;
use yii\db\Query;
use yii\db\Expression;

class DuplicateRowsMerger extends BaseObject
{
    public $table;
    public $uniqueColumn;
    public $primaryKey = 'id';
    public $queryLimit = 100;

    public function init()
    {
        parent::init();

        if(empty($this->uniqueColumn) || empty($this->table)) {
            throw new Exception("uniqueColumn and table have to be set.");
        }
    }

    protected function getDuplicatedUniqueColumnQuery()
    {
        return (new Query())
            ->select($this->uniqueColumn)
            ->distinct()
            ->from(['c' => (new Query())
                ->select($this->uniqueColumn)
                ->from($this->table)
                ->groupBy($this->uniqueColumn)
                ->having(new Expression("COUNT({$this->uniqueColumn}) > 1"))
            ]);
    }

    protected function getDuplicatedRows($value)
    {
        return (new Query())
            ->from($this->table)
            ->where([$this->uniqueColumn => $value])
            ->orderBy([$this->primaryKey => SORT_ASC])
            ->all();
    }

    public function run()
    {
        $query = $this->getDuplicatedUniqueColumnQuery();
        $total = $query->count();
        $counter = 0;

        echo "Total: {$total}\n";

        do {
            $duplicatedColumnValues = $query->limit($this->queryLimit)->column();

            foreach ($duplicatedColumnValues as $duplicatedColumnValue) {
                echo "Processing {$duplicatedColumnValue}\n";

                $counter++;
                $duplicatedRows = $this->getDuplicatedRows($duplicatedColumnValue);

                $baseRow = array_shift($duplicatedRows);

                foreach ($duplicatedRows as $duplicatedRow) {
                    foreach ($duplicatedRow as $key => $value) {
                        if(!empty($baseRow[$key]) || empty($value)) {
                            continue;
                        }

                        $baseRow[$key] = $value;
                    }
                }

                $id = $baseRow['id'];
                unset($baseRow['id']);

                Yii::$app->get('db')->createCommand()->update($this->table, $baseRow, [$this->primaryKey => $id])->execute();
                Yii::$app->get('db')->createCommand()->delete($this->table, ['and', [$this->uniqueColumn => $duplicatedColumnValue], ['<>', $this->primaryKey, $id]])->execute();
            }

            echo !empty($duplicatedColumnValues) ? "Working... (Offset: {$counter}/{$total})\n" : "";

        } while (!empty($duplicatedColumnValues));
    }
}