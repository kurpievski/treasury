<?php
/**
 * Boris Matic <boris.matic@2amigos.us>
 * Company: 2amigOS! <https://2amigos.us>
 */

namespace common\components\behaviors;

class EHistoryBehavior extends HistoryBehavior
{
    public $history_source = self::SOURCE_USER;

    public function leaveTrail($action, $name = null, $value = null, $old_value = null, $data = '')
    {
        if (!$this->active) {
            return true;
        }

        $historyModel = $this->historyModelClass;

        $log = new $historyModel();
        $className = $this->owner->className();

        $log->model = $className;
        $log->old_value = $old_value;
        $log->new_value = $value;
        $log->action = $action;
        $log->model_id = (string) $this->getNormalizedPk();
        $log->field = $name;
        $log->{$this->idAttribute} = $this->getId();
        $log->source = $this->history_source;
        $log->data = $data;

        if (!$log->save()) {
            throw new \yii\base\Exception(print_r($log->getErrors(), true));
        }

        return true;
    }
}