<?php
namespace common\components\behaviors\loggable;

use Yii;
use yii\base\Behavior;
use common\models\AuditTrail;
use yii\db\ActiveRecord;
use Exception;

class SettingsLoggableBehavior extends Behavior
{
    private $_oldattributes = array();
    public $allowed = array();
    public $ignored = array();
    public $ignoredClasses = array();
    public $dateFormat = 'Y-m-d H:i:s';
    public $userAttribute = null;
    public $storeTimestamp = false;
    public $skipNulls = true;
    public $active = true;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
        ];
    }

    public function afterFind($event)
    {
        $this->setOldAttributes($this->owner->getOldAttributes());
    }

    public function afterUpdate($event)
    {
        $this->audit(false);
    }

    public function audit($insert)
    {
        $newattributes = $this->owner->getAttributes();
        $oldattributes = $this->getOldAttributes();

        $acceptedCardsOld = $oldattributes['accepted_credit_cards'];
        $acceptedCardsNew = $newattributes['accepted_credit_cards'];
        unset($oldattributes['accepted_credit_cards']);
        unset($newattributes['accepted_credit_cards']);
        
        unset($newattributes['donation_amount']);
        unset($newattributes['donation_amount']);

        $endRecurringDatesOld = $oldattributes['max_recurring_end_dates'];
        $endRecurringDatesNew = $newattributes['max_recurring_end_dates'];
        unset($oldattributes['max_recurring_end_dates']);
        unset($newattributes['max_recurring_end_dates']);

        unset($oldattributes['ach_disclaimer']);
        unset($oldattributes['disclaimer']);
        unset($oldattributes['membership_disclaimer']);
        unset($oldattributes['discount_note']);
        unset($oldattributes['signature_acknowledgment']);

        unset($newattributes['ach_disclaimer']);
        unset($newattributes['disclaimer']);
        unset($newattributes['membership_disclaimer']);
        unset($newattributes['discount_note']);
        unset($newattributes['signature_acknowledgment']);

        // If no difference then WHY?
        // There is some kind of problem here that means "0" and 1 do not diff for array_diff so beware: stackoverflow.com/questions/12004231/php-array-diff-weirdness :S
        if (count(array_diff_assoc($newattributes, $oldattributes)) <= 0 &&
            count(array_diff_assoc($acceptedCardsNew, $acceptedCardsOld)) <= 0 &&
            count(array_diff_assoc($endRecurringDatesNew, $endRecurringDatesOld)) <= 0
        ) {
            return;
        }

        // Now lets actually write the attributes
        $this->auditAttributes($insert, $newattributes, $oldattributes);
        $this->auditCardAttributes($insert, $acceptedCardsNew, $acceptedCardsOld);
        $this->auditRecurringDatesAttribute($insert, $endRecurringDatesNew, $endRecurringDatesOld);

        // Reset old attributes to handle the case with the same model instance updated multiple times
        $this->setOldAttributes($this->owner->getAttributes());
    }

    public function auditAttributes($insert, $newattributes, $oldattributes = array())
    {
        foreach ($newattributes as $name => $value) {
            $old = isset($oldattributes[$name]) ? $oldattributes[$name] : '';

            // If we are skipping nulls then lets see if both sides are null
            if ($this->skipNulls && empty($old) && empty($value)) {
                continue;
            }

            // If they are not the same lets write an audit log
            if ($value != $old) {
                $this->leaveTrail($insert ? 'SET' : 'CHANGE', $name, $value, $old);
            }
        }
    }

    public function auditCardAttributes($insert, $acceptedCardsNew, $acceptedCardsOld = array())
    {
        foreach ($acceptedCardsNew as $name) {
            foreach ($acceptedCardsOld as $oldName) {
                if ($name == $oldName) {
                    continue 2;
                }
            }

            $this->leaveTrail($insert ? 'SET' : 'CHANGE', $name, 1, 0);
        }

        foreach ($acceptedCardsOld as $name) {
            foreach ($acceptedCardsNew as $newName) {
                if ($name == $newName) {
                    continue 2;
                }
            }

            $this->leaveTrail($insert ? 'SET' : 'CHANGE', $name, 0, 1);
        }
    }

    public function leaveTrail($action, $name = null, $value = null, $old_value = null)
    {
        if ($this->active) {
            $log = new AuditTrail();
            $log->old_value = $old_value;
            $log->new_value = $value;
            $log->action = $action;
            $log->model = $this->owner->className(); // Gets a plain text version of the model name
            $log->model_id = AuditTrail::EMPTY_MODEL;
            $log->field = $name;
            $log->stamp = $this->storeTimestamp ? time() : date($this->dateFormat); // If we are storing a timestamp lets get one else lets get the date
            $log->user_id = (string) $this->getUserId(); // Lets get the user id
            return $log->save();
        } else {
            return true;
        }
    }

    public function getOldAttributes()
    {
        return $this->_oldattributes;
    }

    public function setOldAttributes($value)
    {
        $this->_oldattributes = $value;
    }

    public function getUserId()
    {
        if (isset($this->userAttribute)) {
            $data = $this->owner->getAttributes();

            return isset($data[$this->userAttribute]) ? $data[$this->userAttribute] : null;
        } else {
            try {
                $userid = Yii::$app->user->id;

                return empty($userid) ? null : $userid;
            } catch (Exception $e) { //If we have no user object, this must be a command line program
                return null;
            }
        }
    }

    protected function getNormalizedPk()
    {
        $pk = $this->owner->getPrimaryKey();

        return is_array($pk) ? json_encode($pk) : $pk;
    }

    private function auditRecurringDatesAttribute($insert, $endRecurringDatesNew, $endRecurringDatesOld)
    {
        foreach ($endRecurringDatesNew as $processorId => $value) {

            $oldValue = !empty($endRecurringDatesOld[$processorId]) ? $endRecurringDatesOld[$processorId] : null;

            if ($oldValue == $value) {
                continue;
            }

            $this->leaveTrail($insert ? 'SET' : 'CHANGE', $processorId, $value, $oldValue);
        }

        foreach ($endRecurringDatesOld as $processorId => $value) {

            $newValue = !empty($endRecurringDatesNew[$processorId]) ? $endRecurringDatesNew[$processorId] : null;

            if ($newValue == $value) {
                continue;
            }

            $this->leaveTrail($insert ? 'SET' : 'CHANGE', $processorId, $value, $newValue);
        }
    }
}
