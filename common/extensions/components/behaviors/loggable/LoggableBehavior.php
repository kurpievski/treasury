<?php
namespace common\components\behaviors\loggable;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use Exception;

class LoggableBehavior extends Behavior
{
    const MYSQL_DATE_FORMAT = 'Y-m-d H:i:s';

    /** @var ActiveRecord */
    public $owner;

    private $_oldAttributes = [];

    public $allowedAttributes = [];
    public $ignored = [];
    public $ignoredClasses = [];
    public $dateFormat = self::MYSQL_DATE_FORMAT;
    public $userAttribute = null;
    public $storeTimestamp = false;
    public $skipNulls = true;
    public $active = true;
    public $auditTrailClass = 'common\models\AuditTrail';

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    public function afterDelete()
    {
        $this->leaveTrail('DELETE');
    }

    public function afterFind()
    {
        $this->setOldAttributes($this->getAllowedAttributes());
    }

    public function afterInsert()
    {
        $this->audit(true);
    }

    public function afterUpdate()
    {
        $this->audit(false);
    }

    protected function getAllowedAttributes()
    {
        return $this->owner->getAttributes(!empty($this->allowedAttributes) ? $this->allowedAttributes : null);
    }

    public function audit($insert)
    {

        $newAttributes = $this->getAllowedAttributes();
        $oldAttributes = $this->getOldAttributes();

        if (!empty($this->ignoredClasses) && in_array($this->owner->className(), $this->ignoredClasses)) {
            return;
        }

        if (count(array_diff_assoc($newAttributes, $oldAttributes)) <= 0) {
            return;
        }

        $transaction = $this->owner->getDb()->beginTransaction();

        try {
            if ($insert) {
                $this->leaveTrail('CREATE');
            }

            $this->auditAttributes($insert, $newAttributes, $oldAttributes);

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        $this->setOldAttributes($this->getAllowedAttributes());
    }

    public function auditAttributes($isInsert, $newAttributes, $oldAttributes = [])
    {
        foreach ($newAttributes as $name => $value) {
            $old = isset($oldAttributes[$name]) ? $oldAttributes[$name] : '';

            // If we are skipping nulls then lets see if both sides are null
            if ($this->skipNulls && empty($old) && empty($value)) {
                continue;
            }

            // If they are not the same lets write an audit log
            if ($value != $old) {
                $this->leaveTrail($isInsert ? 'SET' : 'CHANGE', $name, $value, $old);
            }
        }
    }

    public function leaveTrail($action, $name = null, $newValue = null, $oldValue = null)
    {
        if (!$this->active) {
            return true;
        }

        $modelClass = $this->auditTrailClass;

        return (new $modelClass([
            'old_value' => $oldValue,
            'new_value' => $newValue,
            'action' => $action,
            'model' => $this->owner->className(),
            'model_id' => (string) $this->getNormalizedPk(),
            'field' => $name,
            'stamp' => $this->storeTimestamp ? time() : date($this->dateFormat),
            'user_id' => $this->getUserId(),
        ]))->save();
    }

    public function getOldAttributes()
    {
        return $this->_oldAttributes;
    }

    public function setOldAttributes($value)
    {
        $this->_oldAttributes = $value;
    }

    public function getUserId()
    {
        if ($this->userAttribute !== null) {
            return $this->owner->getAttribute($this->userAttribute);
        } else {
            try {
                $userId = Yii::$app->user->id;

                return empty($userId) ? null : $userId;
            } catch (Exception $e) {
                return null;
            }
        }
    }

    protected function getNormalizedPk()
    {
        $pk = $this->owner->getPrimaryKey();

        return is_array($pk) ? json_encode($pk) : $pk;
    }
}
