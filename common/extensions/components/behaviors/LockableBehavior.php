<?php
/**
 * LockableBehavior.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 29-Jan-16
 * Time: 18:05
 */

namespace common\components\behaviors;

use common\extensions\ActiveRecord;
use common\helpers\TimeHelper;
use Yii;
use yii\base\Behavior;
use yii\base\ModelEvent;
use yii\db\BaseActiveRecord;

class LockableBehavior extends Behavior
{
    public $lockedByAttribute;
    public $lockTimeAttribute;
    public $lockExpireSeconds = 60;
    public $primaryKeyAttribute = 'id';

    /** @var ActiveRecord */
    public $owner;

    protected $_isDisabled = false;

    public function init()
    {
        $this->_isDisabled = \Yii::$app->request->isConsoleRequest;
        parent::init();
    }

    public function events()
    {
        return [
            BaseActiveRecord::EVENT_BEFORE_VALIDATE => 'checkLock'
        ];
    }

    protected function saveLockChanges()
    {
        if(!empty($this->owner->{$this->lockedByAttribute})) {
            /** @var ActiveRecord $ownerClass */
            $ownerClass = $this->getOwnerClass();

            $ownerClass::updateAll([$this->lockedByAttribute => null, $this->lockTimeAttribute => null], [
                'and',
                ['=', $this->lockedByAttribute, $this->owner->{$this->lockedByAttribute}],
                ['<>', $this->primaryKeyAttribute, $this->owner->{$this->primaryKeyAttribute}]
            ]);
        }

        $this->owner->updateAttributes([$this->lockedByAttribute, $this->lockTimeAttribute]);
    }

    public function removeLock()
    {
        if (!$this->canAcquireLock() || $this->_isDisabled) {
            return false;
        }

        $this->owner->{$this->lockedByAttribute} = null;
        $this->owner->{$this->lockTimeAttribute} = null;

        $this->saveLockChanges();

        return true;
    }

    public function lock()
    {
        if (!$this->canAcquireLock() || $this->_isDisabled) {
            return false;
        }

        $this->owner->{$this->lockedByAttribute} = Yii::$app->user->getId();
        $this->owner->{$this->lockTimeAttribute} = date(TimeHelper::SQL_DATETIME_FORMAT, time());

        $this->saveLockChanges();

        return true;
    }

    public function checkLock(ModelEvent $event)
    {
        if ($this->_isDisabled) {
            return;
        }

        if (Yii::$app->user->getIsGuest()) {
            $this->owner->addError($this->lockedByAttribute, 'User is not logged in.');
            $event->isValid = false;

            return;
        }

        if (!$this->canAcquireLock()) {
            $this->owner->addError($this->lockedByAttribute, 'This item is currently locked by another user.');
            $event->isValid = false;
        }
    }

    public function isLocked()  {
        if(empty($this->owner->{$this->lockedByAttribute})){
            return false;
        }

        $userId = Yii::$app->user->getId();

        if (empty($this->owner->{$this->lockedByAttribute}) ||
            $userId == $this->owner->{$this->lockedByAttribute}) {
            return false;
        }

        return !$this->isLockExpired();
    }

    protected function isLockExpired()
    {
        $lockTime = $this->owner->{$this->lockTimeAttribute};

        $expireTime = $lockTime !== null ? strtotime($lockTime) + $this->lockExpireSeconds : 0;
        $currentTime = time();

        return ($currentTime > $expireTime);
    }

    public function canAcquireLock()
    {
        if ($this->_isDisabled) {
            return true;
        }

        $userId = Yii::$app->user->getId();

        if (empty($this->owner->{$this->lockedByAttribute}) ||
            $userId == $this->owner->{$this->lockedByAttribute}) {
            return true;
        }

        return $this->isLockExpired();
    }

    protected function getOwnerClass()
    {
        return get_class($this->owner);
    }
}