<?php
/**
 * Author: Srdjan Drakul
 * Date: 2/22/2016 5:03 PM
 * Email: srdjan_drakul@yahoo.com
 */

namespace common\components\behaviors;

use common\extensions\ActiveRecord;
use Yii;
use yii\base\Behavior;
use yii\base\Model;
use yii\db\BaseActiveRecord;
use yii\web\HttpException;
use yii\web\UploadedFile;

class FileBehavior extends Behavior
{
    /**
     * @var string model's attribute name that will be associated with original file name
     */
    public $nameAttribute = 'original_name';

    /**
     * @var string model's attribute name that will be associated with storage key
     */
    public $storageKeyAttribute = 'storage_key';


    /** @var string Mime type attribute */
    public $mimeTypeAttribute = 'mime_type';

    /**
     * @var string file attribute
     */
    public $fileAttribute = 'uploaded_file';

    public $resourceManager = 'documentResourceManager';

    /**
     * @var string shouldDeleteFromStorage;
     */
    public $shouldDeleteFromStorage = false;

    /**
     * @var string tmp file holder
     */
    private $_tmpFile = null;

    /**
     * @var string location for tmp file
     */
    private $_tmpStorageLocation = '@app/runtime/';

    public function __destruct()
    {
        @unlink($this->_tmpFile);
    }

    public function events()
    {
        return [
            BaseActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsert',
            BaseActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
            BaseActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdate'
        ];
    }

    /**
     * @return UploadedFile|null
     */
    protected function getUploadedFile()
    {
        /** @var Model $owner */
        $owner = $this->owner;

        if (!$owner->hasProperty($this->fileAttribute)) {
            return null;
        }

        if (empty($owner->{$this->fileAttribute})) {
            $attribute = "{$owner->formName()}[{$this->fileAttribute}]";

            return UploadedFile::getInstanceByName($attribute);
        }

        return $owner->{$this->fileAttribute};
    }

    public function beforeInsert()
    {
        $file = $this->getUploadedFile();

        if (empty($file)) {
            return true;
        }

        $this->processUploadedFile();
    }

    public function beforeUpdate()
    {
        $file = $this->getUploadedFile();

        if (empty($file)) {
            return true;
        }

        $this->deleteFromStorage();

        $this->processUploadedFile();
    }

    public function processUploadedFile()
    {
        /** @var ActiveRecord $owner */
        $owner = $this->owner;
        $file = $this->getUploadedFile();

        $this->_tmpFile = $file->tempName;

        $storageKey = $this->getGeneratedStorageKey($file->getExtension());

        if (empty($owner->{$this->nameAttribute})) {
            $owner->{$this->nameAttribute} = $file->name;
        }

        $owner->{$this->storageKeyAttribute} = $storageKey;

        if ($owner->hasAttribute($this->mimeTypeAttribute)) {
            $owner->{$this->mimeTypeAttribute} = $file->type;
        }

        if (!$this->saveOnStorageAs($storageKey)) {
            throw new HttpException(500, 'Sorry, we are unable to upload your file.');
        }
    }

    public function beforeDelete()
    {
        if (!$this->shouldDeleteFromStorage) {
            return true;
        }

        $storageKey = $this->getStorageKey();

        if (empty($storageKey)) {
            return true;
        }

        return $this->deleteFromStorage();
    }

    /**
     * Get File storage key
     * @return string
     */
    public function getStorageKey()
    {
        return $this->owner->{$this->storageKeyAttribute};
    }

    /**
     * Generate unique storage_key
     * @param string $extension
     * @return string
     */
    protected function getGeneratedStorageKey($extension = '')
    {
        return time() . '_' . md5(microtime(true)) . '_' . mt_rand(1, 500) . ".{$extension}";
    }

    /**
     * Get file path on local storage
     * @return null|string
     */
    public function getLocalFilePath()
    {
        if (!empty($this->_tmpFile)) {
            return $this->_tmpFile;
        }

        $fileStorageKey = $this->getStorageKey();
        $resourceManager = $this->getResourceManager();
        $fileData = $resourceManager->getFileData($fileStorageKey);
        $fileExtension = '.' . pathinfo($fileStorageKey, PATHINFO_EXTENSION);
        $filePath = tempnam(Yii::getAlias($this->_tmpStorageLocation), md5($fileStorageKey) . '-') . $fileExtension;

        file_put_contents($filePath, $fileData);

        return $this->_tmpFile = $filePath;
    }

    /**
     * Save file on storage using object storage key
     * @return bool
     */
    public function saveOnStorage()
    {
        return $this->saveOnStorageAs($this->getStorageKey());
    }

    public function hasFile()
    {
        return $this->getResourceManager()->fileExists($this->owner->{$this->storageKeyAttribute});
    }

    /**
     * Save File on storage as different file name
     * @param string $storageKey
     * @return bool
     */
    protected function saveOnStorageAs($storageKey)
    {
        $resourceManager = $this->getResourceManager();
        return $resourceManager->sendFile($this->getLocalFilePath(), $storageKey);
    }

    /**
     * Delete File from storage
     * @return bool
     */
    protected function deleteFromStorage()
    {
        $resourceManager = $this->getResourceManager();
        $filename = $this->getStorageKey();
        if ($resourceManager->fileExists($filename)) {
            return $resourceManager->deleteFile($filename);
        } else {
            return true;
        }
    }

    public function getFileUrl()
    {
        $resourceManager = $this->getResourceManager();
        return $resourceManager->getUrl($this->getStorageKey(), Yii::$app->params['file.expire']);
    }

    public function getResourceManager()
    {
        return Yii::$app->{$this->resourceManager};
    }
}