<?php
/**
 * Email: srdjan.drakul@2amigos.us
 * Date: 12/29/2017
 * Time: 2:47 PM
 */

namespace common\components\behaviors;

use yii\base\Behavior;
use yii\caching\Cache;
use yii\db\BaseActiveRecord;

class QueryCacheBehavior extends Behavior
{
    public function events()
    {
        return [
            BaseActiveRecord::EVENT_AFTER_INSERT => 'invalidateCache',
            BaseActiveRecord::EVENT_AFTER_UPDATE => 'invalidateCache',
            BaseActiveRecord::EVENT_AFTER_DELETE => 'invalidateCache'
        ];
    }

    public function invalidateCache()
    {
        /** @var Cache $queryCache */
        $queryCache = \Yii::$app->queryCache;

        $queryCache->flush();
    }
}