<?php
namespace common\components\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;

class HistoryBehavior extends Behavior
{
    const ACTION_DELETE = 'DELETE';
    const ACTION_CREATE = 'CREATE';
    const ACTION_SET = 'SET';
    const ACTION_CHANGE = 'CHANGE';
    const ACTION_NOTE = 'NOTE';
    const ACTION_SAVE = 'SAVE';
    const ACTION_MERGE = 'MERGE';

    const SOURCE_USER = 'user';
    const SOURCE_CAC = 'cac';
    const SOURCE_SIMEX = 'simex';
    const SOURCE_PROCEDURE = 'procedure';
    const SOURCE_MANUAL_MERGE = 'manual_merge';
    const SOURCE_AUTOMATIC_MERGE = 'automatic_merge';

    protected $_oldAttributes = [];
    public $allowed = [];
    public $ignored = [];
    public $ignoredClasses = [];
    public $userAttribute = null;
    public $storeTimestamp = false;
    public $skipNulls = true;
    public $active = true;
    public $historyModelClass;
    public $idAttribute;
    public $ownerIdAttribute;
    public $source = self::SOURCE_USER;
    public $data = '';

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    public function afterDelete($event)
    {
        $trackedAttributes = array_diff($this->owner->attributes(), $this->getIgnoredFields());
        $attributes = $this->owner->getAttributes($trackedAttributes);

        $this->leaveTrail(self::ACTION_DELETE, null, null, null, json_encode($attributes));
    }

    public function afterFind($event)
    {
        $this->setOldAttributes($this->owner->getAttributes());
    }

    public function afterInsert($event)
    {
        $this->audit(true);
    }

    public function afterUpdate($event)
    {
        $this->audit(false);
    }

    public function audit($insert)
    {
        $allowedFields = $this->allowed;
        $ignoredFields = $this->getIgnoredFields();
        $ignoredClasses = $this->ignoredClasses;

        $newattributes = $this->owner->getAttributes();
        $oldattributes = $this->getOldAttributes();

        if (sizeof($ignoredClasses) > 0) {
            if (array_search(get_class($this->owner), $ignoredClasses) !== false){
                return;
            }
        }

        if (sizeof($allowedFields) > 0) {
            foreach ($newattributes as $f => $v) {
                if (array_search($f, $allowedFields) === false) {
                    unset($newattributes[$f]);
                }
            }

            foreach ($oldattributes as $f => $v) {
                if (array_search($f, $allowedFields) === false) {
                    unset($oldattributes[$f]);
                }
            }
        }

        if (sizeof($ignoredFields) > 0) {
            foreach ($newattributes as $f => $v) {
                if (array_search($f, $ignoredFields) !== false) {
                    unset($newattributes[$f]);
                }
            }

            foreach ($oldattributes as $f => $v) {
                if (array_search($f, $ignoredFields) !== false) {
                    unset($oldattributes[$f]);
                }
            }
        }

        // There is some kind of problem here that means "0" and 1 do not diff for array_diff so beware: stackoverflow.com/questions/12004231/php-array-diff-weirdness :S
        if (count(array_diff_assoc($newattributes, $oldattributes)) <= 0) {
            return;
        }

        if ($insert) {
            $this->leaveTrail(self::ACTION_CREATE);
        }

        $this->auditAttributes($insert, $newattributes, $oldattributes);

        $this->setOldAttributes($this->owner->getAttributes());
    }

    public function auditAttributes($insert, $newattributes, $oldattributes = [])
    {
        foreach ($newattributes as $name => $value) {
            $old = isset($oldattributes[$name]) ? $oldattributes[$name] : '';

            if ($this->skipNulls && empty($old) && empty($value)) {
                continue;
            }

            if ($value != $old) {
                $this->leaveTrail($this->getAction($insert), $name, $value, $old);
            }
        }
    }

    public function leaveTrail($action, $name = null, $value = null, $old_value = null, $data = '')
    {
        if (!$this->active) {
            return true;
        }

        $historyModel = $this->historyModelClass;

        $log = new $historyModel();
        $className = $this->owner->className();

        $log->model = $className;
        $log->old_value = $old_value;
        $log->new_value = $value;
        $log->action = $action;
        $log->model_id = (string) $this->getNormalizedPk();
        $log->field = $name;

        if(!empty($this->idAttribute)) {
            $log->{$this->idAttribute} = $this->getId();
        }

        $log->source = $this->source;
        $log->data = !empty($data) ? $data : $this->data;


        if (!$log->save()) {
            throw new \yii\base\Exception(print_r($log->getErrors(), true));
        }

        return true;
    }

    public function getOldAttributes()
    {
        return $this->_oldAttributes;
    }

    public function setOldAttributes($value)
    {
        $this->_oldAttributes = $value;
    }

    protected function getNormalizedPk()
    {
        $pk = $this->owner->getPrimaryKey();

        return is_array($pk) ? json_encode($pk) : $pk;
    }

    protected function getIgnoredFields()
    {
        return array_merge($this->ignored, [
            'updated_at',
            'created_at',
            'update_id',
            'create_id',
            'is_deleted'
        ]);
    }

    protected function getId()
    {
        if (empty($this->ownerIdAttribute)) {
            return null;
        }

        if (is_callable($this->ownerIdAttribute)) {
            return call_user_func($this->ownerIdAttribute, $this->owner);
        }

        return $this->owner->{$this->ownerIdAttribute};
    }

    protected function getAction($insert)
    {
        return $insert ? self::ACTION_SET : self::ACTION_CHANGE;
    }
}
