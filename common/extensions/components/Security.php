<?php
/**
 * Date: 3/8/2016
 * Time: 5:01 PM
 * Author: Igor Golub <goolub.igor@gmail.com>
 */

namespace common\components;


class Security extends \yii\base\Security
{
    public function encryptByKey($data, $inputKey = null, $info = null)
    {
        $inputKey = $inputKey ? : $this->getEncryptionKey();
        $encryptedData = parent::encryptByKey($data, $inputKey, $info);

        return base64_encode($encryptedData);
    }

    public function decryptByKey($data, $inputKey = null, $info = null)
    {
        $inputKey = $inputKey ? : $this->getEncryptionKey();
        $data = base64_decode($data);
        return parent::decryptByKey($data, $inputKey, $info);
    }

    protected function getEncryptionKey()
    {
        return \Yii::$app->params['encryptionKey'];
    }
}