<?php
/**
 * Date: 9/20/2016
 * Time: 7:53 PM
 * Author: Igor Golub <igor.golub@2amigos.us>
 */

namespace common\components;


class DbMessageSource extends \yii\i18n\DbMessageSource
{
    public $version = '';

    protected function loadMessages($category, $language)
    {
        if ($this->enableCaching) {
            $key = [
                __CLASS__,
                $this->version,
                $category,
                $language,
            ];
            $messages = $this->cache->get($key);
            if ($messages === false) {
                $messages = $this->loadMessagesFromDb($category, $language);
                $this->cache->set($key, $messages, $this->cachingDuration);
            }

            return $messages;
        } else {
            return $this->loadMessagesFromDb($category, $language);
        }
    }

    public function reloadMessages($category, $language)
    {
        $key = [
            __CLASS__,
            $this->version,
            $category,
            $language,
        ];

        $messages = $this->loadMessagesFromDb($category, $language);
        $this->cache->set($key, $messages, $this->cachingDuration);
    }
}