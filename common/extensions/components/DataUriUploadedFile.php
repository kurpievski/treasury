<?php
/**
 * DataUriUploadedFile.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 08-Jan-16
 * Time: 16:06
 */

namespace common\components;

use Yii;
use yii\base\InvalidConfigException;
use yii\web\UploadedFile;

class DataUriUploadedFile extends UploadedFile
{
    public $fileKey;
    public $dataUri;

    protected static $_uriFiles = [];

    public function init()
    {
        parent::init();

        if (empty($this->dataUri)) {
            throw new InvalidConfigException('dataUri must be filled.');
        }

        $this->error = UPLOAD_ERR_OK;

        $dataParts = explode(',', $this->dataUri);

        $parsedUri = [];

        if (count($dataParts) !== 2 || !preg_match('/^data\:(.+\/.+);(.+)$/i', $dataParts[0], $parsedUri)) {
            $this->error = UPLOAD_ERR_NO_FILE;
            return;
        }

        if ($parsedUri[2] !== 'base64') {
            $this->error = UPLOAD_ERR_NO_FILE;
            return;
        }

        $this->type = $parsedUri[1];

        $this->tempName = tempnam(sys_get_temp_dir(), 'dataUriUpload-');
        file_put_contents($this->tempName, base64_decode($dataParts[1]));

        $this->size = filesize($this->tempName);
        $this->name = $this->getFileName();
    }

    public static function getInstanceByName($name)
    {
        $files = self::loadDataUriFiles($name);
        return isset($files[$name]) ? $files[$name] : null;
    }

    public static function getInstancesByName($name)
    {
        $files = self::loadDataUriFiles($name);
        if (isset($files[$name])) {
            return [$files[$name]];
        }
        $results = [];
        foreach ($files as $key => $file) {
            if (strpos($key, "{$name}[") === 0) {
                $results[] = $file;
            }
        }

        return $results;
    }

    public function saveAs($file, $deleteTempFile = true)
    {
        if (!file_exists($this->tempName)) {
            return false;
        }

        $result = copy($this->tempName, $this->fileKey);

        if ($deleteTempFile) {
            unlink($this->tempName);
        }

        return $result;
    }

    protected static function loadDataUriFiles($name)
    {
        $postData = Yii::$app->request->post($name);

        if ($postData === null) {
            return static::$_uriFiles;
        }

        if (!isset(static::$_uriFiles[$name])) {
            if (is_array($postData)) {
                foreach ($postData as $index => $dataUri) {
                    static::loadDataUriFilesRecursive($name, $dataUri, $index);
                }
            } else {
                self::setDataUriInstance($name, 1, $postData);
            }
        }

        return static::$_uriFiles;
    }

    protected static function loadDataUriFilesRecursive($key, $dataUris, $indexKey)
    {
        if (is_array($dataUris)) {
            foreach ($dataUris as $i => $dataUri) {
                self::loadDataUriFilesRecursive($key . '[' . $i . ']', $dataUri, $indexKey . '-' . $i);
            }
            return;
        }

        self::setDataUriInstance($key, $indexKey, $dataUris);
    }

    protected static function setDataUriInstance($name, $indexKey, $dataUri)
    {
        self::$_uriFiles[$name] = new static([
            'fileKey' => $indexKey,
            'dataUri' => $dataUri
        ]);
    }

    protected function getFileName()
    {
        $fileName = 'file-' . md5(microtime(true)) . '-' . $this->fileKey;

        switch ($this->type) {
            case 'image/jpeg':
            case 'image/jpg':
                return $fileName . '.jpg';
            case 'image/png':
                return $fileName . '.png';
            default:
                return $fileName;
        }
    }
}