<?php

/**
 * Sep 10, 2015
 * 3:20:20 AM
 * @author Nikola Radovic <nikola@2amigos.us>
 */
namespace common\components;

use Yii;
use yii\filters\AccessControl as AAccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\User;

class AccessControl extends AAccessControl
{
    /**
     * Denies the access of the user.
     * The default implementation will redirect the user to the login page if he is a guest;
     * if the user is already logged, a 403 HTTP exception will be thrown.
     * @param User $user the current user
     * @throws ForbiddenHttpException if the user is already logged in.
     */
    protected function denyAccess($user)
    {
        if ($user->getIsGuest()) {
            $user->loginRequired();
        } else {
            /** @var \common\models\User $model */
            $model = $user->getIdentity();

            Yii::$app->session->setFlash('error', "
            You are currently logged in as ({$model->username}).
            If you would like to logon as another user please logout and log back in.");
            Yii::$app->getResponse()->redirect(['/site/index']);
        }
    }
}
