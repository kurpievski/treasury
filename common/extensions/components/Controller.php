<?php

/**
 * Sep 10, 2015
 * 3:01:50 AM
 * @author Nikola Radovic <nikola@2amigos.us>
 */

namespace common\components;

use Yii;
use yii\web\Controller as AController;

class Controller extends AController
{
    public $layout = 'backend';

    public function beforeAction($action)
    {
        if (Yii::$app->user->getIsGuest()) {
            Yii::$app->user->loginUrl = ['/admin/login'];
            Yii::$app->user->logoutUrl = ['/admin/logout'];
        }

        if (!Yii::$app->user->getIsGuest() && Yii::$app->user->getIsPatient()) {
            $this->redirect(['/site/index']);
            return false;
        }

        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

}
