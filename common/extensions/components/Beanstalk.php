<?php
/**
 * Created by PhpStorm.
 * User: Nikola Radovic <nikola@2amigos.us>
 * Date: 5/11/2016
 * Time: 6:09 PM
 */

namespace common\components;

use Pheanstalk\Exception\ConnectionException;
use Pheanstalk\Job;
use Pheanstalk\Pheanstalk;
use Pheanstalk\PheanstalkInterface;
use Pheanstalk\Response;
use Yii;
use yii\base\Component;

/**
 * Class Beanstalk
 * base taken from https://github.com/udokmeci/yii2-beanstalk
 *
 * @method Beanstalk useTube($tube)
 * @method Beanstalk watch($tube)
 * @method Job reserve($timeout = null)
 * @method Response statsJob($job)
 * @method void bury($job, $priority = Pheanstalk::DEFAULT_PRIORITY)
 * @method Beanstalk release($job, $priority = Pheanstalk::DEFAULT_PRIORITY, $delay = Pheanstalk::DEFAULT_DELAY);
 * @method Beanstalk delete($job);
 */
class Beanstalk extends Component
{
    const DEFAULT_TIME_TO_RUN = 3600;

    /** @var  Pheanstalk */
    public $_beanstalk;

    public $host = "127.0.0.1";
    public $port = 11300;

    public $priority;
    public $delay;
    public $timeToRun;

    public $connectTimeout = 1;
    public $connected = false;
    public $sleep = false;

    public function init()
    {
        parent::init();

        try {
            $this->_beanstalk = new Pheanstalk($this->host, $this->port, $this->connectTimeout);
        } catch (ConnectionException $e) {
            Yii::error($e);
        }

        if (empty($this->priority)) {
            $this->priority = PheanstalkInterface::DEFAULT_PRIORITY;
        }

        if (empty($this->delay)) {
            $this->delay = PheanstalkInterface::DEFAULT_DELAY;
        }

        if (empty($this->timeToRun)) {
            $this->timeToRun = self::DEFAULT_TIME_TO_RUN;
        }
    }

    private function isJson($string)
    {
        json_decode($string);

        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * {@inheritDoc}
     */
    public function put($data, $priority = null, $delay = null, $timeToRun = null)
    {
        try {
            if (!is_string($data)) {
                $data = json_encode($data);
            }

            $priority = !empty($priority) ? $priority : $this->priority;
            $delay = !empty($delay) ? $delay : $this->delay;
            $timeToRun = !empty($timeToRun) ? $timeToRun : $this->timeToRun;

            return $this->_beanstalk->put($data, $priority, $delay, $timeToRun);
        } catch (ConnectionException $e) {
            Yii::error($e);

            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function putInTube($tube, $data, $priority = null, $delay = null, $timeToRun = null)
    {

        try {
            $this->_beanstalk->useTube($tube);


            $priority = !empty($priority) ? $priority : $this->priority;
            $delay = !empty($delay) ? $delay : $this->delay;
            $timeToRun = !empty($timeToRun) ? $timeToRun : $this->timeToRun;

            return $this->put($data, $priority, $delay, $timeToRun);
        } catch (ConnectionException $e) {
            Yii::error($e);

            return false;
        }
    }

    public function delay($job, $priority = null, $delay = null)
    {
        $priority = !empty($priority) ? $priority : $this->priority;
        $delay = !empty($delay) ? $delay : $this->delay;
        
        return $this->_beanstalk->release($job, $priority, $delay);
    }

    public function __call($method, $args)
    {

        try {
            $result = call_user_func_array(array($this->_beanstalk, $method), $args);

            // Chaining.
            if ($result instanceof Pheanstalk) {
                return $this;
            }

            // Check for json data.
            if ($result instanceof Job) {
                if ($this->isJson($result->getData())) {
                    $result = new Job($result->getId(), json_decode($result->getData(), true));
                }
            }

            return $result;

        } catch (ConnectionException $e) {
            Yii::error($e);

            return false;
        }
    }
}
