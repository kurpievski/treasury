<?php
namespace common\components\filereader;
use common\components\filereader\parsers\AbstractParser;
use common\components\filereader\parsers\DocxParser;
use common\components\filereader\parsers\PdfParser;
use common\helpers\FileHelper;
use yii\base\Component;
use yii\base\Exception;

/**
 * Created by PhpStorm.
 * User: igor.golub@2amigos.us
 * Date: 08/06/2017
 * Time: 12:04
 */

class Reader extends Component {

    const PARSER_PDF = 'pdfparser';
    const PARSER_DOCX = 'docxparser';

    /** @var string mime type */
    public $mimeType;

    /** @var string Selected file parser */
    public $selectedParser;

    /** @var array File Parser config. */
    public $fpConfig = [];

    /** @var AbstractParser */
    protected $_parserInstance = null;

    public function getParsers()
    {
        return [
            self::PARSER_PDF => PdfParser::class,
            self::PARSER_DOCX => DocxParser::class
        ];
    }

    public function init()
    {
        if (!empty($this->mimeType)) {
            $this->selectParserByMimeType();
        }

        if (!array_key_exists($this->selectedParser, $this->getParsers())) {
            throw new Exception("Selected file parser does not exist.");
        }

        parent::init();
    }

    public function getParserInstance()
    {
        if (empty($this->_parserInstance)) {
            $className = $this->getParsers()[$this->selectedParser];
            $parserConfig = !empty($this->fpConfig[$this->selectedParser]) ?
                $this->fpConfig[$this->selectedParser] : [];

            $this->_parserInstance = new $className($parserConfig);
        }

        return $this->_parserInstance;
    }

    public function loadFile($file)
    {
        return $this->getParserInstance()->loadFile($file);
    }

    public function hasError()
    {
        return $this->getParserInstance()->hasError();
    }

    public function getErrorMessage()
    {
        return $this->getParserInstance()->getErrorMessage();
    }

    public function getAsString()
    {
        return $this->getParserInstance()->getAsString();
    }

    private function selectParserByMimeType()
    {
        if ($this->mimeType == FileHelper::MIME_TYPE_PDF) {
            $this->selectedParser = self::PARSER_PDF;
        }

        if ($this->mimeType == FileHelper::MIME_TYPE_DOCX) {
            $this->selectedParser = self::PARSER_DOCX;
        }
    }

}