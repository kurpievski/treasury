<?php
/**
 * Created by PhpStorm.
 * User: igor.golub@2amigos.us
 * Date: 08/06/2017
 * Time: 12:15
 */

namespace common\components\filereader\parsers;

use Throwable;
use yii\base\BaseObject;
use yii\base\Exception;

abstract class AbstractParser extends BaseObject
{
    /** @var string File path */
    private $_file;

    /** @var boolean has error */
    private $_error;

    /** @var string error message */
    private $_errorMessage;

    /**
     * @param  string $file
     * @return bool
     */
    public function loadFile($file)
    {
       try {
           if (!file_exists($file)) {
               throw new FileNotFoundException($file);
           }

           if (!$this->isFileSupported($file)) {
               throw new FileNotSupported($file);
           }

           $this->_file = $file;

           return true;
       }catch (Exception $e) {
            $this->setError($e->getMessage());

            return false;
       }
    }

    public function getFile()
    {
        return $this->_file;
    }

    /**
     * @param string $file
     * @return bool
     */
    protected abstract function isFileSupported($file);

    /**
     * @param string $message
     */
    protected function setError($message)
    {
        $this->_error = true;
        $this->_errorMessage = $message;
    }

    /**
     * @return bool
     */
    public function hasError()
    {
        return $this->_error? true : false;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->_errorMessage;
    }

    /**
     * @return string
     */
    public abstract function getAsString();

}
class FileNotFoundException extends Exception {
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $message = "File not found: {$message}";
        parent::__construct($message, $code, $previous);
    }
}
class FileNotSupported extends Exception {
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $message = "This file extension is not supported by parser: {$message}";
        parent::__construct($message, $code, $previous);
    }
}