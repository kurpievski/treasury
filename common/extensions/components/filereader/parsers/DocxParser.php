<?php
/**
 * Created by PhpStorm.
 * User: igor.golub@2amigos.us
 * Date: 09/06/2017
 * Time: 16:47
 */

namespace common\components\filereader\parsers;


class DocxParser extends AbstractParser
{
    /** @var array supported extension */
    public $supportedExtensions = ['docx'];

    /**
     * @param string $file
     * @return bool
     */
    protected function isFileSupported($file)
    {
        $path_parts = pathinfo($file);
        if (in_array($path_parts['extension'], $this->supportedExtensions)) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getAsString()
    {
        return $this->readDocx();
    }

    private function readDocx()
    {
        $content = '';

        $zip = zip_open($this->getFile());

        if (!$zip || is_numeric($zip)) {
            return false;
        }

        while ($zip_entry = zip_read($zip)) {

            if (zip_entry_open($zip, $zip_entry) == FALSE) {
                continue;
            }

            if (zip_entry_name($zip_entry) != "word/document.xml") {
                continue;
            }

            $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

            zip_entry_close($zip_entry);
        }

        zip_close($zip);

        $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
        $content = str_replace('</w:r></w:p>', "\r\n", $content);
        $striped_content = strip_tags($content);

        return $striped_content;
    }
}