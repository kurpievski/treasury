<?php
/**
 * Created by PhpStorm.
 * User: Nikola Radovic <nikola@2amigos.us>
 * Date: 12/1/2015
 * Time: 12:53 AM
 */

namespace common\components\image;

use Imagick;
use Yii;
use yii\base\Behavior;
use yii\db\BaseActiveRecord;
use yii\imagine\Image as Imagine;
use Imagine\Image\Point;
use Imagine\Image\Box;
use Imagine\Image\ManipulatorInterface;
use common\models\ImageThumb;
use yii\web\HttpException;
use yii\web\UploadedFile;

class ImageBehavior extends Behavior implements ImageBehaviorInterface
{
    /**
     * @var string model's attribute name that will be associated with original file name
     */
    public $nameAttribute = 'original_name';

    /**
     * @var string model's attribute name that will be associated with original file width
     */
    public $widthAttribute = 'width';

    /**
     * @var string model's attribute name that will be associated with original file height
     */
    public $heightAttribute = 'height';

    /**
     * @var string model's attribute name that will be associated with storage key
     */
    public $storageKeyAttribute = 'storage_key';

    /**
     * @var string model's attribute name that will be associated with annotation storage key
     */
    public $annotationStorageKeyAttribute = 'annotation_storage_key';


    /** @var string Mime type attribute */
    public $mimeTypeAttribute = 'mime_type';

    /**
     * @var string file attribute
     */
    public $fileAttribute = 'file';

    /**
     * @var string tmp file holder
     */
    private $_tmpFile = null;

    /**
     * @var string location for tmp file
     */
    private $_tmpStorageLocation = '@app/runtime/';

    /**
     * @var array common thumb sizes that will be pre created
     */
    private $commonThumbSizes = [
        //Specifications array MUST be from highest to lowest and portrait orientation ratio MUST be used!
        ImageSpecification::PRINT_PORTRAIT_SPEC,
        ImageSpecification::MAIN_VIEW_PAN,
        ImageSpecification::THUMB_STRIP_SMALL,
    ];

    /**
     * @inheritdoc
     */
    public function __destruct()
    {
        if (file_exists($this->_tmpFile)) {
            @unlink($this->_tmpFile);
        }
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            BaseActiveRecord::EVENT_BEFORE_INSERT => 'processUploadedFile',
            BaseActiveRecord::EVENT_BEFORE_UPDATE => 'processAnnotationUploadedFile'
        ];
    }

    /**
     * @return UploadedFile|null
     */
    protected function getUploadedFile()
    {
        if ($this->owner->hasProperty($this->fileAttribute)) {
            return $this->owner->{$this->fileAttribute};
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function processUploadedFile()
    {
        $file = $this->getUploadedFile();

        if (empty($file)) {
            return;
        }

        $this->_tmpFile = $file->tempName;

        $owner = $this->owner;

        $storageKey = $this->getGeneratedStorageKey($file->getExtension());

        $tmpFile = Imagine::getImagine()->open($file->tempName);
        $fileSize = $tmpFile->getSize();

        $owner->{$this->nameAttribute} = $file->name;
        $owner->{$this->mimeTypeAttribute} = $file->type;
        $owner->{$this->storageKeyAttribute} = $storageKey;
        $owner->{$this->widthAttribute} = $fileSize->getWidth();
        $owner->{$this->heightAttribute} = $fileSize->getHeight();

        if (!$this->saveOnStorageAs($storageKey)) {
            throw new HttpException(500, 'Sorry, we are unable to upload your image.');
        }
    }

    /**
     * @inheritdoc
     */
    public function processAnnotationUploadedFile()
    {
        $file = $this->getUploadedFile();

        if (empty($file)) {
            $this->owner->{$this->annotationStorageKeyAttribute} = null;
            return;
        }

        $this->applyTransformations(ImageSpecification::PRINT_LANDSCAPE_SPEC, true);
        $imagine = new \Imagine\Imagick\Imagine();
        $originalImage = $imagine->open($this->getLocalFilePath())->getImagick();

        $this->_tmpFile = $file->tempName;
        $this->applyTransformations(ImageSpecification::PRINT_LANDSCAPE_SPEC, true);
        $annotatedImage = $imagine->open($file->tempName)->getImagick();

        $originalImage->compositeImage($annotatedImage, Imagick::COMPOSITE_OVER, 0, 0);
        $originalImage->writeImage($this->_tmpFile);

        $storageKey = $this->getGeneratedStorageKey($file->getExtension());
        $this->owner->{$this->annotationStorageKeyAttribute} = $storageKey;
        if (!$this->saveOnStorageAs($storageKey)) {
            throw new HttpException(500, 'Sorry, we are unable to upload your image.');
        }
    }

    /**
     * Check is image landscape oriented
     * @return bool
     */
    public function isLandscape()
    {
        return $this->getOrientation() === ImageSpecification::LANDSCAPE;
    }

    /**
     * Check is image portrait oriented
     * @return bool
     */
    public function isPortrait()
    {
        return $this->getOrientation() === ImageSpecification::PORTRAIT;
    }

    /**
     * Get image orientation
     * @return string|void
     */
    public function getOrientation()
    {
        if ($this->owner->height && $this->owner->width) {
            return $this->owner->height >= $this->owner->width ?
                ImageSpecification::PORTRAIT :
                ImageSpecification::LANDSCAPE;
        }

        return ImageSpecification::PORTRAIT;
    }

    /**
     * Apply transformation specification to an image
     *
     * @param $spec
     */
    public function applyTransformations($spec, $forceSize = false)
    {
        $specObj = new ImageSpecification($spec);

        if ($specObj->hasCroppedParams()) {
            $this->crop($specObj->getCroppedX(), $specObj->getCroppedY(), $specObj->getCroppedWidth(), $specObj->getCroppedHeight());
        }

        if ($specObj->hasResizeParams()) {
            $this->resize($specObj->getWidth(), $specObj->getHeight(), false, $forceSize);
        }

        if ($specObj->hasRotateParams()) {
            $this->rotate($specObj->getRotate());
        }
    }

    /**
     * Resize image
     * @param $width
     * @param $height
     * @param bool $outbound
     */
    public function resize($width, $height, $outbound = false, $forceSize = false)
    {
        $imagePath = $this->getLocalFilePath();

        if (!file_exists($imagePath)) {
            return;
        }

        $file = Imagine::getImagine()->open($imagePath);
        $fileSize = $file->getSize();
        $aspectRatio = $fileSize->getWidth() / $fileSize->getHeight();

        if ($height === null) {
            $height = ceil($width / $aspectRatio);
        }

        if ($width === null) {
            $width = ceil($height * $aspectRatio);
        }

        if ($forceSize) {
            if ($fileSize->getWidth() < $width) {
                $file->resize($fileSize->widen($width));
            }

            if ($fileSize->getHeight() < $height) {
                $file->resize($fileSize->heighten($height));
            }
        }

        if ($outbound) {
            $file->thumbnail(new Box($width, $height), ManipulatorInterface::THUMBNAIL_OUTBOUND)->save($imagePath);
        } else {
            $file->thumbnail(new Box($width, $height))->save($imagePath);
        }
    }

    /**
     * Crop image
     * @param $x
     * @param $y
     * @param $width
     * @param $height
     */
    public function crop($x, $y, $width, $height, $extension = '')
    {
        $imagePath = $this->getLocalFilePath();

        if (!file_exists($imagePath)) {
            return;
        }

        Imagine::getImagine()->open($imagePath)
            ->crop(new Point($x, $y), new Box($width, $height))
            ->save($imagePath, ['format' => $extension]);
    }

    /**
     * Rotate Image
     * @param $angle
     */
    public function rotate($angle)
    {
        $imagePath = $this->getLocalFilePath();

        if (!file_exists($imagePath)) {
            return;
        }

        Imagine::getImagine()->open($imagePath)->rotate($angle)->save($imagePath);
    }

    /**
     * Get Image storage key
     * @return string
     */
    public function getStorageKey()
    {
        return $this->owner->{$this->storageKeyAttribute};
    }

    /**
     * Regenerate image storage key, using this will put out of use all thumbs already created
     * Use example: when image is transformed
     */
    public function regenerateStorageKey()
    {
        $fileExtension = pathinfo($this->getStorageKey(), PATHINFO_EXTENSION);
        $this->owner->{$this->storageKeyAttribute} = $this->getGeneratedStorageKey($fileExtension);
    }

    /**
     * Generate unique storage_key
     * @param string $fileExtension
     * @return string
     */
    protected function getGeneratedStorageKey($fileExtension = '')
    {
        return $this->getEnvPrefix() .
        md5(microtime(true) . $this->owner->{$this->nameAttribute}) .
        '_' . mt_rand(0, 5000) .
        ".{$fileExtension}";
    }

    /**
     * Generate thumb storage key
     * @param $spec
     * @return mixed
     * @throws ImageSpecificationException
     */
    protected function generateThumbStorageKey($spec)
    {
        $specObj = new ImageSpecification($spec);
        $newPrefix = $this->getThumbPath() . $this->getEnvPrefix() . $specObj->getKey() . '_';

        return $newPrefix . str_replace($this->getEnvPrefix(), '', $this->getStorageKey());
    }

    /**
     * Get thumb path
     * @return string
     */
    protected function getThumbPath()
    {
        return trim(Yii::$app->params['image.thumb.path'], '/') . '/';
    }

    /**
     * Get environment prefix
     * @return string
     */
    protected function getEnvPrefix()
    {
        return defined('YII_ENV') && YII_ENV !== 'prod' ? YII_ENV . '__' : '';
    }

    /**
     * Set width and height file size from local file
     */
    public function setSizeFromLocalFile()
    {
        $file = $this->getLocalFile();
        if (!$file) {
            return;
        }

        $this->owner->{$this->widthAttribute} = $file->getSize()->getWidth();
        $this->owner->{$this->heightAttribute} = $file->getSize()->getHeight();
    }

    /**
     * Get local file instance
     * @return \Imagine\Image\ImageInterface|void
     */
    public function getLocalFile()
    {
        $imagePath = $this->getLocalFilePath();

        if (!file_exists($imagePath)) {
            return;
        }

        return Imagine::getImagine()->open($imagePath);
    }

    /**
     * Get image filepath on local storage
     * @return null|string
     */
    protected function getLocalFilePath()
    {
        if (!empty($this->_tmpFile)) {
            return $this->_tmpFile;
        }

        $fileStorageKey = $this->getStorageKey();
        $fileData = Yii::$app->resourceManager->getFileData($fileStorageKey);
        $fileExtension = '.' . pathinfo($fileStorageKey, PATHINFO_EXTENSION);
        $filePath = tempnam(Yii::getAlias($this->_tmpStorageLocation), md5($fileStorageKey) . '-') . $fileExtension;

        file_put_contents($filePath, $fileData);

        return $this->_tmpFile = $filePath;
    }

    /**
     * Save image on storage using object storage key
     * @return bool
     */
    public function saveOnStorage()
    {
        return $this->saveOnStorageAs($this->getStorageKey());
    }

    /**
     * Save image on storage as different file name
     * @param string $storageKey
     * @return bool
     */
    protected function saveOnStorageAs($storageKey)
    {
        return Yii::$app->resourceManager->sendFile($this->getLocalFilePath(), $storageKey);
    }

    /**
     * Delete Image from storage
     * @return bool
     */
    protected function deleteFromStorage()
    {
        $filename = $this->getStorageKey();
        if (Yii::$app->resourceManager->fileExists($filename)) {
            return Yii::$app->resourceManager->delete($filename);
        } else {
            return true;
        }
    }

    /**
     * Get image url
     * @param string $spec
     * @return string url
     */
    public function getUrl($spec = ImageSpecification::THUMB_STANDARD)
    {
        $imageSpec = new ImageSpecification($spec);

        return Yii::$app->urlManagerApi->createUrl([
                '/v1/image/view',
                'id' => $this->owner->id,
                'spec' => $imageSpec->getKey()]
            + Yii::$app->user->getAuthQueryParam()
        );
    }

    public function getPrintUrl()
    {
        return Yii::$app->urlManagerApi->createUrl([
                '/v1/image/view-print',
                'id' => $this->owner->id
            ] + Yii::$app->user->getAuthQueryParam()
        );
    }

    /**
     * Find appropriate image thumbAR object
     * @param $spec
     * @return null|static
     */
    public function findThumb($spec)
    {
        $specObj = new ImageSpecification($spec);

        return ImageThumb::findOne([
            'spec_key' => $specObj->getKey(),
            'image_id' => $this->owner->id,
            'storage_key' => $this->generateThumbStorageKey($specObj->getKey())
        ]);
    }

    /**
     * Get image Thumb object
     * @param $spec
     * @return ImageThumb
     */
    public function getThumb($spec)
    {
        $specObj = new ImageSpecification($spec);
        $thumb = $this->findThumb($specObj);

        if (empty($thumb)) {
            $thumb = $this->createThumb($specObj->getKey());
        }

        return $thumb;
    }

    public function getPrintThumb()
    {
        $spec = $this->isPortrait() ? ImageSpecification::PRINT_PORTRAIT_SPEC : ImageSpecification::PRINT_LANDSCAPE_SPEC;

        return $this->getThumb($spec);
    }

    public function hasAnnotatedImage()
    {
        $annotationKey = $this->owner->{$this->annotationStorageKeyAttribute};
        return !empty($annotationKey);
    }

    public function getAnnotationImageUrl()
    {
        return Yii::$app->resourceManager->getUrl($this->owner->{$this->annotationStorageKeyAttribute},
            Yii::$app->params['image.thumb.expire']);
    }

    /**
     * Create Image thumb object
     * @param $spec
     * @return ImageThumb
     * @throws HttpException
     */
    protected function createThumb($spec)
    {
        $specObj = new ImageSpecification($spec);

        $this->applyTransformations($specObj);

        $storageKey = $this->generateThumbStorageKey($specObj->getKey());

        $this->saveOnStorageAs($storageKey);

        $thumb = new ImageThumb([
            'spec_key' => $specObj->getKey(),
            'image_id' => $this->owner->id,
            'storage_key' => $storageKey
        ]);

        if (!$thumb->save()) {
            throw new HttpException(500, 'Unable to save image thumbnail.');
        }

        return $thumb;
    }

    public function createCommonThumbs()
    {
        foreach ($this->commonThumbSizes as $size) {
            $spec = new ImageSpecification($size);

            if ($this->isLandscape()) {
                $spec->swapDimensions();
            }

            if (!$this->findThumb($spec)) {
                $this->createThumb($spec);
            }
        }
    }

}
