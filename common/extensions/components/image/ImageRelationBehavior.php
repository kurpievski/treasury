<?php
/**
 * Author: Srdjan Drakul
 * Date: 12/18/2015 8:10 PM
 * Email: srdjan_drakul@yahoo.com
 */

namespace common\components\image;

use common\behaviors\RecycleBinBehavior;
use common\extensions\ActiveRecord;
use common\helpers\TransactionHelper;
use Yii;
use yii\base\Behavior;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;

class ImageRelationBehavior extends Behavior
{
    const MODIFY_TYPE_CREATE = 'create-relations';
    const MODIFY_TYPE_UPDATE = 'update-relations';
    const MODIFY_TYPE_DELETE = 'delete-relations';


    /** @var ActiveRecord */
    public $owner;

    public $className;
    public $attribute;

    public $createModelAttributes = [];

    public $canModifyCallback = null;
    public $afterModifyCallback = null;

    public $allowFailOnDelete = false;

    public function makeRelations($data)
    {
        if (empty($data) || !is_array($data) || !$this->canModify()) {
            return false;
        }

        /** @var ActiveRecord[] $relations */
        $relations = $this->getExistingRelations();

        $this->processOrderedRelations($data, $relations);
        $this->processUnorderedRelations($data, $relations);

        $this->assignDataAttributes($data, $relations);

        return $this->saveInTransaction($relations, self::MODIFY_TYPE_CREATE);
    }

    public function updateRelations($data)
    {
        if (empty($data) || !is_array($data) || !$this->canModify()) {
            return false;
        }

        $indexedData = ArrayHelper::map($data, 'image_id', 'order');
        asort($indexedData);

        /** @var ActiveRecord[] $relationModels */
        $relations = $this->getExistingRelations();

        foreach ($indexedData as $imageId => $order) {
            if (empty($relations[$imageId])) {
                continue;
            }

            $model = $relations[$imageId];

            $occupiedIndex = $this->getOccupiedArrayKey($order, $imageId, $relations);

            if ($occupiedIndex !== null) {
                $this->sortModelOrderByIndex($relations, $occupiedIndex);
            }

            $model->order = $order;
        }

        $this->sortModelOrder($relations);
        $this->assignDataAttributes($data, $relations);

        return $this->saveInTransaction($relations, self::MODIFY_TYPE_UPDATE);
    }


    public function deleteRelations($imageIds, $moveToRecycleBin = true)
    {
        if (!preg_match('/^\d+(,\d+)*$/', $imageIds)) {
            throw new BadRequestHttpException('Invalid request sent.');
        }

        $imageIdArray = explode(',', $imageIds);

        if (empty($imageIdArray) || !is_array($imageIdArray) || !$this->canModify()) {
            return false;
        }

        /** @var ActiveRecord $class */
        $class = $this->className;

        $models = $class::find()
            ->where(["{$this->attribute}" => $this->owner->id, 'image_id' => $imageIdArray])
            ->all();

        $transaction = Yii::$app->db->beginTransaction();

        try {
            /** @var $model RecycleBinBehavior|ActiveRecord  */
            foreach ($models as $model) {

                if ($moveToRecycleBin) {
                    $isSuccessful = $model->delete() || $this->allowFailOnDelete;
                } else {
                    $isSuccessful = $model->deletePermanently();
                }

                if (!$isSuccessful || $model->hasErrors()) {
                    throw new \Exception(implode('<br>', $model->getFirstErrors()));
                }
            }

            TransactionHelper::commitAll($transaction);
            $this->runAfterModify(self::MODIFY_TYPE_DELETE);

        } catch (\Exception $ex) {
            TransactionHelper::rollbackAll($transaction);
            throw new \Exception('Unable to delete model. ' . $ex->getMessage());
        }

        return true;
    }

    protected function canModify()
    {
        $modifyCallback = $this->canModifyCallback;

        if (is_callable($modifyCallback)) {
            return $modifyCallback();
        }

        return true;
    }

    protected function runAfterModify($modificationType)
    {
        $afterModifyCallback = $this->afterModifyCallback;

        if (is_callable($afterModifyCallback)) {
            $afterModifyCallback($modificationType);
        }
    }

    private function getNewRelationInstance($id, $imageId, $order)
    {
        /** @var ActiveRecord $class */
        $class = $this->className;

        /** @var ActiveRecord $model */
        $model = $class::find()
            ->includeDeleted()
            ->where([
                $this->attribute => $id,
                'image_id' => $imageId
            ])
            ->one();

        if (empty($model)) {
            $model = new $class([
                $this->attribute => $id,
                'image_id' => $imageId
            ]);
        }

        $model->setAttribute('order', $order);

        if ($model->hasAttribute('is_deleted')) {
            $model->setAttribute('is_deleted', 0);
        }

        foreach ($this->createModelAttributes as $attribute => $value) {
            if ($model->hasAttribute($attribute)) {
                $model->setAttribute($attribute, $value);
            }
        }

        return $model;
    }

    protected function processOrderedRelations($data, &$relations)
    {
        $orderedData = ArrayHelper::map($data, 'image_id', 'order');

        foreach ($orderedData as $imageId => $order) {
            if (!is_numeric($order)) {
                unset($orderedData[$imageId]);
            }
        }

        asort($orderedData);

        foreach ($orderedData as $imageId => $order) {
            $occupiedIndex = $this->getOccupiedArrayKey($order, $imageId, $relations);

            if ($occupiedIndex !== null) {
                $this->sortModelOrderByIndex($relations, $occupiedIndex);
            }

            if (!empty($relations[$imageId])) {
                $relations[$imageId]->order = $order;
                continue;
            }

            $relations[$imageId] = $this->getNewRelationInstance($this->owner->id, $imageId, $order);
        }

        $this->sortModelOrder($relations);
    }


    private function sortModelOrder(&$models)
    {
        ArrayHelper::multisort($models, 'order', SORT_ASC);

        $models = ArrayHelper::index($models, 'image_id');

        $order = 0;
        foreach ($models as $model) {
            $model->order = $order++;
        }
    }

    private function getOccupiedArrayKey($order, $imageId, &$models)
    {
        $indexCounter = 0;
        foreach ($models as $key => $model) {
            $indexCounter++;
            if ($model->order == $order && $model->image_id != $imageId) {
                return $indexCounter;
            }
        }

        return null;
    }

    private function sortModelOrderByIndex(&$models, $occupiedIndex)
    {
        $indexCounter = 0;
        foreach ($models as $key => $model) {
            if ($indexCounter >= $occupiedIndex) {
                $model->order++;
            }

            $indexCounter++;
        }
    }

    protected function processUnorderedRelations($data, &$relations)
    {
        $nextOrder = count($relations);
        $processedData = ArrayHelper::map($data, 'image_id', 'order');

        foreach ($processedData as $imageId => $order) {
            if (is_numeric($order)) {
                unset($processedData[$imageId]);
            }
        }

        foreach ($processedData as $imageId => $order) {
            if (!empty($relations[$imageId])) {
                continue;
            }

            $relations[$imageId] = $this->getNewRelationInstance($this->owner->id, $imageId, $nextOrder++);
        }
    }

    public function saveInTransaction($models, $modificationType)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            /** @var ActiveRecord $model */
            foreach ($models as $model) {
                if (!$model->save()) {
                    throw new \Exception(implode('<br>', $model->getFirstErrors()));
                }
            }

            $this->runAfterModify($modificationType);

            TransactionHelper::commitAll($transaction);
        } catch (\Exception $ex) {
            TransactionHelper::rollbackAll($transaction);
            throw $ex;
        }

        return true;
    }

    protected function assignDataAttributes($data, &$relations)
    {
        /** @var  $relations ActiveRecord[] */

        $dataItems = ArrayHelper::map($data, 'image_id', function ($item) {
            return !empty($item['data']) ? $item['data'] : [];
        });

        foreach ($dataItems as $imageId => $itemData) {
            if (empty($relations[$imageId])) {
                continue;
            }

            $relations[$imageId]->setAttributes($itemData);
        }
    }

    protected function getExistingRelations()
    {
        return ArrayHelper::index(
            $this->owner->hasMany($this->className, [
                $this->attribute => 'id'
            ])
            ->orderBy(['order' => SORT_ASC])
            ->all(), 'image_id');
    }

}