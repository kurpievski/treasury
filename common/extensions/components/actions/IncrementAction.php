<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 8.2.2017.
 * Time: 16:46
 */

namespace common\components\actions;


use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\web\NotFoundHttpException;

class IncrementAction extends Action
{
    public $modelClass;
    public $attribute;

    public function init()
    {
        if ($this->modelClass === null) {
            throw new InvalidConfigException('"modelClass" cannot be empty.');
        }

        if ($this->attribute === null) {
            throw new InvalidConfigException('"attribute" cannot be empty.');
        }

        parent::init();
    }

    public function run($id)
    {
        $modelClass = $this->modelClass;
        $model = $modelClass::findOne($id);

        if (empty($model)) {
            throw new NotFoundHttpException('Object not found.');
        }

        $attribute = $model->{$this->attribute};

        return $model->updateAttributes([
                $this->attribute => $attribute + 1
        ]);
    }
}