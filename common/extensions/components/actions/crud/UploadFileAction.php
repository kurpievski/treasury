<?php
/**
 * Created by PhpStorm.
 * User: Nikola Radovic <nikola@2amigos.us>
 * Date: 8/26/2016
 * Time: 6:48 PM
 */

namespace common\components\actions\crud;

use common\models\UploadFileForm;
use Yii;
use yii\web\Response;
use yii\web\UploadedFile;

class UploadFileAction extends Action
{
    public $modelClass;
    public $relatedIdAttribute;

    public function run($id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new UploadFileForm();

        $model->load(Yii::$app->request->post());
        $model->id = $id;
        $model->modelClass = $this->modelClass;
        $model->relatedIdAttribute = $this->relatedIdAttribute;

        $model->files = UploadedFile::getInstances($model, 'files');

        if ($model->save()) {
            return ['success' => true, 'files' => $model->getUploadInfo()];

        }

        return ['success' => false, 'files' => $model->getUploadInfo()];
    }
}
