<?php

namespace common\components\actions\crud;

use common\extensions\ActiveRecord;
use common\helpers\FlashHelper;
use Yii;
use yii\db\BaseActiveRecord;

/**
 * Class UpdateAction
 *
 * @author Ivan Kudinov <i.kudinov@frostealth.ru>
 * @package strongsquirrel\crud
 */
class UpdateAction extends ItemAction
{
    /**
     * @var string the name of the view action.
     */
    public $view = 'update';

    /**
     * @var callable
     * The signature of the callable should be:
     *
     * ```php
     * function ($model) {
     *     // $model is the requested model instance.
     *     return $this->redirect(['my-action', 'id' => $model->getPrimaryKey()]);
     * }
     * ```
     */
    public $afterUpdate;

    /**
     * @param string $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        /** @var ActiveRecord $model */
        $model = $this->findModel($id);
        $this->checkAccess($model);
        $model->setScenario($this->scenario);

        if ($model->load(\Yii::$app->getRequest()->post())) {
            if ($model->save()) {
                FlashHelper::setSuccess(get_class($model) . ' successfully updated!');
                $afterUpdate = $this->afterUpdate;
                if (empty($afterUpdate)) {
                    $afterUpdate = function (BaseActiveRecord $model) {
                        return $this->controller->redirect(['update', 'id' => $model->getPrimaryKey()]);
                    };
                }

                return call_user_func($afterUpdate, $model);
            } else {
                FlashHelper::setError('There is an error, ' . $model->getPublicName() . ' cannot be updated!');
            }
        }

        $params = $this->resolveParams(['model' => $model]);

        return $this->controller->render($this->view, $params);
    }
}
