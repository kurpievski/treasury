<?php

namespace common\components\actions\crud;

use common\extensions\ActiveRecord;
use common\helpers\FlashHelper;
use Yii;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

/**
 * Class DeleteAction
 *
 * @author Ivan Kudinov <i.kudinov@frostealth.ru>
 * @package strongsquirrel\crud
 */
class DeleteAction extends ItemAction
{
    /**
     * @var callable
     * The signature of the callable should be:
     *
     * ```php
     * function ($model) {
     *     // $model is the requested model instance.
     *     return $this->redirect(['my-action');
     * }
     * ```
     */
    public $afterDelete;

    public $asJson = false;

    public $message = '{name} successfully deleted!';

    /**
     * @param string $id
     *
     * @return mixed
     * @throws ServerErrorHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        /** @var ActiveRecord $model */
        $model = $this->findModel($id);
        $this->checkAccess($model);

        $isDeleted = $model->delete();

        if ($this->asJson) {
            return $this->getJsonResponse($isDeleted, $model);
        }

        return $this->getStandardResponse($isDeleted, $model);
    }

    protected function getJsonResponse($isDeleted, $model)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'success' => $isDeleted ? 'success' : false,
            'message' => $this->getSuccessMessage($model)
        ];
    }

    protected function getStandardResponse($isDeleted, $model)
    {
        if (!empty($this->afterDelete)) {
            return call_user_func($this->afterDelete, $model);
        }


        if ($isDeleted) {
            FlashHelper::setSuccess($this->getSuccessMessage($model));
        }

        return $this->controller->redirect(['index']);
    }

    protected function getSuccessMessage($model)
    {
        /** @var ActiveRecord $model */
        return Yii::t('app', $this->message, [
            'name' => $model->getPublicName()
        ]);
    }

}
