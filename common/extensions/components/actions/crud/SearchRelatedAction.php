<?php

namespace common\components\actions\crud;

use yii\base\InvalidConfigException;
use yii\base\Model;

/**
 * Class SearchAction
 *
 * @author Ivan Kudinov <i.kudinov@frostealth.ru>
 * @package strongsquirrel\crud
 */
class SearchRelatedAction extends ItemAction
{
    const FORM_METHOD_GET = 'get';
    const FORM_METHOD_POST = 'post';
    const FORM_METHOD_ANY = 'any';

    public $relatedModelClass;
    public $relationKey;

    /**
     * @var string
     */
    public $scenario = Model::SCENARIO_DEFAULT;

    /**
     * @var string the name of the view action.
     */
    public $view = 'related_models';

    public $partialView = '_grid';

    /**
     * @var string
     */
    public $formMethod = self::FORM_METHOD_GET;

    /**
     * The method should return an instance of [[DataProviderInterface]].
     *
     * @var string
     */
    public $searchMethod = 'search';

    /**
     * @var array
     */
    public $searchOptions = [];

    /**
     * Initializes the object.
     * This method is invoked at the end of the constructor after the object is initialized with the
     * given configuration.
     *
     * @throws InvalidConfigException
     */
    public function init()
    {
        if ($this->modelClass === null) {
            $className = get_class($this);
            throw new InvalidConfigException("$className::\$modelClass must be set.");
        }

        if ($this->relatedModelClass === null) {
            $className = get_class($this);
            throw new InvalidConfigException("$className::\$relatedModelClass must be set.");
        }

        if ($this->relationKey === null) {
            $className = get_class($this);
            throw new InvalidConfigException("$className::\$relationKey must be set.");
        }


        parent::init();
    }

    /**
     * @return string
     */
    public function run($id)
    {
        $model = $this->findModel($id);
        $this->checkAccess($model);

        list($dataProvider, $filterModel) = $this->prepare($id);
        $params = $this->resolveParams([
            'partialView' => $this->partialView,
            'model' => $model,
            'dataProvider' => $dataProvider,
            'filterModel' => $filterModel,
        ]);

        return $this->controller->render($this->view, $params);
    }

    /**
     * @return array [$dataProvider, $filterModel]
     */
    protected function prepare($id)
    {
        /** @var Model $model */
        $model = new $this->relatedModelClass(['scenario' => $this->scenario]);

        $data = $this->getData();

        if (empty($data)) {
            $data = $this->getActionDataFromSession(['id' => $id]);
        } else {
            $this->setActionDataToSession($data, ['id' => $id]);
        }

        $model->load($data);

        $model->{$this->relationKey} = $id;

        return [call_user_func([$model, $this->searchMethod], $this->searchOptions), $model];
    }

    /**
     * @return array|null
     * @throws InvalidConfigException
     */
    protected function getData()
    {
        $request = \Yii::$app->request;

        switch ($this->formMethod) {
            case self::FORM_METHOD_GET:
                $data = $request->get();
                break;
            case self::FORM_METHOD_POST:
                $data = $request->post();
                break;
            case self::FORM_METHOD_ANY:
                $data = $_REQUEST;
                break;
            default:
                throw new InvalidConfigException('Unsupported method "' . $this->formMethod . '".');
        }
        unset($data['id']);

        return $data;
    }
}
