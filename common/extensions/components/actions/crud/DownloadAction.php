<?php
/**
 * Created by PhpStorm.
 * User: Goran Sarenac <goransarenac91@hotmail.com>
 * Date: 10/13/2016
 * Time: 3:46 PM
 */

namespace common\components\actions\crud;

use common\helpers\FileHelper;
use Yii;
use yii\base\InvalidConfigException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class DownloadAction extends Action
{
    public $modelClass = null;
    public $findModel = null;
    public $logCallback = null;

    public function run($id)
    {
        /** @var ActiveRecord $class */
        $class = $this->modelClass;

        if (empty($class)) {
            throw new InvalidConfigException('Controller must have modelClass property defined.');
        }

        $model = $class::findOne($id);

        if (is_callable($this->findModel)) {
            $model = call_user_func($this->findModel, $id);
        }

        if (empty($model)) {
            throw new NotFoundHttpException('Page not found!');
        }

        if (is_callable($this->logCallback)) {
            call_user_func($this->logCallback, $id, Yii::$app->request);
        }

        $originalName = FileHelper::sanitizeName($model->original_name);

        Yii::$app->response->format = Response::FORMAT_RAW;

        return Yii::$app->response->sendFile($model->getLocalFilePath(), $originalName, [
            'mimeType' => $model->mime_type
        ]);
    }
}
