<?php

namespace common\components\actions\crud;


use common\helpers\FlashHelper;
use Yii;
use yii\web\Response;
use yii\db\BaseActiveRecord;

/**
 * Class CreateAction
 *
 * @author Ivan Kudinov <i.kudinov@frostealth.ru>
 * @package strongsquirrel\crud
 */
class CreateAction extends ItemAction
{
    /**
     * @var string the name of the view action.
     */
    public $view = 'create';

    public $modalView = 'create_modal';

    /**
     * @var callable
     * The signature of the callable should be:
     *
     * ```php
     * function ($model) {
     *     // $model is the requested model instance.
     *     return $this->redirect(['my-action', 'id' => $model->getPrimaryKey()]);
     * }
     * ```
     */
    public $afterSave;

    /**
     * @return mixed
     */
    public function run()
    {
        $this->checkAccess();

        /** @var ActiveRecord $model */
        $model = \Yii::createObject($this->modelClass);
        $model->setScenario($this->scenario);

        if ($model->load(\Yii::$app->getRequest()->post())) {
            if ($model->save()) {
                $message = $model->getPublicName() . ' successfully created!';

                if (Yii::$app->request->getIsAjax()) {
                    Yii::$app->response->format = Response::FORMAT_JSON;

                    $returnMessge = [
                        'success' => true,
                        'message' => $message
                    ];

                    if (Yii::$app->request->get('returnAttributes', false)) {
                        $attributes = explode(',', Yii::$app->request->get('returnAttributes'));

                        foreach ($attributes as $attribute) {
                            if ($model->hasProperty($attribute)) {
                                $returnMessge['attributes'][$attribute] = $model->{$attribute};
                            } else if ($model->hasAttribute($attribute)) {
                                $returnMessge['attributes'][$attribute] = $model->getAttribute($attribute);
                            } else {
                                $returnMessge['attributes'][$attribute] = null;
                            }
                        }

                    }

                    return $returnMessge;
                }

                FlashHelper::setSuccess($message);

                $afterSave = $this->afterSave;
                if (empty($afterSave)) {
                    $afterSave = function (BaseActiveRecord $model) {
                        return $this->controller->redirect(['update', 'id' => $model->getPrimaryKey()]);
                    };
                }

                return call_user_func($afterSave, $model);
            }

            $errorMesssage = $model->getPublicName() . ' cannot be created!<br> Errors: ' . implode('<br>', $model->getFirstErrors());

            if (Yii::$app->request->getIsAjax()) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                $returnMessge = [
                    'success' => false,
                    'message' => $errorMesssage
                ];
                return $returnMessge;
            }
            FlashHelper::setError($errorMesssage);
        }

        $params = $this->resolveParams(['model' => $model]);

        return $this->render($params);
    }

    private function render(array $params = [])
    {
        $view = Yii::$app->request->getIsAjax() ? $this->modalView : $this->view;

        return $this->controller->renderAjaxConditional($view, $params);
    }
}
