<?php
/**
 * ErrorAction.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 07-Aug-15
 * Time: 19:37
 */

namespace common\components\actions;

class ErrorAction extends \yii\web\ErrorAction
{
    public $layout = false;

    public function run()
    {
        if ($this->layout !== false) {
            $this->controller->layout = $this->layout;
        }

        parent::run();
    }
}
