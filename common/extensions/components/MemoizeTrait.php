<?php

namespace common\components;

trait MemoizeTrait
{
    private $_memoizeData = [];

    public function memoize($key, $processCallback)
    {
        if (!array_key_exists($key, $this->_memoizeData)) {
            return $this->_memoizeData[$key] = $processCallback();
        }

        return $this->_memoizeData[$key];
    }

    public static function staticMemoize($key, $processCallback)
    {
        static $memoize = [];

        if (!array_key_exists($key, $memoize)) {
            return $memoize[$key] = $processCallback();
        }

        return $memoize[$key];
    }
}