<?php
/**
 * Nikola Radovic <hola@2amigos.us>
 * Company: 2amigOS! <https://2amigos.us>.
 */

namespace common\components;


use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Inflector;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

class Model extends \yii\base\Model
{
    public static $loadDefaultColumnValues = false;

    public static $createdAtAttr = 'create_time';
    public static $updatedAtAttr = 'update_time';
    public static $createdByAttr = 'create_id';
    public static $updatedByAttr = 'update_id';

    /**
     * Creates and populates a set of models.
     *
     * @param string $modelClass
     * @param array $post
     * @param array $multipleModels
     * @param string $scenario
     * @return array
     */
    public static function createMultiple($modelClass, $post = [], $multipleModels = [], $scenario = null)
    {
        $models = [];

        if (!empty($multipleModels)) {
            $keys = array_keys(ArrayHelper::map($multipleModels, 'id', 'id'));
            $multipleModels = array_combine($keys, $multipleModels);
        }

        if ($post && is_array($post)) {
            foreach ($post as $i => $item) {
                if (isset($item['id']) && !empty($item['id']) && isset($multipleModels[$item['id']])) {
                    $model = $multipleModels[$item['id']];
                } else {
                    $model = new $modelClass;
                }
                if ($scenario) {
                    $model->setScenario($scenario);
                }

                $models[] = $model;
            }
        }

        return $models;
    }

    public static function insertMultiple(array $models, $additionalImportValidation = false)
    {
        if (!static::validateMultiple($models)) {
            return false;
        }

        $rows = [];
        $model = reset($models);
        $defaults = static::getModelDefaultValues($model);
        $count = count($models);

        foreach ($models as $model) {
            if ($additionalImportValidation && method_exists($model, 'shouldSkipOnImport') && $model->shouldSkipOnImport()) {
                $count--;
                continue;
            }

            $rows[] = ArrayHelper::merge($model->attributes, $defaults);
        }

        return Yii::$app->db
                ->createCommand()
                ->batchInsert($model::tableName(), $model->attributes(), $rows)
                ->execute() == $count;
    }

    private static function getModelDefaultValues(ActiveRecord $model)
    {
        $defaults = [
            'is_acr_valid' => 1,
            'is_deleted' => 0,
        ];

        if (static::$loadDefaultColumnValues) {
            foreach ($model->getTableSchema()->columns as $column =>$columnSchema) {
                if ($columnSchema->defaultValue !== null) {
                    $defaults[$column] = $columnSchema->defaultValue;
                }
            }
        }

        if ($model->hasProperty(static::$createdAtAttr)) {
            $defaults[static::$createdAtAttr] = time();
        }

        if ($model->hasProperty(static::$updatedAtAttr)) {
            $defaults[static::$updatedAtAttr] = time();
        }

        if (Yii::$app->has('user')) {
            $userId = Yii::$app->get('user')->id;
            if ($model->hasProperty(static::$createdByAttr)) {
                $defaults[static::$createdByAttr] = $userId;
            }
            if ($model->hasProperty(static::$updatedByAttr)) {
                $defaults[static::$updatedByAttr] = $userId;
            }
        }

        return $defaults;
    }

    public static function hasErrorsIn(array $models)
    {
        foreach ($models as $model) {
            if ($model->hasErrors()) {
                return true;
            }
        }

        return false;
    }

    public static function serializeErrors(array $models, $indexAttribute = null)
    {
        $errors = [];

        foreach ($models as $model) {

            $modelErrors = [];

            foreach ($model->getFirstErrors() as $name => $message) {
                $modelErrors[] = [
                    'field' => $name,
                    'message' => $message,
                ];
            }

            if (empty($modelErrors)) {
                continue;
            }

            if ($indexAttribute) {
                $errors[$model->$indexAttribute] = $modelErrors;
            } else {
                $errors[] = $modelErrors;
            }
        }

        return $errors;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getFormId()
    {
        return Inflector::slug($this->formName()) . '-id';
    }

    /**
     * @return string
     */
    public function getPublicName()
    {
        return Inflector::titleize($this->getBaseName());
    }

    /**
     * @return string
     */
    public function getBaseName()
    {
        return StringHelper::basename(get_class($this));
    }

}
