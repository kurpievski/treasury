<?php
/**
 * FilterTargetGlobalsTrait.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 19-Aug-15
 * Time: 16:47
 */

namespace common\components;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

trait FilterTargetGlobalsTrait
{
    public $filterGlobalVars = [];

    public $filterMerge = [];

    protected function getContextMessage()
    {
        $context = [];
        foreach ($this->logVars as $name) {
            if (! empty($GLOBALS[$name])) {
                $varContents = $GLOBALS[$name];
                if (in_array($name, $this->filterGlobalVars)) {
                    $varContents = $this->getFilteredContents($name, $varContents);
                }

                $context[] = "\${$name} = " . VarDumper::dumpAsString($varContents);
            }
        }

        return implode("\n\n", $context);
    }



    protected function getFilteredContents($varName, $varContents)
    {
        $excludeVariableNames = $this->filterMerge[$varName];

        return ArrayHelper::merge($varContents, $excludeVariableNames);
    }
}