<?php
/**
 * DocumentRasterizer.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 19-Feb-16
 * Time: 18:37
 */

namespace common\components;


use common\components\pdftoimage\PdfToImage;
use common\models\RawDocument;
use Yii;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\base\BaseObject;

class DocumentRasterizer extends BaseObject
{
    /** @var RawDocument */
    public $document;

    public function init()
    {
        parent::init();

        if (empty($this->document)) {
            throw new InvalidConfigException("document must be set.");
        }
    }

    /**
     * Perform checks for starting rasterization and if everything is fine and file is in PDF format processing is started.
     * @return array
     * @throws Exception
     */
    public function rasterize()
    {
        if (!$this->document->isRasterizable()) {
            throw new Exception("Document is not rasterizable.");
        }

        if (empty($this->document->file)) {
            throw new Exception("Document file is not available.");
        }

        switch($this->document->getOriginalExtension()) {
            case 'pdf':
                return $this->rasterizePdf();
        }

        throw new Exception("Cannot rasterize document.");
    }

    /**
     * Converts PDF to images and returns Image models.
     * @return array
     * @throws Exception
     */
    protected  function rasterizePdf()
    {
        $pdfToImage = new PdfToImage($this->document->file->tempName, $this->document->original_name);

        if (!$pdfToImage->saveOnStorage()) {
            throw new Exception("Documents are not properly converted to images.");
        }

        return $pdfToImage->imageModels;
    }
}