<?php
/**
 * BulkElasticSearch.php
 *
 * Author: Aleksandar Panic <aleksandar.panic@2amigos.us>
 * Timestamp: 13-Sep-16 12:38
 */

namespace common\components;


use common\helpers\ArrayHelper;
use yii\base\Exception;
use yii\elasticsearch\Connection;
use yii\helpers\Json;

class BulkElasticSearch extends Connection
{
    public $flushAfterRequestCount = 1000;

    public $disableUpdate = false;

    public $flushCallback = null;

    protected $_isBulkApi = false;

    protected $_bulkRequests = [];

    protected $_sendRequestList = [];
    protected $_dataResolveList = [];

    public function beginBulk()
    {
        $this->_isBulkApi = true;
        $this->_bulkRequests = [];
    }

    public function isBulkIndexing()
    {
        return $this->_isBulkApi;
    }

    protected function checkShouldFlush()
    {
        $totalRequestCount = 0;

        foreach ($this->_bulkRequests as $method => &$requests) {
            $totalRequestCount += count($requests);
        }

        if ($totalRequestCount >= $this->flushAfterRequestCount) {
            $this->flushBulkIndex();
        }
    }

    public function endBulk()
    {
        if (!$this->_isBulkApi) {
            throw new Exception("Please use beginBulk() before endBulk().");
        }

        $results = $this->flushBulkIndex();

        $this->_isBulkApi = false;

        return $results;
    }

    public function post($url, $options = [], $body = null, $raw = false)
    {
        if ($this->disableUpdate) {
            return ['_id' => -1, '_version' => -1];
        }

        if ($this->_isBulkApi) {
            $this->_bulkRequests['POST'][] = ['url' => $url, 'options' => $options, 'body' => $body, 'isRaw' => $raw];
            $this->checkShouldFlush();
            return ['_id' => -1, '_version' => -1];
        }
        return parent::post($url, $options, $body, $raw);
    }

    public function put($url, $options = [], $body = null, $raw = false)
    {
        if ($this->disableUpdate) {
            return ['_id' => -1, '_version' => -1];
        }

        if ($this->_isBulkApi) {
            $this->_bulkRequests['PUT'][] = ['url' => $url, 'options' => $options, 'body' => $body, 'isRaw' => $raw];
            $this->checkShouldFlush();
            return ['_id' => -1, '_version' => -1];
        }

        return parent::put($url, $options, $body, $raw);
    }

    protected function getDataResolveKey($item, $operation, $method)
    {
        return $operation . '-' . $item['_index'] . '-' . $item['_type'] . '-' . $item['_id'] . '-' . $method;
    }

    public function flushBulkIndex()
    {
        $results = $this->processBulkRequest($this->getRequestList());
        $this->_bulkRequests = [];

        $method = $this->flushCallback;

        if (is_callable($method)) {
            $method($results);
        }

        return $results;
    }

    protected function getRequestList()
    {
        $requestList = [];
        $this->_dataResolveList = [];

        foreach ($this->_bulkRequests as $method => $requests) {
            foreach ($requests as $request) {
                $indexData = $request['url'];

                $operationType = ArrayHelper::getValue($request, 'options.op_type');

                if (end($indexData) == '_update') {
                    $operationType = 'update';
                }

                if ($operationType == null) {
                    print_r($indexData);
                    throw new Exception("Invalid operation type.");
                }

                $operationData = [
                    '_index' => $indexData[0],
                    '_type' => $indexData[1],
                    '_id' => $indexData[2]
                ];
                $requestList[$method][] = Json::encode([$operationType => $operationData]);
                $requestList[$method][] = $request['body'];
                $this->_dataResolveList[$method][$this->getDataResolveKey($operationData, $operationType, $method)] = $request['body'];
            }
        }

        return $requestList;
    }

    protected function sendBulkRequest($requestList)
    {
        $results = [];

        $this->open();

        foreach ($requestList as $method => $requests) {
            $requestBody = implode("\n", $requests) . "\n";
            $response = $this->httpRequest($method, [$this->nodes[$this->activeNode]['http_address'], '_bulk'], $requestBody, true);
            $results[$method] = json_decode($response, true);
        }

        return $results;
    }

    protected function processErrorItems(&$results)
    {
        $errorItems = [];
        foreach ($results as $method => $response) {
            if (!$response['errors']) {
                continue;
            }
            foreach ($response['items'] as $item) {
                foreach ($item as $operation => $data) {
                    $itemInfo = $item[$operation];
                    if ($itemInfo['status'] >= 400) {
                        $key = $this->getDataResolveKey($itemInfo, $operation, $method);
                        $errorItems[$method][] = [
                            'operation' => $operation,
                            'requestBody' => isset($this->_dataResolveList[$method][$key]) ? $this->_dataResolveList[$method][$key] : 'Not found for key: ' . $key,
                            'response' => $item[$operation]
                        ];
                    }
                }
            }
        }

        if (!empty($errorItems)) {
            throw new Exception("Error during bulk indexing: " . PHP_EOL . print_r($errorItems, true));
        }
    }

    public function processBulkRequest($requestItems)
    {
        $results = $this->sendBulkRequest($requestItems);
        $this->processErrorItems($results);
        return $results;
    }
}