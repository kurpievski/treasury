<?php
/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 **/

namespace common\components;


use common\helpers\JsHelper;
use Yii;
use yii\base\ActionEvent;
use yii\base\Component;
use yii\base\Event;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Request;
use yii\web\Response;

class AjaxScriptInjector extends Component
{
    protected $injectScripts = true;

    /** @var Request */
    protected $request;

    public $injectAfterLastTag = true;

    public function init()
    {
        parent::init();

        $this->request = Yii::$app->request;

        if ($this->request->isAjax || $this->request->isPjax) {
            Event::on(Controller::class, Controller::EVENT_AFTER_ACTION, [$this, 'injectAjaxScripts']);
        }
    }

    public function disableScriptInject()
    {
        $this->injectScripts = false;
    }

    public function enableScriptInject()
    {
        $this->injectScripts = true;
    }

    public function injectAjaxScripts(ActionEvent $event)
    {
        if (!$this->injectScripts) {
            return;
        }

        if (Yii::$app->response->format !== Response::FORMAT_HTML) {
            return;
        }

        if ($this->injectAfterLastTag) {
            $script = JsHelper::getInitJs();
            $inject = Html::tag('script', $script);

            if (is_string($event->result)) {
                preg_match_all('/\<\/\w+>/sim', $event->result, $matches, PREG_OFFSET_CAPTURE);

                $lastTag = end($matches[0]);

                list($tag, $position) = !empty($lastTag) && count($lastTag) === 2 ? $lastTag : ['', 0];

                if (stripos($tag, 'script') !== false) {
                    $inject = $script;
                }

                $event->result = substr($event->result, 0, $position).$inject.$tag;
            }
        } else {
            $event->result .= Html::tag('script', JsHelper::getInitJs());
        }
    }
}