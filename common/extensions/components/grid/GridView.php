<?php
namespace common\components\grid;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * Created by PhpStorm.
 * User: 2amigosbih
 * Date: 01/06/2017
 * Time: 11:39
 */
class GridView extends \kartik\grid\GridView
{
    public $tableOptions = ['class' => 'blue-tbl table keyword-tbl'];
    public $export = false;
    public $layout = "<div class='table-responsive' style='overflow: hidden'>{items}</div><div class='table-nav'><div class='row'><div class='col-md-4 pager_wrap'><span>{summary}</span>{pager}</div><div class='col-md-8 text-right'><span></span><div class='per-page-wrapper'>{perPage}</div></div></div></div>";
    public $pjax = true;
    public $panelTemplate = "<div class='clearfix'>{panelBefore}</div><div class='table-responsive' style='overflow: hidden'>{items}</div><div class='table-nav'><div class='row'><div class='col-md-4 pager_wrap'><span>{summary}</span>{pager}</div></div></div>";

    public $usePerPage = false;
    public $perPageOptions = [];

    public $usePostInPjax = false;
    public $pjaxSortConfig = [];

    public function init()
    {
        if ($this->usePostInPjax) {
            $this->pjaxSettings['options']['clientOptions'] = [
                'method' => 'POST'
            ];
        }

        parent::init();

        if ($this->usePostInPjax) {
            $this->registerPostSortBinding();
        }

        $this->perPageOptions = ArrayHelper::merge([
            'defaultPageSize' => 10,
            'pageSizeParam' => 'per-page',
            'selectOptions' => [10, 20, 50, 100],
            'wrapperCssClass' => 'paging-blk rsl-blk',
            'selectCssClass' => 'rsl-dropdown',
            'labelCssClass' => 'rsl-txt',
            'label' => 'Rows per page'
        ], $this->perPageOptions);

        if(!$this->usePostInPjax) {
            $this->filterSelector = empty($this->filterSelector) ? 'select[name="per-page"]' : ($this->filterSelector . ', select[name="per-page"]');
        }
    }

    protected function registerPostSortBinding()
    {
        $view  = $this->getView();
        PostGridSortPagerAsset::register($view);

        $sortConfig = $this->pjaxSortConfig;

        if (empty($sortConfig['gridId'])) {
            $sortConfig['gridId'] = $this->options['id'];
        }

        $config = Json::encode($sortConfig);
        $view->registerJs("PostGridSortPager.initialize({$config});");
    }

    protected function initLayout()
    {
        $this->layout = str_replace('{perPage}', $this->usePerPage ? $this->renderPerPageWidget() : '', $this->layout);

        parent::initLayout();
    }

    protected function renderPerPageWidget()
    {
        $select = $this->renderPerPageSelect();
        $label = Html::tag('span', $this->perPageOptions['label'], ['class' => $this->perPageOptions['labelCssClass']]);

        return Html::tag('div', "{$label}\n{$select}", ['class' => $this->perPageOptions['wrapperCssClass']]);
    }

    protected function renderPerPageSelect()
    {
        $selections = ArrayHelper::isAssociative($this->perPageOptions['selectOptions']) ?
            $this->perPageOptions['selectOptions'] :
            array_combine($this->perPageOptions['selectOptions'], $this->perPageOptions['selectOptions']);

        return Html::dropDownList(
            "per-page",
            ArrayHelper::getValue($this, 'dataProvider.pagination.pageSize'),
            $selections,
            [
                'id' => "{$this->getId()}-per-page",
                'class' => $this->perPageOptions['selectCssClass']
            ]
        );
    }
}