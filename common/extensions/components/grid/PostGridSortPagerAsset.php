<?php
/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 **/

namespace common\components\grid;


use yii\web\AssetBundle;

class PostGridSortPagerAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $js = ['post-grid-sort-pager.js'];
}