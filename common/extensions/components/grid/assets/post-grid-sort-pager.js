/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 **/

"use strict";

var PostGridSortPager = (function ($) {

    return {
        initialize: initializeSort
    };

    function initializeSort(config) {
        var grid = $('#' + config.gridId);
        var pjaxContainer = grid.closest('[data-pjax-container]');

        if (pjaxContainer.hasClass('post-grid-sort-pager')) {
            return;
        }

        var sortChanged = false;
        var parts = queryStringToObject(window.location.search.substr(1));
        var sortByItem = parts.sort || "";
        var currentPage = parts.page || "";
        var perPage = parts['per-page'] || "";

        config.hijackExportButton && initExportConfig();

        if (!config.filterFormResolver) {
            $(document).on('beforeFilter', '#' + config.gridId, function () {
                var filterForm = pjaxContainer.find('form.gridview-filter-form');

                var actionUrl = filterForm.attr('action') || window.location.pathname + window.location.search;
                var newLink = prepareLink(actionUrl);
                filterForm.attr('action', newLink);
            });
        }

        pjaxContainer.on('pjax:complete', function () {
            grid = $('#' + config.gridId);
            loadParts();

            config.hijackExportButton && initExportConfig();
        });

        loadParts();
        initializeSortPost();
        initializePaginationPost();
        initializePerPagePost();

        pjaxContainer.addClass('post-grid-sort-pager');

        function loadParts()
        {
            parts = queryStringToObject(window.location.search.substr(1));
            sortByItem = parts.sort || "";
            currentPage = parts.page || "";
            perPage = parts['per-page'] || "";
            sortChanged = false;
        }

        function initializePaginationPost() {
            pjaxContainer.on('click', '#' + config.gridId + ' a[data-page]', function (e) {
                e.preventDefault();

                var params = queryStringToObject($(this).attr('href').split('?')[1] || "");

                currentPage = params.page;
                sortByItem = params.sort ? params.sort : '';

                if (config.filterFormResolver) {
                    applyFilterFormResolver();
                    return;
                }

                sortChanged = !!sortByItem;

                grid.yiiGridView("applyFilter");
            });
        }

        function initializeSortPost() {
            pjaxContainer.on('click', '#' + config.gridId + ' a[data-sort]', function (e) {
                e.preventDefault();
                sortByItem = $(this).data('sort');
                sortChanged = true;

                if (config.filterFormResolver) {
                    applyFilterFormResolver();
                    return;
                }

                grid.yiiGridView("applyFilter");
            });
        }

        function initializePerPagePost() {
            pjaxContainer.on('change', '#' + config.gridId + ' select[name="per-page"]', function (e) {
                e.preventDefault();

                perPage = $(this).val();

                if (config.filterFormResolver) {
                    applyFilterFormResolver();
                    return;
                }

                grid.yiiGridView("applyFilter");
            });
        }

        function applyFilterFormResolver() {
            var form = config.filterFormResolver(grid);
            var actionUrl = form.attr('action') || window.location.pathname + window.location.search;
            var newLink = prepareLink(actionUrl);
            form.attr("action", newLink);
            form.submit();
        }

        function initExportConfig() {
            var form = $(config.exportFormSelector || 'form.kv-export-full-form');

            form.on('submit', function () {
                var inputItems = config.filterFormId ? $('#' + config.filterFormId).find('input,select,textarea') : grid.find('input,select,textarea');

                form.find('.appended-grid-filters').remove();

                inputItems.each(function (i, e) {
                    var elKind = e.tagName.toLowerCase();

                    var formEl = document.createElement(elKind);
                    var inputName = e.getAttribute('name');
                    var inputType = e.getAttribute('type');

                    if (!inputName) {
                        return;
                    }

                    formEl.setAttribute('name', e.getAttribute('name'));

                    inputType && formEl.setAttribute('type', e.getAttribute('type'));

                    if (elKind === 'select') {
                        formEl.innerHTML = e.innerHTML;
                    } else {
                        formEl.setAttribute('value', e.value);
                    }


                    formEl.classList.add('appended-grid-filters');

                    form.append(formEl);
                });
            });
        }

        function prepareLink(actionUrl) {
            var routeParts = parseUrlRoute(actionUrl);
            var currentSort = routeParts.query.sort || "";

            var isDescending = currentSort.substr(0, 1) === '-';
            var value = isDescending ? currentSort.substr(1) : currentSort;

            var newValue = "";

            if (sortChanged) {
                if (sortByItem == value) {
                    newValue = isDescending ? sortByItem : '-' + sortByItem;
                } else {
                    newValue = sortByItem;
                }
            } else {
                newValue = routeParts.query.sort || "";
            }


            if ((!newValue || newValue == '-')) {
                ('sort' in routeParts.query) && (delete routeParts.query.sort);
            } else {
                routeParts.query.sort = newValue;
            }

            if (!currentPage) {
                ('page' in routeParts.query) && (delete routeParts.query.page);
            } else {
                routeParts.query.page = currentPage;
            }

            if(perPage) {
                $.extend(true, routeParts, {query: {'per-page': perPage}});
            }

            return combineRouteLink(routeParts);
        }
    }



    function parseUrlRoute(url) {
        var urlParts = url.split('?');
        var route = urlParts[0];
        var query = queryStringToObject((urlParts[1] || ""));

        return {
            route: route,
            query: query
        }
    }

    function combineRouteLink(routeParts) {
        var params = $.param(routeParts.query);
        return routeParts.route + (params.length > 0 ? '?' + params : '');
    }

    function queryStringToObject(query) {
        return query.split('&')
            .map(function (item) {
                return item.split('=');
            })
            .filter(function (item) {
                return item[0].length > 0;
            })
            .reduce(function (state, item) {
                if (item.length == 2) {
                    state[item[0]] = item[1];
                } else {
                    state[item[0]] = '';
                }

                return state;
            }, {});
    }

})(jQuery);
