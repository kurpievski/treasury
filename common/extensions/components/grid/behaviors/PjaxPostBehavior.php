<?php
/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 **/

namespace common\components\grid\behaviors;


use common\components\grid\PostGridSortPagerAsset;
use dvamigos\Yii2\GridView\GridView;
use yii\base\Behavior;
use yii\helpers\Json;

class PjaxPostBehavior extends Behavior
{
    /** @var GridView */
    public $owner;

    public $clientOptions = [];

    public function events()
    {
        return [
            GridView::EVENT_AFTER_RUN => 'initializePostPjaxBehavior'
        ];
    }

    public function initializePostPjaxBehavior()
    {
        $view  = $this->owner->getView();

        if (!isset($this->owner->options['id'])) {
            $this->owner->options['id'] = $this->owner->getId();
        }

        PostGridSortPagerAsset::register($view);

        $sortConfig = $this->clientOptions;

        if (empty($sortConfig['gridId'])) {
            $sortConfig['gridId'] = $this->owner->options['id'];
        }

        $config = Json::encode($sortConfig);
        $view->registerJs("!function() {
            PostGridSortPager.initialize({$config});
        }();");
    }
}