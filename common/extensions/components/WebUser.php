<?php
/**
 * Author: Srdjan Drakul
 * Date: 10/5/2016 3:10 PM
 * Email: srdjan.drakul@2amigos.us
 */

namespace common\components;

use common\helpers\ArrayHelper;
use common\helpers\MenuHelper;
use common\helpers\RbacHelper;
use common\helpers\WorkflowHelper;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Yii;
use yii\web\MethodNotAllowedHttpException;
use yii\web\User;

class WebUser extends User
{
    private $_access = [];

    public function writeDocumentToSession($document)
    {
        $documents = $this->getDocumentsFromSession();

        $documents[] = $document;

        Yii::$app->session->set('user.documents', $documents);
    }

    public function getDocumentsFromSession()
    {
        return Yii::$app->session->get('user.documents');
    }

    public function removeDocumentFromSession($id)
    {
        $documentData = $this->getDocumentsFromSession();

        $index = null;

        if (is_array($documentData) || $documentData instanceof \Traversable) {
            foreach ($documentData as $key => $document) {
                if ($document['id'] == $id) {
                    $index = $key;
                }
            }
        }

        unset($documentData[$index]);

        Yii::$app->session->set('user.documents', $documentData);
    }

    public function isDocumentInSession($id)
    {
        $documentData = $this->getDocumentsFromSession();

        if (empty($documentData)) {
            return false;
        }

        foreach ($documentData as $document) {
            if ($document['id'] == $id) {
                return true;
            }
        }

        return false;
    }

    public function getAccessToken()
    {
        return Yii::$app->session->get('user.access_token');
    }

    public function getRefreshToken()
    {
        return Yii::$app->session->get('user.refresh_token');
    }

    public function setAccessToken($token)
    {
        Yii::$app->session->set('user.access_token', $token);
    }

    public function setRefreshToken($token)
    {
        Yii::$app->session->set('user.refresh_token', $token);
    }

    public function setCompany($company)
    {
        Yii::$app->session->set('user.company', $company);
    }

    public function getCompany()
    {
        return Yii::$app->session->get('user.company');
    }

    public function setErrorFlag()
    {
        Yii::$app->session->set('user.error_flag', true);
    }

    public function resetErrorFlag()
    {
        Yii::$app->session->set('user.error_flag', false);
    }

    public function getHasErrorFlag()
    {
        return Yii::$app->session->get('user.error_flag', false);
    }

    public function getShouldReloadRequestedRoute()
    {
        return !$this->getHasErrorFlag();
    }

    public function getFullName()
    {
        $identity = $this->getIdentity();

        return $identity ? $identity->getFullName() : '';
    }

    public function getCompanyId()
    {
        $company = $this->getCompany();

        return !empty($company) && is_array($company) ? $company['id'] : '';
    }

    public function getCompanyName()
    {
        $company = $this->getCompany();

        return !empty($company) && is_array($company) ? $company['name'] : '';
    }

    public function hasAccess()
    {
        return !$this->getIsGuest() && !RbacHelper::userHasRole(RbacHelper::ROLE_SUPERADMIN);
    }

    public function can($permissionName, $params = [], $allowCaching = true)
    {
        if ($allowCaching && empty($params) && isset($this->_access[$permissionName])) {
            return $this->_access[$permissionName];
        }

        if (($accessChecker = $this->getAccessChecker()) === null) {
            return false;
        }

        $access = $accessChecker->checkAccess($this->getIntegrationId(), $permissionName, $params);
        if ($allowCaching && empty($params)) {
            $this->_access[$permissionName] = $access;
        }

        return $access;
    }

    public function getIntegrationId()
    {
        /** @var \common\models\User $identity */
        $identity = $this->getIdentity();

        return $identity !== null ? $identity->getIntegrationId() : null;
    }

    public function getUsername()
    {
        /** @var \common\models\User $identity */
        $identity = $this->getIdentity();

        return ArrayHelper::getValue($identity, 'username');
    }

    public function logout($destroySession = true)
    {
        if ($this->getIsGuest()) {
            throw new MethodNotAllowedHttpException('You are not allowed to execute this action');
        }

        if ($refreshToken = $this->getRefreshToken()) {
            try {
                $auth = Yii::$app->params['auth'];
                (new Client())->request('POST', $auth['baseUrl'] . $auth['revokePath'], [
                    'verify' => false,
                    'form_params' => [
                        'token' => $refreshToken
                    ]
                ]);

                Yii::$app->getCache()->flush();
            } catch (ClientException $e) {
                return false;
            }
        }

        return parent::logout($destroySession);
    }

    public function getCompanyIds()
    {
        if(Yii::$app->session->get('user.company_ids') === null) {
            /** @var \common\models\User $identity */
            $identity = $this->getIdentity();

            Yii::$app->session->set('user.company_ids', $identity->getCompanyIds());
        }

        return Yii::$app->session->get('user.company_ids');
    }

    public function loadWorkflow()
    {
        $workflow = WorkflowHelper::getActiveWorkflow(Yii::$app->user->id);
        if (!empty($workflow)) {
            Yii::$app->session->set('user.workflow_id', $workflow->id);
        }
    }

    public function setWorkflowId($id)
    {
        Yii::$app->session->set('user.workflow_id', $id);
    }

    public function getWorkflowId()
    {
        return Yii::$app->session->get('user.workflow_id', null);
    }

    public function getIsInsideWorkflow()
    {
        return !empty($this->getWorkflowId());
    }

    public function getHasAccess()
    {
        foreach (RbacHelper::getPermissions() as $permission) {
            if ($this->can($permission)) {
                return true;
            }
        }

        return false;
    }

    public function getRedirectionUrl()
    {
        foreach (MenuHelper::getItems() as $item) {
            if (!isset($item['visible']) || $item['visible']) {
                if (is_array($item['url'])) {
                    return $item['url'];
                }

                if (!$item['items']) {
                    continue;
                }

                return $this->searchSubItems($item['items']);
            }
        }

        $this->logout();

        throw new \Exception("Unable to find appropriate redirection url.");
    }

    public function searchSubItems($items)
    {
        foreach ($items as $item) {
            if (!isset($item['visible']) || $item['visible']) {
                if (is_array($item['url'])) {
                    return $item['url'];
                }

                if (!$item['items']) {
                    continue;
                }

                return $this->searchSubItems($item['items']);
            }
        }

        throw new \Exception("Unable to find appropriate redirection url.");
    }

    public function getIsAdmin()
    {
        return $this->can(RbacHelper::MASTER_EMSMART_ROLE);
    }

    public function getExportUserData()
    {
        return [
            'workflowId' => $this->getWorkflowId()
        ];
    }
}