<?php
namespace common\components\highchartsajax;

use backend\assets\HighChartsAsset;
use Yii;
use yii\base\Widget;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * Class HighChartsAjaxWidget
 * @package common\components\highchartsajax
 */
class HighChartsAjaxWidget extends Widget
{
    public const JS_VARIABLE = 'HighChartsAjax';

    /**
     * @var string
     */
    public $template;

    /**
     * @var array
     */
    public $controls;

    /**
     * @var ChartInterface[]
     */
    public $charts;

    /**
     * @var ActiveForm
     */
    private $form;

    public function init()
    {
        parent::init();

        HighChartsAsset::register($this->getView());
        $this->initCharts();
        $this->initControls();
    }

    protected function initCharts()
    {
        $charts = [];
        foreach ($this->charts as $chart) {
            $charts[$chart['class']] = Yii::createObject($chart);
        }
        $this->charts = $charts;
    }

    protected function initControls()
    {
        foreach ($this->controls as $field  => &$control) {
            list($class, $field) = explode(':', $field);

            if (isset($this->charts[$class])) {
                $model = $this->charts[$class];
            } else {
                $model = Yii::createObject($class);
            }

            /** @var array $options */
            $options = \array_merge($control, [
                'model' => $model,
                'attribute' => $field
            ]);

            $control = Yii::createObject($options);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        ob_start();

        $this->form = ActiveForm::begin([
            'method' => 'post'
        ]);

        $this->registerJs();

        echo $this->render('highchartsajax', ['controls' => $this->controls, 'charts' => $this->charts]);

        ActiveForm::end();

        return ob_get_clean();
    }

    /**
     * @return array
     */
    protected function getChartsJsArray()
    {
        $chartsJs = [];
        foreach ($this->charts as $chart) {
            $chartName = StringHelper::basename(get_class($chart));
            $chartsJs[$chartName] = [
                'url' => $chart->getDataUrl(),
                'type' => $chart->getChartType(),
            ];
        }

        return $chartsJs;
    }

    protected function registerJs()
    {
        $jsVariable = self::JS_VARIABLE;
        $chartsJs = $this->getChartsJsArray();
        $chartJsEncoded = Json::encode($chartsJs);
        $chartIds = Json::encode(array_keys($chartsJs));
        $formId = $this->form->getId();

        $js = <<<JS
        var {$jsVariable} = {$chartJsEncoded};
    
        function loadHighCharts(charts) {
            for (i = 0; i < charts.length; i++) { 
                 loadHighChart({$jsVariable}[charts[i]]);
            }
        }
    
        function loadHighChart(chart) {
           $.ajax({
               url: chart.url,
               data: $('#$formId').serialize(),
               type: 'POST',
               dataType : 'json',
               success: function(data){
                   if (chart.type === 'Map') {
                        new Highcharts.Map(data);
                   } else {
                        new Highcharts.Chart(data);
                   }
               },
               error: function(json, statut, error){
                   console.log(json, statut, error);
               }
           });
        }
        
        loadHighCharts({$chartIds});
   
JS;
        $this->getView()->registerJs($js, View::POS_END);
    }
}
