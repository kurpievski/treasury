<?php
namespace common\components\highchartsajax;

use common\components\actions\Action;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\web\BadRequestHttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\Response;

/**
 * Class ChartConfigAction
 * @package common\components\tracking
 */
class ChartConfigAction extends Action
{
    /**
     * @var array
     */
    public $chartConfig;

    /**
     * Ensures POST ajax request and sets JSON response format
     *
     * @return bool
     * @throws BadRequestHttpException
     * @throws MethodNotAllowedHttpException
     */
    public function beforeRun()
    {
        $request = Yii::$app->getRequest();
        $response = Yii::$app->getResponse();

        if ($request->getMethod() != 'POST') {
            $response->getHeaders()->set('Allow', 'POST');
            throw new MethodNotAllowedHttpException('Method Not Allowed. This URL can only handle POST requests.');
        }

        if (!$request->getIsAjax()) {
            throw new BadRequestHttpException('Request must be XMLHttpRequest.');
        }

        $response->format = Response::FORMAT_JSON;

        return parent::beforeRun();
    }

    /**
     * @param string|null $id
     * @return array
     * @throws Exception
     */
    public function run($id = null)
    {
        /** @var ChartInterface $form */
        $chart = Yii::createObject($this->chartConfig);

        if (!($chart instanceof ChartInterface)) {
            throw new Exception('Chart has to implement '.ChartInterface::class);
        }

        $chartName = StringHelper::basename($this->chartConfig['class']);
        $ancestors = $this->getChartAncestors();

        $post = Yii::$app->request->post();
        $params = $post[$chartName] ?? [];

        foreach ($post as $form => $formParams) {
            if (in_array($form, $ancestors) && is_array($formParams)) {
                $params = ArrayHelper::merge($params, $formParams);
            }
        }

        $chart->load([$chartName => $params]);

        return $chart->getConfig();
    }

    /**
     * @return array
     */
    protected function getChartAncestors()
    {
        $reflection = new \ReflectionClass($this->chartConfig['class']);
        $ancestors = [];
        while ($reflection = $reflection->getParentClass()) {
            $ancestors[] = StringHelper::basename($reflection->getName());
        }

        return $ancestors;
    }
}
