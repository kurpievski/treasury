<?php

/** @var Widget[] $controls */
/** @var ChartInterface[] $charts */

use common\components\highchartsajax\ChartInterface;
use common\components\tracking\models\charts\Device;
use common\components\tracking\models\charts\endpoint\Endpoint;
use common\components\tracking\models\charts\Map;
use common\components\tracking\models\charts\BaseTrackingChart;
use yii\base\Widget;

?>
<div class="highchartsajax">

	<div class="row highchartsajax-controls highchartsajax-controls-main">

		<?php if (isset($controls[BaseTrackingChart::getField('date_range')])): ?>
		<div class="col-md-4">
			<?= $controls[BaseTrackingChart::getField('date_range')]->run() ?>
		</div>
        <?php endif ?>

        <?php if (isset($controls[Endpoint::getField('total')])): ?>
		<div class="col-md-3">
			<?= $controls[Endpoint::getField('total')]->run() ?>
		</div>
        <?php endif ?>

	</div>

	<div class="row highchartsajax-controls highchartsajax-controls-minor">
        <?php if (isset($controls[Endpoint::getField('type')])): ?>
		<div class="col-md-3">
			<?= $controls[Endpoint::getField('type')]->run() ?>
		</div>
        <?php endif ?>

		<div class="col-md-6"></div>

		<?php if (isset($controls[BaseTrackingChart::getField('mode')])): ?>
		<div class="col-md-3 text-right">
            <?= $controls[BaseTrackingChart::getField('mode')]->run() ?>
		</div>
        <?php endif ?>
	</div>

	<div class="row" style="width: 100%">
		<div class="col-md-12">
			<div id="<?= $charts[Endpoint::class]->getId()?>"></div>
		</div>
	</div>

	<div class="row highchartsajax-controls">
		<div class="col-md-6">
            <?php if (isset($controls[Device::getField('type')])): ?>
                <?= $controls[Device::getField('type')]->run() ?>
            <?php endif ?>
		</div>
		<div class="col-md-6"></div>
	</div>

	<div class="row" style="width: 100%">
		<div class="col-md-6">
			<div id="<?= $charts[Device::class]->getId() ?>"></div>
		</div>
		<div class="col-md-6">
			<div id="<?= $charts[Map::class]->getId() ?>"></div>
		</div>
	</div>


</div>