<?php
/*
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace common\components\highchartsajax;

use yii\base\Model;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/**
 * Class BaseChart
 * @package common\components\highchartsajax
 */
abstract class BaseChart extends Model implements ChartInterface
{
    /**
     * @var string url to ajax action providing data
     */
    public $dataUrl;

    /**
     * @var string
     */
    protected $chartType;

    /**
     * @inheritdoc
     */
    public function getChartType()
    {
        return $this->chartType;
    }

    /**
     * @inheritdoc
     */
    public function getDataUrl()
    {
        return $this->dataUrl;
    }

    /**
     * @inheritdoc
     */
    public function getAttribute($name)
    {
        return isset($this->_attributes[$name]) ? $this->_attributes[$name] : null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return 'chart-'.Inflector::camel2id(StringHelper::basename(get_class($this)));
    }

    /**
     * @inheritdoc
     */
    public static function getField($field)
    {
        return static::class.':'.$field;
    }
}
