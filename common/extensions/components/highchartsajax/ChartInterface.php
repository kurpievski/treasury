<?php
namespace common\components\highchartsajax;

/**
 * Interface ChartInterface
 * @package common\components\tracking\contracts
 */
interface ChartInterface
{
    public const TYPE_CHART = 'Chart';
    public const TYPE_MAP = 'Map';

    /**
     * @return string
     */
    public function getChartType();

    /**
     * HighCharts config
     *
     * @return array
     */
    public function getConfig();

    /**
     * Url to ajax action providing HighCharts config
     *
     * @return string
     */
    public function getDataUrl();

    /**
     * Compatibility with some Widgets
     *
     * @param string $name
     * @return mixed
     */
    public function getAttribute(string $name);

    /**
     * Returns a field with Chart namespace (namespace\ChartClass:field)
     *
     * @param string $field
     * @return string
     */
    public static function getField(string $field);

    /**
     * @return string
     */
    public function getId();
}