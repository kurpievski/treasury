<?php
/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 **/

namespace common\components\export;


use yii\data\ActiveDataProvider;

interface ExportableModelInterface
{
    /** @return ActiveDataProvider */
    public function search();

    /**
     * Returns key => value array with model parameters which will be usable for export.
     *
     * @return array
     */
    public function getExportParams();

    /**
     * Load data from export params
     *
     * @param $params array Params passed using getExportParams method
     */
    public function loadExportParams($params);
}