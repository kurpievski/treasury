<?php
/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 **/

namespace common\components\export\exporters;

use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use yii\data\BaseDataProvider;
use yii\grid\Column;
use yii\grid\DataColumn;

class PHPExcelExporter extends BaseExporter
{
    public $exportType = 'Xlsx';
    public $outputFilename = "export";

    public $emptyText = null;

    public $headerRowStyle = [
        'font' => [
            'bold' => true
        ],
        'fill' => [
            'type' => Fill::FILL_SOLID,
            'startcolor' => ['rgb' => 'EDEDED']
        ]
    ];

    public $columnTypes = [];

    protected $phpExcel = null;

    /**
     * @var Worksheet
     */
    protected $sheet = null;

    /** @var Column[]|DataColumn[] */
    protected $columns = null;
    protected $columnNames = [];
    protected $renderRow = 1;

    /**
     * Performs export logic
     */
    public function prepareExport()
    {
        $this->phpExcel = new Spreadsheet();
        $this->sheet = $this->phpExcel->setActiveSheetIndex(0);
        $this->columns = $this->getExportColumns();
        $this->renderRow = 1;

        foreach ($this->columns as $index => $column) {
            $columnName = $this->getColumnName($index);

            $this->sheet->getColumnDimension($columnName)->setAutoSize(true);

            $cell = $this->sheet->getCell($columnName . $this->renderRow);
            $this->setHeaderCell($cell, $this->getHeaderCellLabel($column));
        }

        $this->renderRow = 2;
    }

    public function renderChunk(BaseDataProvider $dataProvider)
    {
        if ($dataProvider->getTotalCount() == 0) {
            $cell = $this->sheet->getCell("A" . $this->renderRow);
            $cell->setValue($this->emptyText);
            return;
        }

        $models = $dataProvider->getModels();
        $keys = $dataProvider->getKeys();

        foreach ($models as $rowIndex => $row) {
            $key = $keys[$rowIndex];

            foreach ($this->columns as $index => $column) {
                $columnName = $this->getColumnName($index);
                $cell = $this->sheet->getCell($columnName . $this->renderRow);
                $this->setCell($cell, $columnName, $this->getDataCellContent($column, $row, $key, $rowIndex));
            }

            $this->renderRow++;
        }
    }

    protected function getHeaderCellLabel(Column $column)
    {
        return strip_tags($column->renderHeaderCell());
    }

    protected function getDataCellContent(Column $column, $row, $key, $index)
    {
        return strip_tags($column->renderDataCell($row, $key, $index));
    }

    protected function setHeaderCell($cell, $data)
    {
        /** @var Cell $cell */
        $cell->setValue($data);
        $cell->getStyle()->applyFromArray($this->headerRowStyle);
    }

    protected function setCell($cell, $columnName, $data)
    {
        /** @var Cell $cell */
        $cell->setValue($data);
        $cell->setDataType($this->getColumnType($columnName));
    }

    protected function getColumnType($columnName)
    {
        return !empty($this->columnTypes[$columnName]) ? $this->columnTypes[$columnName] : DataType::TYPE_STRING;
    }

    public function doOutput($destination)
    {
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($this->phpExcel, $this->exportType);
        $writer->save($destination);
    }

    protected function getColumnName($columnIndex)
    {
        if (empty($this->columnNames[$columnIndex])) {
            for ($letter = ""; $columnIndex >= 0; $columnIndex = intval($columnIndex / 26) - 1) {
                $letter = chr($columnIndex % 26 + 0x41) . $letter;
            }
            $this->columnNames[$columnIndex] = $letter;
        }

        return $this->columnNames[$columnIndex];
    }

    /**
     * @return array key value config of header => value for output headers.
     */
    public function getExportHeaders()
    {
        $fileName = $this->outputFilename . '.' . strtolower($this->exportType);
        return [
            "Content-Type" => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            "Content-Disposition" => "attachment;filename=\"{$fileName}\"",
            "Cache-Control" => "max-age=0"
        ];
    }


    public function getExtension()
    {
        return 'xlsx';
    }
}