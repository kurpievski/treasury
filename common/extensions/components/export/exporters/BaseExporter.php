<?php
/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 **/

namespace common\components\export\exporters;


use dvamigos\Yii2\GridView\GridView;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;
use yii\data\BaseDataProvider;
use yii\grid\Column;
use yii\grid\DataColumn;

abstract class BaseExporter extends BaseObject
{
    public $dataColumnClass = DataColumn::class;

    /** @var array Keys for columns to exclude from export */
    public $excludeColumnKeys = [];

    /** @var array Class types to exclude from export */
    public $excludeColumnClasses = [];

    /** @var array Columns to export */
    public $exportColumns = [];

    /** @var null|string Empty text which is going to be used. If empty grid's own will be used. */
    public $emptyText = '';

    /** @var GridView|null */
    public $grid;

    public function init()
    {
        parent::init();
        $this->emptyText = $this->grid->emptyText ?? '';
    }

    /**
     * Prepare export
     */
    public abstract function prepareExport();

    /**
     * Renders export chunk with data provider.
     */
    public abstract function renderChunk(BaseDataProvider $dataProvider);


    /**
     * Writes export contents to browser
     *
     * @param string Destination protocol where the output will go to. Default is php://output.
     */
    public abstract function doOutput($destination);

    /**
     * @return array key value config of header => value for output headers.
     */
    public abstract function getExportHeaders();

    /**
     * Outputs export to browser
     */
    public function outputExport($destination = 'php://output', $outputHeaders = true)
    {
        if ($outputHeaders) {
            $this->outputHeaders();
        }

        $this->doOutput($destination);
    }


    protected function outputHeaders()
    {
        $headers = $this->getExportHeaders();

        foreach ($headers as $header => $value) {
            header("{$header}: {$value}");
        }
    }

    /** @return Column[]|DataColumn[] */
    public function getExportColumns()
    {
        if (empty($this->exportColumns)) {
            $columnObjects = $this->grid->columns;
        } else {
            $columnObjects = [];
            foreach ($this->exportColumns as $column) {
                $columnObject = $this->createColumn($column);

                if ($columnObject === null) {
                    continue;
                }

                $columnObjects[] = $columnObject;
            }
        }

        foreach ($columnObjects as $index => $object) {
            if (in_array($index, $this->excludeColumnKeys)) {
                unset($columnObjects[$index]);
                continue;
            }

            if (in_array(get_class($object), $this->excludeColumnClasses)) {
                unset($columnObjects[$index]);
            }
        }

        return array_values($columnObjects);
    }

    /** @return Column|DataColumn */
    protected function createColumn($columnConfig)
    {
        if (is_string($columnConfig)) {
            $column = $this->createDataColumn($columnConfig);
        } else {
            $column = Yii::createObject(array_merge([
                'class' => $this->dataColumnClass,
                'grid' => $this->grid,
            ], $columnConfig));
        }

        if (!$column->visible) {
            return null;
        }

        return $column;
    }

    protected function createDataColumn($text)
    {
        if (!preg_match('/^([^:]+)(:(\w*))?(:(.*))?$/', $text, $matches)) {
            throw new InvalidConfigException('The column must be specified in the format of "attribute", "attribute:format" or "attribute:format:label"');
        }

        return Yii::createObject([
            'class' => $this->dataColumnClass,
            'grid' => $this->grid,
            'attribute' => $matches[1],
            'format' => isset($matches[3]) ? $matches[3] : 'text',
            'label' => isset($matches[5]) ? $matches[5] : null,
        ]);
    }

    public function getExtension()
    {
        return '';
    }
}