<?php
/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 **/

namespace common\components\export;


use common\components\export\exporters\BaseExporter;
use common\helpers\ArrayHelper;
use Yii;
use yii\base\BaseObject;
use yii\base\Exception;
use yii\data\BaseDataProvider;
use yii\di\Instance;

class ExporterManager extends BaseObject
{
    protected static $typeMap;

    /** @var BaseDataProvider */
    public $dataProvider;

    /** @var string|array|BaseExporter */
    public $exporter;

    /** @var bool */
    public $setHeaders = null;

    /** @var int|bool */
    public $pageSize = 1000;

    /** @var GridConfig|null */
    public $gridConfig;

    /** @var null|callable */
    public $logger = null;

    public static function defineType($type, $config = [])
    {
        static::$typeMap[$type] = $config;
    }

    /** @return  static */
    public static function fromType($type, $typeConfig = [], $managerConfig = [])
    {
        if (empty($type) || empty(static::$typeMap[$type])) {
            return null;
        }

        $exporterConfig = static::$typeMap[$type];
        $exporterClass = is_array($exporterConfig) ? $exporterConfig : ['class' => $exporterConfig];

        return new static(ArrayHelper::merge([
            'exporter' => ArrayHelper::merge($exporterClass, $typeConfig)
        ], $managerConfig));
    }

    public function init()
    {
        parent::init();

        if (empty($this->dataProvider)) {
            throw new Exception('dataProvider must be provided.');
        }

        if (empty($this->exporter)) {
            throw new Exception('exporter must be provided.');
        }

        $this->exporter = Instance::ensure($this->exporter, BaseExporter::class);

        if (!($this->dataProvider instanceof BaseDataProvider)) {
            throw new Exception("dataProvider must be an instance of BaseDataProvider.");
        }

        $this->setHeaders = $this->setHeaders ?? !Yii::$app->request->isConsoleRequest;
    }

    public function getExtension()
    {
        return $this->exporter->getExtension();
    }

    public function export($outputDestination = 'php://output')
    {
        $exportColumns = $this->gridConfig->getExportColumnConfig();

        if ($exportColumns !== false) {
            $this->exporter->exportColumns = $exportColumns;
        }

        $this->exporter->prepareExport();

        if ($this->pageSize !== false) {
            $this->performPagedExport();
        } else {
            $this->performAllExport();
        }

        $this->exporter->outputExport($outputDestination, $this->setHeaders);
    }

    protected function performPagedExport()
    {
        $exporter = $this->exporter;
        $dataProvider = $this->dataProvider;

        $pagination = $dataProvider->getPagination();

        $pagination->totalCount = $dataProvider->getTotalCount();
        $pagination->setPageSize($this->pageSize);
        $pageCount = $pagination->getPageCount();

        $startPage = 0;

        for ($page = $startPage; $page < $pageCount; $page++) {
            $this->logData('pagedExport', [
                'page' => $page,
                'total' => $pageCount
            ]);

            $dataProvider->getPagination()->setPage($page);
            $dataProvider->refresh();

            $exporter->renderChunk($dataProvider);
        }
    }

    protected function performAllExport()
    {
        $this->logData('allExport', 'Rendering all exported data.');

        $this->dataProvider->setPagination(false);
        $this->dataProvider->refresh();
        $this->exporter->renderChunk($this->dataProvider);
    }

    protected function logData($type, $data)
    {
        if ($this->logger === null) {
            return;
        }

        $logger = $this->logger;
        $logger($type, $data);
    }
}