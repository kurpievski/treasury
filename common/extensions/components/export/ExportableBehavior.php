<?php
/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 **/

namespace common\components\export;


use common\components\export\exporters\PHPExcelExporter;
use common\extensions\queue\GenerateReportJob;
use common\extensions\SqsQueue;
use common\models\GeneratedReports;
use common\widgets\exportdropdown\ExportDropdown;
use dvamigos\Yii2\GridView\GridEvent;
use dvamigos\Yii2\GridView\GridView;
use Yii;
use yii\base\Behavior;
use yii\base\Exception;
use yii\base\WidgetEvent;
use yii\di\Instance;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class ExportableBehavior extends Behavior
{
    const EXPORT_PARAM_METHOD_POST = 'POST';
    const EXPORT_PARAM_METHOD_GET = 'GET';

    /** @var GridView */
    public $owner;

    public $queueFlashMessage = 'This export cannot be processed immediately because requested export is over {rowCount} rows. Please check your notifications for report status.';

    /** @var bool */
    public $usePagedExport = true;

    /** @var int */
    public $pageSize = 1000;

    public $exportParam = 'exportAs';

    public $exportParamMethod = self::EXPORT_PARAM_METHOD_POST;

    /** @var int */
    public $queueCountThreshold = 1000;

    /** @var string|SqsQueue */
    public $queueComponent = 'priorityQueue';

    /** @var string|GenerateReportJob */
    public $exporterJobClassName = GenerateReportJob::class;

    /** @var GridConfig */
    public $gridConfig;

    /** @var array */
    public $exportColumns = [];

    /** @var array */
    public $exporterMap = [
        'xls' => [
            'label' => 'Export to Excel',
            'component' => PHPExcelExporter::class
        ]
    ];

    /** @var array */
    public $excludeColumnClasses = [];

    /** @var array */
    public $excludeColumnKeys = [];

    public $exportTitle = '';

    public $exportButtonTemplateName = '{exportButton}';

    public $exportButtonConfig = [
        'method' => 'POST',
        'action' => '',
    ];

    public $renderAfterRun = false;

    public $renderInTemplate = true;

    protected $_params = null;

    protected $_exportButton = null;

    public function init()
    {
        parent::init();

        foreach ($this->exporterMap as $type => $result) {
            ExporterManager::defineType($type, $result['component']);
        }

        $this->queueComponent = Instance::ensure($this->queueComponent, SqsQueue::class);

        if (empty($this->gridConfig)) {
            throw new Exception('gridConfig must be passed.');
        }
    }

    public function events()
    {
        $events = [
            GridView::EVENT_AFTER_INIT => 'performExport'
        ];

        if ($this->renderInTemplate) {
            $events[GridView::EVENT_GET_CUSTOM_SECTIONS] = 'getCustomExportableSection';
        }

        if ($this->renderAfterRun) {
            $events[GridView::EVENT_AFTER_RUN] = 'renderExportableAfterRun';
        }

        return $events;
    }

    public function performExport()
    {
        if (!$this->shouldExport()) {
            return;
        }

        $this->gridConfig->loadModelExportParams($this->getExportParams());
        $this->owner->dataProvider = $this->gridConfig->getDataProvider();

        if ($this->canExportImmediate()) {
            set_time_limit(0);
            Yii::$app->response->clearOutputBuffers();
            $this->getExportManager()->export();
            die;
        }

        $this->pushExportJob();
    }

    public function shouldExport()
    {
        return !empty($this->getExportParams());
    }

    public function getExportType()
    {
        return ArrayHelper::getValue($this->getExportParams(), ExportDropdown::PARAM_TYPE);
    }

    public function getCustomExportableSection(GridEvent $event)
    {
        $event->pushResult($this, [
                $this->exportButtonTemplateName => [$this, 'renderExportButton']
            ] + $event->getLastResult());
    }

    public function renderExportableAfterRun(WidgetEvent $event)
    {
        $event->result = strtr($event->result, [
            $this->exportButtonTemplateName => $this->renderExportButton()
        ]);
    }

    public function canExportImmediate()
    {
        return $this->gridConfig->getDataProvider()->getTotalCount() <= $this->queueCountThreshold;
    }

    public function getExportParams()
    {
        if ($this->gridConfig->getIsExportJobMode()) {
            return [];
        }

        if ($this->_params !== null) {
            return $this->_params;
        }

        $params = $this->exportParamMethod == self::EXPORT_PARAM_METHOD_POST ?
            Yii::$app->request->post($this->exportParam, []) :
            Yii::$app->request->get($this->exportParam, []);

        if (is_string($params)) {
            $params = Json::decode($params);
        }

        return $this->_params = $params;
    }


    protected function pushExportJob()
    {
        /** @var GenerateReportJob $jobClassName */
        $jobClassName = $this->exporterJobClassName;

        $this->queueComponent->postJob(new $jobClassName([
            'id' => GeneratedReports::create($this)->id
        ]));

        Yii::$app->session->setFlash('warning', Yii::t('app', $this->queueFlashMessage, [
            'rowCount' => $this->queueCountThreshold
        ]));
    }

    /** @return ExporterManager */
    public function getExportManager($exportTo = null)
    {
        $grid = $this->owner;
        $exportTo = $exportTo ?: $this->getExportType();

        $manager = ExporterManager::fromType($exportTo, [
            'grid' => $grid,
            'exportColumns' => $this->exportColumns,
            'excludeColumnClasses' => $this->excludeColumnClasses,
            'excludeColumnKeys' => $this->excludeColumnKeys,
        ], [
            'dataProvider' => $grid->dataProvider,
            'gridConfig' => $this->gridConfig,
        ]);

        $manager->pageSize = $this->usePagedExport ? $this->pageSize : false;

        return $manager;
    }

    public function renderExportButton()
    {
        if ($this->_exportButton !== null) {
            return $this->_exportButton;
        }

        $exportTypes = [];

        foreach ($this->exporterMap as $type => $exporter) {
            $exportTypes[$type] = $exporter['label'];
        }

        return $this->_exportButton = ExportDropdown::widget(ArrayHelper::merge($this->exportButtonConfig, [
            'exportTypes' => $exportTypes,
            'exportParam' => $this->exportParam,
            'gridConfig' => $this->gridConfig
        ]));
    }
}