<?php
/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 **/

namespace common\components\export;

use common\helpers\ArrayHelper;
use common\widgets\exportdropdown\ExportDropdown;
use Yii;
use yii\base\BaseObject;
use yii\data\BaseDataProvider;

/**
 * Class GridConfig
 * @package common\components\export
 */
abstract class GridConfig extends BaseObject
{
    /** @var ExportableModelInterface|string */
    public $model;

    protected $_dataProvider = null;

    public $exportJobParams = null;

    public function init()
    {
        if (is_string($this->model)) {
            $this->model = Yii::createObject($this->model);
        }

        parent::init();
    }

    public function getConfig()
    {
        if ($this->getIsExportJobMode()) {
            $columns = $this->getExportColumnConfig();
            if ($columns === false) {
                $columns = $this->getColumnConfig();
            }
        } else {
            $columns = $this->getColumnConfig();
        }

        return ArrayHelper::merge([
            'dataProvider' => $this->getDataProvider(),
            'columns' => $columns,
            'behaviors' => $this->getBehaviors()
        ], $this->getBaseConfig());
    }

    /** @return BaseDataProvider */
    public function getDataProvider()
    {
        if ($this->_dataProvider === null) {
            if ($this->getIsExportJobMode()) {
                $this->loadModelExportParams($this->exportJobParams);
            }

            $this->setDataProvider($this->model->search());
        }

        return $this->_dataProvider;
    }

    public function setDataProvider(BaseDataProvider $dataProvider)
    {
        $this->_dataProvider = $dataProvider;

        if ($this->getIsExportJobMode()) {
            $this->_dataProvider
                ->getSort()
                ->setAttributeOrders($this->exportJobParams[ExportDropdown::PARAM_SORT] ?? []);
        }
    }

    protected function getExportableBehavior($config = [])
    {
        return [
            $this->getExporterName() => ArrayHelper::merge([
                'class' => ExportableBehavior::class,
                'gridConfig' => $this
            ], $config)
        ];
    }

    public static function create($config = [])
    {
        return new static($config);
    }

    public abstract function getBaseConfig();

    public function getExporterName()
    {
        return 'exportable';
    }

    protected function getColumnConfig()
    {
        return [];
    }

    /**
     * Returns export column config.
     *
     * If return value is false then getColumnConfig() is used.
     *
     * @return bool
     */
    public function getExportColumnConfig()
    {
        return false;
    }

    protected function getBehaviors()
    {
        return [];
    }

    public function getIsExportJobMode()
    {
        return !empty($this->exportJobParams);
    }

    public function getExportJobConfig()
    {
        return [
            'class' => static::class,
            'model' => get_class($this->model)
        ];
    }

    public function getExportJobType()
    {
        if (!$this->getIsExportJobMode()) {
            return null;
        }

        return $this->exportJobParams[ExportDropdown::PARAM_TYPE] ?? null;
    }

    public function loadModelExportParams($exportParams = [])
    {
        $this->model->loadExportParams($exportParams[ExportDropdown::PARAM_FILTER] ?? []);
        $this->_dataProvider = null;
    }

    public function getReportTemplateLayout()
    {
        $templateLayout = "<div class='export-button-handler'>{exportButton}</div> {summary}\n{items}";
        $templateLayout .= '<div class="table-nav">
                <div class="row">
                        <div class="col-md-8 pager_wrap">{pager}</div>
                        <div class="col-md-4 text-right"><span></span><div class="per-page-wrapper">{perPage}</div></div>
                    </div>
                </div>';

        return $templateLayout;
    }
}