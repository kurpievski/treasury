<?php

namespace common\components;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class FilesystemUploadFile extends UploadedFile
{
    public $fileUrl;

    protected static $_uriFiles = [];

    private $_tmpFile = null;
    private $_tmpStorageLocation = '@app/runtime/';

    public function __destruct()
    {
        @unlink($this->_tmpFile);
    }

    public function init()
    {
        parent::init();

        if (empty($this->fileUrl)) {
            throw new InvalidConfigException('fileUrl must be filled.');
        }

        $this->error = UPLOAD_ERR_OK;

        $fileData = file_get_contents($this->fileUrl);

        $baseName = basename($this->fileUrl);
        $ext = pathinfo($baseName, PATHINFO_EXTENSION);
        $this->_tmpFile = tempnam(Yii::getAlias($this->_tmpStorageLocation), '') . (!empty($ext) ? '.' . $ext : '');

        file_put_contents($this->_tmpFile, $fileData);

        if (!file_exists($this->_tmpFile)) {
            $this->error = UPLOAD_ERR_NO_FILE;
            return;
        }

        $this->type = FileHelper::getMimeType($this->_tmpFile);
        $this->tempName = $this->_tmpFile;

        $this->size = filesize($this->_tmpFile);
        $this->name = basename($this->fileUrl);
    }

    public static function getInstanceByPath($filePath)
    {
        if (!empty(self::$_uriFiles['byPath'][$filePath])) {
            return self::$_uriFiles['byPath'][$filePath];
        }

        static::setFSInstance('byPath', $filePath, $filePath);

        return self::$_uriFiles['byPath'][$filePath];
    }

    /** @return static */
    public static function getInstanceByName($name)
    {
        $files = self::loadFSFiles($name);
        return isset($files[$name][0]) ? $files[$name][0] : null;
    }

    public static function getInstancesByName($name)
    {
        return self::loadFSFiles($name);
    }

    public function saveAs($file, $deleteTempFile = true)
    {
        $result = copy($this->tempName, $this->fileKey);

        if ($deleteTempFile && file_exists($this->tempName)) {
            unlink($this->tempName);
        }

        return $result;
    }

    protected static function loadFSFiles($name)
    {
        $getData = Yii::$app->request->get($name);

        if ($getData === null) {
            return static::$_uriFiles;
        }

        if (!isset(static::$_uriFiles[$name])) {
            if (is_array($getData)) {
                foreach ($getData as $index => $filePath) {
                    static::loadFSFilesRecursive($name, $filePath, $index);
                }
            } else {
                self::setFSInstance($name, 0, $getData);
            }
        }

        return static::$_uriFiles;
    }

    protected static function loadFSFilesRecursive($key, $filePaths, $indexKey)
    {
        if (is_array($filePaths)) {
            foreach ($filePaths as $i => $filePath) {
                self::loadFSFilesRecursive($key . '[' . $i . ']', $filePath, $indexKey . '-' . $i);
            }
            return;
        }

        self::setFSInstance($key, $indexKey, $filePaths);
    }

    protected static function setFSInstance($name, $indexKey, $filePath)
    {
        self::$_uriFiles[$name][$indexKey] = new static([
            'fileUrl' => $filePath
        ]);
    }
}