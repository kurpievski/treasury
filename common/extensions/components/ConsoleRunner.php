<?php

namespace common\components;
use Yii;
use yii\base\Component;

/**
 * Class Runner - a component for running console command in yii2 web applications
 */
class ConsoleRunner extends Component
{
    /**
     * @var string yii console application file that will be executed
     */
    public $yiiscript;
    /**
     * @var string path to php executable
     */    
    public $phpexec;
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        set_time_limit(0);
        if($this->yiiscript == null) {
            $this->yiiscript = "@app/yii";
            
        }
    }
    /**
     * Runs yii console command
     *
     * @param $cmd command with arguments
     * @param string $output filled with the command output
     * @return int termination status of the process that was run
     */
    public function run($cmd, &$output = '')
    {     
        //echo $this->buildCommand($cmd);exit;
        $handler = popen($this->buildCommand($cmd), 'r');
        while(!feof($handler))
            $output .= fgets($handler);
        $output = trim($output);
        $status = pclose($handler);
        return $status;
    }
    /**
     * Builds the command string
     *
     * @param $cmd Yii command
     * @return string full command to execute
     */
    protected function buildCommand($cmd)
    {
        return $this->getPHPExecutable() . ' ' . Yii::getAlias($this->yiiscript) . ' ' . $cmd . ' --no-color 2>&1';
    }
    /**
     * If property $phpexec is set it will be used as php executable
     *
     * @return string path to php executable
     */
    public function getPHPExecutable()
    {
        if($this->phpexec) {
            return $this->phpexec;
        }
        return PHP_BINDIR . '/php';
    }
}