<?php
/**
 * Created by PhpStorm.
 * User: Nikola Radovic <nikola@2amigos.us>
 * Date: 3/21/2016
 * Time: 9:21 PM
 */

namespace common\components;

use yii\base\Component;

class Settings extends Component
{
    public $settingsModelClass = '\common\models\Settings';

    public function get($attribute, $default = null)
    {
        $modelClass = $this->settingsModelClass;
        return $modelClass::getParam($attribute, $default);
    }

    public function set($attribute, $value)
    {
        $modelClass = $this->settingsModelClass;
        $modelClass::setParam($attribute, $value);
    }

}