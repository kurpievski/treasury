<?php
/**
 * Email: srdjan.drakul@2amigos.us
 * Date: 4/10/2019
 * Time: 14:53
 */

namespace common\components;

use common\helpers\ArrayHelper;
use common\helpers\StringHelper;
use common\models\Location;
use GuzzleHttp\Client;
use yii\base\Component;

class MapboxApi extends Component
{
    const MILE_CONSTANT = 0.000621371;

    public $key;
    public $apiUrl = 'https://api.mapbox.com';

    public function getKey()
    {
        return $this->key;
    }

    public function geocodeAddressArray($address)
    {
        $geocodeLocation = Location::findGeocodeLocation($address);

        if($geocodeLocation === null) {
            $geocodeLocation = $this->geocode($address);
        }

        return [
            'lat' => ArrayHelper::getValue($geocodeLocation, 'lat'),
            'lng' => ArrayHelper::getValue($geocodeLocation, 'lng')
        ];
    }

    protected function sendRequest($url, $asArray = true)
    {
        $content = (new Client(['base_uri' => $this->apiUrl]))
            ->get($url . (strpos($url, '?') === false ? "?access_token={$this->key}" : "&access_token={$this->key}"))
            ->getBody()
            ->getContents();

        if ($asArray) {
            return json_decode($content, true);
        }

        return $content;
    }

    public function geocode($addressArray)
    {
        $address = is_array($addressArray) ? StringHelper::formatAddress($addressArray) : $addressArray;

        if(empty($address)) {
            return null;
        }

        $address = urlencode($address);

        $rawResponse = $this->sendRequest("/geocoding/v5/mapbox.places/{$address}.json");
        $response = ArrayHelper::getValue($rawResponse, "features.0.geometry.coordinates");

        if(!empty($response)) {
            $response = $this->convertArrayToLocation($response);
            Location::storeGeocodeResult(array_merge($addressArray, $response));
        }

        return $response;
    }

    public function convertArrayToLocation($array)
    {
        return [
            'lng' => $array[0],
            'lat' => $array[1]
        ];
    }

    public function getMileDistanceFromLocation($origin, $destination)
    {
        $distance = $this->getDistanceFromLocation($origin, $destination);

        return !empty($distance) ? $distance * self::MILE_CONSTANT : null;
    }

    public function getDistanceFromLocation($origin, $destination)
    {
        $origin = $this->geocodeAddressArray($origin);
        $destination = $this->geocodeAddressArray($destination);

        if(empty($origin['lat']) || empty($origin['lng']) || empty($destination['lat']) || empty($destination['lng'])) {
            return 0;
        }

        $result = $this->sendRequest("/directions-matrix/v1/mapbox/driving/{$origin['lng']},{$origin['lat']};{$destination['lng']},{$destination['lat']}?annotations=distance");

        if($result['code'] === 'Ok') {
            return ArrayHelper::getValue($result, "distances.0.1");
        }

        return 0;
    }
}