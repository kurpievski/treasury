<?php
/**
 * Created by PhpStorm.
 * User: Goran Sarenac <goransarenac91@hotmail.com>
 * Date: 9/12/2016
 * Time: 3:45 PM
 */

namespace common\components;


use yii\base\Application;
use yii\base\BootstrapInterface;

class LanguageSwitcher implements BootstrapInterface
{

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if (!$app->request->isConsoleRequest) {
            $app->language = $app->request->cookies->getValue('language', $app->sourceLanguage);
        }
    }
}