<?php
/**
 * Vladimir Prorok <hola@2amigos.us>
 * Company: 2amigOS! <https://2amigos.us>
 */

namespace common\components;

use \yii\caching\DummyCache as DCache;

class DummyCache extends DCache
{
    public $cachePath = null;
}