<?php
/**
 * ResourceManager.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 09-Nov-15
 * Time: 17:31
 */

namespace common\components\resourcemanager;

use common\components\resourcemanager\wrappers\AbstractStorage;
use common\components\resourcemanager\wrappers\AWSStorage;
use common\components\resourcemanager\wrappers\FTPStorage;
use common\components\resourcemanager\wrappers\LocalStorage;
use yii\base\Component;
use yii\base\Exception;
use yii\web\UploadedFile;

class ResourceManager extends Component
{
    const LOCAL_STORAGE = 'local-storage';
    const FTP_STORAGE = 'ftp-storage';
    const AWS_STORAGE = 'aws-storage';

    /** @var array File system config. */
    public $fsConfig = [];

    /** @var string Selected storage driver */
    public $selectedDriver = self::LOCAL_STORAGE;

    /** @var AbstractStorage */
    protected $_storageInstance = null;

    public function getStorageDrivers()
    {
        return [
            self::LOCAL_STORAGE => LocalStorage::class,
            self::FTP_STORAGE => FTPStorage::class,
            self::AWS_STORAGE => AWSStorage::class
        ];
    }

    public function init()
    {
        $drivers = $this->getStorageDrivers();

        if (!array_key_exists($this->selectedDriver, $drivers)) {
            throw new Exception("Selected storage driver does not exist.");
        }

        parent::init();
    }

    public function getStorageInstance()
    {
        if (empty($this->_storageInstance)) {
            $className = $this->getStorageDrivers()[$this->selectedDriver];
            $driverConfig = !empty($this->fsConfig[$this->selectedDriver]) ?
                $this->fsConfig[$this->selectedDriver] : [];

            $this->_storageInstance = new $className($driverConfig);
        }

        return $this->_storageInstance;
    }

    public function sendFile($fromPath, $toPath, array $config = [])
    {
        return $this->getStorageInstance()->writeFile($fromPath, $toPath, $config);
    }

    public function getFileData($filePath)
    {
        return $this->getStorageInstance()->readFile($filePath);
    }

    public function fileExists($filePath)
    {
        return $this->getStorageInstance()->fileExists($filePath);
    }

    public function renameFile($fromFilePath, $toFilePath)
    {
        if($this->fileExists($toFilePath)) {
            $this->deleteFile($toFilePath);
        }
        return $this->getStorageInstance()->renameFile($fromFilePath, $toFilePath);
    }

    public function deleteFile($filePath)
    {
        return $this->getStorageInstance()->deleteFile($filePath);
    }

    public function getFileSize($filePath)
    {
        return $this->getStorageInstance()->getFileSize($filePath);
    }

    public function getFileTimestamp($filePath)
    {
        return $this->getStorageInstance()->getFileTimestamp($filePath);
    }

    public function getFileMimeType($filePath)
    {
        return $this->getStorageInstance()->getMimeType($filePath);
    }

    public function getFileMetadata($filePath)
    {
        return $this->getStorageInstance()->getFileMetaData($filePath);
    }

    public function listDirectory($directoryPath, $listRecursive = false)
    {
        return $this->getStorageInstance()->listDirectory($directoryPath, $listRecursive);
    }

    public function listFiles($directoryPath)
    {
        return $this->getStorageInstance()->listFiles($directoryPath);
    }

    public function sendUploadedFile(UploadedFile $file, $destinationFilePath)
    {
        return $this->sendFile($file->tempName, $destinationFilePath);
    }

    public function getUrl($name, $expires = NULL)
    {
        return $this->getStorageInstance()->getUrl($name, $expires);
    }

    public function downloadFile($filePath)
    {
        return$this->getStorageInstance()->download($filePath);
    }

    public function disconnect()
    {
        return$this->getStorageInstance()->disconnect();
    }
}