<?php
/**
 * Date: 6/17/2016
 * Time: 4:36 PM
 * Author: Igor Golub <igor.golub@2amigos.us>
 */

namespace common\components\resourcemanager\wrappers;


use League\Flysystem\Sftp\SftpAdapter;

class FTPStorage extends AbstractStorage
{
    public $config;
    public $downloadDir = '@app/runtime/';
    
    
    public function getStorageAdapter()
    {
        return new SftpAdapter($this->config);
    }
    
    public function getDownloadDir()
    {
        return $this->downloadDir;
    }

}