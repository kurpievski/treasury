<?php
/**
 * Date: 6/17/2016
 * Time: 2:51 PM
 * Author: Igor Golub <igor.golub@2amigos.us>
 */

namespace common\components\resourcemanager\wrappers;


use League\Flysystem\Adapter\Local;

class LocalStorage extends AbstractStorage
{
    public $root;
    public $downloadDir = '@app/runtime/';

    public function getStorageAdapter()
    {
        return new Local($this->root);
    }
    
    public function getDownloadDir()
    {
        return $this->downloadDir;
    }

    public function disconnect()
    {
        return true;
    }
}
