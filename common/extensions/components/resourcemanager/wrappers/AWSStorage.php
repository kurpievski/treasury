<?php
/**
 * AWSStorage.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 09-Nov-15
 * Time: 17:33
 */

namespace common\components\resourcemanager\wrappers;


use Aws\S3\S3Client;
use League\Flysystem\AdapterInterface;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use yii\base\InvalidConfigException;

class AWSStorage extends AbstractStorage
{
    /**
     * @var string
     */
    public $key;
    /**
     * @var string
     */
    public $secret;
    /**
     * @var string
     */
    public $region;

    /**
     * @var string
     */
    public $version;

    /**
     * @var string
     */
    public $baseUrl;
    /**
     * @var string
     */
    public $bucket;
    /**
     * @var string|null
     */
    public $prefix;
    /**
     * @var array
     */
    public $options = ['visibility' => AdapterInterface::VISIBILITY_PRIVATE];

    public $downloadDir = '@app/runtime/';

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->key === null) {
            throw new InvalidConfigException('The "key" property must be set.');
        }
        if ($this->secret === null) {
            throw new InvalidConfigException('The "secret" property must be set.');
        }
        if ($this->bucket === null) {
            throw new InvalidConfigException('The "bucket" property must be set.');
        }

        parent::init();
    }

    protected function getConfig()
    {
        $config = [
            'credentials' => [
                'key' => $this->key,
                'secret' => $this->secret
            ]
        ];

        if ($this->region !== null) {
            $config['region'] = $this->region;
        }
        if ($this->version !== null) {
            $config['version'] = $this->version;
        }

        if ($this->baseUrl !== null) {
            $config['base_url'] = $this->baseUrl;
        }

        $config['prefix'] = 'local';

        return $config;
    }

    protected function getClient()
    {
        return new S3Client($this->getConfig());
    }

    public function getStorageAdapter()
    {
        return new AwsS3Adapter(
            $this->getClient(),
            $this->bucket,
            $this->prefix,
            $this->options
        );
    }

    public function getUrl($key, $expires = null)
    {
        $path = $this->getStorageAdapter()->applyPathPrefix($key);
        $command = $this->getClient()->getCommand('GetObject', [
            'Bucket' => $this->bucket,
            'Key'    => $path
        ]);

        $request = $this->getClient()->createPresignedRequest($command, $expires);
        return (string) $request->getUri();
    }

    public function getDownloadDir()
    {
        return $this->downloadDir;
    }

}