<?php
/**
 * AbstractStorage.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 09-Nov-15
 * Time: 18:34
 */

namespace common\components\resourcemanager\wrappers;

use League\Flysystem\AdapterInterface;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemInterface;
use yii\base\BaseObject;

abstract class AbstractStorage extends BaseObject
{
    /** @var FilesystemInterface */
    private $_fileSystem;

    public function init()
    {
        $this->_fileSystem = new Filesystem($this->getStorageAdapter());

        parent::init();
    }

    /** @return AdapterInterface */
    protected abstract function getStorageAdapter();

    /** @return string download dir path ending on / */
    protected abstract function getDownloadDir();

    public function writeFile($sourceFilePath, $destinationFilePath, array $config = [])
    {
        $stream = fopen($sourceFilePath, 'rb');

        $data = $this->_fileSystem->putStream($destinationFilePath, $stream, $config);

        if (is_resource($stream)) {
            fclose($stream);
        }

        return $data;
    }

    public function readFile($filePath)
    {
        return $this->_fileSystem->read($filePath);
    }

    public function fileExists($filePath)
    {
        return $this->_fileSystem->has($filePath);
    }

    public function deleteFile($filePath)
    {
        return $this->_fileSystem->delete($filePath);
    }

    public function getFileSize($filePath)
    {
        return $this->_fileSystem->getSize($filePath);
    }

    public function renameFile($fromFilePath, $toFilePath)
    {
        return $this->_fileSystem->rename($fromFilePath, $toFilePath);
    }

    public function getMimeType($filePath)
    {
        return $this->_fileSystem->getMimetype($filePath);
    }

    public function getFileTimestamp($filePath)
    {
        return $this->_fileSystem->getTimestamp($filePath);
    }

    public function listDirectory($directoryPath, $listRecursive = false)
    {
        return $this->_fileSystem->listContents($directoryPath, $listRecursive);
    }

    public function listFiles($directoryPath)
    {
        $directoryContent = $this->listDirectory($directoryPath, false);
        $files = [];

        foreach ($directoryContent as $item) {
            if($item['type'] == 'file') {
                $files[] = $item['path'];
            }
        }

        return $files;
    }

    public function createDirectory($directoryPath, $config = [])
    {
        return $this->_fileSystem->createDir($directoryPath, $config);
    }

    public function removeDirectory($directoryPath)
    {
        return $this->_fileSystem->deleteDir($directoryPath);
    }

    public function copyFile($fromPath, $toPath)
    {
        return $this->_fileSystem->copy($fromPath, $toPath);
    }

    public function setFileVisibility($path, $visibility)
    {
        return $this->_fileSystem->setVisibility($path, $visibility);
    }

    public function getFileVisibility($path)
    {
        return $this->_fileSystem->getVisibility($path);
    }

    public function getFileLink($path)
    {
        return $this->_fileSystem->get($path);
    }

    public function getFileMetaData($filePath)
    {
        return $this->_fileSystem->getMetadata($filePath);
    }

    public function download($filePath) {
        $fileContent = $this->readFile($filePath);
        $localFile = \Yii::getAlias($this->getDownloadDir()) . basename($filePath);
        
        if(!file_put_contents($localFile, $fileContent)) {
            return false;
        }
        
        return $localFile;
    }

    public function disconnect()
    {
        return $this->_fileSystem->getAdapter()->disconnect();
    }
}