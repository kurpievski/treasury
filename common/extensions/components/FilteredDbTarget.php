<?php
/**
 * FilteredDbTarget.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 19-Aug-15
 * Time: 01:07
 */

namespace common\components;

use yii\log\DbTarget;

class FilteredDbTarget extends DbTarget
{
    use FilterTargetGlobalsTrait;
}