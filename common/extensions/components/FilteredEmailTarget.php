<?php
/**
 * FilteredTarget.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 19-Aug-15
 * Time: 01:07
 */

namespace common\components;

use yii\log\EmailTarget;

class FilteredEmailTarget extends EmailTarget
{
    use FilterTargetGlobalsTrait;
}