<?php
/**
 * Formatter.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 05-Nov-15
 * Time: 19:34
 */

namespace common\components;

use common\helpers\MoneyHelper;

class Formatter extends \yii\i18n\Formatter
{
    const MONEY_FORMAT = 'money';

    public function asMoney($value)
    {
        if (in_array($value, ['N/A', null])) {
            return 'N/A';
        }

        return MoneyHelper::format($value);
    }
}