<?php
/**
 * Created by PhpStorm.
 * User: Nikola Radovic <nikola@2amigos.us>
 * Date: 6/13/2016
 * Time: 11:41 PM
 */

namespace common\components;

use PHPExcel_Writer_HTML;
use PHPExcel_Calculation;
use PHPExcel_Worksheet;

class PHPExcelHTMLWriter extends PHPExcel_Writer_HTML
{
    public $buildCss = false;

    public function getPhpExcel()
    {
        if (property_exists(__CLASS__, '_phpExcel')) {
            return $this->_phpExcel;
        }

        return $this->phpExcel;
    }

    public function getIsPdf()
    {
        if (property_exists(__CLASS__, '_isPdf')) {
            return $this->_isPdf;
        }

        return $this->isPdf;
    }

    /**
     * @inheritdoc
     */
    public function getHTMLContent()
    {
        $this->getPhpExcel()->garbageCollect();

        $saveDebugLog = PHPExcel_Calculation::getInstance($this->getPhpExcel())->getDebugLog()->getWriteDebugLog();
        PHPExcel_Calculation::getInstance($this->getPhpExcel())->getDebugLog()->setWriteDebugLog(false);
        $saveArrayReturnType = PHPExcel_Calculation::getArrayReturnType();
        PHPExcel_Calculation::setArrayReturnType(PHPExcel_Calculation::RETURN_ARRAY_AS_VALUE);

        if ($this->buildCss) {
            $this->buildCSS(!$this->getUseInlineCss());
        }

        $html = '';

        $html .= $this->generateHTMLHeader(!$this->getUseInlineCss());

        if ((!$this->getIsPdf()) && ($this->getGenerateSheetNavigationBlock())) {
            $html .= $this->generateNavigation();
        }

        $html .= $this->generateSheetData();
        $html .= $this->generateHTMLFooter();

        PHPExcel_Calculation::setArrayReturnType($saveArrayReturnType);
        PHPExcel_Calculation::getInstance($this->getPhpExcel())->getDebugLog()->setWriteDebugLog($saveDebugLog);


        $html = preg_replace('/<style>(.*?)<\/style>/s', '', $html);
        $html = preg_replace('/<style type="text\/css">(.*?)<\/style>/s', '', $html);

        return $html;
    }


    /**
     * Generate HTML header
     *
     * @param    boolean $pIncludeStyles Include styles?
     * @return    string
     * @throws PHPExcel_Writer_Exception
     */
    public function generateHTMLHeader($pIncludeStyles = false)
    {
        $html = '';
        $html .= '<html>' . PHP_EOL;
        $html .= '  <head>' . PHP_EOL;
        $html .= '  </head>' . PHP_EOL;
        $html .= '  <body>' . PHP_EOL;

        return $html;
    }


}