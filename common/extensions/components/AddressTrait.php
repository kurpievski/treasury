<?php
/**
 * Email: srdjan.drakul@2amigos.us
 * Date: 4/15/2019
 * Time: 21:11
 */

namespace common\components;

use common\helpers\StringHelper;

trait AddressTrait
{
    public function getFullAddress($asArray = false)
    {
        if (empty($this->address) && empty($this->city) && empty($this->state) && empty($this->zip)) {
            return $asArray ? [] : '';
        }

        $address = array_combine(
            ['address', 'city', 'state', 'zip'],
            [$this->address, $this->city, $this->state, $this->zip]
        );

        if($asArray) {
            return $address;
        }

        return StringHelper::formatAddress($address);
    }
}