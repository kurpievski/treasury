<?php
/**
 * Date: 5/4/2016
 * Time: 6:55 PM
 * Author: Igor Golub <igor.golub@2amigos.us>
 */

namespace common\components;


use common\helpers\ArrayHelper;
use Yii;
use yii\helpers\Html;
use yii\web\AssetBundle;

class View extends \yii\web\View
{
    public function registerJsFile($url, $options = [], $key = null)
    {
        $url = Yii::getAlias($url);
        $key = $key ?: $url;
        $depends = ArrayHelper::remove($options, 'depends', []);
        $appendTimestamp = ArrayHelper::remove($options, 'appendTimestamp', false);
        $file = Yii::getAlias("@webroot/$url");

        if ($appendTimestamp && ($timestamp = @filemtime("$file")) > 0) {
            $url=  "{$url}?v={$timestamp}";
        }

        if (empty($depends)) {
            $position = ArrayHelper::remove($options, 'position', self::POS_END);
            $this->jsFiles[$position][$key] = Html::jsFile($url, $options);
        } else {
            $this->getAssetManager()->bundles[$key] = new AssetBundle([
                'baseUrl' => '',
                'js' => [strncmp($url, '//', 2) === 0 ? $url : ltrim($url, '/')],
                'jsOptions' => $options,
                'depends' => (array) $depends,
            ]);

            $this->registerAssetBundle($key);
        }
    }
}