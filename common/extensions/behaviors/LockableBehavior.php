<?php
/**
 * LockableBehavior.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 29-Jan-16
 * Time: 18:05
 */

namespace common\behaviors;


use common\extensions\ActiveRecord;
use Yii;
use yii\base\Behavior;
use yii\base\Exception;
use yii\base\ModelEvent;
use yii\db\BaseActiveRecord;

class LockableBehavior extends Behavior
{
    public $lockedByAttribute;
    public $lockTimeAttribute;
    public $lockExpireSeconds = 120;

    /** @var ActiveRecord */
    public $owner;

    protected $_isDisabled = false;

    public function init()
    {
        $this->_isDisabled = php_sapi_name() === 'cli';

        parent::init();
    }

    public function events()
    {
        return [
            BaseActiveRecord::EVENT_BEFORE_VALIDATE => 'checkLock'
        ];
    }

    protected function saveLockChanges()
    {
        $this->owner->updateAttributes([$this->lockedByAttribute, $this->lockTimeAttribute]);
    }

    public function removeLock()
    {
        if (!$this->canAcquireLock() || $this->_isDisabled) {
            return false;
        }

        $this->owner->{$this->lockedByAttribute} = null;
        $this->owner->{$this->lockTimeAttribute} = null;

        $this->saveLockChanges();

        return true;
    }

    public function lock()
    {
        if (!$this->canAcquireLock() || $this->_isDisabled) {
            return false;
        }

        $this->owner->{$this->lockedByAttribute} = Yii::$app->user->getId();
        $this->owner->{$this->lockTimeAttribute} = time();

        $this->saveLockChanges();

        return true;
    }

    public function checkLock(ModelEvent $event)
    {
        if ($this->_isDisabled) {
            return;
        }

        if (Yii::$app->user->getIsGuest()) {
            $this->owner->addError($this->lockedByAttribute, 'User is not logged in.');
            $event->isValid = false;

            return;
        }

        if (!$this->canAcquireLock()) {
            $this->owner->addError($this->lockedByAttribute, 'This item is currently locked by another user.');
            $event->isValid = false;
        }
    }

    public function isLockExpired()
    {
        $lockTime = $this->owner->{$this->lockTimeAttribute};

        $expireTime = $lockTime !== null ? $lockTime + $this->lockExpireSeconds : 0;
        $currentTime = time();

        return ($currentTime > $expireTime);
    }

    public function canAcquireLock()
    {
        if ($this->_isDisabled) {
            return true;
        }

        $userId = Yii::$app->user->getId();

        if (empty($this->owner->{$this->lockedByAttribute}) ||
            $userId == $this->owner->{$this->lockedByAttribute}) {
            return true;
        }

        return $this->isLockExpired();
    }
}