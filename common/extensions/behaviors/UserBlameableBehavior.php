<?php
/**
 * UserBlameableBehavior.php
 *
 * Author: Aleksandar Panic <aleksandar.panic@2amigos.us>
 * Timestamp: 15-Jul-16 19:04
 */

namespace common\extensions\behaviors;


use common\components\WebUser;
use common\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;

class UserBlameableBehavior extends BlameableBehavior
{
    public $createdByAttribute = 'create_id';
    public $updatedByAttribute = 'update_id';

    protected function getValue($event)
    {
        if ($this->value === null) {
            /** @var WebUser $user */
            $user = Yii::$app->get('user', false);

            if ($user && !$user->IsIdentityClassOf(User::class)) {
                return null;
            }

            return $user && !$user->isGuest ? $user->id : null;
        } else {
            return call_user_func($this->value, $event);
        }
    }
}