<?php
/**
 * Email: srdjan.drakul@2amigos.us
 * Date: 12/17/2018
 * Time: 3:07 PM
 */

namespace common\behaviors;

use common\models\SettingsHistory;
use yii\base\Behavior;
use yii\base\Exception;
use yii\db\ActiveRecord;

class SettingsHistoryBehavior extends Behavior
{
    const ACTION_DELETE = 'DELETE';
    const ACTION_CREATE = 'CREATE';
    const ACTION_SET = 'SET';
    const ACTION_CHANGE = 'CHANGE';
    const ACTION_SAVE = 'SAVE';

    protected $_oldValue = [];

    public $trackedKeys = [];
    public $ignoredClasses = [];
    public $keyAttribute = 'key';
    public $valueAttribute = 'value';

    public $historyModelClass = SettingsHistory::class;
    public $source = 'user';
    public $skipOnEmpty = true;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    public function afterDelete($event)
    {
        $this->leaveTrail(self::ACTION_DELETE);
    }

    public function afterFind($event)
    {
        $this->setOldValue(unserialize($this->owner->{$this->valueAttribute}));
    }

    public function afterInsert($event)
    {
        $this->audit(true);
    }

    public function afterUpdate($event)
    {
        $this->audit(false);
    }

    public function audit($insert)
    {
        $ignoredClasses = $this->ignoredClasses;
        $key = $this->owner->{$this->keyAttribute};

        if(!in_array($key, $this->trackedKeys)) {
            return;
        }

        if (sizeof($ignoredClasses) > 0) {
            if (array_search(get_class($this->owner), $ignoredClasses) !== false) {
                return;
            }
        }

        if ($insert) {
            $this->leaveTrail(self::ACTION_CREATE);
        }

        $newValue = unserialize($this->owner->{$this->valueAttribute});
        $oldValue = $this->getOldValue();

        $this->auditAttributes($insert, $newValue, $oldValue);
        $this->setOldValue($newValue);
    }

    public function auditAttributes($insert, $newValue, $oldValue = '')
    {
        if ($this->skipOnEmpty && empty($newValue) && empty($oldValue)) {
            return;
        }

        $key = $this->owner->{$this->keyAttribute};

        if ($newValue != $oldValue) {
            $this->leaveTrail($this->getAction($insert), $key, $newValue, $oldValue);
        }
    }

    public function leaveTrail($action, $key = null, $newValue = null, $oldValue = null)
    {
        $historyClass = $this->historyModelClass;

        /** @var ActiveRecord $log */
        $log = new $historyClass();

        $log->model = $this->owner->className();
        $log->old_value = $oldValue;
        $log->new_value = $newValue;
        $log->action = $action;
        $log->key = $key;
        $log->source = $this->source;

        if (!$log->save()) {
            throw new Exception(print_r($log->getErrors(), true));
        }

        return true;
    }

    public function getOldValue()
    {
        return $this->_oldValue;
    }

    public function setOldValue($value)
    {
        $this->_oldValue = $value;
    }

    protected function getAction($insert)
    {
        return $insert ? self::ACTION_SET : self::ACTION_CHANGE;
    }
}