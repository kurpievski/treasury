<?php
/**
 * Date: 10/18/2016
 * Time: 7:34 PM
 * Author: Igor Golub <igor.golub@2amigos.us>
 */

namespace common\extensions\behaviors;


use common\helpers\LanguageHelper;
use Yii;
use yii\base\ActionFilter;

class LanguageFilter extends ActionFilter
{
    public function beforeAction($action)
    {
        /** @var $action Action */

        if (!$this->isActive($action)) {
            return parent::beforeAction($action);
        }
        
        $language = Yii::$app->language;

        if (empty(Yii::$app->request->getQueryParam('lang'))) {
            return $this->owner->redirect([$action->id, 'lang' => $language]);
        }

        $isPageRefreshed = Yii::$app->request->referrer == Yii::$app->request->absoluteUrl;
        $isLanguageChanged = $language != Yii::$app->request->getQueryParam('lang');

        if ($isPageRefreshed && $isLanguageChanged) {
            return $this->owner->redirect([$action->id, 'lang' => $language]);
        }

        if ($isLanguageChanged) {
            LanguageHelper::setLanguage(Yii::$app->request->getQueryParam('lang'));
            return $this->owner->refresh();
        }

        return true;
    }
}