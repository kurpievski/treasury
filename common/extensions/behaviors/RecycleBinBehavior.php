<?php
/**
 * Created by PhpStorm.
 * User: Minja Siljegovic <minja.siljegovic@2amigos.us>
 * Date: 29/12/2015
 * Time: 18:51
 */

namespace common\behaviors;

use common\components\BulkElasticSearch;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\helpers\ArrayHelper;
use \common\extensions\elasticsearch\ActiveRecord as EsActiveRecord;

class RecycleBinBehavior extends Behavior
{
    const RECYCLED_EVENT = 'active-record-recycled';
    const RESTORED_EVENT = 'active-record-restored';

    /** @var \common\extensions\ActiveRecord|static */
    public $owner;

    /** @var string */
    public $esModelClass;

    /** @var callable */
    public $onSaveCallback = null;

    /** @var callable */
    public $onBeforeDeleteCallback = null;

    /** @var callable */
    public $onAfterDeleteCallback = null;

    /** @var callable */
    public $onRecycleCallback = null;

    /** @var callable */
    public $onRestoreCallback = null;

    /** @var bool */
    public $runValidationOnSave = true;

    /** @var array */
    public $saveAttributeNames = null;

    public $setAttributesFromDbModel = true;

    protected $_recycledModel = null;

    protected $_skipRecycleCheck = false;

    protected $_isRecycle = false;

    public $recycledAttribute = 'is_in_recycle_bin';

    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete'
        ];
    }

    public function deletePermanently()
    {
        $this->_skipRecycleCheck = true;
        $this->owner->deleteRecycledModel();
        return $this->owner->delete();
    }

    public function beforeDelete($event)
    {
        $owner = $this->owner;

        if ($this->isPermanentDelete()) {
            $callback = $this->onBeforeDeleteCallback;
            if (is_callable($callback)) {
                $callback();
            }

            return true;
        }

        /** @var BlameableBehavior $blameBehavior */
        $blameBehavior = $this->owner->getBehavior('parentBlameable');

        /** @var TimestampBehavior $timestampBehavior */
        $timestampBehavior = $this->owner->getBehavior('parentTimestamp');

        $updateAttributes = [
            $this->recycledAttribute => 1,
            $blameBehavior->updatedByAttribute => Yii::$app->user->getId(),
            $timestampBehavior->updatedAtAttribute => time()
        ];

        $callback = $this->onRecycleCallback;
        if (is_callable($callback)) {
            $updateAttributes = $callback($updateAttributes);
        }

        $owner->updateAttributes($updateAttributes);

        $this->_isRecycle = true;

        $event->isValid = false;
        $this->saveRecycleBinEsIndexModel();

        $this->owner->trigger(self::RECYCLED_EVENT);
        $this->owner->afterDelete();

        return false;
    }

    public function afterDelete()
    {
        if (!$this->isPermanentDelete()) {
            return;
        }

        $callback = $this->onAfterDeleteCallback;
        if (is_callable($callback)) {
            $callback();
        }
    }

    public function saveRecycleBinEsIndexModel()
    {
        if (empty($this->esModelClass)) {
            return;
        }

        $this->saveOneRecycleBinModel($this->owner, [
            'class' => $this->esModelClass,
            'setAttributesFromDbModel' => $this->setAttributesFromDbModel,
            'runValidationOnSave' => $this->runValidationOnSave,
            'saveAttributeNames' => $this->saveAttributeNames,
            'onSaveCallback' => $this->onSaveCallback
        ]);
    }

    public function deleteRecycledModel()
    {
        $recycledModel = $this->getRecycledModel();

        if (!empty($recycledModel)) {
            $recycledModel->delete();
        }
    }

    public function getRecycledModel()
    {
        if (!empty($this->_recycledModel)) {
            return $this->_recycledModel;
        }

        /** @var ActiveRecord $owner */
        $owner = $this->owner;

        if (empty($this->esModelClass)) {
            return null;
        }

        /** @var EsActiveRecord $esModelClass */
        $esModelClass = $this->esModelClass;

        $this->_recycledModel = $esModelClass::find()->where(['item' => $owner->tableName() . $owner->id])->one();

        return $this->_recycledModel;
    }

    public function restoreFromRecycleBin()
    {
        /** @var ActiveRecord|HasEsIndexBehavior $owner */
        $owner = $this->owner;

        if (!$owner->hasAttribute($this->recycledAttribute)) {
            return;
        }

        $owner->updateAttributes([$this->recycledAttribute => 0]);

        if ($owner->hasMethod('saveEsIndexModel')) {
            $owner->saveEsIndexModel();
        }

        $this->deleteRecycledModel();

        $callback = $this->onRestoreCallback;

        if (is_callable($callback)) {
            $callback();
        }

        $owner->trigger(self::RESTORED_EVENT);
    }


    protected function saveOneRecycleBinModel(ActiveRecord $dbModel, $config)
    {
        /** @var EsActiveRecord $esModelClass */
        $esModelClass = $config['class'];


        /** @var BulkElasticSearch $es */
        $es = $esModelClass::getDb();

        /** @var EsActiveRecord $model */
        $model = !$es->isBulkIndexing() ? $esModelClass::find()->where(['item' => $this->owner->tableName() . $this->owner->id])->one() : null;

        if (empty($model)) {
            $model = new $esModelClass();
        }

        $this->setRecycleBinModel($dbModel, $model, $config);

        $model->save(
            ArrayHelper::getValue($config, 'runValidationOnSave', true),
            ArrayHelper::getValue($config, 'saveAttributeNames', null)
        );
    }

    protected function setRecycleBinModel(ActiveRecord $dbModel, EsActiveRecord $model, $config)
    {
        if (ArrayHelper::getValue($config, 'setAttributesFromDbModel', true)) {
            $model->setAttributesFromDbModel($dbModel, true);
        }

        $onSaveCallback = ArrayHelper::getValue($config, 'onSaveCallback', null);

        if (!empty($onSaveCallback) && is_callable($onSaveCallback)) {
            $onSaveCallback($model);
        }
    }

    protected function isPermanentDelete()
    {
        if (!$this->owner->hasAttribute($this->recycledAttribute)) {
            return true;
        }

        return ($this->owner->{$this->recycledAttribute} == 1 && !$this->_isRecycle) || $this->_skipRecycleCheck;
    }
}