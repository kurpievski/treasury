<?php

namespace common\behaviors;

use common\helpers\AttributeHelper;
use common\helpers\DocumentHelper;
use common\helpers\DocumentTypeHelper;
use common\models\Attribute;
use common\models\BatchAttribute;
use common\models\DocumentAttribute;
use common\models\DocumentType;
use common\models\DocumentTypeAttribute;
use Yii;
use yii\base\Behavior;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class HasAttributesBehavior extends Behavior
{
    public $typeAttribute = 'document_type_id';

    public $newAttributesProperty;
    public $checkUniquenessAttribute = null;

    /** @var string */
    public $className;
    public $attributeClassName = 'common\models\DocumentAttribute';
    public $attributeClassConfig = [];
    public $attributeOwnerIdColumn = 'document_id';
    public $ownerAttributeRelation = 'getDocumentAttributes';
    public $checkIsMandatory = true;
    public $checkIsReadOnly = true;
    public $checkUniqueness = true;

    protected $assignableAttributes;
    protected $existingAttributes;
    protected $oldAttributeValues = [];
    protected $documentType;

    protected $errors;

    public function init()
    {
        if (empty($this->typeAttribute) || empty($this->className) || empty($this->newAttributesProperty)) {
            throw new Exception("modelClass and typeAttribute must be set.");
        }

        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'setOldAttributeValues',
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'validateTypeAttributes',
            ActiveRecord::EVENT_AFTER_INSERT => 'resetCachedValues',
            ActiveRecord::EVENT_AFTER_UPDATE => 'resetCachedValues',
        ];
    }

    public function resetCachedValues()
    {
        $this->assignableAttributes = null;
        $this->existingAttributes = null;
        $this->documentType = null;
    }

    public function setOldAttributeValues()
    {
        $existingAttributes = $this->owner->{$this->ownerAttributeRelation}()->all();
        /* @var DocumentAttribute $existingAttribute */
        foreach ($existingAttributes as $existingAttribute) {
            $this->oldAttributeValues[$existingAttribute->attribute_id] = $existingAttribute->value;
        }
    }

    public function validateTypeAttributes()
    {
        $type = $this->getDocumentType();

        if (empty($type)) {
            return false;
        }

        $this->validateValuesOfNewlyAssignedAttributes();

        if ($this->checkIsMandatory) {
            $this->validateMandatoryAttributes();
        }

        if ($this->checkIsReadOnly) {
            $this->validateReadOnlyAttributes();
        }

        if (empty($this->errors) && $this->checkUniqueness && $this->isPaymentDocumentType()) {
            $this->validateUniqueness();
        }

        if (!empty($this->errors)) {
            $this->owner->addErrors($this->errors);
        }
    }

    public function tryUpdatingAttributes()
    {
        $this->createNewAttributes();
        $this->updateExistingAttributes();
    }

    public function hasDocumentAttributeChanged($attributeId)
    {
        return $this->getAttributeOldValue($attributeId) != $this->getAttributeNewValue($attributeId);
    }

    public function getAttributeOldValue($attributeId)
    {
        return empty($this->oldAttributeValues[$attributeId]) ? null : $this->oldAttributeValues[$attributeId];
    }

    public function getAttributeNewValue($attributeId)
    {
        /** @var DocumentAttribute $currentAttribute */
        foreach ($this->getAssignedAttributes() as $currentAttribute) {
            if ($currentAttribute->attribute_id == $attributeId) {
                return $currentAttribute->value;
            }
        }
        return null;
    }


    protected function createNewAttributes()
    {
        $newAttributes = $this->owner->{$this->newAttributesProperty};

        /** @var DocumentTypeAttribute[] $defaultValueAttributes */
        $defaultValueAttributes = [];

        /** @var DocumentTypeAttribute $assignableAttribute */
        foreach ($this->getAssignableAttributes() as $assignableAttribute) {
            if (!isset($newAttributes[$assignableAttribute->attribute_id]) && $assignableAttribute->hasDefaultValue()) {
                $defaultValueAttributes[] = $assignableAttribute;
            }
        }

        $previousAttributes = $this->getPreviouslyAssignedAttributes();

        foreach ($newAttributes as $newAttributeId => $value) {
            foreach ($previousAttributes as $existingAttribute) {
                if ($existingAttribute->attribute_id == $newAttributeId) {
                    continue 2;
                }
            }

            $attr = $this->createAttribute($newAttributeId);
            $attr->setValue($value);
            $this->saveAttribute($attr);
        }

        foreach ($defaultValueAttributes as $defaultValueAttribute) {
            foreach ($previousAttributes as $existingAttribute) {
                if ($existingAttribute->attribute_id == $defaultValueAttribute->attribute_id) {
                    continue 2;
                }
            }

            $attr = $this->createAttribute($defaultValueAttribute->attribute_id);
            $attr->setDefaultValue();
            $this->saveAttribute($attr);
        }
    }

    protected function createAttribute($attributeId, $customOwnerId = null)
    {
        /** @var DocumentAttribute|BatchAttribute $attr */
        $attr = new $this->attributeClassName($this->attributeClassConfig);

        $attr->setAttributes([
            $this->attributeOwnerIdColumn => $customOwnerId !== null ? $customOwnerId : $this->owner->id,
            'attribute_id' => $attributeId,
        ]);

        return $attr;
    }

    protected function saveAttribute($attribute)
    {
        /** @var ActiveRecord $attribute */
        if (!$attribute->save()) {
            throw new Exception('New Attribute could not be created' . print_r($attribute->getAttributes(), true));
        }
    }

    protected function updateExistingAttributes()
    {
        $newAttributes = $this->owner->{$this->newAttributesProperty};

        foreach ($newAttributes as $newAttributeId => $value) {
            $existingAttributes = $this->getPreviouslyAssignedAttributes();
            foreach ($existingAttributes as $existingAttribute) {
                if ($existingAttribute->attribute_id == $newAttributeId) {

                    if ($existingAttribute->getValue() == $value) {
                        continue 2;
                    }

                    $existingAttribute->setValue($value);
                    $this->saveAttribute($existingAttribute);
                }
            }
        }
    }

    protected function validateValuesOfNewlyAssignedAttributes()
    {
        $assignedAttributes = $this->getAssignedAttributes();

        foreach ($assignedAttributes as $assignedAttribute) {

            if (!$this->isAssignable($assignedAttribute->attribute_id)) {
                throw new UnassignableAttributeException(print_r($assignedAttribute->getAttributes(), true));
            }

            $assignedAttribute->documentType = $this->getDocumentType();

            if (!$assignedAttribute->validate()) {
                $this->errors = ArrayHelper::merge($this->errors, $assignedAttribute->getErrors());
            }
        }
    }

    protected function validateMandatoryAttributes()
    {
        /** @var DocumentTypeAttribute $assignableAttribute */
        foreach ($this->getAssignableAttributes() as $assignableAttribute) {

            if (!$assignableAttribute->isMandatory() || $assignableAttribute->hasDefaultValue()) {
                continue;
            }

            if (!$this->isAttributeAssigned($assignableAttribute->attribute_id)) {
                $this->errors = ArrayHelper::merge($this->errors, [
                    "{$this->newAttributesProperty}[{$assignableAttribute->attribute_id}]" =>
                        $assignableAttribute->getAttributeName() . ' is mandatory'
                ]);
            }
        }
    }

    protected function validateReadOnlyAttributes()
    {
        $newAttributes = $this->owner->{$this->newAttributesProperty};

        /** @var DocumentTypeAttribute $assignableAttribute */
        foreach ($this->getAssignableAttributes() as $assignableAttribute) {
            if (isset($newAttributes[$assignableAttribute->attribute_id]) && $assignableAttribute->isReadOnly()) {
                $this->errors = ArrayHelper::merge($this->errors, [
                    "{$this->newAttributesProperty}[{$assignableAttribute->attribute_id}]" =>
                        $assignableAttribute->getAttributeName() . ' is read only.'
                ]);
            }
        }
    }

    protected function isPaymentDocumentType()
    {
        $documentType = $this->getDocumentType();

        if (empty($documentType)) {
            return false;
        }

        return DocumentTypeHelper::isPaymentDocumentType($documentType->id);
    }

    protected function validateUniqueness()
    {
        if (!empty($this->checkUniquenessAttribute) && $this->owner->{$this->checkUniquenessAttribute} == 0) {
            return;
        }

        $attributes = ArrayHelper::map($this->getAssignedAttributes(), 'attribute_id', 'value');

        $duplicateIds = DocumentHelper::getDuplicateDocumentIds($attributes, $this->owner->id, DocumentTypeHelper::getPaymentTypeId());

        if (!empty($duplicateIds)) {
            $this->errors = ArrayHelper::merge($this->errors, [
                'duplicateIds' => implode(',', $duplicateIds),
                "{$this->newAttributesProperty}[duplicateIds]" => "Duplicate document of: " . implode(',', $duplicateIds)
            ]);
        }
    }

    protected function isAttributeAssigned($attributeId)
    {
        foreach ($this->getAssignedAttributes() as $assignedAttribute) {
            if ($assignedAttribute->attribute_id == $attributeId) {
                return true;
            }
        }

        return false;
    }

    protected function isAssignable($attributeId)
    {
        foreach ($this->getAssignableAttributes() as $assignableAttribute) {
            if ($assignableAttribute->attribute_id == $attributeId) {
                return true;
            }
        }

        return false;
    }

    protected function getAssignableAttributes()
    {
        if (empty($this->assignableAttributes)) {
            $type = $this->getDocumentType();

            if (empty($type)) {
                return [];
            }

            $this->assignableAttributes = $type->getDocumentTypeAttributes()->with('attributeModel')->all();
        }

        return $this->assignableAttributes ? $this->assignableAttributes : [];
    }

    protected function getDocumentType()
    {
        if (empty($this->documentType)) {
            $typeId = $this->owner->{$this->typeAttribute};
            if (empty($typeId)) {
                return null;
            }
            $this->documentType = DocumentType::findOne(['id' => $typeId]);
        }

        return $this->documentType;
    }

    protected function getAssignedAttributes()
    {
        $newAttributesData = $this->owner->{$this->newAttributesProperty};

        $newDocumentAttributes = null;

        if (!empty($newAttributesData)) {
            $newDocumentAttributes = $this->constructNewAttributes($newAttributesData);
        }

        $previousAttributes = $this->getPreviouslyAssignedAttributes();

        if (!empty($newDocumentAttributes)) {
            foreach ($previousAttributes as $attributeId => $attribute) {
                if (!isset($newDocumentAttributes[$attributeId])) {
                    $newDocumentAttributes[$attributeId] = $attribute;
                }
            }

            return $newDocumentAttributes;
        }

        return $previousAttributes;
    }

    protected function constructNewAttributes($newAttributesData)
    {
        $documentAttributes = [];

        foreach ($newAttributesData as $attributeId => $value) {
            $documentAttributes[$attributeId] = $this->createNewDocumentAttribute($attributeId, $value);
        }

        return $documentAttributes;
    }

    protected function createNewDocumentAttribute($attributeId, $value)
    {
        $attribute = Attribute::findOne(['id' => $attributeId]);

        if (empty($attribute)) {
            throw new NonExistingAttributeException(print_r($attributeId, true));
        }

        //we need this model only to validate type and attribute value - hack below serve that purpose
        $attr = $this->createAttribute($attributeId, 0);

        //inject document type so validations dependant on it can work
        $attr->documentType = $this->getDocumentType();
        $attr->setValue($value);

        return $attr;
    }

    protected function getPreviouslyAssignedAttributes()
    {
        if (empty($this->existingAttributes)) {
            $this->existingAttributes = ArrayHelper::index(
                $this->owner->{$this->ownerAttributeRelation}()->all(), 'attribute_id'
            );

            foreach ($this->existingAttributes as $attributeId => $attribute) {
                if (!$this->isAssignable($attributeId)) {
                    unset($this->existingAttributes[$attributeId]);
                }
            }
        }

        return $this->existingAttributes ? $this->existingAttributes : [];
    }
}

class UnassignableAttributeException extends Exception
{

}

class NonExistingAttributeException extends Exception
{

}

class MissingPaymentDocumentTypeException extends Exception
{

}