<?php
/**
 * LoggableBehavior.php
 *
 * Author: Aleksandar Panic <aleksandar.panic@2amigos.us>
 * Timestamp: 17-Nov-16 07:35
 */

namespace common\behaviors;


use yii\db\BaseActiveRecord;

class LoggableBehavior extends \sammaye\audittrail\LoggableBehavior
{
    const AUDIT_RECYCLED = 'RECYCLED';
    const AUDIT_RESTORED = 'RESTORED';
    const AUDIT_DELETE = 'DELETE';
    const AUDIT_CREATE = 'CREATE';
    const AUDIT_SET = 'SET';
    const AUDIT_CHANGE = 'CHANGE';

    protected $_wasRecycled = false;

    public function events()
    {
        return [
            BaseActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            BaseActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            BaseActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            RecycleBinBehavior::RECYCLED_EVENT => 'afterRecycle',
            RecycleBinBehavior::RESTORED_EVENT => 'afterRestore',
            BaseActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }


    public function afterRestore()
    {
        $this->leaveTrail(self::AUDIT_RESTORED);
    }

    public function afterRecycle()
    {
        $this->leaveTrail(self::AUDIT_RECYCLED);

        $this->_wasRecycled = true;
    }

    public function afterDelete($event)
    {
        if (!$this->_wasRecycled) {
            $this->leaveTrail(self::AUDIT_DELETE);
        }

        $this->_wasRecycled = false;
    }
}