<?php
/**
 * FileRelationBehavior.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 04-Feb-16
 * Time: 21:50
 */

namespace common\behaviors;


use common\extensions\ActiveRecord;
use common\extensions\ActiveRecordQuery;
use common\helpers\TransactionHelper;
use yii\base\Behavior;
use yii\base\Exception;

class RelationBehavior extends Behavior
{
    /** @var ActiveRecord */
    public $owner;

    /** @var array Relation config
     *
     *  [
     *      'relationName' => 'attribute'
     *  ]
     *
     */
    public $relationConfig = [];

    public function addRelations($relationName, $idList)
    {
        if (!is_array($idList) || empty($idList)) {
            return false;
        }

        /** @var ActiveRecordQuery $relation */
        $relation = $this->owner->getRelation($relationName);

        $relationKey = $this->relationConfig[$relationName];

        /** @var ActiveRecord[] $existingModels */
        $existingModels = $relation->where([$relationKey => $idList])->includeDeleted()->all();

        $existingIds = [];

        foreach ($existingModels as $model) {
            if ($model->hasSoftDelete()) {
                $model->is_deleted = 0;
            }

            $existingIds[] = $model->{$relationKey};
        }

        $newIdsList = array_merge(array_diff($idList, $existingIds), array_diff($existingIds, $idList));

        /** @var ActiveRecord[] $models */
        $models = array_merge($existingModels, $this->getNewModels($relationName, $relation, $newIdsList));

        return $this->saveModels($models);
    }

    protected function getNewModels($relationName, $relation, $idList)
    {
        $modelClass = $relation->modelClass;

        $modelKey = key($relation->link);

        $models = [];

        foreach ($idList as $id) {
            $models[] = new $modelClass([
                $modelKey => $this->owner->primaryKey,
                $this->relationConfig[$relationName] => $id
            ]);
        }

        return $models;
    }

    protected function saveModels($models)
    {
        $transaction = $this->owner->getDb()->beginTransaction();

        try {
            /** @var ActiveRecord[] $models */
            foreach ($models as $model) {
                if (!$model->save()) {
                    throw new Exception("Unable to save model." . implode('<br>', $model->getFirstErrors()));
                }
            }

            TransactionHelper::commitAll($transaction);

            return true;
        } catch (\Exception $e) {
            TransactionHelper::rollbackAll($transaction);
            throw $e;
        }
    }
}