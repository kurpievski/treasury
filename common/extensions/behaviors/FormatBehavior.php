<?php

namespace common\extensions\behaviors;

use Yii;
use yii\base\Behavior;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;

/**
 * Class FormatBehavior
 *
 * Allow use the format extension from Yii 2 inside of AR attributes.
 *
 * Example:
 * ~~~php
 * public function behaviors() {
 *     return [
 *         "formatDate" => [
 *             "class" => FormatBehavior::class,
 *                 "attributes" => [
 *                     'attribute1',
 *                     'attribute2' => [
 *                        'viewFormat' => [],
 *                        'saveFormat' => []
 *                     ] // This view format is used instead of global.
 *                 ],
 *             "viewFormat" => [
 *                 'date',
 *                 'php:m/d/Y',
 *                 'customFormatter' => function($value, $saveFormatConfig) {
 *                      return $value; // Custom formatting.
 *                 }
 *             ],
 *             "saveFormat" => [
 *                 'date',
 *                 'php:Y-m-d',
 *                 'customFormatter' => function($value, $saveFormatConfig) {
 *                      return $value; // Custom formatting.
 *                 }
 *             ],
 *         ],
 *     ];
 * }
 * ~~~
 *
 * The idea was taken from:
 * http://www.yiiframework.com/forum/index.php/topic/63776-need-help-with-kartik-timepicker
 *
 * @author Eric Maicon <eric@ericmaicon.com.br>, Aleksandar Panic <aleksandar.pnc@gmail.com>
 * @package common\behaviors
 */
class FormatBehavior extends Behavior
{
    /** @var ActiveRecord */
    public $owner;

    public $attributes;
    public $viewFormat;
    public $saveFormat;

    public $formatterComponent = 'formatter';

    protected $_attributeFormatStates = [];

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'swapToSaveFormat',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'swapToSaveFormat',
            ActiveRecord::EVENT_AFTER_FIND => 'swapToViewFormat',
            BaseActiveRecord::EVENT_AFTER_INSERT => 'swapToViewFormat',
            BaseActiveRecord::EVENT_AFTER_UPDATE => 'swapToViewFormat',
        ];
    }

    /**
     * Method to change the attribute to the save format
     * Better to do it before validation, so validation don't fail
     *
     */
    public function swapToSaveFormat()
    {
        $dirtyAttributes = [];

        foreach ($this->attributes as $key => $attribute) {
            $ownerAttribute = is_string($key) ? $key : $attribute;

            if ($this->owner->hasAttribute($ownerAttribute) && $this->owner->isAttributeChanged($ownerAttribute)) {
                $dirtyAttributes[] = $ownerAttribute;
            }

            $this->owner->$ownerAttribute = $this->getFormattedSaveValue($ownerAttribute);

            if (!in_array($ownerAttribute, $dirtyAttributes) && $this->owner->hasAttribute($ownerAttribute)) {
                $this->owner->setOldAttribute($ownerAttribute, $this->owner->$ownerAttribute);
            }
        }
    }

    /**
     * This method is called at the end of inserting or updating a record.
     */
    public function swapToViewFormat()
    {
        $dirtyAttributes = [];

        foreach ($this->attributes as $key => $attribute) {
            $ownerAttribute = is_string($key) ? $key : $attribute;

            if ($this->owner->hasAttribute($ownerAttribute) && $this->owner->isAttributeChanged($ownerAttribute)) {
                $dirtyAttributes[] = $ownerAttribute;
            }

            $this->owner->$ownerAttribute = $this->getFormattedViewValue($attribute);

            if (!in_array($ownerAttribute, $dirtyAttributes) && $this->owner->hasAttribute($ownerAttribute)) {
                $this->owner->setOldAttribute($ownerAttribute, $this->owner->$ownerAttribute);
            }
        }
    }

    /**
     * Returns formatted view value for specified attribute.
     *
     * @param $attribute string Attribute from which save value will be retrieved.
     * @return mixed Formatted value
     */
    public function getFormattedViewValue($attribute)
    {
        return $this->getFormattedValue($attribute, 'viewFormat');
    }

    /**
     * Returns formatted save value for specified attribute.
     *
     * @param $attribute string Attribute from which save value will be retrieved.
     * @return mixed Formatted value
     */
    public function getFormattedSaveValue($attribute)
    {
        return $this->getFormattedValue($attribute, 'saveFormat');
    }

    /**
     * Formats and returns value using formatter component.
     *
     * @param $fromValue mixed Value which will be formatted
     * @param $formatConfig array Array using formatter configuration
     * @return mixed Formatted value
     * @throws \yii\base\InvalidConfigException
     */
    protected function getFormattedValue($attribute, $format)
    {
        if ($this->isAttributeInFormatState($attribute, $format)) {
            return $this->owner->$attribute;
        }

        $formatConfig = $this->getAttributeFormat($attribute, $format);

        $currentValue = $this->owner->$attribute;

        if (empty($formatConfig['formatEmpty']) && empty($currentValue)) {
            return $currentValue;
        }

        $this->setAttributeFormatState($attribute, $format);

        if (empty($formatConfig['customFormatter'])) {
            return Yii::$app->get($this->formatterComponent)->format($currentValue, $formatConfig);
        }

        $formatFunction = $formatConfig['customFormatter'];

        return $formatFunction($currentValue, $formatConfig, $attribute);
    }


    protected function getAttributeFormat($attribute, $format)
    {
        return !empty($this->attributes[$attribute][$format]) ? $this->attributes[$attribute][$format] : $this->$format;
    }

    protected function setAttributeFormatState($attribute, $format)
    {
        $this->_attributeFormatStates[$attribute] = $format;
    }

    protected function isAttributeInFormatState($attribute, $format)
    {
        return !empty($this->_attributeFormatStates[$attribute]) && $this->_attributeFormatStates[$attribute] == $format;
    }
}