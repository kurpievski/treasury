<?php
/**
 * ActiveRecord.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 26-Nov-15
 * Time: 18:58
 */

namespace common\extensions;


use common\models\Document;
use Yii;
use yii\base\Exception;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;


class ActiveRecord extends \yii\db\ActiveRecord
{
    public $stripFields = [];

    public function fields()
    {
        $fields = parent::fields();

        if (!empty($this->stripFields)) {
            foreach ($this->stripFields as $key) {
                unset($fields[$key]);
            }
        }

        return $fields;
    }

    public function behaviors()
    {
        return ArrayHelper::merge([
            'parentBlameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'create_id',
                'updatedByAttribute' => 'update_id'
            ],
            'parentTimestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time'
            ]
        ], $this->useWithParentBehaviors());
    }

    protected function useWithParentBehaviors()
    {
        return [];
    }

    public static function hasSoftDelete()
    {
        $columnNames = static::getTableSchema()->getColumnNames();
        return in_array('is_deleted', $columnNames);
    }

    /**
     * @return ActiveRecordQuery
     */
    public static function find()
    {
        /** @var ActiveRecordQuery $query */
        $query = Yii::createObject(ActiveRecordQuery::className(), [get_called_class()]);
        return $query->sortByOrder()->excludeDeleted();
    }

    protected function deleteInternal()
    {
        if (!$this->hasAttribute('is_deleted')) {
            return parent::deleteInternal();
        }

        if (!$this->beforeDelete()) {
            return false;
        }

        $result = $this->updateAttributes(['is_deleted' => 1]);

        $this->setOldAttributes(null);
        $this->afterDelete();

        return $result;
    }

    public function getETag()
    {
        return md5(get_class($this) . $this->getPrimaryKey() . $this->getLastModified());
    }

    public function getLastModified()
    {
        if ($this->hasAttribute('update_time') && $this->getAttribute('update_time')) {
            return $this->getAttribute('update_time');
        }

        if ($this->hasAttribute('create_time') && $this->getAttribute('create_time')) {
            return $this->getAttribute('create_time');
        }

        return 0; //if no create or update time present, we consider resource last update time was 1970-01-01
    }
}