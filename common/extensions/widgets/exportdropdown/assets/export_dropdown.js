/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 **/

"use strict";

!function($) {

    $.fn.exportDropdown = function(options) {

        var self = this;

        options = $.extend({
            params: {},
            method: 'POST',
            action: '',
            exportParam: 'exportAs'
        }, options);

        self.find('[data-type]').click(function() {
            var type = $(this).data('type');
            submitExport(type);
        });


        function submitExport(type) {
            var form = document.createElement('form');
            form.method = options.method || "POST";
            form.action = options.action;

            var params = $.extend({}, options.params, {type: type});

            form.appendChild(createInput(yii.getCsrfParam(), yii.getCsrfToken()));
            form.appendChild(createInput(options.exportParam, JSON.stringify(params)));

            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        }

        function createInput(name, value) {
            var input = document.createElement('input');

            input.type = "hidden";
            input.value = value;
            input.name = name;

            return input;
        }

    };

}(jQuery);