<?php
/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 * @var $this \yii\web\View
 **/

use yii\helpers\Html;

/** @var \common\widgets\exportdropdown\ExportDropdown $context */
$context = $this->context;
?>

<div id="<?= $context->getId(); ?>">
    <div class="dropdown right-grid-button">
        <a class="dropdown-toggle btn btn-xs btn-primary" href="#" data-toggle="dropdown" aria-expanded="false">
            <?= $context->buttonText ?> <b class="caret"></b>
        </a>
        <?= Html::beginTag('ul', $context->dropdownOptions); ?>
            <?php foreach ($context->exportTypes as $type => $label): ?>
                <li><a href="javascript:void(0)" data-type="<?= $type; ?>"><?= $label; ?></a></li>
            <?php endforeach; ?>
        <?= Html::endTag('ul'); ?>
    </div>
</div>
