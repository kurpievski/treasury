<?php
/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 **/

namespace common\widgets\exportdropdown;


use yii\web\AssetBundle;

class ExportDropdownAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $js = [
        'export_dropdown.js'
    ];
}