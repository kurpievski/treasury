<?php
/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 **/

namespace common\widgets\exportdropdown;


use common\components\export\GridConfig;
use common\helpers\ArrayHelper;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;

class ExportDropdown extends Widget
{
    const PARAM_FILTER = 'filter';
    const PARAM_TYPE = 'type';
    const PARAM_SORT = 'sort';

    public $exportTypes = [];

    public $passParams = [];

    public $exportParam = 'exportAs';

    /** @var GridConfig */
    public $gridConfig;

    public $action = '';

    public $method = '';

    public $clientOptions = [];

    public $buttonText = 'Export';

    public $dropdownOptions = [];

    protected $_params = [];

    public function init()
    {
        $this->_params = [
            self::PARAM_TYPE => '',
            self::PARAM_FILTER => $this->gridConfig->model->getExportParams(),
            self::PARAM_SORT => $this->gridConfig->getDataProvider()->getSort()->getAttributeOrders()
        ];

        if(!isset($this->dropdownOptions['class'])) {
            Html::addCssClass($this->dropdownOptions, 'dropdown-menu');
        }

        parent::init();
    }

    public function run()
    {
        ExportDropdownAsset::register($this->getView());

        $id = Json::encode('#' . $this->getId());

        $options = [
            'params' => ArrayHelper::merge($this->_params, $this->passParams),
            'exportParam' => $this->exportParam,
            'action' => $this->action,
            'method' => $this->method
        ];

        $options = Json::encode(ArrayHelper::merge($options, $this->clientOptions));

        $this->getView()->registerJs("!function() {
            $({$id}).exportDropdown({$options});
        }();");

        return $this->render('index');
    }
}