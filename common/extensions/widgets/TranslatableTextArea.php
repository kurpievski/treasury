<?php
/**
 * Created by PhpStorm.
 * User: Goran Sarenac <goransarenac91@hotmail.com>
 * Date: 8/19/2016
 * Time: 6:53 PM
 */

namespace common\widgets;


use common\helpers\LanguageHelper;

class TranslatableTextArea extends \goransaki\translatabletextarea\TranslatableTextArea
{
    public $options = ['rows' => 7];

    public function init()
    {
        $this->languages = LanguageHelper::getLanguageNames($includeSource = true);
        $this->showTabs = \Yii::$app->params['isTranslationDisabled'] ? false : true;
        parent::init();
    }

    public function getViewPath()
    {
        $class = new \ReflectionClass($this);

        return dirname($class->getParentClass()->getFileName()) . DIRECTORY_SEPARATOR . 'views';
    }
}
