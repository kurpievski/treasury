<?php
/**
 * Created by PhpStorm.
 * User: Nikola Radovic <nikola@2amigos.us>
 * Date: 7/7/2016
 * Time: 4:49 PM
 */

namespace common\widgets;

use kartik\base\WidgetAsset;
use Yii;
use yii\web\View;

class DateRangePicker extends \kartik\daterange\DateRangePicker
{
    /**
     * Initializes the widget
     *
     * @throw InvalidConfigException
     */
    public function init()
    {
        $this->pluginOptions = array_merge($this->getDefaultPluginOptions(), $this->pluginOptions);
        $this->options = array_merge($this->getDefaultOptions(), $this->options);

        parent::init();
    }

    /**
     * Initializes the pluginOptions range list
     */
    protected function initRange()
    {
        if (empty($this->pluginOptions['ranges'])) {
            $this->pluginOptions['ranges'] = $this->getDefaultRanges();
        }
        parent::initRange();
    }

    protected function getDefaultOptions()
    {
        return [
            'readonly' => true
        ];
    }

    protected function getDefaultRanges()
    {
        return [
            Yii::t('kvdrp', "Today") => ["moment().startOf('day')", "moment()"],
            Yii::t('kvdrp', "This Week") => ["moment().startOf('week')", "moment().endOf('week')"],
            Yii::t('kvdrp', "This Month") => ["moment().startOf('month')", "moment().endOf('month')"],
            Yii::t('kvdrp', "This Year") => ["moment().startOf('year')", "moment().endOf('year')"]
        ];
    }

    protected function getDefaultPluginOptions()
    {
        return [
            'convertFormat' => true,
            'alwaysShowCalendars' => true,
            'linkedCalendars' => false,
            'locale' => [
                'cancelLabel' => 'Clear',
                'format' => 'MM/DD/YYYY',
                'separator' => ' - ',
            ],
        ];
    }

    protected function getInput($type, $list = false)
    {
        $parent = parent::getInput($type, $list);

        return '<div class="form-group has-feedback readonly-datepicker">
                ' . $parent . '
                <i class="fa fa-calendar form-control-feedback"></i>
                </div>';
    }

    public function registerWidgetJs($js, $pos = View::POS_READY, $key = null)
    {
        if (empty($js)) {
            return;
        }
        $view = $this->getView();
        WidgetAsset::register($view);
        $view->registerJs($js, $pos, $key);
        if (!empty($this->pjaxContainerId) && ($pos === View::POS_LOAD || $pos === View::POS_READY)) {
            $pjax = 'jQuery("#' . $this->pjaxContainerId . '")';
            $evComplete = 'pjax:success.' . hash('crc32', $js);
            $script = "setTimeout(function(){ {$js} }, 100);";
            $view->registerJs("{$pjax}.off('{$evComplete}').on('{$evComplete}',function(){ {$script} });");
            // hack fix for browser back and forward buttons
            if ($this->enablePopStateFix) {
                $view->registerJs("window.addEventListener('popstate',function(){window.location.reload();});");
            }
        }
    }
}