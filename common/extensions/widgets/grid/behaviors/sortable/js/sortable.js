(function ($) {
    var fixHelper = function (e, ui) {
        ui.children().each(function () {
            $(this).width($(this).width());
        });
        return ui;
    };

    $.fn.SortableGridBehavior = function (action, options) {
        var widget = this;
        var grid = $('tbody', this);

        options = $.extend({
            confirm: false,
            confirmMessage: ''
        }, options);

        var initialIndex = [];
        $('tr', grid).each(function () {
            initialIndex.push($(this).data('key'));
        });

        var oldList = grid.find('tr');

        grid.sortable({
            items: 'tr',
            axis: 'y',
            start: function() {
                oldList = $(this).find('tr');
            },
            beforeStop: function() {
                var changed = false;
                grid.find('tr').each(function(i1, e1) {
                    if (changed) {
                        return;
                    }

                    oldList.each(function (i2, e2) {
                        if (e1 === e2 && i1 !== i2) {
                            changed = true;
                            return;
                        }
                    });
                });

                if (!changed) {
                    return;
                }

                if (!options.confirm) {
                    handleChange();
                    return;
                }

                yii.confirm(options.confirmMessage, handleChange, function() {
                    var detachedItems = [];
                    grid.find('tr').each(function(i, e) {
                        detachedItems.push($(e).detach());
                    });

                    oldList.each(function(idx, e) {
                        for(var i = 0; i < detachedItems.length; i++) {
                            if (detachedItems[i][0] === e) {
                                grid[0].appendChild(e);
                            }
                        }
                    });
                });
            },
            helper: fixHelper
        }).disableSelection();

        function handleChange() {
            var items = {};
            var i = 0;

            $('tr', grid).each(function () {
                var currentKey = $(this).data('key');
                if (initialIndex[i] != currentKey) {
                    items[currentKey] = initialIndex[i];
                    initialIndex[i] = currentKey;
                }
                ++i;
            });

            $.ajax({
                'url': action,
                'type': 'post',
                'data': {'items': JSON.stringify(items)},
                'success': function () {
                    widget.trigger('sortableSuccess');
                },
                'error': function (request, status, error) {
                    alert(status + ' ' + error);
                }
            });
        }
    };
})(jQuery);
