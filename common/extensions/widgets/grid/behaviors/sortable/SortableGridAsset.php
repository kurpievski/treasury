<?php
/**
 * Stanislav Uloznik <hola@2amigos.us>
 * Company: 2amigOS! <https://2amigos.us>.
 */

namespace common\widgets\grid\behaviors\sortable;


use yii\web\AssetBundle;

class SortableGridAsset extends AssetBundle
{
    public $sourcePath = __DIR__;

    public $js = ['js/sortable.js'];
}