<?php
/**
 * Stanislav Uloznik <hola@2amigos.us>
 * Company: 2amigOS! <https://2amigos.us>.
 */

namespace common\widgets\grid\behaviors\sortable;

use dvamigos\Yii2\GridView\GridView;
use yii\base\Behavior;
use yii\helpers\Json;

class SortableGridBehavior extends Behavior
{
    /** @var GridView */
    public $owner;

    public $sortAction = 'sort';
    public $options = [];

    public function events()
    {
        return [
            GridView::EVENT_AFTER_RUN => 'initializeSortable'
        ];
    }

    public function initializeSortable()
    {
        $view = $this->owner->getView();
        SortableGridAsset::register($view);

        $gridId = $this->owner->getId();
        $sortAction = Json::encode($this->sortAction);
        $sortOptions = Json::encode($this->options);

        $view->registerJs("!function() {
            $({$gridId}).SortableGridBehavior({$sortAction}, {$sortOptions});
        }()");

    }
}