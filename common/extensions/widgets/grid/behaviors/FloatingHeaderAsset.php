<?php
/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 **/

namespace common\widgets\grid\behaviors;


use yii\web\AssetBundle;

class FloatingHeaderAsset extends AssetBundle
{
    public $sourcePath = '@npm-asset/floatthead/dist';

    public $js = ['jquery.floatThead.min.js'];
}