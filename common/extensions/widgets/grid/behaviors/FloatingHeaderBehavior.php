<?php
/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 **/

namespace common\widgets\grid\behaviors;


use common\helpers\ArrayHelper;
use dvamigos\Yii2\GridView\GridView;
use yii\base\Behavior;
use yii\helpers\Json;
use yii\web\JsExpression;

class FloatingHeaderBehavior extends Behavior
{
    /** @var GridView */
    public $owner;
    public $clientOptions = ['top' => 115];

    public function events()
    {
        return [
            GridView::EVENT_AFTER_RENDER_TABLE_HEADER => 'initializeFloatingHeader'
        ];
    }


    public function initializeFloatingHeader()
    {
        FloatingHeaderAsset::register($this->owner->getView());
        $this->registerScript();
    }

    public function registerScript()
    {
        if (!isset($this->owner->options['id'])) {
            $this->owner->options['id'] = $this->owner->getId();
        }

        $id = Json::encode('#' . $this->owner->options['id']);


        $skipCss = '.kv-grid-group-row,.kv-group-header,.kv-group-footer'; // skip these CSS for IE
        $js = 'function($table){return $table.find("tbody tr:not(' . $skipCss . '):visible:first>*");}';
        $opts = [
            'floatTableClass' => 'kv-table-float',
            'floatContainerClass' => 'kv-thead-float floating-header',
            'getSizingRow' => new JsExpression($js),
        ];

        $options = Json::encode(ArrayHelper::merge($opts, $this->clientOptions));
        $script = "!function($) {
            $({$id}).find('table').floatThead({$options});
        }(jQuery);";

        $this->owner->getView()->registerJs($script);
    }
}