<?php
/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 **/

namespace common\widgets\grid\behaviors;


use common\helpers\ArrayHelper;
use dvamigos\Yii2\GridView\GridEvent;
use dvamigos\Yii2\GridView\GridView;
use yii\base\Behavior;
use yii\bootstrap\Html;

class PerPageBehavior extends Behavior
{
    /** @var GridView */
    public $owner;

    public $templateName = '{perPage}';

    public $options = [];

    public $addToFilterSelector = false;

    public function init()
    {
        parent::init();

        $this->options = ArrayHelper::merge([
            'defaultPageSize' => 10,
            'pageSizeParam' => 'per-page',
            'selectOptions' => [10, 20, 50, 100],
            'wrapperCssClass' => 'paging-blk rsl-blk',
            'selectCssClass' => 'rsl-dropdown',
            'labelCssClass' => 'rsl-txt',
            'label' => 'Rows per page'
        ], $this->options);
    }

    public function events()
    {
        return [
            GridView::EVENT_GET_CUSTOM_SECTIONS => 'addPerPageCustomSection',
            GridView::EVENT_AFTER_INIT => 'initFilterSelectorCheck'
        ];
    }

    public function initFilterSelectorCheck()
    {
        if (!$this->addToFilterSelector) {
            return;
        }

        $selectors = $this->owner->filterSelector;

        if (!empty($selectors)) {
            $selectors = [$selectors];
        } else {
            $selectors = [];
        }

        $selectors[] = '#' . $this->getPerPageId();

        $this->owner->filterSelector = implode(', ', $selectors);
    }

    public function addPerPageCustomSection(GridEvent $event)
    {
        $event->pushResult($this, $event->getLastResult() + [
                $this->templateName => [$this, 'renderPerPageWidget']
            ]);
    }

    public function renderPerPageWidget()
    {
        $select = $this->renderPerPageSelect();
        $label = Html::tag('span', $this->options['label'], ['class' => $this->options['labelCssClass']]);

        return Html::tag('div', "{$label}\n{$select}", ['class' => $this->options['wrapperCssClass']]);
    }

    protected function renderPerPageSelect()
    {
        $pagination = $this->owner->dataProvider->getPagination();

        if ($pagination === false) {
            return '';
        }

        $selections = ArrayHelper::isAssociative($this->options['selectOptions']) ?
            $this->options['selectOptions'] :
            array_combine($this->options['selectOptions'], $this->options['selectOptions']);


        return Html::dropDownList(
            "per-page",
            $pagination->pageSize,
            $selections,
            [
                'id' => $this->getPerPageId(),
                'class' => $this->options['selectCssClass']
            ]
        );
    }

    protected function getPerPageId(): string
    {
        return "{$this->owner->getId()}-per-page";
    }
}