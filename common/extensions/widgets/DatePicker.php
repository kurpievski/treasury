<?php

namespace common\widgets;

/**
 * Jul 31, 2015
 * 11:40:41 PM
 * @author Nikola Radovic <nikola@2amigos.us>
 */
class DatePicker extends \dosamigos\datepicker\DatePicker
{
    const DEFAULT_DATE_FORMAT = 'mm/dd/yyyy';
    const DEFAULT_AUTO_CLOSE = true;

    public $clientOptions = [
        'format' => self::DEFAULT_DATE_FORMAT,
        'autoclose' => self::DEFAULT_AUTO_CLOSE
    ];
}
