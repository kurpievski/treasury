<?php

namespace common\widgets;

use yii\widgets\MaskedInput;

class MaskedMoneyInput extends MaskedInput
{
    public function init()
    {
        $this->name = 'input';
        $this->clientOptions = [
            'alias' =>  'decimal',
            'groupSeparator' => ',',
            'autoGroup' => true,
            'rightAlign' => false,
            'placeholder' => '0',
            'digits' => 2,
            'digitsOptional' =>  false,
            'removeMaskOnSubmit' => true
        ];
        parent::init();
    }
}