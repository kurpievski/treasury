<?php
/**
 * Author: Srdjan Drakul
 * Date: 3/2/2016 4:58 PM
 * Email: srdjan_drakul@yahoo.com
 */

namespace common\widgets\agreement;

use Yii;
use yii\base\Widget;


class Agreement extends Widget
{
    const TYPE_TERMS_OF_USE = 'terms-of-use';
    const TYPE_PRIVACY_POLICY = 'privacy-policy';

    public $appKey;
    public $apiBasePath;
    public $userAgreementLog = 'common\models\UserAgreementLog';

    private $_termsOfUseId = null;
    private $_privacyPolicyId = null;

    public function init()
    {
        $this->setAgreementIds();

        if (empty($this->appKey)) {
            throw new \Exception('Invalid widget configuration.');
        }

        parent::init();
    }

    public function run()
    {
        if (Yii::$app->user->isGuest) {
            return '';
        }

        if (!$this->getShouldRenderPrivacyPolicy() && !$this->getShouldRenderTermsOfUse()) {
            return '';
        }

        $this->registerClientScript();

        return $this->render('index');
    }

    public function getShouldRenderTermsOfUse()
    {
        return !empty($this->_termsOfUseId) && !$this->isAgreementRead($this->_termsOfUseId);
    }

    public function getShouldRenderPrivacyPolicy()
    {
        return !empty($this->_privacyPolicyId) && !$this->isAgreementRead($this->_privacyPolicyId);
    }

    public function getPrivacyPolicyId()
    {
        return $this->_privacyPolicyId;
    }

    public function getTermsOfUseId()
    {
        return $this->_termsOfUseId;
    }

    private function setAgreementIds()
    {
        $agreementProvider = new AgreementProvider($this->apiBasePath, $this->appKey);
        $data = $agreementProvider->getNewestAgreementIds();
        $this->_privacyPolicyId = $data['privacy_policy_id'];
        $this->_termsOfUseId = $data['terms_of_use_id'];
    }

    public function registerClientScript()
    {
        $view = $this->getView();
        AgreementAsset::register($view);
    }

    private function isAgreementRead($id)
    {
        $agreementLog = $this->userAgreementLog;

        return $agreementLog::logExists($id);
    }
}