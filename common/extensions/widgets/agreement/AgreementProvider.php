<?php
/**
 * Created by PhpStorm.
 * User: Nikola Radovic <nikola@2amigos.us>
 * Date: 3/25/2016
 * Time: 2:54 AM
 */

namespace common\widgets\agreement;

use Yii;
use yii\helpers\Json;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use yii\helpers\Url;
use yii\web\HttpException;
use common\helpers\HttpHelper;

class AgreementProvider
{
    public $baseUrl;

    public $appKey;

    private $_idsUrl = 'agreements/newest';
    private $_termsUrl = 'agreements/terms-of-use';
    private $_privacyUrl = 'agreements/privacy-policy';

    public function __construct($baseUrl, $appKey)
    {
        $this->baseUrl = $baseUrl;
        $this->appKey = $appKey;
    }

    public function getNewestAgreementIds()
    {
        return $this->fetchData($this->_idsUrl, ['app_key' => $this->appKey]);
    }

    public function getNewestTerms()
    {
        return $this->fetchData($this->_termsUrl, ['app_key' => $this->appKey]);
    }

    public function getNewestPrivacy()
    {
        return $this->fetchData($this->_privacyUrl, ['app_key' => $this->appKey]);
    }

    public function fetchData($url, array $params = [])
    {
        try {
            $response = (new Client())->request('GET', $this->getApiUrl($url, $params));

            return Json::decode($response->getBody()->getContents(), true);

        } catch (ClientException $e) {
            Yii::error('Unable to fetch agreement data.');

            return null;
        }
    }

    private function getApiUrl($url, array $params = [])
    {
        if (isset($params['id'])) {
            $url = trim($url, '/') . '/' . trim($params['id'], '/');
            unset($params['id']);
        }

        $params = \GuzzleHttp\Psr7\build_query($params);

        return trim($this->baseUrl, '/') . '/' . trim($url, '/') . ($params ? '?' . $params : '');
    }

}