$(function() {
    $('.dismiss-agreement').on('click', function() {
        var markAsSeenUrl = $(this).data('href');
        $.get(markAsSeenUrl);
    });
});