<?php
/**
 * Author: Srdjan Drakul
 * Date: 3/2/2016 8:13 PM
 * Email: srdjan_drakul@yahoo.com
 */

namespace common\widgets\agreement;


use yii\web\AssetBundle;

class AgreementAsset extends AssetBundle
{
    public $sourcePath = __DIR__;

    public $css = [];

    public $js = [
        'js/agreement.js',
    ];

    public $depends = [];
}