<?php

use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Author: Srdjan Drakul
 * Date: 3/3/2016 1:28 PM
 * Email: srdjan_drakul@yahoo.com
 */

$context = $this->context;

?>

<?php Alert::begin([
    'closeButton' => [
        'data-href' => Url::to(['/agreement/mark-as-read', 'id' => $id]),
        'class' => 'dismiss-agreement close'
    ],
    'options' => [
        'class' => 'alert-info agreement-alert'
    ]
]); ?>

<?php if ($type == $context::TYPE_PRIVACY_POLICY): ?>
    <strong>New Privacy Policy Released!</strong>
    <div>
        If you want to take a closer look then click on the following
        <strong><?= Html::a('link.', ['/privacy-policy']); ?></strong>
        In case you want to ignore this release click on the close button.
    </div>
<?php endif; ?>

<?php if ($type == $context::TYPE_TERMS_OF_USE): ?>
    <strong>New Terms Of Use Released!</strong>
    <div>
        If you want to take a closer look then click on the following
        <strong><?= Html::a('link.', ['/terms-of-use']); ?></strong>
        In case you want to ignore this release click on the close button.
    </div>
<?php endif; ?>

<?php Alert::end(); ?>