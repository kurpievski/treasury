<?php

/**
 * Author: Srdjan Drakul
 * Date: 3/2/2016 5:02 PM
 * Email: srdjan_drakul@yahoo.com
 */

$context = $this->context;
?>

<?php if ($context->getShouldRenderTermsOfUse()): ?>
    <?= $this->render('_alert', [
        'id' => $context->getTermsOfUseId(),
        'app_key' => $context->appKey,
        'type' => $context::TYPE_TERMS_OF_USE,
    ]); ?>
<?php endif; ?>

<?php if ($context->getShouldRenderPrivacyPolicy()): ?>
    <?= $this->render('_alert', [
        'id' => $context->getPrivacyPolicyId(),
        'app_key' => $context->appKey,
        'type' => $context::TYPE_PRIVACY_POLICY,
    ]); ?>
<?php endif; ?>



