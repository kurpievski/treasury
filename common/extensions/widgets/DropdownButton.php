<?php
/**
 * by Aleksandar Panic
 * Company: 2amigOS!
 *
 **/

namespace common\widgets;

use yii\bootstrap\Dropdown;
use yii\bootstrap\Html;

class DropdownButton extends Dropdown
{
    public $label = '';

    public $containerOptions = ['class' => 'dropdown'];
    public $buttonOptions = [
        'data-toggle' => 'dropdown',
        'class' => 'dropdown-toggle btn btn-primary'
    ];

    public $buttonSizeClass = '';

    public function run()
    {
        $buttonOptions = $this->buttonOptions;

        Html::addCssClass($buttonOptions, $this->buttonSizeClass);
        $contents = [
            'button' => Html::a(
                $this->label . ' ' . Html::tag('b', '', ['class' => 'caret']),
                '#',
                $buttonOptions
            ),
            'dropdown' => parent::run()
        ];

        return Html::tag('div', implode("", $contents), $this->containerOptions);
    }
}