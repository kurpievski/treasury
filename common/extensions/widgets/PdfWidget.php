<?php
/**
 * PdfWidget.php
 *
 * Author: Eric Maicon <eric@2amigos.us>
 * Date: 05/01/16
 * Time: 09:51
 */

namespace common\widgets;

use kartik\mpdf\Pdf;
use yii\base\InvalidParamException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

class PdfWidget extends Widget
{

    /** @var \PDFMerger $pdfMerge */
    private static $pdfMerge;

    /**
     * @var array
     */
    public $options = [];

    /**
     * e.g.: Yii::getAlias('@common/runtime') . "/pdf";
     * @var string the pdf path to generate a physic file and then, remove it after the merge
     */
    public $pdfFolder;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        static::$pdfMerge = new \PDFMerger();

        //creating the pdf folder
        if (empty($this->pdfFolder)) {
            throw new InvalidParamException('pdfFolder param needs to be filled.');
        }

        if (!is_dir($this->pdfFolder)) {
            FileHelper::createDirectory($this->pdfFolder);
        }

        $this->options = ArrayHelper::merge($this->defaultOptions(), $this->options);
    }

    /**
     * default options
     *
     * @return array
     */
    private function defaultOptions()
    {
        return ;
    }

    /**
     * add a content to a pdf
     *
     * @param $options
     * @throws \exception
     */
    public function add ($options)
    {
        $options = ArrayHelper::merge($options, $this->options);

        if (!array_key_exists('filename', $options) && empty($options['filename'])) {
            throw new InvalidParamException();
        }

        $options['filename'] = $this->pdfFolder . DIRECTORY_SEPARATOR . $options['filename'];

        $pdf = new Pdf($options);
        $pdf->render();

        static::$pdfMerge->addPDF($options['filename'], 'all');
    }

    /**
     * merge and return the raw pdf file.
     *
     * @param $fileName
     * @return raw
     */
    public function store ($fileName)
    {
        return static::$pdfMerge->merge('file', $fileName);
    }
}