<?php
/**
 * MatchHeightAsset.php
 *
 * Author: Aleksandar Panic <aleksandar.panic@2amigos.us>
 * Timestamp: 30-Sep-16 17:36
 */

namespace common\widgets\matchheight;


use yii\web\AssetBundle;

class MatchHeightAsset extends AssetBundle
{
    public function init()
    {
        $this->sourcePath = __DIR__ . '/assets';
        parent::init();
    }

    public $js = [
        'js/jquery.matchHeight-min.js'
    ];

    public $depends = [
        '\yii\web\JqueryAsset'
    ];
}