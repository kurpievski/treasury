<?php
/**
 * MatchHeight.php
 *
 * Author: Aleksandar Panic <aleksandar.panic@2amigos.us>
 * Timestamp: 30-Sep-16 17:27
 */

namespace common\widgets\matchheight;

use common\helpers\ArrayHelper;
use yii\bootstrap\Html;
use yii\bootstrap\Widget;
use yii\helpers\Json;
use yii\web\View;

class MatchHeight extends Widget
{
    public $selector = '';

    public $byRow = true;
    public $property = 'height';
    public $target = null;
    public $remove = false;

    public $reloadAfterPjax = false;

    public $wrapperTag = 'div';

    public $isGlobal = false;

    public $registerOn = View::POS_READY;

    public $clientOptions = [];

    public static function begin($config = [])
    {
        $self = parent::begin($config);

        ob_start();

        return $self;
    }

    public function init()
    {
        parent::init();

        $this->clientOptions = ArrayHelper::merge([
            'byRow' => $this->byRow,
            'property' => $this->property,
            'target' => $this->target,
            'remove' => $this->remove
        ], $this->clientOptions);
    }

    public function run()
    {
        $view = $this->getView();
        MatchHeightAsset::register($view);

        $view = $this->getView();

        $clientOptions = Json::encode($this->clientOptions);
        $pluginVarName = 'jquery_match_height_config_' . hash('crc32', $clientOptions);

        $view->registerJs("var {$pluginVarName} = $clientOptions;", View::POS_END);

        $selectString = Json::encode("{$this->selector}");
        $widgetIdString = Json::encode("#{$this->options['id']}");

        $view->registerJs($this->isGlobal ?
            "jQuery({$selectString}).matchHeight({$pluginVarName});" :
            "jQuery({$selectString}, {$widgetIdString}).matchHeight({$pluginVarName});"
        );

        if ($this->reloadAfterPjax) {
            $view->registerJs("
                $(document).on('pjax:complete', function() {
                  $.fn.matchHeight._update();
                });
            ");
        }

        return Html::tag($this->wrapperTag, ob_get_clean(), $this->options);
    }
}