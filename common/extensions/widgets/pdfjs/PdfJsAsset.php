<?php

namespace common\widgets\pdfjs;

use frontend\assets\AppAsset;
use yii\web\AssetBundle;
use yii\web\View;

/**
 * PdfJs Asset bundle
 * @author Sathit Seethaphon <dixonsatit@gmail.com>
 */
class PdfJsAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $js = [
        'web/compatibility.js',
        'web/l10n.js',
        'build/pdf.js',
        'web/debugger.js',
        'web/viewer.js',
        'yii2-pdfjs.js'
    ];

    public $jsOptions  = [
        'position'=> View::POS_HEAD
    ];

    public $css = [
        'web/viewer.css'
    ];

    public $depends = [
       // AppAsset::class - Dont think this is needed. We will need to check.
    ];
}
 ?>
