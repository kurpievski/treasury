<?php
/**
 * DateRangePicker.php
 *
 * Author: Aleksandar Panic <aleksandar.pnc@gmail.com>
 * Date: 23-Feb-16
 * Time: 15:30
 */

namespace common\widgets\daterangepicker;

use yii\bootstrap\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;

class DateRangePicker extends InputWidget
{
    public $clientOptions = [
        "linkedCalendars" => true,
        "autoUpdateInput" => false,
        'locale' => [
            'format' => 'MM/DD/YYYY'
        ],
        "opens" => "left"
    ];

    public $options = [
        'class' => 'form-control'
    ];

    public $inputViewFormat = "{START} - {END}";

    public function run()
    {
        $this->registerAssets();

        if ($this->hasModel()) {
            echo Html::activeInput('text', $this->model, $this->attribute, $this->options);
            return;
        }

        echo Html::input('text', $this->name, $this->value, $this->options);
    }

    public function registerAssets()
    {
        $id = $this->options['id'];

        $view = $this->getView();
        DateRangePickerAsset::register($view);

        $jsonOptions = Json::encode($this->clientOptions);
        $inputViewFormatJson = Json::encode($this->inputViewFormat);

        $view->registerJs("
            (function() {
                var jsonOptions = {$jsonOptions};
                var initialValue = $('#{$id}').val();
                var datepicker = $('#{$id}').daterangepicker(jsonOptions);
                var dateFormat = {$inputViewFormatJson};
                
                datepicker.on('apply.daterangepicker', function(e, dtPicker) {
                    var formattedStart = dtPicker.startDate.format(dtPicker.locale.format);
                    var formattedEnd = dtPicker.endDate.format(dtPicker.locale.format);
                    
                    dtPicker.element.val(
                        dateFormat
                            .replace('{START}', formattedStart)
                            .replace('{END}', formattedEnd)
                    );
                });
                
            })();
        ");
    }
}