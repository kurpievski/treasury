<?php
/**
 * DateRangePickerAsset.php
 *
 * Author: Aleksandar Panic <aleksandar.panic@2amigos.us>
 * Timestamp: 03-Jun-16 19:31
 */

namespace common\widgets\daterangepicker;


use yii\web\AssetBundle;

class DateRangePickerAsset extends AssetBundle
{
    public function init()
    {
        parent::init();

        $this->sourcePath = __DIR__ . '/assets';
    }

    public $js = [
        'daterangepicker.js'
    ];

    public $css = [
        'daterangepicker.css'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'common\widgets\daterangepicker\MomentAsset'
    ];
}