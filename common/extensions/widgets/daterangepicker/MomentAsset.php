<?php
/**
 * MomentAsset.php
 *
 * Author: Aleksandar Panic <aleksandar.panic@2amigos.us>
 * Timestamp: 03-Jun-16 19:29
 */

namespace common\widgets\daterangepicker;


use yii\web\AssetBundle;

class MomentAsset extends AssetBundle
{
    public function init()
    {
        parent::init();

        $this->sourcePath = __DIR__ . '/assets';
    }

    public $js = [
        'moment.min.js'
    ];

    public $depends = [];
}